angular.module('facebookHandleLookup')
  .config(['cooperativeArtifactProvider', function (cooperativeArtifactProvider) {
    cooperativeArtifactProvider.setArtifactURL('https://dev1.tsgctp.org:33536/services/artifacts/');
  }])

  .config(['cooperativeAuthProvider', function (cooperativeAuthProvider) {
    cooperativeAuthProvider.setAuthURL('https://dev1.tsgctp.org:33792/services/auth');
    cooperativeAuthProvider.setAccountMgmtURL('https://dev1.tsgctp.org:33280/services/amgmt/');
    cooperativeAuthProvider.setAccountMgmtResetURL('https://dev1.tsgctp.org:39936/services/amgmtreset/');
    cooperativeAuthProvider.setAuthSessionKey('ctpauthtoken_dev');
  }])

  .config(['cooperativeConstraintsProvider', function (cooperativeConstraintsProvider) {
    cooperativeConstraintsProvider.setConstraintsURL('https://dev1.tsgctp.org:34816/services/constraint/');
  }])

  .config(['cooperativeContentProvider', function (cooperativeContentProvider) {
    cooperativeContentProvider.setContentURL('https://dev1.tsgctp.org:35072/services/content/');
  }])

  .config(['cooperativeGeoipProvider', function(cooperativeGeoipProvider){
    cooperativeGeoipProvider.setGeoipURL('https://dev1.tsgctp.org:36352/services/geoip/');
  }])

  .config(['cooperativeProvider', function (cooperativeProvider) {
    cooperativeProvider.setBaseDomains(['tsgctp', 'localhost']);
    cooperativeProvider.setAppName('facebookHandleLookup');
    cooperativeProvider.setApiKey('fbe05baa-aa94-4620-844b-583824a5aa13');
    cooperativeProvider.setPartition('dev');
  }])

  .config(['cooperativeLookupProvider', function (cooperativeLookupProvider) {
    cooperativeLookupProvider.setLookupURL('https://dev1.tsgctp.org:51462/services/lookup/');
  }])

  .config(['cooperativeObjectsProvider', function (cooperativeObjectsProvider) {
    cooperativeObjectsProvider.setObjectsURL('https://dev1.tsgctp.org:36864/services/object/');
  }])

  .config(['cooperativeQueriesProvider', function (cooperativeQueriesProvider) {
    cooperativeQueriesProvider.setQueriesURL('https://dev1.tsgctp.org:37376/services/query/');
    cooperativeQueriesProvider.setDuplCheckURL('https://dev1.tsgctp.org:36096/services/duplcheck/');
  }])

  .config(['cooperativeQueuesProvider', function (cooperativeQueuesProvider) {
    cooperativeQueuesProvider.setQueuesURL('https://dev1.tsgctp.org:37632/services/queue/');
  }])

  .config(['cooperativeRegistrationProvider', function(cooperativeRegistrationProvider){
    cooperativeRegistrationProvider.setUserRegQueueName('ADD_ACCOUNT_DEV');
  }])

  .config(['cooperativeResourcesProvider', function (cooperativeResourcesProvider) {
    cooperativeResourcesProvider.setResourcesURL('https://dev1.tsgctp.org:39168/services/resource/');
  }])

  .config(['cooperativeSsoProvider', function (cooperativeSsoProvider) {
    var platformLogin = {
      url : {
        path: 'https://accounts.tsgctp.org:38401/#/login'
      }
    };

    var platformLogout = {
      url : {
        path: 'https://accounts.tsgctp.org:38401/#/logout'
      }
    };

    var platformSignup = {
      url: {
        path: 'https://accounts.tsgctp.org:38401/#/signup'
      }
    };

    var platformPermissionUpdate = {
      url: {
        path: 'https://accounts.tsgctp.org:38401/#/updatePermission'
      }
    };

    var platformAccount = {
      url: {
        path: 'https://accounts.tsgctp.org:38401/#/account'
      }
    };

    cooperativeSsoProvider.setPlatformPermissionUpdate(platformPermissionUpdate);
    cooperativeSsoProvider.setPlatformAccount(platformAccount);
    cooperativeSsoProvider.setPlatformLogin(platformLogin);
    cooperativeSsoProvider.setPlatformLogout(platformLogout);
    cooperativeSsoProvider.setPlatformSignup(platformSignup);
    
  }])

  .config(['cooperativeSysrwProvider', function(cooperativeSysrwProvider){
    cooperativeSysrwProvider.setWriteQueue('SYSWAL-DBWRITE-410637600000-DEV1');
  }])

  .config(['$translateProvider', 'cooperativeProvider', 'cooperativeContentProvider', function($translateProvider, cooperativeProvider, cooperativeContentProvider){
    $translateProvider.useUrlLoader(cooperativeContentProvider.$get().getTranslatedContent(cooperativeProvider.$get().apiKey, 'facebookHandleLookup'));

    // Tell the module what language to use by default
    $translateProvider.preferredLanguage('en');
    // Tell the module to store the language in the local storage
    $translateProvider.useLocalStorage();
    $translateProvider.useSanitizeValueStrategy('sanitize');
  }])
;