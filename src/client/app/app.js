/**
 * Created by Hiten on 19/04/2016.
 */
angular.module('facebookHandleLookup', [
	'ngAnimate',
	'ui.router',
	'ui.bootstrap',
	'oc.lazyLoad',
	'pascalprecht.translate',
	'ngCookies',
	'ngSanitize',
	'cooperative',
	'ctp.osHeader',
	'ctp.components.osHeader',
	'facebookHandleLookup.source',
	'facebookHandleLookup.templates'
]);
