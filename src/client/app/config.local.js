angular.module('facebookHandleLookup')
  .config(['cooperativeArtifactProvider', function (cooperativeArtifactProvider) {
    cooperativeArtifactProvider.setArtifactURL('https://localhost:33536/services/artifacts/');
  }])

  .config(['cooperativeAuthProvider', function (cooperativeAuthProvider) {
    cooperativeAuthProvider.setAuthURL('https://localhost:33792/services/auth');
    cooperativeAuthProvider.setAccountMgmtURL('https://localhost:33280/services/amgmt/');
    cooperativeAuthProvider.setAccountMgmtResetURL('https://localhost:39936/services/amgmtreset/');
    cooperativeAuthProvider.setAuthSessionKey('ctpauthtoken_local');
  }])

  .config(['cooperativeConstraintsProvider', function (cooperativeConstraintsProvider) {
    cooperativeConstraintsProvider.setConstraintsURL('https://localhost:34816/services/constraint/');
  }])

  .config(['cooperativeContentProvider', function (cooperativeContentProvider) {
    cooperativeContentProvider.setContentURL('https://localhost:35072/services/content/');
  }])

  .config(['cooperativeGeoipProvider', function(cooperativeGeoipProvider){
    cooperativeGeoipProvider.setGeoipURL('https://localhost:36352/services/geoip/');
  }])

  .config(['cooperativeProvider', function (cooperativeProvider) {
    cooperativeProvider.setBaseDomains(['localhost']);
    cooperativeProvider.setAppName('facebookHandleLookup');
    cooperativeProvider.setApiKey('fbe05baa-aa94-4620-844b-583824a5aa13');
    cooperativeProvider.setPartition('local');
  }])

  .config(['cooperativeLookupProvider', function (cooperativeLookupProvider) {
    cooperativeLookupProvider.setLookupURL('https://fbsvc.tsgctp.org/services/lookup/');
  }])

  .config(['cooperativeObjectsProvider', function (cooperativeObjectsProvider) {
    cooperativeObjectsProvider.setObjectsURL('https://localhost:36864/services/object/');
  }])

  .config(['cooperativeQueriesProvider', function (cooperativeQueriesProvider) {
    cooperativeQueriesProvider.setQueriesURL('https://localhost:37376/services/query/');
    cooperativeQueriesProvider.setDuplCheckURL('https://localhost:36096/services/duplcheck/');
  }])

  .config(['cooperativeQueuesProvider', function (cooperativeQueuesProvider) {
    cooperativeQueuesProvider.setQueuesURL('https://localhost:37632/services/queue/');
  }])

  .config(['cooperativeRegistrationProvider', function(cooperativeRegistrationProvider){
    cooperativeRegistrationProvider.setUserRegQueueName('ADD_ACCOUNT_LOCAL');
  }])

  .config(['cooperativeResourcesProvider', function (cooperativeResourcesProvider) {
    cooperativeResourcesProvider.setResourcesURL('https://localhost:39168/services/resource/');
  }])

  .config(['cooperativeSsoProvider', function (cooperativeSsoProvider) {
    var platformLogin = {
      url: {
        path: 'http://0.0.0.0:8001/#/login'
      }
    };

    var platformLogout = {
      url: {
        path: 'http://0.0.0.0:8001/#/logout'
      }
    };

    var platformSignup = {
      url: {
        path: 'http://0.0.0.0:8001/#/signup'
      }
    };

    var platformPermissionUpdate = {
      url: {
        path: 'http://0.0.0.0:8001/#/updatePermission'
      }
    };

    var platformAccount = {
      url: {
        path: 'http://0.0.0.0:8001/#/account'
      }
    };

    cooperativeSsoProvider.setPlatformPermissionUpdate(platformPermissionUpdate);
    cooperativeSsoProvider.setPlatformAccount(platformAccount);
    cooperativeSsoProvider.setPlatformLogin(platformLogin);
    cooperativeSsoProvider.setPlatformLogout(platformLogout);
    cooperativeSsoProvider.setPlatformSignup(platformSignup);
  }])

  .config(['cooperativeSysrwProvider', function(cooperativeSysrwProvider){
    cooperativeSysrwProvider.setWriteQueue('SYSWAL-DBWRITE-410637600000-DEV-CORE');
  }])

  .config(['$translateProvider', 'cooperativeProvider', 'cooperativeContentProvider', function($translateProvider, cooperativeProvider, cooperativeContentProvider){
    $translateProvider.useUrlLoader(cooperativeContentProvider.$get().getTranslatedContent(cooperativeProvider.$get().apiKey, 'facebookHandleLookup'));

    // Tell the module what language to use by default
    $translateProvider.preferredLanguage('en');
    // Tell the module to store the language in the local storage
    $translateProvider.useLocalStorage();
    $translateProvider.useSanitizeValueStrategy('sanitize');
  }])
;
