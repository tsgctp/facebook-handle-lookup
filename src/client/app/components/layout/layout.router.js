angular.module('facebookHandleLookup.components.layout')
.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise(function ($injector) {
    var $state = $injector.get('$state');
    $state.go('ctpbase.facebookHandleLookup.home_home');
  });
  $stateProvider
    .state('ctpbase.facebookHandleLookup', {
      abstract: true,
      url: '',
      templateUrl: 'components/layout/layout.html',
      controller: 'facebookHandleLookup.components.layout.layoutCtrl'
    });
}]);
