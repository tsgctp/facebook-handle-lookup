angular.module('facebookHandleLookup.components.ctpLookupRequester')
  .controller('facebookHandleLookup.components.ctpLookupRequester.ctpLookupRequesterCtrl', ['$scope','facebookHandleLookup.components.ctpLookupRequester.ctpLookupRequesterFactory', function($scope,ctpLookupRequesterFactory) {

    var vm = this;

    var queryParams;

    $scope.$watch(function(){return vm.queryParams;},function(newVal, oldVal){
      if(!newVal || newVal == oldVal){
        return;
      }
      if(!vm.appSession){
        vm.appSession = false;
      }
      $scope[vm.busy] = true;
      ctpLookupRequesterFactory.lookupRequest(newVal, vm.appSession).then(
        function(resp){
          if(resp.returnStatus.data.length>0){
            $scope[vm.scopeprefix]=resp.returnStatus.data;
            $scope[vm.busy] = false;
          }
        },function(err){
          $scope[vm.busy] = false;
          console.log('err',err);
        }
      );
    });

  }]);
