angular.module('facebookHandleLookup.components.ctpLookupRequester')
  .factory('facebookHandleLookup.components.ctpLookupRequester.ctpLookupRequesterFactory', [ 'cooperativeLookup', 'noShardRequesterFactory', function(cooperativeLookup,noShardRequesterFactory) {
    var factoryMembers = {};

    /*

    */

    factoryMembers.lookupRequest = function(queryParams, appSession){
        return noShardRequesterFactory.GET(cooperativeLookup.lookupURL,[],queryParams,undefined,appSession).then(
          function(res){
            return res;
          }, function(err){
            return err;
          }
        );
    }

    return factoryMembers;
  }]);
