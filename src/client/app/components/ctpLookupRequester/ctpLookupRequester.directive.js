angular.module('facebookHandleLookup.components.ctpLookupRequester')
  .directive('ctpLookupRequester', function() {

    var directive = {};
  //  directive.templateUrl = 'components/ctpLookupRequester/ctpLookupRequester.html';
    directive.restrict = 'E';
    directive.controller = 'facebookHandleLookup.components.ctpLookupRequester.ctpLookupRequesterCtrl';
    directive.controllerAs = 'ctpLookupRequesterCtrl';
    directive.bindToController = {
      queryParams:'=',
      appSession:'@',
      busy:'@',
      scopeprefix:'@'
    };

    directive.link = function(scope, element, attrs) {

    };

    return directive;
  });
