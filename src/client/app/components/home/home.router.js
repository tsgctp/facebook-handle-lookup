angular.module('facebookHandleLookup.components.home')
  .config(['$stateProvider', function($stateProvider) {
    $stateProvider.state('ctpbase.facebookHandleLookup.home_home', {
      url: '/home',
      templateUrl: 'components/home/home.html',
      controller: 'facebookHandleLookup.components.home.homeCtrl',
      controllerAs: 'homeCtrl'
    });
  }]);
