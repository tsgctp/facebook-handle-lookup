angular.module('facebookHandleLookup.components.home')
  .controller('facebookHandleLookup.components.home.homeCtrl', ['$scope', function($scope) {

    var vm = this;

    vm.today = new Date();

    vm.search = function(){
      if(vm.searchTxt)
      {
        vm.searchItem = vm.searchTxt;
      }
    };

  }]);
