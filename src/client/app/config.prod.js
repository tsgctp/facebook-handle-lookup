angular.module('facebookHandleLookup')
  .config(['cooperativeArtifactProvider', function (cooperativeArtifactProvider) {
    cooperativeArtifactProvider.setArtifactURL('https://artifacts.tsgctp.org/services/artifacts/');
  }])

  .config(['cooperativeAuthProvider', function (cooperativeAuthProvider) {
    cooperativeAuthProvider.setAuthURL('https://auth.tsgctp.org/services/auth');
    cooperativeAuthProvider.setAccountMgmtURL('https://acct-mgmt.tsgctp.org/services/amgmt/');
    cooperativeAuthProvider.setAccountMgmtResetURL('https://acct-reset.tsgctp.org/services/amgmtreset/');
    cooperativeAuthProvider.setAuthSessionKey('ctpauthtoken_prod');
  }])

  .config(['cooperativeConstraintsProvider', function (cooperativeConstraintsProvider) {
    cooperativeConstraintsProvider.setConstraintsURL('https://constraints.tsgctp.org:34816/services/constraint/');
  }])

  .config(['cooperativeContentProvider', function (cooperativeContentProvider) {
    cooperativeContentProvider.setContentURL('https://content.tsgctp.org/services/content/');
  }])

  .config(['cooperativeGeoipProvider', function(cooperativeGeoipProvider){
    cooperativeGeoipProvider.setGeoipURL('https://geoip-query.tsgctp.org/services/geoip/');
  }])

  .config(['cooperativeProvider', function (cooperativeProvider) {
    cooperativeProvider.setBaseDomains(['tsgctp']);
    cooperativeProvider.setAppName('facebookHandleLookup');
    cooperativeProvider.setApiKey('fbe05baa-aa94-4620-844b-583824a5aa13');
    cooperativeProvider.setPartition('prod');
  }])

  .config(['cooperativeLookupProvider', function (cooperativeLookupProvider) {
    cooperativeLookupProvider.setLookupURL('https://lookup-query.tsgctp.org/services/lookup/');
  }])

  .config(['cooperativeObjectsProvider', function (cooperativeObjectsProvider) {
    cooperativeObjectsProvider.setObjectsURL('https://objects-sysrw.tsgctp.org:36864/services/object/');
  }])

  .config(['cooperativeQueriesProvider', function (cooperativeQueriesProvider) {
    cooperativeQueriesProvider.setQueriesURL('https://query.tsgctp.org/services/query/');
    cooperativeQueriesProvider.setDuplCheckURL('https://dupls-query.tsgctp.org/services/duplcheck/');
  }])

  .config(['cooperativeQueuesProvider', function (cooperativeQueuesProvider) {
    cooperativeQueuesProvider.setQueuesURL('https://queue.tsgctp.org/services/queue/');
  }])

  .config(['cooperativeRegistrationProvider', function(cooperativeRegistrationProvider){
    cooperativeRegistrationProvider.setUserRegQueueName('ADD_ACCOUNT');
  }])

  .config(['cooperativeResourcesProvider', function (cooperativeResourcesProvider) {
    cooperativeResourcesProvider.setResourcesURL('https://resource.tsgctp.org/services/resource/');
  }])

  .config(['cooperativeSsoProvider', function (cooperativeSsoProvider) {
    var platformLogin = {
      url: {
        path: 'https://accounts.tsgctp.org/#/login'
      }
    };

    var platformLogout = {
      url: {
        path: 'https://accounts.tsgctp.org/#/logout'
      }
    };

    var platformSignup = {
      url: {
        path: 'https://accounts.tsgctp.org/#/signup'
      }
    };

    var platformPermissionUpdate = {
      url: {
        path: 'https://accounts.tsgctp.org/#/updatePermission'
      }
    };

    var platformAccount = {
      url: {
        path: 'https://accounts.tsgctp.org/#/account'
      }
    };

    cooperativeSsoProvider.setPlatformPermissionUpdate(platformPermissionUpdate);
    cooperativeSsoProvider.setPlatformAccount(platformAccount);
    cooperativeSsoProvider.setPlatformLogin(platformLogin);
    cooperativeSsoProvider.setPlatformLogout(platformLogout);
    cooperativeSsoProvider.setPlatformSignup(platformSignup);
  }])

  .config(['cooperativeSysrwProvider', function(cooperativeSysrwProvider){
    cooperativeSysrwProvider.setWriteQueue('SYSWAL-DBWRITE-410637600000');
  }])

  .config(['$translateProvider', 'cooperativeProvider', 'cooperativeContentProvider', function($translateProvider, cooperativeProvider, cooperativeContentProvider){
    $translateProvider.useUrlLoader(cooperativeContentProvider.$get().getTranslatedContent(cooperativeProvider.$get().apiKey, 'facebookHandleLookup'));

    // Tell the module what language to use by default
    $translateProvider.preferredLanguage('en');
    // Tell the module to store the language in the local storage
    $translateProvider.useLocalStorage();
    $translateProvider.useSanitizeValueStrategy('sanitize');
  }])
;