angular.module("ctp.source", []);
angular.module('ctp.components.osHeader', ['ctp.components.osHeader.osAppDrawer', 'ctp.components.osHeader.osUserDrawer']);

angular.module('ctp.components.osHeader.osAppDrawer', []);

angular.module('ctp.components.osHeader.osUserDrawer', []);

angular.module('ctp.components.osHeader.osAppDrawer')
  .controller('ctp.components.osHeader.osAppDrawer.osAppDrawerCtrl', ['osHeader', 'osAppDrawer', 'authorizationManagement', 'appUserSession', 'dataFactory', function(osHeader, osAppDrawer, authorizationManagement, appUserSession,dataFactory) {
    var vm = this;

    vm.osHeaderProvider = osHeader;
    vm.osAppDrawerProvider = osAppDrawer;

    vm.permissions = {
      appsLoaded: false,
      loaded: false,
      sets: {}
    };

    vm.getAppPermissions = function (results) {
      if (vm.permissions.appsLoaded !== true) {
        var permissionData = {};
        for (var i = 0; i < results.length; i++) {
          var appPermissionResource = dataFactory.returnObjects(undefined, 'IdentificationObject_001', '0', 'PermissionResource', undefined, results[i].data.io, undefined);
          var appPermissionResourceDev = dataFactory.returnObjects(undefined, 'IdentificationObject_001', '0', 'PermissionDevResource', undefined, results[i].data.io, undefined);
          var appPermissionResourceQa = dataFactory.returnObjects(undefined, 'IdentificationObject_001', '0', 'PermissionQaResource', undefined, results[i].data.io, undefined);

          if (appPermissionResource[0] === undefined || appPermissionResourceDev[0] === undefined || appPermissionResourceQa[0] === undefined)
            {console.log( appPermissionResource, appPermissionResourceDev, appPermissionResourceQa);}

          var permissionResource = {
            application: [
              {
                layer: 5,
                action: 'read',
                context: 'get',
                targets: ['*'],
                resource: appPermissionResource[0].typeValue,
                roleAlias: 'read_write_role'
              }
            ]
          };

          permissionData[results[i].id + '_prod'] = permissionResource;

          var permissionResourceDev = {
            application: [
              {
                layer: 5,
                action: 'read',
                context: 'get',
                targets: ['*'],
                resource: appPermissionResourceDev[0].typeValue,
                roleAlias: 'read_write_role'
              }
            ]
          };

          permissionData[results[i].id + '_dev'] = permissionResourceDev;

          var permissionResourceQA = {
            application: [
              {
                layer: 5,
                action: 'read',
                context: 'get',
                targets: ['*'],
                resource: appPermissionResourceQa[0].typeValue,
                roleAlias: 'read_write_role'
              }
            ]
          };

          permissionData[results[i].id + '_qa'] = permissionResourceQA;

        }

        authorizationManagement.checkPermissionSets(appUserSession.getUser().session, permissionData, 'v001', true)
          .then(function(data){
            vm.permissions.loaded = true;
            vm.permissions.sets = data;
          });

        vm.permissions.appsLoaded = true;
      }



      return true;
    };

    
    this.stuffs = {
 
    };

    this.increament = function(groupid){
      if(!this.stuffs.hasOwnProperty(groupid)){
        this.stuffs[groupid] = {counter : 0};
      }
      this.stuffs[groupid].counter++;
    }

  }]);

angular.module('ctp.components.osHeader.osAppDrawer')
  .directive('osAppDrawer', function() {

    var directive = {};
    directive.templateUrl = 'components/osHeader/osAppDrawer/osAppDrawer.html';
    directive.restrict = 'E';
    directive.controller = 'ctp.components.osHeader.osAppDrawer.osAppDrawerCtrl';
    directive.controllerAs = 'osAppDrawerCtrl';
    directive.transclude = false;
    directive.scope = {
        currentUser: '='
    };
    directive.bindToController = {

    };

    directive.link = function(scope, element, attrs) {

    };

    return directive;
  }).filter('appGroupings', ['_', function (_){
    return _.memoize(function(items){
        var filtered = {};

        angular.forEach(items, function(item) {
            var key = '';

            for (var i=0; i < item.data.io.length; i++){
                var entity_object = item.data.io[i];
                if (entity_object.signature === 'RelationshipObject_001' &&
                    entity_object.type === 'appGroup'){
                    key = entity_object.typeValue;
                }
            }

            if (key !== ''){
                if (!filtered.hasOwnProperty(key)){
                    filtered[key] = [];
                }

                filtered[key].push({data: item.data});
            }

        });

        return filtered;
    });}]);

angular.module('ctp.components.osHeader.osAppDrawer')
  .provider('osAppDrawer', [function() {
    var osAppDrawer = {};

    var platformAppsUrl = 'http://0.0.0.0:8001/#/apps';

    osAppDrawer.setplatformAppsUrl = function(_platformAppsUrl){
      platformAppsUrl = _platformAppsUrl;
    }

    osAppDrawer.$get = function() {
      var getter = {};
      getter.platformAppsUrl = platformAppsUrl;
      return getter;
    };

    return osAppDrawer;
  }]);

angular.module('ctp.components.osHeader')
  .controller('ctp.components.osHeader.osHeaderCtrl', ['osHeader', function(osHeader) {
    this.osHeaderProvider = osHeader;
  }]);

angular.module('ctp.components.osHeader')
  .directive('osHeader', function() {

    var directive = {};
    directive.templateUrl = 'components/osHeader/osHeader.html';
    directive.restrict = 'E';
    directive.controller = 'ctp.components.osHeader.osHeaderCtrl';
    directive.controllerAs = 'osHeaderCtrl';
    directive.transclude = false;
    directive.scope = {
        applicationNav : '@',
        currentUser: '='
    };
    directive.bindToController = {

    };

    directive.link = function(scope, element, attrs) {

    };

    return directive;
  });

angular.module('ctp.components.osHeader')
  .provider('osHeader', [function() {
    var osHeaderProvider = {};

    var environment = 'Prod';
    var showAppDrawer = true;
    var showUserDrawer = true;

    osHeaderProvider.setEnvironment = function(_environment){
      environment = _environment;
    };

    osHeaderProvider.setShowAppDrawer = function(_showAppDrawer){
      showAppDrawer = _showAppDrawer;
    };

    osHeaderProvider.setShowUserDrawer = function(_showUserDrawer){
      showUserDrawer = _showUserDrawer;
    };

    osHeaderProvider.$get = function() {
      var getter = {};
      
      getter.environment = environment;
      getter.showAppDrawer = showAppDrawer;
      getter.showUserDrawer = showUserDrawer;

      return getter;
    };

    return osHeaderProvider;
  }]);
angular.module('ctp.components.osHeader.osUserDrawer')
  .controller('ctp.components.osHeader.osUserDrawer.osUserDrawerCtrl', [function() {

  }]);

angular.module('ctp.components.osHeader.osUserDrawer')
  .directive('osUserDrawer', function() {

    var directive = {};
    directive.templateUrl = 'components/osHeader/osUserDrawer/osUserDrawer.html';
    directive.restrict = 'E';
    directive.controller = 'ctp.components.osHeader.osUserDrawer.osUserDrawerCtrl';
    directive.controllerAs = 'osUserDrawerCtrl';
    directive.transclude = false;
    directive.scope = {
        currentUser : '='
    };
    directive.bindToController = {

    };

    directive.link = function(scope, element, attrs) {

    };

    return directive;
  });
