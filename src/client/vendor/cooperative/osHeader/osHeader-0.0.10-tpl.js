angular.module('ctp.osHeader', ['components/osHeader/osAppDrawer/osAppDrawer.html', 'components/osHeader/osHeader.html', 'components/osHeader/osUserDrawer/osUserDrawer.html']);

angular.module("components/osHeader/osAppDrawer/osAppDrawer.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("components/osHeader/osAppDrawer/osAppDrawer.html",
    "<style media=all>.app-drawer-section {\n" +
    "        padding: 10px;\n" +
    "        overflow: auto;\n" +
    "    }\n" +
    "    \n" +
    "    .app-drawer-heading {\n" +
    "        border-bottom: dotted 1px #58666e;\n" +
    "        color: #58666e;\n" +
    "        font-weight: bold;\n" +
    "        margin-bottom: 5px;\n" +
    "    }\n" +
    "    \n" +
    "    .app-drawer-item {\n" +
    "        padding-left: 2px;\n" +
    "        padding-right: 2px;\n" +
    "    }\n" +
    "    \n" +
    "    .app-drawer-item-body {\n" +
    "        height: 100px;\n" +
    "        margin-bottom: 2px;\n" +
    "    }\n" +
    "    \n" +
    "    .app-drawer-item-body img {\n" +
    "        max-width: 100%;\n" +
    "    }\n" +
    "    \n" +
    "    .app-drawer-item-media {\n" +
    "        overflow: visible;\n" +
    "        text-align: center;\n" +
    "        height: 57px;\n" +
    "    }\n" +
    "    \n" +
    "    .app-drawer-app-name {\n" +
    "        display: block;\n" +
    "        overflow: auto;\n" +
    "        text-align: center;\n" +
    "        font-size: 12px;\n" +
    "        color: #ff8300 !important;\n" +
    "    }\n" +
    "    \n" +
    "    .lnk-app-store {\n" +
    "        text-align: center;\n" +
    "    }\n" +
    "    \n" +
    "    .lnk-app-store a {\n" +
    "        color: #ff8300 !important;\n" +
    "    }\n" +
    "    \n" +
    "    .app-store-not-logged-in {\n" +
    "        text-align: center;\n" +
    "    }\n" +
    "    /*.app-drawer-item-wrapper{\n" +
    "        overflow: auto;\n" +
    "    }*/</style><button class=dropdown-toggle data-toggle=dropdown aria-haspopup=true aria-expanded=false aria-label=\"TechSoup Apps\"><span class=\"glyphicon glyphicon-th\" aria-hidden=true></span></button><div class=dropdown-menu><div ng-if=currentUser.isAuthenticated><ctp-search-paging-requester scopeprefix=results query-string=\"data @> '{&quot;io&quot;:[{&quot;signature&quot;: &quot;EntityObject_001&quot;, &quot;type&quot;: &quot;System&quot;, &quot;typeValue&quot;: &quot;APP&quot;}]}'\" signature-list='[\"EntityObject_001\", \"NameObject_001\", \"DescriptiveTextObject_001\",\"StatusObject_001\", \"WebsiteObject_001\", \"ArtifactObject_001\", \"RelationshipObject_001\", \"IdentificationObject_001\"]' size=300 return-mode=true union=false intersection=false use-app-api-key=true shard=user><section class=app-drawer-section ng-repeat=\"(k,v) in results.items | appGroupings | orderBy: k\" class=text-center ng-if=\"results.items.length >0\"><div ng-if=osAppDrawerCtrl.getAppPermissions(results.items)><div class=app-drawer-heading ng-if=\"osAppDrawerCtrl.stuffs[k].counter > 0\"><ctp-data-requester poid={{k}} signaturelist='[\"NameObject_001\"]' scopeprefix=ioDataGroup use-app-api-key=true shard=content><ctp-data-finder collection=ioDataGroup signature=NameObject_001 type=groupName index=0 scopeprefix=groupName><h5>{{groupName[0].typeValue}}</h5></ctp-data-finder></ctp-data-requester><div class=clear></div></div><div class=app-drawer-item-wrapper><div class=app-drawer-item-wrapper ng-repeat=\"a in v\"><ctp-data-finder collection=a.data.io signature=IdentificationObject_001 type=PermissionResource index=0 scopeprefix=appPermission><ctp-data-finder collection=a.data.io signature=WebsiteObject_001 type=app{{osAppDrawerCtrl.osHeaderProvider.environment}}Url index=0 scopeprefix=appUrl><div ng-if=\"appPermission.length > 0 && appUrl.length>0\"><div ng-if=\"osAppDrawerCtrl.permissions.loaded && (osAppDrawerCtrl.permissions.sets[a.data.id + '_prod'] === true || osAppDrawerCtrl.permissions.sets[a.data.id + '_dev'] === true || osAppDrawerCtrl.permissions.sets[a.data.id + '_qa'] === true)\"><div class=\"col-md-4 col-sm-4 col-xs-2 app-drawer-item\"><div class=app-drawer-item-body ng-init=osAppDrawerCtrl.increament(k)><div class=app-drawer-item-media><a ng-href={{appUrl[0].typeValue}} target=_blank><span ng-if=\"a.data.io.length > 0 && a.data.id !== undefined\"><ctp-image-upload-viewer collection=a.data.io poid=a.data.id type=productImg use-app-api-key=true></ctp-image-upload-viewer></span></a></div><div><ctp-data-finder collection=a.data.io signature=NameObject_001 type=appName index=0 scopeprefix=appName><a ng-href={{appUrl[0].typeValue}} target=_blank class=app-drawer-app-name>{{appName[0].typeValue}}</a></ctp-data-finder></div></div></div></div></div></ctp-data-finder></ctp-data-finder></div></div><div class=clear></div></div><div class=clear></div></section></ctp-search-paging-requester><h4 class=lnk-app-store><a class=lnk-app-store ng-href={{osAppDrawerCtrl.osAppDrawerProvider.platformAppsUrl}} target=_blank>more apps</a></h4></div><div ng-if=!currentUser.isAuthenticated><p class=app-store-not-logged-in>Your are currently not logged in</p><h4 class=lnk-app-store><a class=lnk-app-store ng-href={{osAppDrawerCtrl.osAppDrawerProvider.platformAppsUrl}} target=_blank>more apps</a></h4></div></div>");
}]);

angular.module("components/osHeader/osHeader.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("components/osHeader/osHeader.html",
    "<nav class=navbar><div class=navbar-header><button ng-if=\"applicationNav && applicationNav != ''\" class=\"pull-left visible-xs app-nav-menu\" ui-toggle-class=show-application-nav data-target={{applicationNav}} ui-scroll-to=app>Toggle App Nav</button> <a class=navbar-brand href=#><img src=https://accounts.tsgctp.org/img/ts_icon.jpg alt=TechSoup> <span ng-if=\"osHeaderCtrl.osHeaderProvider.environment != 'Prod'\">{{osHeaderCtrl.osHeaderProvider.environment}} </span></a><button class=\"navbar-toggle collapsed\" data-toggle=collapse data-target=#os-header-collapse aria-expanded=false>Toggle navigation</button></div><div class=\"collapse navbar-collapse\" id=os-header-collapse><header ng-if=!currentUser.getIsAuthenticated() class=visible-xs><img src=https://accounts.tsgctp.org/img/ts_icon.jpg alt=TechSoup><p>One account for everything TechSoup</p></header><ul class=\"nav navbar-nav navbar-right\"><li class=dropdown ng-if=\"osHeaderCtrl.osHeaderProvider.showUserDrawer === true\"><os-user-drawer current-user=currentUser id=osUserDrawer></os-user-drawer></li><li class=dropdown ng-if=\"osHeaderCtrl.osHeaderProvider.showAppDrawer === true\"><os-app-drawer current-user=currentUser id=osAppDrawer></os-app-drawer></li></ul></div></nav>");
}]);

angular.module("components/osHeader/osUserDrawer/osUserDrawer.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("components/osHeader/osUserDrawer/osUserDrawer.html",
    "<button class=dropdown-toggle data-toggle=dropdown aria-haspopup=true aria-expanded=false aria-label=\"TechSoup Account\"><span class=\"glyphicon glyphicon-user\" aria-hidden=true></span></button><ul class=dropdown-menu><li ng-if=!currentUser.getIsAuthenticated()><a ng-click=currentUser.gotoPlatformLogin(); class=nav-btn-xs>Sign in <small>View and edit your profile and organizations.</small></a></li><li ng-if=!currentUser.getIsAuthenticated()><a ng-click=currentUser.gotoPlatformSignup(); class=nav-btn-xs>Create an Account <small>An account provides you access to all things TechSoup. This is still in Beta and more tools and services are soon to come.</small></a></li><li ng-if=currentUser.getIsAuthenticated() class=inline-xs><a ng-click=currentUser.gotoPlatformAccount(true)><span class=hidden-xs>View my account</span> <span class=visible-xs><span class=\"glyphicon glyphicon-user\" aria-hidden=true></span> {{ currentUser.username }}</span> <small>Manage your profile and your organizations.</small></a></li><li ng-if=currentUser.getIsAuthenticated() class=inline-xs><a ng-click=currentUser.gotoPlatformLogout() class=nav-btn-xs>Sign out <small>This will log you out of all participating apps.</small></a></li><li class=hidden-xs><p>We're in BETA! <small>We will be adding all that TechSoup has to offer to this system as well as new, fun tools to power up your social impact.</small></p></li></ul>");
}]);
