/*
 * angular-ctp
 * http://angular-ctp.tsgctp.org

 * Version: 0.1.46 - 2017-09-22
 * License: This source code is derived from previous works created by many
 * individuals and organizations, each attempting to improve information
 * technology use and processing results based on concepts such as
 * lightweight polyglot microservices, data stores, and various machine
 * learning methods.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This preamble and a reference to the License shall be included in any
 * derivative works.

 */

angular.module("cooperative", ["cooperative.biz.admin","cooperative.biz.auth2","cooperative.biz.auth2.account","cooperative.biz.auth2.authentication","cooperative.biz.auth2.authentication.oauth","cooperative.biz.auth2.authorization","cooperative.biz.auth2.bootstrap","cooperative.biz.auth2.broadcast","cooperative.biz.auth2.constant","cooperative.biz.auth2.context","cooperative.biz.auth2.core","cooperative.biz.constraints","cooperative.biz.content","cooperative.biz.data","cooperative.biz.geo","cooperative.biz.outreach","cooperative.biz.queries","cooperative.biz.registration","cooperative.biz.state","cooperative.biz.uploads","cooperative.biz.utils","cooperative.biz.workflow","cooperative.config","cooperative.dal.core","cooperative.dal.core.requester","cooperative.dal.ws","cooperative.directive.auth.permissions","cooperative.directive.auth.request","cooperative.directive.auth.user","cooperative.directive.constraints.request","cooperative.directive.content.activitycode","cooperative.directive.content.request","cooperative.directive.data","cooperative.directive.data.request","cooperative.directive.registration","cooperative.directive.registration.request","cooperative.directive.search","cooperative.directive.search.directive.search2","cooperative.directive.ui","cooperative.directive.ui.formControl","cooperative.directive.ui.formControl.validation","cooperative.directive.ui.panel","cooperative.directive.ui.searchBox","cooperative.directive.ui.utils.form","cooperative.directive.uploads","cooperative.directive.workflow.request","cooperative.filters","cooperative.filters.content","cooperative.interceptor","cooperative.provider","cooperative.utils","cooperative.tpls"]);
var cooperativeBizAdmin = angular.module('cooperative.biz.admin', []);
var cooperativeBizAdmin; //Defined in 0module.js

/**
 * @ngdoc service
 * @name adminDataFactory
 *
 * @description
 * Author hiten
 *
 * Date 01/05/15
 *
 * Business service for getting a new Resource Object.
 */
cooperativeBizAdmin.factory('adminDataFactory', ['$q', 'resourceApiFactory',function ($q, resourceApiFactory) {
    var adminDataFactory = {};

    adminDataFactory.getCooperativeResourceObject = function(resourceName, version, appSession){
        var deferred = $q.defer();

        resourceApiFactory.getCooperative(resourceName, version, appSession)
            .then(function (processObject) {
                if (processObject.io.length > 0) {
                    var resourceObject = processObject.io[0];
                    if (resourceObject.signature == 'ResourceObject_001') {
                        var entityObject = resourceObject.instance.arrayRes;
                        deferred.resolve(entityObject);
                    } else {
                        deferred.reject('incorrect response');
                    }
                } else {
                    deferred.reject('incorrect response');
                }
            }).catch(function(data){
                deferred.reject('Unable to retrieve resource object');
            });

        return deferred.promise;
    };

    return adminDataFactory;
}]);
var cooperativeBizAuth2Account = angular.module('cooperative.biz.auth2.account', ['cooperative.biz.data']);
var cooperativeBizAuth2Account;

cooperativeBizAuth2Account
	.factory('accountManagement', ['$q', 'authApiFactory', 'processObjectFactory', function($q, authApiFactory, processObjectFactory) {
		var accountManagement = {};

		accountManagement.updateUsername = function(currentUsername, newUsername, password, version, appSession) {
			var deferred = $q.defer();

			processObjectFactory.new(version, appSession)
				.then(function(processObject) {

					processObject.directives = {
						'identity_old': currentUsername,
						'identity_new': newUsername,
						'credential_old': password,
						'this_context': 'self',
						'default_context': 'self',
						'additional_context': 'self',
						'session_type': 'default'
					};

					authApiFactory.postAuthUpdate(processObject, version, appSession)
						.then(function(data) {
							deferred.resolve('Update Successful');
						}, function(data) {
							deferred.reject('Update not successful');
						});
				}).catch(function(data) {
					deferred.reject('ProcessObject cannon be retrieved!');
				});

			return deferred.promise;
		};

		accountManagement.updatePassword = function(username, currentPassword, newPassword, version, appSession) {
			var deferred = $q.defer();

			processObjectFactory.new(version, appSession)
				.then(function(processObject) {

					processObject.directives = {
						'identity_old': username,
						'credential_old': currentPassword,
						'credential_new': newPassword,
						'this_context': 'self',
						'default_context': 'self',
						'additional_context': 'self',
						'session_type': 'default'
					};

					authApiFactory.postAuthUpdate(processObject, version, appSession)
						.then(function(data) {
							deferred.resolve('Update Successful');
						}, function(data) {
							deferred.reject('Update not successful');
						});
				}).catch(function(data) {
					deferred.reject('ProcessObject cannon be retrieved!');
				});

			return deferred.promise;
		};

		accountManagement.resetPassword = function(username) {
			return authApiFactory.accountReset(username, undefined, true);
		};

		return accountManagement;
	}]);
var cooperativeBizAuth2Account;

cooperativeBizAuth2Account
	.factory('accountUtils', ['$q', 'authApiFactory', function($q, authApiFactory) {
		var accountUtils = {};

		accountUtils.sessionCheck = function(sessionId, version, appSession) {
			return authApiFactory.sessionCheck(sessionId, version, appSession);
		};

		accountUtils.getIdsByContext = function(sessionId, context, version, appSession) {
			return authApiFactory.getIdsByContext(sessionId, context, version, appSession);
		};

		accountUtils.isUserNameUnique = function(username, version, appSession) {
			var deferred = $q.defer();

			authApiFactory.getPermissionsByUserName(username, version, appSession)
				.then(function(data) {
					if (!data.state) {
						deferred.resolve(true);
					} else {
						deferred.resolve(false);
					}
				})
				.catch(function() {
					deferred.resolve(false);
				});

			return deferred.promise;
		};

		accountUtils.getUserIdByOrgId = function(orgProcessObjectId, version, appSession) {
			var deferred = $q.defer();

			var returnlist = [];

			authApiFactory.getUserIdByOrgId(orgProcessObjectId, version, appSession)
				.then(function(data) {
					for (var key in data[orgProcessObjectId]) {
						for (var userId in data[orgProcessObjectId][key]) {
							var userObject = {
								userId: userId,
								userName: data[orgProcessObjectId][key][userId]
							};

							returnlist.push(userObject);
						}
					}

					deferred.resolve(returnlist);
				}).catch(function() {
					deferred.reject(returnlist);
				});


			return deferred.promise;
		};


		accountUtils.getUserByUsername = function(username, version, appSession) {
			return authApiFactory.getPermissionsByUserName(username, version, appSession);
		};

		return accountUtils;
	}]);
var cooperativeBizAuth2authenticationOAuth = angular.module('cooperative.biz.auth2.authentication.oauth', []);
var cooperativeBizAuth2authenticationOAuth;
cooperativeBizAuth2authenticationOAuth
	.factory('oAuthenticate', ['$window', '$state', 'transcodeFactory', 'cooperativeAuth', 'appUserSession', 'loginBroadcast', function ($window, $state, transcodeFactory, cooperativeAuth, appUserSession, loginBroadcast) {
		var authenticate = {};

		authenticate.requestLoginByProvider = function (provider, username, returnUrl) {
			if (returnUrl === undefined || returnUrl === null || returnUrl === '') {
				returnUrl = $state.href($state.current.name, $state.params, { absolute: true });
			}

			if (returnUrl.indexOf('?') === -1) {
				returnUrl = returnUrl + '?oauth=';
			} else {
				returnUrl = returnUrl + '&oauth=';
			}

			transcodeFactory.encrypt(returnUrl, 'v_001', true)
				.then(function (encryptedReturnURL) {
					$window.open(cooperativeAuth.oAuthURL + appUserSession.getUser().getSession(true) + '/' + provider + '/' + username + '/' + encryptedReturnURL, '_self');
				}).catch(function () {
					loginBroadcast.broadcastFailure(appUserSession.getUser());
				});
		};

		authenticate.confirmLoginByProvider = function (oauthEncryptedData) {
			transcodeFactory.decrypt(oauthEncryptedData, 'v_001', true)
				.then(function (decryptedData) {
					decryptedData = JSON.parse(decryptedData);
					if (decryptedData.ctp_user !== 'failed') {
						appUserSession.create(decryptedData.ctp_user, decryptedData.ctp_session);
						loginBroadcast.broadcastSuccess(appUserSession.getUser());
					} else {
						loginBroadcast.broadcastFailure(appUserSession.getUser());
					}
				}).catch(function () {
					loginBroadcast.broadcastFailure(appUserSession.getUser());
				});
		};

		return authenticate;
	}]);
var cooperativeBizAuth2Authentication = angular.module('cooperative.biz.auth2.authentication', []);
var cooperativeBizAuth2Authentication;
cooperativeBizAuth2Authentication
	.factory('authenticate', ['$q', 'authApiFactory', 'appUserSession', 'loginBroadcast', 'loginConstants', 'logoutBroadcast', function ($q, authApiFactory, appUserSession, loginBroadcast, LOGIN_CONSTANTS, logoutBroadcast) {
		var authenticate = {};

		authenticate.login = function (username, password) {
			var userData = {
				username: username,
				password: password
			};

			var deferred = $q.defer();

			authApiFactory.postWithCredentials(userData)
				.then(function (sessionId) {
					appUserSession.create(userData.username, sessionId);
					deferred.resolve({
						status: LOGIN_CONSTANTS.loginSuccess,
						data: appUserSession.getUser()
					});
				}).catch(function (data) {
					deferred.reject({
						status: LOGIN_CONSTANTS.loginFailed,
						data: appUserSession.getUser()
					});
				});

			return deferred.promise;
		};

		authenticate.platformLogin = function (username, password) {
			var deferred = $q.defer();
			authenticate.login(username, password)
				.then(function (data) {					
					deferred.resolve(data.data);
				}).catch(function (data) {					
					deferred.reject(data.data);
				});
			return deferred.promise;

		};

		authenticate.logout = function () {

			var deferred = $q.defer();

			authApiFactory.logout(appUserSession.getUser().getSession())
				.finally(function () {
					appUserSession.destroy();
					logoutBroadcast.broadcast();
					deferred.resolve();
				});

			return deferred.promise;
		};



		return authenticate;
	}]);
var cooperativeBizAuth2Authorization = angular.module('cooperative.biz.auth2.authorization', ['ui.router']);
var cooperativeBizAuth2Authorization;

cooperativeBizAuth2Authorization
    .factory('authorizationManagement', ['$q', 'authApiFactory', 'processObjectFactory', function($q, authApiFactory, processObjectFactory) {

	var authorizationManagement = {};

    authorizationManagement.addPermissions = function(sessionId, userName, permissionsList, additionalContext, version, appSession){
        var deferred = $q.defer();

        processObjectFactory.new(version, appSession)
            .then(function(processObject){

                processObject.directives = {
                    'user_session': sessionId,
                    'identity_old': userName,
                    'additional_context': 'self',
                    'add_permission': permissionsList
                };

                if (additionalContext){
                    if (additionalContext !== null && additionalContext !== ''){
                        processObject.directives.additional_context = additionalContext;
                    }
                }

                authApiFactory.postAuthUpdate(processObject, version, appSession)
                    .then(function(data){
                        deferred.resolve('Add Successful');
                    }, function(data){
                        deferred.reject('Add not successful');
                    });
            }).catch(function(data){
                deferred.reject('ProcessObject cannon be retrieved!');
            });

        return deferred.promise;
    };

    authorizationManagement.removePermissions = function(sessionId, userName, permissionsList, additionalContext, version, appSession){
        var deferred = $q.defer();

        processObjectFactory.new(version, appSession)
            .then(function(processObject){

                processObject.directives = {
                    'user_session': sessionId,
                    'identity_old': userName,
                    'additional_context': 'self',
                    'remove_permission': permissionsList
                };

                if (additionalContext){
                    if (additionalContext !== null && additionalContext !== ''){
                        processObject.directives.additional_context = additionalContext;
                    }
                }

                authApiFactory.postAuthUpdate(processObject, version, appSession)
                    .then(function(data){
                        deferred.resolve('Delete Successful');
                    }, function(data){
                        deferred.reject('Delete not successful');
                    });
            }).catch(function(data){
                deferred.reject('ProcessObject cannon be retrieved!');
            });

        return deferred.promise;
    };

    authorizationManagement.editPermissions = function(sessionId, userName, permissionsListAdd, permissionsListRemove, additionalContext, version, appSession){
        var deferred = $q.defer();

        processObjectFactory.new(version, appSession)
            .then(function(processObject){

                processObject.directives = {
                    'user_session': sessionId,
                    'identity_old': userName,
                    'additional_context': 'self',
                    'remove_permission': permissionsListRemove,
                    'add_permission': permissionsListAdd
                };

                if (additionalContext){
                    if (additionalContext !== null && additionalContext !== ''){
                        processObject.directives.additional_context = additionalContext;
                    }
                }

                authApiFactory.postAuthUpdate(processObject, version, appSession)
                    .then(function(data){
                        deferred.resolve('Edit Successful');
                    }, function(data){
                        deferred.reject('Edit not successful');
                    });
            }).catch(function(data){
                deferred.reject('ProcessObject cannon be retrieved!');
            });

        return deferred.promise;
    };

    authorizationManagement.checkPermissions = function(sessionId, permissions, version, appSession){
        var deferred = $q.defer();

        processObjectFactory.new(version, appSession)
            .then(function(processObject){

                processObject.directives = {
                    'check_permissions_session': sessionId,
                    'permissions': permissions
                };

                authApiFactory.postPermissionCheck(processObject, version, appSession)
                    .then(function(data){
                        deferred.resolve(data);
                    }, function(data){
                        deferred.reject(data);
                    });
            }).catch(function(data){
                deferred.reject('ProcessObject cannon be retrieved!');
            });

        return deferred.promise;
    };

    authorizationManagement.checkPermissionSets = function(sessionId, permissionSets, version, appSession){
        var deferred = $q.defer();

        processObjectFactory.new(version, appSession)
            .then(function(processObject){

                processObject.directives = {
                    'check_permissions_session': sessionId,
                    'permissions_sets': permissionSets
                };

                authApiFactory.postPermissionCheck(processObject, version, appSession)
                    .then(function(data){
                        deferred.resolve(data);
                    }, function(data){
                        deferred.reject(data);
                    });
            }).catch(function(data){
                deferred.reject('ProcessObject cannon be retrieved!');
            });

        return deferred.promise;
    };

    return authorizationManagement;
}]);
var cooperativeBizAuth2Authorization;

cooperativeBizAuth2Authorization
	.run(['$rootScope', 'authorizationRouterRun',function($rootScope, authorizationRouterRun) {
		$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
			authorizationRouterRun.run(event, toState, toParams, fromState, fromParams);
		});
	}]);
var cooperativeBizAuth2Authorization;

cooperativeBizAuth2Authorization
    .factory('authorizationRouterRun', ['$rootScope', '$state', '$timeout', '$q', 'aesFactory', 'appUserSession', 'init', 'authorizationManagement', function ($rootScope, $state, $timeout, $q, aesFactory, appUserSession, init, authorizationManagement) {
        var authorizationRouterRun = {};

        authorizationRouterRun.success = function (toState, toParams, fromState, fromParams) {
            if (!$rootScope.$broadcast('$stateChangeStart', toState, toParams, fromState, fromParams).defaultPrevented) {
                $rootScope.$broadcast('$stateChangePermissionAccepted', toState, toParams);

                $state.go(toState.name, toParams, {
                    notify: false
                }).then(function () {
                    $rootScope.$broadcast('$stateChangeSuccess', toState, toParams, fromState, fromParams);
                });
            }
        };

        authorizationRouterRun.fail = function (ctpRoutePermissions, toState, toParams, fromState, fromParams) {
            if (!$rootScope.$broadcast('$stateChangeStart', toState, toParams, fromState, fromParams).defaultPrevented) {
                $rootScope.$broadcast('$stateChangePermissionDenied', toState, toParams);

                var redirectTo = ctpRoutePermissions.redirectTo;
                var includeReturnUrl = ctpRoutePermissions.includeReturnUrl;

                if (angular.isFunction(redirectTo)) {
                    redirectTo = redirectTo();

                    $q.when(redirectTo).then(function (newState) {
                        if (newState) {
                            $state.go(newState, toParams);
                        }
                    });

                } else {
                    if (redirectTo) {
                        if (includeReturnUrl) {
                            var returnURL = $state.href(toState.name, toParams, {
                                absolute: true
                            });

                            var encryptedReturnURL = aesFactory.encrypt(returnURL);

                            $state.go(redirectTo, { returnURL: encryptedReturnURL });
                        } else {
                            $state.go(redirectTo, toParams);
                        }
                    }
                }
            }
        };

        authorizationRouterRun.run = function (event, toState, toParams, fromState, fromParams) {
            if (toState.$$finishAuthorize) {
				return;
			}

			var ctpRoutePermissions;

            if (toState.data && toState.data.ctpRoutePermissions) {
                ctpRoutePermissions = toState.data.ctpRoutePermissions;
            }

            if (ctpRoutePermissions) {
                event.preventDefault();

                toState = angular.extend({
                    '$$finishAuthorize': true
                }, toState);

                if ($rootScope.$broadcast('$stateChangePermissionStart', toState, toParams).defaultPrevented) {
                    return;
                }

                if (appUserSession.getUser() !== null) {
                    if (ctpRoutePermissions.isAuthenticated === undefined || ctpRoutePermissions.isAuthenticated === appUserSession.getUser().getIsAuthenticated()) {
                        if (ctpRoutePermissions.permissionObject) {
                            if (appUserSession.getUser().getIsAuthenticated() === true){
                                authorizationManagement.checkPermissions(appUserSession.getUser().getSession(), ctpRoutePermissions.permissionObject, undefined, false)
                                .then(function () {
                                    authorizationRouterRun.success(toState, toParams, fromState, fromParams);
                                })
                                .catch(function () {
                                    authorizationRouterRun.fail(ctpRoutePermissions, toState, toParams, fromState, fromParams);
                                });
                            } else {
                                authorizationRouterRun.fail(ctpRoutePermissions, toState, toParams, fromState, fromParams);
                            }
                        } else {
                            authorizationRouterRun.success(toState, toParams, fromState, fromParams);
                        }
                    } else {
                        authorizationRouterRun.fail(ctpRoutePermissions, toState, toParams, fromState, fromParams);
                    }
                } else {
                    init.do().finally(function () {
                        $state.go(toState.name, toParams);
                    });
                }
            }
        };

        return authorizationRouterRun;
    }]);

var cooperativeBizAuth2Authorization;

cooperativeBizAuth2Authorization
    .factory('authorizationUtils', ['$q', 'authApiFactory', 'dataFactory', function($q, authApiFactory, dataFactory) {

        var authorizationUtils = {};

        authorizationUtils.createPermissionObjectCallback = function(layer, action, context, targetList, resource, roleAlias, appSession, callback) {
            authorizationUtils.createPermissionObject(layer, action, context, targetList, resource, roleAlias, appSession)
                .then(function(data) {
                    callback(data);
                }).catch(function(data) {
                    callback(data);
                });
        };

        authorizationUtils.createPermissionObject = function(layer, action, context, targetList, resource, roleAlias, appSession) {
            var deferred = $q.defer();

            /* TODO: CHECK IF THIS IS ALL GOOD */

            dataFactory.getNewEntityObject(undefined, undefined, 'RoleObject_001', undefined, undefined, undefined, appSession)
                .then(function(roleObject) {
                    var permission = roleObject.instance.role.uri[0];
                    
                    permission.layer = layer;
                    permission.action = action;
                    permission.context = context;
                    permission.targets = targetList;
                    permission.resource = resource;
                    permission.roleAlias = roleAlias;

                    deferred.resolve({
                        status: true,
                        permission: permission
                    });
                }).catch(function() {
                    deferred.resolve({
                        status: false,
                        permission: null
                    });
                });

            return deferred.promise;
        };
        return authorizationUtils;

    }]);
var cooperativeBizAuth2Bootstrap = angular.module('cooperative.biz.auth2.bootstrap', []);
var cooperativeBizAuth2Bootstrap;

cooperativeBizAuth2Bootstrap
	.controller('ctpbaseCtrl', ['$scope', '$state', 'appUserSessionChangedBroadcast', 'appUserSession', '$stateParams', function ($scope, $state, appUserSessionChangedBroadcast, appUserSession, $stateParams) {
		$scope.setCurrentUser = function (user) {
			$scope.currentUser = user;
		};

		$scope.setCurrentOrg = function (orgIndex) {
			if (orgIndex === '' || orgIndex === null || orgIndex === undefined) {
				orgIndex = 0;
			}
			else {
				orgIndex = parseInt(orgIndex);
				if (orgIndex < 0 || isNaN(orgIndex)) {
					orgIndex = 0;
				}
			}

			var currentUser = $scope.currentUser;
			if (currentUser !== undefined && currentUser !== null && currentUser.context !== undefined && currentUser.context !== null && currentUser.context.org !== undefined && currentUser.context.org.length > 0) {
				
				if (orgIndex > currentUser.context.org.length - 1) {
					orgIndex = 0;
				}
				$scope.currentOrg = currentUser.context.org[orgIndex];
			}
		};

		$scope.getCurrentOrg = function () {
			return $scope.currentOrg;
		};


		$scope.getCurrentUser = function () {
			return $scope.currentUser;
		};

		$scope.setCurrentUser(appUserSession.getUser());

		appUserSessionChangedBroadcast.listner(function () {
			$scope.$apply(function () {
				$scope.setCurrentUser(appUserSession.getUser());
				$scope.setCurrentOrg($stateParams.o);
				$state.reload();
			});
		});

		$scope.$watchCollection('$stateParams', function () {
			$scope.setCurrentOrg($stateParams.o);
		});
	}]);
var cooperativeBizAuth2Bootstrap;

cooperativeBizAuth2Bootstrap
	.config(['$stateProvider', function($stateProvider) {
		$stateProvider.state('ctpbase', {
			url: '?o',
			abstract: true,
			template: '<div ui-view></div>',
			controller: 'ctpbaseCtrl',
			resolve: {
				init: ['init', function(init) {
					return init.do();
				}]
			}
		});
	}]);
var cooperativeBizAuth2Bootstrap;

cooperativeBizAuth2Bootstrap
	.factory('init', ['$q', 'sharedAuthCookie', 'sharedContextCookie', 'appUserSession', 'authUser', 'cooperativeAuth', function ($q, sharedAuthCookie, sharedContextCookie, appUserSession, AuthUser, cooperativeAuth) {
		var init = {};

		init.do = function () {

			var deferred = $q.defer();

			if (appUserSession.getUser() === null) {
				sharedAuthCookie.init()
					.then(function () {
						var cookie = sharedAuthCookie.get();
						if (cookie !== null) {
							appUserSession.setUser(new AuthUser(cookie.username, cookie.session, cookie.isAuthenticated));
							sharedContextCookie.init(sharedAuthCookie.getEncryptedCookie())
								.then(function () {
									var contextCookie = sharedContextCookie.get();
									appUserSession.setUser(new AuthUser(cookie.username, cookie.session, cookie.isAuthenticated, contextCookie));
								});
						} else {
							appUserSession.setUser(new AuthUser());
						}

						deferred.resolve();
					});
			} else {
				deferred.resolve();
			}

			return deferred.promise;

		};

		return init;
	}]);
var cooperativeBizAuth2Broadcast = angular.module('cooperative.biz.auth2.broadcast', []);
var cooperativeBizAuth2Broadcast;

cooperativeBizAuth2Broadcast
	.factory('appUserSessionChangedBroadcast', ['$rootScope', 'sessionConstants', function($rootScope, SESSION_CONSTANTS) {
		var appUserSessionChangedBroadcast = {};

		var session_changed = SESSION_CONSTANTS.sessionChanged;

		appUserSessionChangedBroadcast.broadcast = function() {
			$rootScope.$broadcast(session_changed, {});
		};

		appUserSessionChangedBroadcast.listner = function(callback) {
			$rootScope.$on(session_changed, callback);
		};

		return appUserSessionChangedBroadcast;
	}]);
var cooperativeBizAuth2Broadcast;

cooperativeBizAuth2Broadcast
	.factory('loginBroadcast', ['$rootScope', 'loginConstants', function($rootScope, LOGIN_CONSTANTS) {
		var loginBroadcast = {};

		var login_success = LOGIN_CONSTANTS.loginSuccess;
		var login_failure = LOGIN_CONSTANTS.loginFailed;

		loginBroadcast.broadcastSuccess = function(data) {
			$rootScope.$broadcast(login_success, data);
		};

		loginBroadcast.listnerSuccess = function(callback) {
			$rootScope.$on(login_success, callback);
		};

		loginBroadcast.broadcastFailure = function(data) {
			$rootScope.$broadcast(login_failure, data);
		};

		loginBroadcast.listnerFailure = function(callback) {
			$rootScope.$on(login_failure, callback);
		};

		return loginBroadcast;
	}]);
var cooperativeBizAuth2Broadcast;

cooperativeBizAuth2Broadcast.factory('logoutBroadcast', ['$rootScope', 'logoutConstants', function($rootScope, LOGOUT_CONSTANTS) {
	var logoutBroadcast = {};

	var logout_success = LOGOUT_CONSTANTS.logoutSucess;

	logoutBroadcast.broadcast = function() {
		$rootScope.$broadcast(logout_success, {});
	};

	logoutBroadcast.listner = function(callback) {
		$rootScope.$on(logout_success, callback);
	};

	return logoutBroadcast;
}]);
var cooperativeBizAuth2Broadcast;

cooperativeBizAuth2Broadcast.factory('sharedAuthCookieChangedBroadcast', ['$rootScope', 'cookieConstants', function($rootScope, COOKIE_CONSTANTS) {
	var sharedAuthCookieChangedBroadcast = {};

	var cookie_changed = COOKIE_CONSTANTS.cookieChanged;

	sharedAuthCookieChangedBroadcast.broadcast = function() {
		$rootScope.$broadcast(cookie_changed, {});
	};

	sharedAuthCookieChangedBroadcast.listner = function(callback) {
		$rootScope.$on(cookie_changed, callback);
	};

	return sharedAuthCookieChangedBroadcast;
}]);

cooperativeBizAuth2Broadcast.factory('sharedContextCookieChangedBroadcast', ['$rootScope', 'cookieConstants', function($rootScope, COOKIE_CONSTANTS) {
	var sharedContextCookieChangedBroadcast = {};

	var cookie_changed = COOKIE_CONSTANTS.cookieContextChanged;

	sharedContextCookieChangedBroadcast.broadcast = function() {
		$rootScope.$broadcast(cookie_changed, {});
	};

	sharedContextCookieChangedBroadcast.listner = function(callback) {
		$rootScope.$on(cookie_changed, callback);
	};

	return sharedContextCookieChangedBroadcast;
}]);
var cooperativeBizAuth2Constant = angular.module('cooperative.biz.auth2.constant', []);
var cooperativeBizAuth2Constant;
cooperativeBizAuth2Constant
	.constant('cookieConstants', {
		cookieChanged: 'ctp-auth-shared-cookie-changed',
		cookieContextChanged: 'ctp-auth-context-shared-cookie-changed'
	});
var cooperativeBizAuth2Constant;
cooperativeBizAuth2Constant
	.constant('loginConstants', {
		loginSuccess: 'auth-login-success',
		loginFailed: 'auth-login-failed',
		logoutSuccess: 'auth-logout-success'
	});
var cooperativeBizAuth2Constant;
cooperativeBizAuth2Constant
	.constant('logoutConstants', {
		logoutSuccess: 'auth-logout-success'
	});
var cooperativeBizAuth2Constant;
cooperativeBizAuth2Constant
	.constant('sessionConstants', {
		sessionChanged: 'ctp-auth-app-user-session-changed'
	});
var cooperativeBizAuth2Context = angular.module('cooperative.biz.auth2.context', []);
var cooperativeBizAuth2Context;
(function (cooperativeBizAuth2Context) {
    'use strict';

    cooperativeBizAuth2Context.factory('contextFactory', contextFactory);
    contextFactory.$inject = ['$q', 'organisationContextFactory', 'sharedAuthCookie', 'sharedContextCookie', 'cooperativeAuth'];

    function contextFactory($q, organisationContextFactory, sharedAuthCookie, sharedContextCookie, cooperativeAuth) {

        var contextFactoryObj = {};

        contextFactoryObj.initContext = function (sessionId) {
            var context = {};
            var deferred = $q.defer();

            var promises = [];
            promises.push(organisationContextFactory.getUserOrgs(sessionId));

            $q.all(promises).then(function (data) {
                var userOrgs = data[0];
                context['org'] = userOrgs;
                var userCookie = sharedAuthCookie.getEncryptedCookie();
                if (userCookie != null) {
                    sharedContextCookie.create(context, null, userCookie);
                    sharedContextCookie.init(userCookie)
                        .then(function () {
                            deferred.resolve(context);
                        });
                }
            });

            return deferred.promise;
        };

        return contextFactoryObj;
    }

})(cooperativeBizAuth2Context);
var cooperativeBizAuth2Context;
(function(cooperativeBizAuth2Context){
    'use strict';

    cooperativeBizAuth2Context.factory('organisationContextFactory', organisationContextFactory);
    organisationContextFactory.$inject = ['$q', 'accountUtils', 'searchFactory', 'dataFactory'];

    function organisationContextFactory($q, accountUtils, searchFactory, dataFactory){
        var organisationContextFactoryObj = {};

        organisationContextFactoryObj.getUserOrgs = function (sessionId) {
			var deferred = $q.defer();

			var shard = 'org';
			accountUtils.getIdsByContext(sessionId, shard, undefined, true)
				.then(function (orgIds) {
					var userOrgs = [];

					var userOrganisationIds = [];
					for (var orgId in orgIds) {
						userOrganisationIds.push('\'' + orgId + '\'');
					}

					var queryString = 'id in (' + userOrganisationIds.join() + ')';
					searchFactory.search(shard, undefined, undefined, queryString, ['EntityObject_001', 'NameObject_001'], 0, userOrganisationIds.length, true, undefined, undefined, undefined, true, undefined, undefined)
						.then(function (searchResults) {
							var results = searchResults.results;
							for (var r = 0; r < results.length; r++) {
								var entityObject = dataFactory.returnObjects(null, 'EntityObject_001', 0, undefined, undefined, results[r].data.io, undefined);
								var nameObject = dataFactory.returnObjects(entityObject[0].instanceId, 'NameObject_001', 0, undefined, undefined, results[r].data.io, undefined);
								userOrgs.push({ orgId: entityObject[0].rootId, name: nameObject[0].typeValue });
							}

							deferred.resolve(userOrgs);
						});
				});

			return deferred.promise;
		};

        return organisationContextFactoryObj;
    }


})(cooperativeBizAuth2Context);
var cooperativeBizAuth2Core = angular.module('cooperative.biz.auth2.core', []);
var cooperativeBizAuth2Core;
cooperativeBizAuth2Core
	.factory('appUserSession', ['sharedAuthCookie', 'sharedContextCookie', 'authUser', 'sharedAuthCookieChangedBroadcast', 'sharedContextCookieChangedBroadcast', 'appUserSessionChangedBroadcast', 'cooperativeAuth', function(sharedAuthCookie, sharedContextCookie, AuthUser, sharedAuthCookieChangedBroadcast, sharedContextCookieChangedBroadcast, appUserSessionChangedBroadcast, cooperativeAuth) {
		var appUserSession = {};

		appUserSession.aUser = null;

		appUserSession.getUser = function() {
			return appUserSession.aUser;
		};

		appUserSession.setUser = function(_aUser) {
			appUserSession.aUser = _aUser;
		};

		appUserSession.create = function(username, session) {
			appUserSession.aUser = new AuthUser(username, session, true);
			sharedAuthCookie.create(appUserSession.aUser.getAuthUserObject(), null, cooperativeAuth.authSessionKey);
		};

		appUserSession.destroy = function() {
			appUserSession.aUser = new AuthUser();
		 	sharedContextCookie.destroy(sharedAuthCookie.getEncryptedCookie());
			sharedAuthCookie.destroy();
		};

		sharedAuthCookieChangedBroadcast.listner(function() {
			sharedCookieListener();
		});

		sharedContextCookieChangedBroadcast.listner(function(){
			sharedCookieListener();
		});

		var sharedCookieListener = function()
		{
			if (sharedAuthCookie.get() !== null) {
				var contextCookie = sharedContextCookie.get();
				if(contextCookie !== null){
					appUserSession.aUser = new AuthUser(sharedAuthCookie.get().username, sharedAuthCookie.get().session, sharedAuthCookie.get().isAuthenticated, contextCookie);
					appUserSessionChangedBroadcast.broadcast();
				}
			} else {
				appUserSession.aUser = new AuthUser();
				appUserSessionChangedBroadcast.broadcast();
			}
		};


		return appUserSession;
	}]);
var cooperativeBizAuth2Core;
cooperativeBizAuth2Core
    .factory('authUser', ['$injector', '$window', 'cooperative', 'cooperativeSso', 'aesFactory', 'loginBroadcast', function ($injector, $window, cooperative, cooperativeSso, aesFactory, loginBroadcast) {
        //lazy dep injection
        var authenticate, $state, contextFactory;

        var authUser = function (username, session, isAuthenticated, context) {
            if (username === null || username === undefined || username === '') {
                username = 'anonymous';
            }

            if (session === null || session === undefined || session === '') {
                session = 'nil';
            }

            if (isAuthenticated === null || isAuthenticated === undefined || isAuthenticated === '') {
                isAuthenticated = false;
            }

            this.username = username;
            this.session = session;
            this.isAuthenticated = isAuthenticated;
            this.context = context;
        };

        authUser.prototype.getAuthUserObject = function () {
            var user = {
                username: this.username,
                session: this.session,
                isAuthenticated: this.isAuthenticated
            };

            return user;
        };

        authUser.prototype.getUsername = function () {
            return this.username;
        };

        authUser.prototype.getIsInitialised = function () {
            return this.isInitialised;
        };

        authUser.prototype.getSession = function (appSession) {
            if (appSession === true || appSession === 'true') {
                return cooperative.apiKey;
            } else {
                return this.session;
            }
        };

        authUser.prototype.getIsAuthenticated = function () {
            return this.isAuthenticated;
        };

        authUser.prototype.platformLogin = function (username, password) {
            authenticate = authenticate || $injector.get('authenticate');
            contextFactory = contextFactory || $injector.get('contextFactory');

            authenticate.platformLogin(username, password)
                .then(function (data) {
                    var sessionId = data.getSession(false);
                    contextFactory.initContext(sessionId)
                        .then(function (context) {
                        }).finally(function () {
                            loginBroadcast.broadcastSuccess(data.data);
                        });
                }.bind(this))
                .catch(function (errorData) { 
                    loginBroadcast.broadcastFailure(errorData.data);
                }.bind(this))
                .finally(function () {
                }.bind(this));
        };

        authUser.prototype.gotoPlatformLogin = function (returnURL, reason, userName) {
            $state = $state || $injector.get('$state');
            //****line
            if (!this.getIsAuthenticated()) {
                var encryptedReturnURL = '';

                if (returnURL === undefined || returnURL === null || returnURL === '') {
                    returnURL = $state.href($state.current.name, $state.params, {
                        absolute: true
                    });
                }

                encryptedReturnURL = aesFactory.encrypt(returnURL);

                var sendToURL = cooperativeSso.platformLogin.url.path + '?returnUrl=' + encodeURIComponent(encryptedReturnURL);

                if (reason !== undefined && reason !== null && reason !== '') {
                    sendToURL = sendToURL + '&reason=' + encodeURIComponent(reason);
                }

                if (userName !== undefined && userName !== null && userName !== '') {
                    var encryptedUsername = aesFactory.encrypt(userName);

                    sendToURL = sendToURL + '&username=' + encodeURIComponent(encryptedUsername);
                }

                $window.open(sendToURL, '_self');
            } else {
                $window.location.reload();
            }
        };

        authUser.prototype.platformLogout = function () {
            authenticate = authenticate || $injector.get('authenticate');

            authenticate.logout().finally(function () { });
        };

        authUser.prototype.gotoPlatformLogout = function (returnURL) {
            $state = $state || $injector.get('$state');

            var encryptedReturnURL = '';

            if (returnURL === undefined || returnURL === null || returnURL === '') {
                if ($state.current.name !== '') {
                    returnURL = $state.href($state.current.name, {}, {
                        absolute: true
                    });
                } else {
                    // default to ctpbase is not state is intialised...
                    returnURL = $state.href('ctpbase', {}, {
                        absolute: true
                    });
                }
            }

            encryptedReturnURL = aesFactory.encrypt(returnURL);

            $window.open(cooperativeSso.platformLogout.url.path + '?returnUrl=' + encodeURIComponent(encryptedReturnURL), '_self');
        };

        authUser.prototype.gotoPlatformPermissionUpdate = function (returnURL, reason, username) {
            $state = $state || $injector.get('$state');
            var encryptedReturnURL = '';

            if (returnURL === undefined || returnURL === null || returnURL === '') {
                returnURL = $state.href($state.current.name, {}, {
                    absolute: true
                });
            }

            encryptedReturnURL = aesFactory.encrypt(returnURL);
            var sendToURL = cooperativeSso.platformPermissionUpdate.url.path + '?returnUrl=' + encodeURIComponent(encryptedReturnURL);

            if (reason !== undefined && reason !== null && reason !== '') {
                sendToURL = sendToURL + '&reason=' + encodeURIComponent(reason);
            }

            var encryptedUsername;

            if (username === undefined || username === null || username === '') {
                encryptedUsername = aesFactory.encrypt(this.getUsername());
            } else {
                encryptedUsername = aesFactory.encrypt(username);
            }

            sendToURL = sendToURL + '&username=' + encodeURIComponent(encryptedUsername);

            $window.open(sendToURL, '_self');
        };

        authUser.prototype.gotoPlatformAccount = function (newWindow) {
            var sendToURL = cooperativeSso.platformAccount.url.path;

            var target = '_self';

            if (newWindow) {
                target = '_blank';
            }

            $window.open(sendToURL, target);
        };

        authUser.prototype.gotoPlatformSignup = function (newWindow) {
            var sendToURL = cooperativeSso.platformSignup.url.path;

            var target = '_self';

            if (newWindow) {
                target = '_blank';
            }

            $window.open(sendToURL, target);
        };

        return authUser;
    }]);

var cooperativeBizAuth2Core;

cooperativeBizAuth2Core
	.factory('sharedAuthCookie', ['$rootScope', '$window', '$timeout', '$q', '_xdomaincookie', 'cooperativeAuth', 'sharedAuthCookieChangedBroadcast', 'aesFactory', 'intervalsFactory', 'sharedCookierHelper', function ($rootScope, $window, $timeout, $q, _xdomaincookie, cooperativeAuth, sharedAuthCookieChangedBroadcast, aesFactory, intervalsFactory, SharedCookierHelper) {
		var sharedAuthCookie = {};

		var sharedCookie = SharedCookierHelper.createInstance('sharedAuthCookie', cooperativeAuth.authSessionKey);

		sharedCookie.receiverInitCompleteCookieChanged = function () {
			sharedAuthCookieChangedBroadcast.broadcast();
		};

		sharedAuthCookie.receiver = function (e) {
			sharedCookie.receiver(e);
		};

		sharedAuthCookie.init = function () {
			return sharedCookie.init();
		};

		sharedAuthCookie.get = function () {
			return sharedCookie.cookie;
		};

		sharedAuthCookie.getEncryptedCookie = function () {
			return sharedCookie.encryptedCookie;
		};

		sharedAuthCookie.destroy = function () {
			sharedCookie.destroy();
		};

		sharedAuthCookie.create = function (data, experationDate, key) {
			sharedCookie.create(data, experationDate);
		};

		sharedAuthCookie.getCookieWatch = function () {
			return sharedCookie.cookieWatch;
		};

		sharedAuthCookie.getCallbacks = function () {
			return sharedCookie.callbacks;
		};

		sharedAuthCookie.getInstance = function(){
			return sharedCookie;
		};

		_xdomaincookie.consumer.receiver(sharedAuthCookie.receiver);

		return sharedAuthCookie;
	}]);
var cooperativeBizAuth2Core;
cooperativeBizAuth2Core
	.factory('sharedContextCookie', ['$rootScope', '$window', '$timeout', '$q', '_xdomaincookie', 'cooperativeAuth', 'sharedContextCookieChangedBroadcast', 'aesFactory', 'intervalsFactory', 'sharedAuthCookie', 'sharedCookierHelper', function ($rootScope, $window, $timeout, $q, _xdomaincookie, cooperativeAuth, sharedContextCookieChangedBroadcast, aesFactory, intervalsFactory, sharedAuthCookie, SharedCookierHelper) {
		var sharedContextCookie = {};

		var sharedCookie = new SharedCookierHelper.createInstance('sharedContextCookie');

		sharedCookie.receiverInitComplete = function () {
			sharedContextCookieChangedBroadcast.broadcast();
		};

		sharedCookie.receiverInitCompleteCookieChanged = function () {
			sharedContextCookieChangedBroadcast.broadcast();
		};

		sharedContextCookie.receiver = function (e) {
			sharedCookie.receiver(e);
		};

		sharedContextCookie.init = function (key) {
			sharedCookie.setKey(key);
			return sharedCookie.init();
		};

		sharedContextCookie.get = function () {
			return sharedCookie.cookie;
		};

		sharedContextCookie.destroy = function () {
			sharedCookie.destroy();
		};

		sharedContextCookie.create = function (data, experationDate, key) {
			sharedCookie.setKey(key);
			sharedCookie.create(data, experationDate);
		};

		sharedContextCookie.getCookieWatch = function () {
			return sharedCookie.cookieWatch;
		};

		sharedContextCookie.getCallbacks = function () {
			return sharedCookie.callbacks;
		};

		sharedContextCookie.getInstance = function(){
			return sharedCookie;
		};

		_xdomaincookie.consumer.receiver(sharedContextCookie.receiver);

		return sharedContextCookie;
	}]);

var cooperativeBizAuth2Core;
(function (cooperativeBizAuth2Core) {

    cooperativeBizAuth2Core.factory('sharedCookierHelper', sharedCookierHelper);
    sharedCookierHelper.$inject = ['aesFactory', '$window', '_xdomaincookie', '$q', 'intervalsFactory', '$rootScope', 'cooperativeAuth'];

    function sharedCookierHelper(aesFactory, $window, _xdomaincookie, $q, intervalsFactory, $rootScope, cooperativeAuth) {

        return {
            createInstance: function (intervalName, key) {

                return new SharedCookie(aesFactory, $window, _xdomaincookie, $q, intervalsFactory, $rootScope, cooperativeAuth, intervalName, key);
            }
        };
    }

    var aesFactory, $window, _xdomaincookie, $q, intervalsFactory, $rootScope, cooperativeAuth;

    var SharedCookie = function (_aesFactory, _$window, __xdomaincookie, _$q, _intervalsFactory, _$rootScope, _cooperativeAuth, intervalName, key) {
        var sharedCookie = {};
        sharedCookie.cookie = null;
        sharedCookie.encryptedCookie = null;
        sharedCookie.callbacks = {};
        sharedCookie.cookieWatch = null;
        sharedCookie.key = key;
        sharedCookie.intervalName = intervalName;

        aesFactory = _aesFactory;
        $window = _$window;
        _xdomaincookie = __xdomaincookie;
        $q = _$q;
        intervalsFactory = _intervalsFactory;
        $rootScope = _$rootScope;
        cooperativeAuth = _cooperativeAuth;

        sharedCookie.create = function (data, experationDate) {
            var encryptedToken = aesFactory.encrypt(JSON.stringify(data));

            if (experationDate === null || experationDate === undefined) {
                var now = new $window.Date();

                experationDate = new $window.Date(now.getFullYear(), now.getMonth() + 1, now.getDate());
            }

            _xdomaincookie.consumer.create(this.key, encryptedToken, {
                expires: experationDate
            });
        };

        sharedCookie.init = function () {
            var deferred = $q.defer();
            var _this = this;
            intervalsFactory.create(this.intervalName, function (obj) {
                if (obj.active) {
                    _this.retrieve(deferred, true);
                } else {
                    deferred.resolve();
                    _this.startCookieWatch();
                    intervalsFactory.cancel(_this.intervalName);
                }
            });

            return deferred.promise;
        };

        sharedCookie.receiverInitComplete = function () {

        };

        sharedCookie.receiverInitCompleteCookieChanged = function () {

        };

        sharedCookie.retrieve = function (deferred, init) {
            var messageId = _xdomaincookie.consumer.retrieve(this.key);
            this.callbacks[messageId] = {
                time: new Date(),
                callback: deferred,
                status: 'request',
                data: {
                    init: init
                }
            };
        };

        sharedCookie.receiver = function (e) {
            if (this.callbacks.hasOwnProperty(e.data.messageId)) {
                if (e.data.action === 'retrieve') {
                    var retrievedCookie = e.data.cookie;
                    if (retrievedCookie === '') {
                        retrievedCookie = null;
                    } else {
                        this.encryptedCookie = retrievedCookie;
                        retrievedCookie = this.decryptToken(retrievedCookie);
                    }

                    if (this.callbacks[e.data.messageId].data.init === true) {
                        this.cookie = retrievedCookie;
                        intervalsFactory.cancel(this.intervalName);
                        $rootScope.$apply(this.callbacks[e.data.messageId].callback.resolve());
                        this.startCookieWatch();
                        this.receiverInitComplete();
                    } else {
                        //cookie has changed... trigger auth update please :)
                        if (!angular.equals(this.cookie, retrievedCookie)) {
                            this.cookie = retrievedCookie;
                            this.receiverInitCompleteCookieChanged();
                        }
                    }
                }
            }
        };

        sharedCookie.decryptToken = function (encryptedToken) {
            var decryptedToken = aesFactory.decrypt(encryptedToken);
            var token = JSON.parse(decryptedToken);
            return token;
        };

        sharedCookie.stopCookieWatch = function () {
            clearInterval(this.cookieWatch);
        };

        sharedCookie.startCookieWatch = function () {
            if (this.cookieWatch === null) {
                var _this = this;
                this.cookieWatch = setInterval(function () {
                    _this.retrieve(null, false);
                }, 1000);
            }
        };

        sharedCookie.destroy = function () {
            _xdomaincookie.consumer.destroy(this.key);
        };

        sharedCookie.setKey = function (key) {
            this.key = key;
        };

        sharedCookie.setIntervalName = function (intervalName) {
            this.intervalName = intervalName;
        };

        return sharedCookie;

    };

})(cooperativeBizAuth2Core);
var cooperativeBizAuth2 = angular.module('cooperative.biz.auth2', ['cooperative.biz.auth2.account','cooperative.biz.auth2.authentication', 'cooperative.biz.auth2.authorization','cooperative.biz.auth2.bootstrap','cooperative.biz.auth2.broadcast','cooperative.biz.auth2.constant','cooperative.biz.auth2.context','cooperative.biz.auth2.core']);
var cooperativeBizConstraints = angular.module('cooperative.biz.constraints', []);
var cooperativeBizConstraints; //Defined in 0module.js

/**
 * Created by hiten on 09/06/15.
 */
cooperativeBizConstraints.factory('constraintsFactory', ['$q', 'constraintsApiFactory' ,'resourceApiFactory',function ($q, constraintsApiFactory, resourceApiFactory) {
    var constraintsFactory = {};

    constraintsFactory.getConstraintsInfo = function(version, appSession){
        var deferred = $q.defer();
        resourceApiFactory.getCooperative('constraints', version, appSession)
            .then(function (processObject) {
                if (processObject.type == 'service_request') {
                    if (processObject.io.length > 0) {
                        var resourceObject = processObject.io[0];
                        if (resourceObject.signature == 'ResourceObject_001') {
                            var constraintsMap = resourceObject.instance.mapRes;
                            deferred.resolve(constraintsMap);
                        } else {
                            deferred.reject('incorrect response');
                        }
                    } else {
                        deferred.reject('incorrect response');
                    }
                } else {
                    deferred.reject('incorrect response');
                }
            }).catch(function(data){
                deferred.reject('Unable to retrieve resource object');
            });

        return deferred.promise;
    };

    constraintsFactory.getConstraintsNameList = function(version, appSession){
        var deferred = $q.defer();

        constraintsFactory.getConstraintsInfo(version, appSession)
            .then(function(constraintsMap){
                var constraintNames = [];
                for (var key in constraintsMap){
                    constraintNames.push(key);
                }

                deferred.resolve(constraintNames);
            }, function(){
                deferred.reject([]);
            });

        return deferred.promise;
    };

    constraintsFactory.checkConstraints = function(shard, partition, mode, processObjectId, constraintList, appSession){
        var deferred = $q.defer();

        constraintsApiFactory.getConstraints(shard, partition, mode, processObjectId, constraintList, appSession)
            .then(function(data){
                deferred.resolve(data);
            }, function(){
                deferred.reject([]);
            });

        return deferred.promise;
    };

    constraintsFactory.checkConstraint = function(shard, partition, mode, processObjectId, constraint, appSession){
        var constraintsList = [];
        constraintsList.push(constraint);

        return constraintsFactory.checkConstraints(shard, partition, mode, processObjectId, constraintsList, appSession);
    };

    constraintsFactory.checkAllConstraints = function(shard, partition, mode, processObjectId, appSession){
        var deferred = $q.defer();
        constraintsFactory.getConstraintsNameList(undefined, appSession)
            .then(function(constraintNames){
                constraintsFactory.checkConstraints(shard, partition, mode, processObjectId, constraintNames, appSession)
                    .then(function(checkedconstraints){
                        deferred.resolve(checkedconstraints);
                    }, function(){
                        deferred.reject([]);
                    });
            }, function(){
                deferred.reject([]);
            });

        return deferred.promise;
    };

    return constraintsFactory;
}]);

var cooperativeBizContent = angular.module('cooperative.biz.content', []);
var cooperativeBizContent; //Defined in 0module.js

/**
 * @ngdoc service
 * @name LegalIdentificationFactory
 *
 * @description
 * Author hiten
 *
 * Date 04/30/15
 *
 * Business service for getting legal identifier requirements for a country.
 */
cooperativeBizContent.factory('LegalIdentificationFactory', ['_', 'LookupFactory', '$q',
        function (_, LookupFactory, $q) {
            var LegalIdentificationFactory = {};

            /**
             * @ngdoc method
             * @name LegalIdentificationFactory#getLegalIds
             * @description
             * Returns a list of objects containing legal identifier name and helper translation code.
             *
             * A sample of the data returned: [{"id": [{"name": "EIN", "helper": "usHelper1"}], "type": "nonprofit", "country": "US"}]
             *
             * @param {string} country name of the country
             * @returns {Array} legalType objects containing name and helper translation code
             */
            LegalIdentificationFactory.getLegalIds = function (country) {
                var deferred = $q.defer();
                LookupFactory.getKeyValueList('legalIdentifierTypes').then(function (result) {
                    var legalTypes = _.findWhere(result, {
                        'type': 'nonprofit',
                        'country': country
                    });
                    deferred.resolve(legalTypes);
                })
                .catch(function(errorData){
                    deferred.reject('Error');
                });
                return deferred.promise;
            };

            return LegalIdentificationFactory;
        }]);

var cooperativeBizContent; //Defined in 0module.js

/**
 * @ngdoc service
 * @name LookupFactory
 *
 * @description
 * Author hiten
 *
 * Date 02/04/15
 *
 * Business service for getting common application content.
 */
cooperativeBizContent.factory('LookupFactory', ['$q', '_', 'contentApiFactory', 'cooperative', function ($q, _, contentApiFactory, cooperative) {
    var lookupFactory = {};

    /**
     * @ngdoc method
     * @name LookupFactory#getKeyValueList
     * @description
     * Returns the list of keys and values for a key code of an application.
     * The key value lists are created using the CTP Admin application.
     * The keys that are returned are commonly used for dropdowns and are commonly
     * translated before shown to the user.
     *
     * A sample of the data returned for primary activity types: [{"key": "primaryActivity1","value": "1"},{"key":"primaryActivity2","value": "2"}]
     *
     * @param {string} keyValueListName name of the key value list
     * @param {string} appName application name
     * @returns {Array}
     */
    lookupFactory.getKeyValueList = function (keyValueListName, appName) {
        var deferred = $q.defer();
        contentApiFactory.getKeyValueList(keyValueListName, appName === undefined ? cooperative.appName : appName)
            .then(function (result) {
                deferred.resolve(result);                
            }).catch(function(error){
                deferred.reject('Error');
            });

        return deferred.promise;
    };

    /**
     * @ngdoc method
     * @name LookupFactory#getKeyList
     * @description
     * Returns the list of keys for a key code of an application.
     * The key lists are created using the CTP Admin application.
     * The keys that are returned are commonly used for dropdowns and are commonly
     * translated before shown to the user.
     *
     * A sample of the data returned for website types: ["main", "blog", "other"]
     *
     * @param {string} keyListName name of the key list
     * @param {string=} appName application name
     * @returns {Array}
     */
    lookupFactory.getKeyList = function (keyListName, appName) {
        var deferred = $q.defer();
        contentApiFactory.getKeyList(keyListName, appName === undefined ? cooperative.appName : appName)
            .then(function (result) {
                deferred.resolve(result);
            }).catch(function(error){
                deferred.reject('Error');
            });

        return deferred.promise;
    };

    /**
     * @ngdoc property
     * @name LookupFactory#utils
     * @description
     * Contains helper methods for manipulating key value data
     */
    lookupFactory.utils = {};

    /**
     * @ngdoc method
     * @name LookupFactory#utils.getKeyForValue
     * @description
     * Returns the key that is assigned to a value.  This is commonly used when the value
     * is stored but there is a need to show the key to the user.
     *
     * @param {string} keyValueList key value list to search in
     * @param {string} value value to search for
     * @returns {string} key for the value or undefined if not found
     */
    lookupFactory.utils.getKeyForValue = function(keyValueList, value){
        var object =  _.findWhere(keyValueList, {'value': value});

        return lookupFactory.utils.getPropertyFromItem(object,  'key');
    };

    /**
     * @ngdoc method
     * @name LookupFactory#utils.getValueForKey
     * @description
     * Returns the value that is assigned to a key.
     *
     * @param {string} keyValueList key value list to search in
     * @param {string} key key to search for
     * @returns {string} value for the key or undefined if not found
     */
    lookupFactory.utils.getValueForKey = function(keyValueList, key){
        var object =  _.findWhere(keyValueList, {'key': key});

        return lookupFactory.utils.getPropertyFromItem(object,  'value');

    };

    /**
     * @ngdoc method
     * @name LookupFactory#utils.getItemsFromListByProperty
     * @description
     * Returns array of items where a property is equal to a certain value
     *
     * @param {Array} list list of key values
     * @param {string} property property to compare
     * @param {string} propertyValue property value to find
     * @returns {Array} array of matching values
     */
    lookupFactory.utils.getItemsFromListByProperty = function (list, property, propertyValue) {
        var searchString = {};
        searchString[property] = propertyValue;
        return _.where(list, searchString);
    };

    /**
     * @ngdoc method
     * @name LookupFactory#utils.getPropertyFromItem
     * @description
     * Returns the value for a particular property of an item
     *
     * @param {Object} item item to get property from
     * @param {string} property property to get value for
     * @returns {string} value of the property or '' if not found
     */
    lookupFactory.utils.getPropertyFromItem = function (item, property) {
        if (_.isObject(item) && _.has(item, property)) {
            return _.property(property)(item);
        } else {
            return '';
        }
    };

    return lookupFactory;
}]);
var cooperativeBizData = angular.module('cooperative.biz.data', []);
/* jshint -W018 */
var cooperativeBizData; //Defined in 0module.js
(function (cooperativeBizData) {
  'use strict';

  /**
   * @ngdoc service
   * @name biz.data.dataFactory
   *
   * @description
   * Author hiten
   *
   * Date 04/30/15
   *
   * Business service for
   */
  cooperativeBizData.factory('dataFactory', dataFactory);

  dataFactory.$inject = [
    '$q',
    'objectsApiFactory',
    'noShardRequesterFactory',
    'cooperativeResources',
    'newEntityObjectFactory'
  ];

  function dataFactory($q, objectsApiFactory, noShardRequesterFactory, cooperativeResources, newEntityObjectFactory) {
    var dataFactory = {};

    /**
     * @ngdoc method
     * @name biz.data.dataFactory#post
     * @methodOf biz.data.dataFactory
     *
     * @description
     * Post a single instance object to an existing process object.
     *
     * @param {string} shard shard the data is stored in
     * @param {string} partition partition the data is stored in
     * @param {Array} mode synch or asynch
     * @param {Object} ioObject instance object to post
     * @param {string} appSession session app
     *
     * @returns {Promise} return a promise
     */
    dataFactory.post = function (shard, partition, mode, ioObject, appSession) {

      return dataFactory.postIoList(shard, partition, mode, [ioObject], false, appSession);
    };

    /**
     * @ngdoc method
     * @name biz.data.dataFactory#postIoList
     * @methodOf biz.data.dataFactory
     *
     * @description
     * Post a list of instance objects to an existing process object.
     *
     * @param {string} shard shard the data is stored in
     * @param {string} partition partition the data is stored in
     * @param {Array} mode synch or asynch
     * @param {Array} ioObjectList array of instance objects
     *
     * @param {*} isNew isNew
     * @param {*} appSession appSession
     * @returns {Promise} return a promise
     */
    dataFactory.postIoList = function (shard, partition, mode, ioObjectList, isNew, appSession) {
      var deferred = $q.defer();
      var getId = ioObjectList[0].rootId;
      if (isNew && isNew !== 'false') {
        getId = 'new';
      }

      objectsApiFactory.get(shard, partition, mode, getId, 'any', ['nil'], appSession)
        .then(function (po) {

          if (po.id != ioObjectList[0].rootId) {
            po.id = ioObjectList[0].rootId;
            po.transactionId = ioObjectList[0].rootId;
            po.rev = '0-' + ioObjectList[0].rootId;
          }

          var io = po.io;
          for (var i = 0; i < ioObjectList.length; i++) {
            io.push(ioObjectList[i]);
          }

          dataFactory.postProcessObject(shard, partition, mode, po, appSession)
            .then(function (data) {
              deferred.resolve(data);
            })
            .catch(function (error) {
              deferred.reject('error posting data');
            });
        })
        .catch(function (error) {
          deferred.reject('Unable to retrieve ProcessObject');
        });

      return deferred.promise;
    };

    dataFactory.postProcessObject = function (shard, partition, mode, processObject, appSession) {
      var deferred = $q.defer();

      objectsApiFactory.post(shard, partition, mode, processObject, appSession)
        .then(function (data) {
          deferred.resolve(data);
        })
        .catch(function (error) {
          deferred.reject('error posting data');
        });

      return deferred.promise;
    };

    /**
     * @ngdoc method
     * @name biz.data.dataFactory#get
     * @methodOf biz.data.dataFactory
     *
     * @description
     * Returns a list of instance object(s) filtered by the passed in criteria
     *
     * @param {string} shard shard the data is stored in
     * @param {string} partition partition the data is stored in
     * @param {Array} mode synch or asynch
     * @param {string} poid the process object id
     * @param {string} parentInstanceId instance id of the parent object
     * @param {string} signature signature of the instance object desired
     * @param {string} index index of the instance object to return
     * @param {string} type instance object type
     * @param {string} typeValue type value of the new entity object
     * @param {string} appSession true to use the system app session
     * @returns {Array} a list of instance object(s) filtered by the passed in criteria
     */
    dataFactory.get = function (shard, partition, mode, poid, parentInstanceId, signature, index, type, typeValue, appSession) {
      var deferred = $q.defer();

      var signatureList = [];
      signatureList.push(signature);

      var promise = dataFactory.getBySignatureList(shard, partition, mode, poid, parentInstanceId, signatureList, appSession);

      promise.then(function (collection) {
        deferred.resolve(dataFactory.returnObjects(parentInstanceId, signature, index, type, typeValue, collection));
      }, function (error) {
        deferred.reject('Unable to retrieve ProcessObject');
      });

      return deferred.promise;
    };

    /**
     * @ngdoc method
     * @name biz.data.dataFactory#getBySignatureList
     * @methodOf biz.data.dataFactory
     *
     * @description
     * Returns a list of instance objects from the process object that have the passed in parent instance id and have a
     * signature in the passed in signature list
     *
     * @param {string} shard shard the data is stored in
     * @param {string} partition partition the data is stored in
     * @param {Array} mode synch or asynch
     * @param {string} processObjectId the process object id
     * @param {string} parentInstanceId the instance id of the parent instance object
     * @param {Array} signatureList signatures of the instance objects desired
     * @param {string} appSession true to use the system app session
     *
     * @returns {Array} list of instance objects that match the criteria
     */
    dataFactory.getBySignatureList = function (shard, partition, mode, processObjectId, parentInstanceId, signatureList, appSession) {
      var deferred = $q.defer();
      objectsApiFactory.get(shard, partition, mode, processObjectId, parentInstanceId, signatureList, appSession)
        .then(function (po) {
          var collection = po.io;
          deferred.resolve(collection);
        }).catch(function (data) {
        deferred.reject('Unable to retrieve ProcessObject');
      });

      return deferred.promise;
    };

    dataFactory.objectCache = {};      //Holds previously retreived process objects
    dataFactory.deferredObjects = {};  //Holds promises waiting on previously asked for process objects

    /**
     * @ngdoc method
     * @name dataFactory#clearSignatureListCache
     * @methodOf biz.data.dataFactory
     *
     * @description
     * This method clears the objects that cache data.
     *
     */
    dataFactory.clearSignatureListCache = function () {
      dataFactory.objectCache = {};
      dataFactory.deferredObjects = {};
    };

    /**
     * @ngdoc method
     * @name biz.data.dataFactory#getBySignatureListCached
     * @methodOf biz.data.dataFactory
     *
     * @description
     * This method returns process objects from stored cache or calls the databse when not yet stored.
     * It will also holds promises waiting on a previous request for the same process object and resolves
     * them when the data is received.
     *
     * @param {string} shard shard the data is stored in
     * @param {string} partition partition the data is stored in
     * @param {Array} mode synch or asynch
     * @param {string} processObjectId the process object id
     * @param {string} parentInstanceId the instance id of the parent instance object
     * @param {Array} signatureList signatures of the instance objects desired
     * @param {string} appSession true to use the system app session
     *
     * @returns {Array} list of instance objects that match the criteria
     */
    dataFactory.getBySignatureListCached = function (shard, partition, mode, processObjectId, parentInstanceId, signatureList, appSession) {
      // console.log('getBySignatureListCached', processObjectId);
      var deferred = $q.defer();

      // console.log('dataFactory.objectCache[processObjectId]', dataFactory.objectCache[processObjectId]);
      //Is the process already in the cache
      if (dataFactory.objectCache[processObjectId]) {

        //Is the cache still waiting for the data
        if (dataFactory.objectCache[processObjectId] === 'WAITING') {
          // console.log('getBySignatureListCached WAITING', processObjectId);

          //Create an object to hold the promise
          var waiting = {};
          waiting.processObjectId = processObjectId;
          waiting.deferred = deferred;

          //Create a random string for the key
          var defpropname = '';
          var possible = 'abcdefghijklmnopqrstuvwxyz0123456789';
          for (var i = 0; i < 15; i++) {
            defpropname += possible.charAt(Math.floor(Math.random() * possible.length));
          }

          //Store the promise to be resolved later
          dataFactory.deferredObjects[defpropname] = angular.copy(waiting);

          // console.log(angular.copy(dataFactory.deferredObjects));

        } else {
          // console.log('getBySignatureListCached RETURN CACHE', processObjectId);

          //Send the data back from the cache
          deferred.resolve(dataFactory.objectCache[processObjectId]);

        }

      } else {
        // console.log('getBySignatureListCached GET DATA', processObjectId);
        // console.log('getBySignatureListCached ADDED TO CACHE:', 'BLANK');

        //Make an entry in the cache to let future requests know the call for this process object has already been made
        dataFactory.objectCache[processObjectId] = 'WAITING';

        //Make the data call
        dataFactory.getBySignatureList(shard, partition, mode, processObjectId, parentInstanceId, signatureList, appSession)
          .then(function (po) {
            // console.log('getBySignatureListCached NEW:', po);

            //Store the data in the cache
            dataFactory.objectCache[processObjectId] = angular.copy(po);

            // console.log('getBySignatureListCached ADDED TO CACHE:', processObjectId, dataFactory.objectCache);

            //resolve the current promise
            deferred.resolve(po);

            // console.log('Check waiting promises');

            //Look through the waiting promises and resolve all the ones for the same process object
            for (var defkey in dataFactory.deferredObjects) {
              //Does the current process object id match the waiting promise
              if (dataFactory.deferredObjects[defkey].processObjectId === processObjectId) {

                //Resolve the waiting promise
                dataFactory.deferredObjects[defkey].deferred.resolve(po);

                //Remove the promise
                delete dataFactory.deferredObjects[defkey];

                // console.log('getBySignatureListCached RESOLVE:', dataFactory.deferredObjects);
              }
            }
          }).catch(function (data) {
          //Reject the current promise
          deferred.reject('Unable to retrieve ProcessObject');

          //Since there was an error reject every promise that was waiting on this data
          for (var defkey in dataFactory.deferredObjects) {

            //Does the current process object id match the waiting promise
            if (dataFactory.deferredObjects[defkey].processObjectId === processObjectId) {
              //Reject the waiting promise
              dataFactory.deferredObjects[defkey].deferred.reject('Unable to retrieve ProcessObject');

              //Remove the waiting promise
              delete dataFactory.deferredObjects[defkey];
            }
          }
        });
      }

      // console.log('getBySignatureListCached RETURN DEFERRED PROMISE:', dataFactory.objectCache);
      return deferred.promise;

    };

    /**
     * @ngdoc method
     * @name biz.data.dataFactory#returnObjects
     * @methodOf biz.data.dataFactory
     *
     * @description
     * Returns a filterd list of instance objects.
     *
     * @param {string} parentInstanceId name of the country
     * @param {string} signature signature of the instance object desired
     * @param {string} index index of the instance object to return
     * @param {string} type instance object type
     * @param {string} typeValue instance object typeValue
     * @param {Array} collection the array to filter
     * @param {Array} excludedTypes types to exclude from the final list
     *
     * @returns {Array} filterd list of instance objects
     */
    dataFactory.returnObjects = function (parentInstanceId, signature, index, type, typeValue, collection, excludedTypes) {
      var returnCollection = [];
      var unfilteredCollection = [];

      if (!parentInstanceId) {
        parentInstanceId = 'any';
      }

      if (collection && collection.length > 0) {
        for (var collectionIndex = 0; collectionIndex < collection.length; collectionIndex++) {
          var pushToUnfilteredCollection = true;

          if (type !== undefined && type !== '') {
            if (typeof collection[collectionIndex].type === 'string' && typeof type === 'string') {
              if (collection[collectionIndex].type.toLowerCase() !== type.toLowerCase()) {
                pushToUnfilteredCollection = false;
              }
            } else {
              if (collection[collectionIndex].type !== type) {
                pushToUnfilteredCollection = false;
              }
            }
          }

          if (pushToUnfilteredCollection) {
            if (parentInstanceId !== 'any') {
              if (parentInstanceId.toLowerCase() !== collection[collectionIndex].associateId.toLowerCase()) {
                pushToUnfilteredCollection = false;
              }
            }
          }

          if (pushToUnfilteredCollection) {
            if (typeValue !== undefined && typeValue !== '') {
              if (typeof collection[collectionIndex].typeValue === 'string' && typeof typeValue === 'string') {
                if (collection[collectionIndex].typeValue.toLowerCase().indexOf(typeValue.toLowerCase()) === -1) {
                  pushToUnfilteredCollection = false;
                }
              } else {
                if (collection[collectionIndex].typeValue !== typeValue) {
                  pushToUnfilteredCollection = false;
                }
              }
            }
          }

          if (pushToUnfilteredCollection) {
            if (signature.toLowerCase() !== collection[collectionIndex].signature.toLowerCase()) {
              pushToUnfilteredCollection = false;
            }
          }

          if (pushToUnfilteredCollection) {
            unfilteredCollection.push(collection[collectionIndex]);
          }
        }
      } else {
        collection = [];
      }

      for (var j = 0; j < unfilteredCollection.length; j++) {
        if (typeof unfilteredCollection[j].typeValue === 'string') {
          unfilteredCollection[j].typeValue = unfilteredCollection[j].typeValue.trim();
        }

        if (typeof unfilteredCollection[j].type === 'string') {
          unfilteredCollection[j].type = unfilteredCollection[j].type.trim();
        }
      }

      if (excludedTypes !== undefined && excludedTypes.length > 0) {
        var eType = eval(excludedTypes.toLowerCase());
        for (var i = 0; i < unfilteredCollection.length; i++) {
          if (eType.indexOf(unfilteredCollection[i].type.toString().toLowerCase()) === -1) {
            returnCollection.push(unfilteredCollection[i]);
          }

        }
      } else {
        returnCollection = unfilteredCollection;
      }

      if (index > -1) {
        if (index < returnCollection.length) {
          return [returnCollection[index]];
        } else {
          if (returnCollection.length === 0) {
            return [];
          } else {
            return [returnCollection[returnCollection.length - 1]];
          }
        }
      } else {
        return returnCollection;
      }
    };

    /**
     * @ngdoc method
     * @name biz.data.dataFactory#getNewEntityObject
     * @methodOf biz.data.dataFactory
     *
     * @description
     * Returns an instance object initialized with the passed in data.
     *
     * @param {string} poid the process object id
     * @param {string} parentInstanceId the instance id of the parent instance object
     * @param {string} signature signatures of the instance objects desired
     * @param {string} type of the instance object
     * @param {string} typevalue type value of the new entity object
     * @param {string} version version of the new entity object to retrieve
     * @param {string} appSession true to use the system app session
     *
     * @returns {InstanceObject} instance object initialized with the passed in data
     */
    dataFactory.getNewEntityObject = function (poid, parentInstanceId, signature, type, typevalue, version, appSession) {
      var deferred = $q.defer();
      newEntityObjectFactory.getEntity(signature, version, appSession)
        .then(function (entityObject) {
          if (poid !== undefined) {
            entityObject.rootId = poid;
          }

          if (parentInstanceId !== undefined) {
            entityObject.associateId = parentInstanceId;
          }

          if (type === undefined) {
            entityObject.type = '';
          } else {
            entityObject.type = type;
          }

          if (typevalue === undefined) {
            entityObject.typeValue = '';
          } else {
            entityObject.typeValue = typevalue;
          }

          for (var key in entityObject.instance) {
            if (isNaN(entityObject.instance[key])) {
              if (entityObject.instance[key] === 'nil') {
                entityObject.instance[key] = '';
              }
            }
          }

          deferred.resolve(entityObject);
        }).catch(function (err) {
        deferred.reject(err);
      });

      return deferred.promise;
    };

    dataFactory.entityObjectSignatures = {};
    dataFactory.entityObjectIdList = [];

    /**
     * @ngdoc method
     * @name biz.data.dataFactory#clearEntityObjectCache
     * @methodOf biz.data.dataFactory
     * @description
     * This method clears the objects that cache entity objects.
     *
     */
    dataFactory.clearEntityObjectCache = function () {
      dataFactory.entityObjectSignatures = {};
      dataFactory.entityObjectIdList = [];
    };

    /**
     * @ngdoc method
     * @name biz.data.dataFactory#getNewEntityObjectCached
     * @methodOf biz.data.dataFactory
     *
     * @description
     * This method returns process objects from stored cache or calls the databse when not yet stored.
     * It will also holds promises waiting on a previous request for the same process object and resolves
     * them when the data is received.
     *
     * @param {string} poid the root id of the new entity object
     * @param {string} parentInstanceId the accosiate id of the new entity object
     * @param {string} signature signature of the new entity object
     * @param {string} type type of the new entity object
     * @param {string} typevalue type value of the new entity object
     * @param {string} version version of the new entity object to retrieve
     * @param {string} appSession true to use the system app session
     *
     * @returns {Object} new entity object
     */
    dataFactory.getNewEntityObjectCached = function (poid, parentInstanceId, signature, type, typevalue, version, appSession) {

      // console.log('getNewEntityObjectCached:', signature, 'ids left:', factoryMembers.entityObjectIdList.length);

      var deferred = $q.defer();

      //See if an entity object with the signature has already been saved
      if (dataFactory.entityObjectSignatures.hasOwnProperty(signature)) {
        // console.log('get stored object');

        //entity object to be returned
        var entityObject = {};

        //get a copy of the saved entity object
        entityObject = angular.copy(dataFactory.entityObjectSignatures[signature]);

        //populate any properties received as parameters
        if (parentInstanceId !== undefined && parentInstanceId !== '') {
          entityObject.associateId = parentInstanceId;
        }
        if (type !== undefined && type !== '') {
          entityObject.type = type;
        }
        if (typevalue !== undefined && typevalue !== '') {
          entityObject.typevalue = typevalue;
        }

        //are there any instance id in the list
        if (dataFactory.entityObjectIdList.length > 0) {

          //pop a new instance id from the array
          entityObject.instanceId = dataFactory.entityObjectIdList.pop();

          //return the new entity object
          deferred.resolve(entityObject);

        } else {
          // console.log('get ids');
          //get a list of instance ids
          noShardRequesterFactory.GET(cooperativeResources.resourcesURL, ['cooperative', 'generate', 'uuidlist'], {}, version, appSession)
            .then(function (data) {
              // console.log('save ids');
              //store the ids in the list
              dataFactory.entityObjectIdList = data.io[0].instance.setRes;

              //pop a new instance id from the array
              entityObject.instanceId = dataFactory.entityObjectIdList.pop();

              //return the new entity object
              deferred.resolve(entityObject);

            }).catch(function (data) {
            deferred.reject(data);
          });

        } //if dataFactory.entityObjectIdList.length > 0

      } else {
        // console.log('get new object');
        //get a new entity object for the given signature
        dataFactory.getNewEntityObject(poid, parentInstanceId, signature, type, typevalue, version, appSession)
          .then(function (entity) {
            // console.log('save new object');
            //store the object by signature in the object
            dataFactory.entityObjectSignatures[signature] = angular.copy(entity);

            //return the new entity object
            deferred.resolve(entity);

          }).catch(function (data) {
          deferred.reject(data);
        });

      } // if factoryMembers.entityObjectSignatures.hasOwnProperty(signature)

      return deferred.promise;

    };

    return dataFactory;
  }
})(cooperativeBizData);

/* jshint -W018 */
var cooperativeBizData; //Defined in 0module.js

(function (cooperativeBizData) {
  'use strict';

  /**
   * @ngdoc service
   * @name biz.data.newEntityObjectFactory
   *
   * @author Andrei Sandu
   * @created 08/05/17
   *
   * @description
   * Service that provides new EntityObject from api or cache.
   *
   * #Properties
   *   - **uuidList**: List of uuid based on version (e.g.: `{v_001: ['uuidGenerated1','uuidGenerated2']}`)
   *   - **entitiesCached**: All new entities cached from api request
   *
   * @requires $q
   * @requires dal.core.resourceApiFactory
   * @requires biz.utils.queueFactory
   *
   * @returns {Object} An object instance of type NewEntityObject
   *
   *
   */
  cooperativeBizData.factory('newEntityObjectFactory', newEntityObjectFactory);
  newEntityObjectFactory.$inject = ['$q', 'queueFactory', 'resourceApiFactory'];

  function newEntityObjectFactory($q, queueFactory, resourceApiFactory) {

    /**
     * @class
     * @description
     *
     * Initializer
     *
     * @returns {Object} Newly created NewEntityObject.
     */
    function NewEntityObject() {
      this.uuidList = {};
      this.entitiesCached = {};
      this.queue = queueFactory();
    }

    NewEntityObject.prototype._getNewObjectEntity = function (signature, version, appSession) {
      var self = this;
      var deferred = $q.defer();
      version = version || 'v_001';

      if (self.entitiesCached.hasOwnProperty(signature)) {
        var entity = angular.copy(self.entitiesCached[signature]);

        self._getPairUuIdList(version, appSession)
          .then(function (uuIdsList) {
            entity.timestamp = new Date().getTime();
            entity.instanceId = uuIdsList.pop();
            entity.rootId = uuIdsList.pop();
            entity.associateId = entity.rootId;
            deferred.resolve(entity);
          }).catch(function (err) {
          deferred.reject('error getting UUID');
        });
      } else {
        resourceApiFactory.getModel(signature, version, appSession)
          .then(function (processObject) {
            processObject = processObject.io.pop();

            if (!processObject) {
              deferred.reject('io length is 0');
            } else if (processObject.signature !== 'ResourceObject_001') {
              deferred.reject('incorrect signature');
            } else {
              self.entitiesCached[signature] = processObject.instance.arrayRes.pop();
              deferred.resolve(self.entitiesCached[signature]);
            }
          }).catch(function () {
          deferred.reject('Unable to retrieve resource object');
        });
      }

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name biz.data.newEntityObjectFactory#getEntity
     * @methodOf biz.data.newEntityObjectFactory
     * @description
     *
     * Get a new Entity based on signature.
     *
     *
     * @param {string} signature Signature of entityObject (e.g.: PhoneObject_001, EmailObject_001)
     * @param {string} version Version of api
     * @param {boolean} appSession use apiKey or sessionId
     *
     * @returns {Promise} Return a promise.
     */

    NewEntityObject.prototype.getEntity = function (signature, version, appSession) {
      var self = this;
      var deferred = $q.defer();
      var args = [].slice.call(arguments);

      self.queue.addInQueue(function (args) {
        return this._getNewObjectEntity.apply(this, args).then(function (data) {
          deferred.resolve(data);
        }).catch(function (err) {
          deferred.reject(err);
        });
      }, self, [args]);

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name biz.data.newEntityObjectFactory#_updateUuIdList
     * @methodOf biz.data.newEntityObjectFactory
     * @description
     * @private
     *
     * Get a uuidList from api.
     *
     *
     * @param {string} version Version of api
     * @param {boolean} appSession use apiKey or sessionId
     *
     * @returns {Promise} Return a promise.
     */

    NewEntityObject.prototype._updateUuIdList = function (version, appSession) {
      var self = this;
      var deferred = $q.defer();

      resourceApiFactory.getCooperative('uuidlist', version, appSession).then(function (data) {
        data = data.io.pop();
        if (data && data.instance && data.instance.setRes.length > 1) {
          self.uuidList[version] = data.instance.setRes;
          deferred.resolve();
        } else {
          deferred.reject('uuidlist sent no data');
        }
      }).catch(function (err) {
        deferred.reject(err.returnStatus.reason);
      });

      return deferred.promise;
    };

    /**
     * @ngdoc function
     * @name biz.data.newEntityObjectFactory#_getPairUuIdList
     * @methodOf biz.data.newEntityObjectFactory
     * @description
     *
     * Get 2 uuid from uuidList
     *
     * @private
     *
     * @param {string} version Version of api
     * @param {boolean} appSession use apiKey or sessionId
     *
     * @returns {Promise} Return a promise.
     */
    NewEntityObject.prototype._getPairUuIdList = function (version, appSession) {
      var self = this;
      var deferred = $q.defer();

      if (self.uuidList.hasOwnProperty(version) && self.uuidList[version].length > 1) {
        deferred.resolve(self.uuidList[version].splice(0, 2));
      } else {
        self._updateUuIdList(version, appSession)
          .then(function () {
            deferred.resolve(self.uuidList[version].splice(0, 2));
          })
          .catch(function (err) {
            deferred.reject(err);
          });
      }

      return deferred.promise;
    };

    return new NewEntityObject();
  }
})(cooperativeBizData);

/* jshint -W018 */
var cooperativeBizData; //Defined in 0module.js

(function (cooperativeBizData) {
    'use strict';
    /**
     * @ngdoc service
     * @name biz.data.processDataFactory
     *
     * @description
     * Author Andrei Sandu
     *
     * Date 07/24/17
     *
     * Business service for processing list of ioObjects
     */
    cooperativeBizData.factory('processDataFactory', ProcessDataFactory);
    ProcessDataFactory.$inject = ['$q', '$timeout'];

    function ProcessDataFactory($q, $timeout) {

        var dataFactory = {};

        dataFactory.process = process;

        return dataFactory;

        /**
         * @ngdoc method
         * @name biz.data.processDataFactory#process
         * @methodOf biz.data.processDataFactory
         *
         *
         * @description
         * Returns a filtered object of instance objects
         * based on childrenListObjects and/or parentObject.
         *
         * @param {Array.<Object>} collection List of objects `(ioObject)`
         * @param {Array.<Object>} children List of child Object.
         *
         * ***childObject*** attributes:
         *  - **signature** – `{string}` – Signature to check
         *  - **key** – `{string}` – Key were is stored the filtered list for this child
         *  - **index** – `{number=}` – Return the result from index in the filtered list for this child
         *  - ***** - {*=} - Any property from instance object `(ioObject)`
         *
         *
         * @param {Object=} parent Get all children filtered by parentObject.
         *
         * ***parentObject*** attributes:
         *  - **signature** – `{string}` – Signature to check
         *  - ***** - {*=} - Any property from instance object `(ioObject)`
         *
         *
         *
         * @returns {Promise} return a promise
         *
         * The resolve returns {Object} with keys of childObject.key value = array of instance objects
         * ***Note:***
         *
         * If passed a parentObject and is founded in list of instance objects
         * then on the return data it is assigned `parent` attribute
         * *`(eg.: data.parent = <ioObject> based on parentObject)`*
         *
         */
        function process(collection, children, parent) {
            var _arguments = [];

            for (var i = 0; i < arguments.length; i++) {
                _arguments.push(_evalValue(arguments[i]));
            }

            return _getProcessedDataFrom.apply(null, _arguments);
        }

        /**
         *
         * Returns a filtered object of instance objects
         * based on childrenListObjects and/or parentObject.
         *
         *
         * @private
         * @param {Array.<Object>} collection List of objects `(ioObject)`
         * @param {Array.<Object>} childrenListObjects List of child Object.
         *
         * ***childObject*** attributes:
         *  - **signature** – `{string}` – Signature to check
         *  - **key** – `{string}` – Key were is stored the filtered list for this child
         *  - **index** – `{number=}` – Return the result from index in the filtered list for this child
         *  - ***** - {*=} - Any property from instance object `(ioObject)`
         *
         *
         * @param {Object=} parentObject Get all children filtered by parentObject.
         *
         * ***parentObject*** attributes:
         *  - **signature** – `{string}` – Signature to check
         *  - ***** - {*=} - Any property from instance object `(ioObject)`
         *
         *
         *
         * @returns {Promise} Resolution Object with keys of childObject.key value = array of instance objects
         *
         * ***Note:***
         *
         * If passed a parentObject and is founded in list of instance objects
         * then on the return data it is assigned `parent` attribute
         * *`(eg.: data.parent = <ioObject> based on parentObject)`*
         *
         */
        function _getProcessedDataFrom(collection, childrenListObjects, parentObject) {
            var deferred = $q.defer();
            var output = {};

            childrenListObjects = childrenListObjects || [];
            parentObject = parentObject || {};

            $timeout(function () {
                if (!Array.isArray(collection) || !Array.isArray(childrenListObjects) ||
                    collection.length === 0 || childrenListObjects.length === 0) {
                    return deferred.resolve(output);
                }

                if (_hasInvalidObject(childrenListObjects, ['signature', 'key'])) {
                    return deferred.reject('Child objects should have "signature" and "key" property');
                }

                if (_isObject(parentObject) && !_isEmptyObject(parentObject)) {
                    var collectionFilteredByParent = _getFilteredCollectionByObj(collection, parentObject);

                    if (!!collectionFilteredByParent &&
                        collectionFilteredByParent.length > 0 &&
                        !!collectionFilteredByParent[0].instanceId) {
                        output.hasParent = true;
                        output.parent = collectionFilteredByParent[0];
                        collection = _getFilteredCollectionByObj(collection, {associateId: collectionFilteredByParent[0].instanceId});
                    }
                }

                childrenListObjects.forEach(function (childObj) {
                    output[childObj.key] = _getFilteredCollectionByObj(collection, childObj, ['key']);

                    if (childObj.index !== undefined && childObj.index !== -1 && childObj.index < output[childObj.key].length) {
                        output[childObj.key] = [output[childObj.key][childObj.index]];
                    }
                });

                return deferred.resolve(output);
            }, 0);

            return deferred.promise;
        }

        /**
         * Determines if a reference is an `Object`. Unlike `typeof` in JavaScript, `null`s are not
         * considered to be objects. Note that JavaScript arrays are objects.
         *
         * @private
         * @param {*} value Reference to check.
         * @returns {boolean} True if `value` is an `Object` but not `null`.
         */
        function _isObject(value) {
            return !!value && typeof value === 'object';
        }

        /**
         * Determine if a value is an object with no properties
         *
         * @private
         * @param {*} value Reference to check.
         * @returns {boolean} True if `value` is an `Object` with no properties
         */
        function _isEmptyObject(value) {
            return !!value && typeof value === 'object' && Object.keys(value).length === 0;
        }

        /**
         * This method validate the objects from  the list based of list of properties.
         *
         * @private
         * @param {Array} objectList List of Objects
         * @param {Array<string>} validatePropsList List of properties
         * @returns {boolean} True if one of object from list has one of property from validatePropsList is undefined
         */
        function _hasInvalidObject(objectList, validatePropsList) {
            return objectList.some(function (obj) {
                return validatePropsList.some(function (prop) {
                    return !!obj[prop] === false;
                });
            });
        }

        /**
         * This method evaluate the string value.
         *
         * @private
         * @param {string} value Reference to evaluate
         * @returns {Object|Array|string|Number} Evaluated string
         */
        function _evalValue(value) {
            if (typeof value === 'string' && value !== '') {
                try {
                    return eval('(' + value + ')');
                } catch (err) {
                    return undefined;
                }
            }

            return value;
        }

        /**
         * This method filter a list of objects by an object and ignoredList of properties.
         *
         * @private
         * @param {Array.<Object>} collection The list of objects
         * @param {Object} filterObject The object which filter the list of objects
         * @param {Array.<string>=} ignoreProps The list of props which are ignored by filter
         *
         * @returns {Array} Return filtered list
         */
        function _getFilteredCollectionByObj(collection, filterObject, ignoreProps) {
            var temp;

            temp = Object.assign({}, filterObject);

            //make sure index attribute is remove from filter
            delete temp.index;

            if (Array.isArray(ignoreProps)) {
                ignoreProps.forEach(function (prop) {
                    delete temp[prop];
                });
            }

            Object.keys(temp).forEach(function (t) {
                if (!(!!temp[t])) {
                    delete temp[t];
                }
            });

            return collection.filter(function (item) {
                return Object.keys(temp).every(function (t) {
                    return item[t] === temp[t];
                });
            });
        }
    }
})(cooperativeBizData);

var cooperativeBizData; //Defined in 0module.js

/**
 * @ngdoc service
 * @name processObjectFactory
 *
 * @description
 * Author hiten
 *
 * Date 04/30/15
 *
 * Business service for
 */
cooperativeBizData.factory('processObjectFactory', ['$q', 'dataApiFactory', 'queriesApiFactory', 'resourceApiFactory',function ($q, dataApiFactory, queriesApiFactory, resourceApiFactory) {
    var processObjectFactory = {};

    /**
     * @ngdoc method
     * @name processObjectFactory#get
     * @description
     * Gets an existing process object by its id.
     *
     * @param {string} processObjectId id of process object to get
     * @returns process object
     */
    processObjectFactory.get = function(shard, partition, mode, processObjectId, appSession){
        var deferred = $q.defer();

        dataApiFactory.getProcessObject(shard, partition, mode, processObjectId, appSession)
            .then(function(po){
                deferred.resolve(po);
            })
            .catch(function(error){
                deferred.reject('Unable to retrieve ProcessObject');
            });

        return deferred.promise;
    };

    /**
     * @ngdoc method
     * @name processObjectFactory#new
     * @description
     * Gets an new process object.
     *
     * @returns process object
     */
    processObjectFactory.new = function(version, appSession){
        var deferred = $q.defer();
        resourceApiFactory.getModel('processobject', version, appSession)
            .then(function (data) {
                deferred.resolve(data);
            }).catch(function(data){
                deferred.reject('Unable to retrieve process object');
            });

        return deferred.promise;
    };

    /**
     * @ngdoc method
     * @name processObjectFactory#save
     * @description
     * Not currently used.
     *
     * @param {ProcessObject} process object to save
     * @returns promise
     */
    processObjectFactory.save = function (shard, partition, mode, processObject, appSession) {
        var deferred = $q.defer();

        dataApiFactory.updateProcessObject(shard, partition, mode, processObject, appSession)
            .then(function(data){
                deferred.resolve(data);
            })
            .catch(function(error){
                deferred.reject('error posting data');
            });

        return deferred.promise;
    };

    return processObjectFactory;
}]);

var cooperativeBizGeo = angular.module('cooperative.biz.geo', []);
/**
 * Created by hiten on 01/11/2015.
 */
var cooperativeBizGeo;

cooperativeBizGeo.factory('ipFactory', ['$q', 'geoipApiFactory', function ($q, geoipApiFactory) {
    var ipFactory = {};

    ipFactory.getGeoInfoByIp = function(ip, version, appSession){
        return geoipApiFactory.getByIp(ip, version, appSession);
    };

    ipFactory.getClientGeoInfo = function(version, appSession){
        return geoipApiFactory.getByIp('browser', version, appSession);
    };

    ipFactory.getClientIp = function(version, appSession){
        var deferred = $q.defer();

        geoipApiFactory.getByIp('browser', version, appSession).then(
            function(data){
                var ip = '127.0.0.1';

                for (var key in data){
                    ip = data[key].ip_start;
                    break;
                }

                deferred.resolve(ip);
            }
        ).catch(function(){
                deferred.resolve('127.0.0.1');
            });

        return deferred.promise;
    };

    ipFactory.getClientCountryCode = function(version, appSession){
        var deferred = $q.defer();

        geoipApiFactory.getByIp('browser', version, appSession).then(
            function(data){
                var ip = '127.0.0.1';

                for (var key in data){
                    ip = data[key].country;
                    break;
                }

                deferred.resolve(ip);
            }
        ).catch(function(){
                deferred.resolve('US');
            });

        return deferred.promise;
    };

    return ipFactory;
}]);

var cooperativeBizOutreach = angular.module('cooperative.biz.outreach', []);
/**
 * Created by hiten on 03/02/2017.
 */
var cooperativeBizOutreach;

cooperativeBizOutreach.factory('outreachFactory', ['$q', 'outreachApiFactory', function ($q, outreachApiFactory) {
    var outreachFactory = {};

    outreachFactory.send = function(email, template, processObjectId, version, appSession){
        return outreachApiFactory.send(email, template, processObjectId, version, appSession);
    };

    outreachFactory.sendToList = function(emails, template, processObjectId, version, appSession){
        return outreachApiFactory.sendToList(emails, template, processObjectId, version, appSession);
    };

    return outreachFactory;
}]);
var cooperativeBizQueries = angular.module('cooperative.biz.queries', []);
var cooperativeBizQueries; //Defined in 0module.js

/**
 * @ngdoc service
 * @name searchFactory
 *
 * @description
 * Author hiten
 *
 * Date 04/05/15
 *
 * Business service for ?????????????.
 */
cooperativeBizQueries.factory('searchFactory', ['$q', 'queriesApiFactory', 'resourceApiFactory' ,function ($q, queriesApiFactory, resourceApiFactory) {
    var searchFactory = {};

    searchFactory.search = function(shard, partition, mode, queryString ,signatureList, from, size, returnMode, cacheId, rankThreshold, version, appSession, queryBypassSimilarity, queryBypassPrivacy){
        var deferred = $q.defer();

        resourceApiFactory.getModel('ProcessObject_001', version, appSession)
            .then(function(processObject){
                // console.log(JSON.stringify(processObject));
                if (processObject.io.length > 0) {
                    var resourceObject = processObject.io[0];
                    if (resourceObject.signature == 'ResourceObject_001'){
                        var po = resourceObject.instance.arrayRes[0];



                        if (queryString === null || queryString === undefined || queryString === ''){
                            po.directives['query_string'] = 'nil';
                        } else {
                            po.directives['query_string'] = queryString;
                        }


                        if (returnMode === null || returnMode === undefined){
                            po.directives['return_mode'] = true.toString();
                        } else {
                            po.directives['return_mode'] = returnMode.toString();
                        }

                        if (from === null || from === undefined || isNaN(from)){
                            po.directives['from'] = '0';
                        } else {
                            po.directives['from'] = from.toString();
                        }

                        if (size === null || size === undefined || isNaN(size)){
                            po.directives['size'] = '1000000';
                        } else {
                            po.directives['size'] = size.toString();
                        }

                        if (cacheId !== null && cacheId !== undefined){
                            po.directives['cache_id'] = cacheId.toString();
                        }

                        if (rankThreshold !== null && rankThreshold !== undefined){
                            po.directives['rank_threshold'] = rankThreshold;
                        }

                        var strSignatureList = '';

                        for (var i=0; i < signatureList.length; i++){
                            if (strSignatureList !== ''){
                                strSignatureList = strSignatureList + ',';
                            }

                            strSignatureList = strSignatureList + signatureList[i];
                        }

                        po.directives['signatures'] = strSignatureList;

                        if(queryBypassSimilarity!==null && queryBypassSimilarity !== undefined && queryBypassSimilarity !== ''){
                          po.directives['query_bypass_similarity']=queryBypassSimilarity.toString();
                        }

                        if(queryBypassPrivacy!==null && queryBypassPrivacy !== undefined && queryBypassPrivacy !== ''){
                          po.directives['query_bypass_privacy']=queryBypassPrivacy.toString();
                        }

                        // console.log(JSON.stringify(po));
                        queriesApiFactory.query(shard, partition, mode, po, appSession)
                            .then(function(results){
                                deferred.resolve(results);
                            })
                            .catch(function(){
                                deferred.reject('Query post error');
                            });
                    } else {
                        deferred.reject('incorrect signature');
                    }
                } else {
                    deferred.reject('io length is 0');
                }
            })
            .catch(function(){
                deferred.reject('Unable to retrieve resource object');
            });

        return deferred.promise;
    };

    // Todo - Fixed shard parameter
    searchFactory.searchDuplicates = function(partition, mode, orgLegalName, orgAddress, orgCity, orgStateRegion, orgPostalCode, orgCountryCode, version, appSession, queryBypassSimilarity, queryBypassPrivacy){
        var deferred = $q.defer();

        resourceApiFactory.getModel('ProcessObject_001', version, appSession)
            .then(function(processObject) {
                if (processObject.io.length > 0) {
                    var resourceObject = processObject.io[0];
                    if (resourceObject.signature == 'ResourceObject_001') {
                        var po = resourceObject.instance.arrayRes[0];

                        po.directives['dupl_org_name'] = orgLegalName;
                        po.directives['dupl_org_address'] = orgAddress;
                        po.directives['dupl_org_city'] = orgCity;
                        po.directives['dupl_org_state_region'] = orgStateRegion;
                        po.directives['dupl_org_postal_code'] = orgPostalCode;
                        po.directives['dupl_country_code'] = orgCountryCode;
                        po.directives['return_mode'] = 'true';
                        if(queryBypassSimilarity!==null && queryBypassSimilarity !== undefined && queryBypassSimilarity !== ''){
                          po.directives['query_bypass_similarity']=queryBypassSimilarity.toString();
                        }

                        if(queryBypassPrivacy!==null && queryBypassPrivacy !== undefined && queryBypassPrivacy !== ''){
                          po.directives['query_bypass_privacy']=queryBypassPrivacy.toString();
                        }
                        queriesApiFactory.dupCheck('object', partition, mode, po, appSession)
                            .then(function(data){
                                deferred.resolve(data);
                            }).catch(function(error){
                                deferred.reject('error obtaining duplicates');
                        });
                    } else {
                        deferred.reject('resourceObject malformed');
                    }
                }
            }).catch(function(error){
                deferred.reject('Unable to retrieve resource object');
            });

        return deferred.promise;
    };

    searchFactory.trigram = function(partition, mode, orgLegalName, orgAddress, orgWebsite, orgPhone, orgEmail, orgLegalId, return_mode, size, from, signatures, threshold, version, appSession, queryBypassSimilarity, queryBypassPrivacy){

        var deferred = $q.defer();

        resourceApiFactory.getModel('ProcessObject_001', version, appSession)
            .then(function(processObject) {
                if (processObject.io.length > 0) {
                    var resourceObject = processObject.io[0];
                    if (resourceObject.signature == 'ResourceObject_001') {
                        var po = resourceObject.instance.arrayRes[0];

                        if(orgLegalName !== null && orgLegalName !== undefined && orgLegalName !== ''){
                            po.directives['org_name'] = orgLegalName;
                        }

                        if(orgAddress !== null && orgAddress !== undefined && orgAddress !== ''){
                            po.directives['org_address'] = orgAddress;
                        }

                        if(orgWebsite !== null && orgWebsite !== undefined && orgWebsite !== ''){
                            po.directives['org_website'] = orgWebsite;
                        }

                        if(orgPhone !== null && orgPhone !== undefined && orgPhone !== ''){
                           po.directives['org_phone'] = orgPhone;
                        }

                        if(orgEmail !== null && orgEmail !== undefined && orgEmail !== ''){
                            po.directives['org_email'] = orgEmail;
                        }

                        if(orgLegalId !== null && orgLegalId !== undefined && orgLegalId !== ''){
                            po.directives['org_lid'] = orgLegalId;
                        }

                        if(return_mode !== null && return_mode !== undefined && return_mode !== ''){
                            po.directives['return_mode'] = return_mode.toString();
                        }

                        if(size !== null && size !== undefined && size !== ''){
                            po.directives['size'] = size.toString();
                        }

                        if(from !== null && from !== undefined && from !== ''){
                            po.directives['from'] = from.toString();
                        }

                        if(signatures !== null && signatures !== undefined && signatures !== ''){
                            po.directives['signatures'] = signatures;
                        }

                        if(threshold !== null && threshold !== undefined && threshold !== ''){
                            po.directives['threshold'] = threshold.toString();
                        }

                        if(queryBypassSimilarity!==null && queryBypassSimilarity !== undefined && queryBypassSimilarity !== ''){
                          po.directives['query_bypass_similarity']=queryBypassSimilarity.toString();
                        }

                        if(queryBypassPrivacy!==null && queryBypassPrivacy !== undefined && queryBypassPrivacy !== ''){
                          po.directives['query_bypass_privacy']=queryBypassPrivacy.toString();
                        }

                        queriesApiFactory.trigramQuery('object', partition, mode, po, appSession)
                            .then(function(data){
                                deferred.resolve(data);
                            }).catch(function(error){
                                deferred.reject('error performing trigram seach');
                        });
                    } else {
                        deferred.reject('resourceObject malformed');
                    }
                }
            }).catch(function(error){
                deferred.reject('Unable to retrieve resource object');
            });

        return deferred.promise;
    };


    //trilike post
    searchFactory.trilikeSearch = function(partition, mode, query_trilike, size, return_mode, version, appSession, queryBypassSimilarity, queryBypassPrivacy){
        var deferred = $q.defer();

        resourceApiFactory.getModel('ProcessObject_001', version, appSession)
            .then(function(processObject) {
                if (processObject.io.length > 0) {
                    var resourceObject = processObject.io[0];
                    if (resourceObject.signature == 'ResourceObject_001') {
                        var po = resourceObject.instance.arrayRes[0];

                        if(query_trilike !== null && query_trilike !== undefined && query_trilike !== ''){
                            po.directives['query_trilike'] = query_trilike;
                        }

                        if(size !== null && size !== undefined && size !== ''){
                          po.directives['size'] = size.toString();
                        }

                        if(return_mode !== null && return_mode !== undefined && return_mode !== ''){
                            po.directives['return_mode'] = return_mode.toString();
                        }

                        if(queryBypassSimilarity!==null && queryBypassSimilarity !== undefined && queryBypassSimilarity !== ''){
                          po.directives['query_bypass_similarity']=queryBypassSimilarity.toString();
                        }

                        if(queryBypassPrivacy!==null && queryBypassPrivacy !== undefined && queryBypassPrivacy !== ''){
                          po.directives['query_bypass_privacy']=queryBypassPrivacy.toString();
                        }

                        queriesApiFactory.trilikeQuery('object', partition, mode, po, appSession)
                            .then(function(data){
                                deferred.resolve(data);
                            }).catch(function(error){
                                deferred.reject('error performing trigram seach');
                        });
                    } else {
                        deferred.reject('resourceObject malformed');
                    }
                }
            }).catch(function(error){
                deferred.reject('Unable to retrieve resource object');
            });

        return deferred.promise;
    };

    return searchFactory;
}]);

var cooperativeBizQueries; //Defined in 0module.js

/**
 * @ngdoc service
 * @name searchFactory
 *
 * @description
 * Author hiten
 *
 * Date 07/15/15
 *
 * Business service for ?????????????.
 */

cooperativeBizQueries.factory('searchPagingFactory', ['searchFactory', '_', '$q', function(searchFactory, _, $q) {
    var searchPagingFactory = function(shard, partition, mode, trilike, queryString, signatureList, size, returnMode, cacheId, rankThreshold, orgLegalName, orgAddress, orgWebsite, orgPhone, orgEmail, orgLegalId, appSession, queryBypassSimilarity, queryBypassPrivacy) {
        this.items = [];
        this.busy = false;
        this.from = 0;
        this.size = 200;
        this.trilike = trilike;
        this.queryString = queryString;
        this.signatureList = signatureList;
        this.returnMode = returnMode;
        this.cacheId = cacheId;
        this.rankThreshold = rankThreshold;
        this.pageSize = size;
        this.complete = true;
        this.appSession = appSession;
        this.shard = shard;
        this.partition = partition;
        this.mode = mode;

        this.orgLegalName = orgLegalName;
        this.orgAddress = orgAddress;
        this.orgWebsite = orgWebsite;
        this.orgPhone = orgPhone;
        this.orgEmail = orgEmail;
        this.orgLegalId = orgLegalId;

        //if specificied, use that else true
        if(queryBypassSimilarity!==null && queryBypassSimilarity !== undefined && queryBypassSimilarity !== ''){
          this.queryBypassSimilarity=queryBypassSimilarity.toString();
        }else{
          this.queryBypassSimilarity = 'true';
        }
        //if specificied use that else use false
        if(queryBypassPrivacy!==null && queryBypassPrivacy !== undefined && queryBypassPrivacy !== ''){
          this.queryBypassPrivacy=queryBypassPrivacy.toString();
        }else{
          this.queryBypassSimilarity = 'false';
        }

    };




    searchPagingFactory.prototype.nextPage = function() {
        console.log('____nextpage___11');
        if (this.busy) {
            return;
        }

        this.busy = true;
        this.complete = false;

        var promises = [];

        if(this.queryString !== undefined && this.queryString !== null && this.queryString !== ''){
            promises.push(searchFactory.search(this.shard, this.partition, this.mode, this.queryString , this.signatureList, this.from, this.pageSize, this.returnMode, this.cacheId, this.rankThreshold, undefined, this.appSession, this.queryBypassSimilarity, this.queryBypassPrivacy));
        }

        if(this.trilike !== undefined && this.trilike !== null && this.trilike !== ''){
            promises.push(searchFactory.trilikeSearch(this.partition, this.mode, this.trilike, this.pageSize, this.returnMode, undefined, this.appSession,this.queryBypassSimilarity, this.queryBypassPrivacy));
        }

        if( (this.orgLegalName !== undefined && this.orgLegalName !== null && this.orgLegalName !== '') ||
            (this.orgAddress !== undefined && this.orgAddress !== null && this.orgAddress !== '') ||
            (this.orgWebsite !== undefined && this.orgWebsite !== null && this.orgWebsite !== '') ||
            (this.orgPhone !== undefined && this.orgPhone !== null && this.orgPhone !== '') ||
            (this.orgEmail !== undefined && this.orgEmail !== null && this.orgEmail !== '') ||
            (this.orgLegalId !== undefined && this.orgLegalId !== null && this.orgLegalId !== ''))
        {        
          promises.push(searchFactory.trigram(this.partition, this.mode, this.orgLegalName, this.orgAddress, this.orgWebsite, this.orgPhone, this.orgEmail, this.orgLegalId, this.returnMode, this.pageSize, this.from, this.signatureList, this.rankThreshold, undefined, this.appSession,this.queryBypassSimilarity, this.queryBypassPrivacy));
        }

       $q.all(promises).then(function (data) {
            for(var j = 0; j < data.length; j++){
                var complete = data[j];
                if (this.cacheId === null || this.cacheId === undefined){
                    if (complete.results.length > 0){
                        this.cacheId = complete.results[0].crc;
                        this.trilike = null;
                        this.queryString = null;
                        this.size = this.pageSize;
                    }
                }

                for (var i=0; i < complete.results.length; i++){
                    if (_.findIndex(this.items, {id: complete.results[i].id}) == -1){
                        this.items.push(complete.results[i]);
                    }
                }
                this.from = this.items.length - 1;

                if (complete.results.length >= this.size){
                    this.busy = false;
                } else {
                    this.complete = true;
                }
            }
        }.bind(this));
    };

    return searchPagingFactory;
}]);

var cooperativeBizQueries;

/**
 * @ngdoc service
 * @name searchRequesterFactory
 *
 * @requires $q
 * @requires configOne
 * @requires shardRequesterFactory
 * @requires transcodeFactory
 * @requires resourceApiFactory
 *
 * @description
 * Methods for quering
 *
 *
 * @author Hiten Vaghela (hiten.vaghela@gmail.com), Lee Grisham (lgrisham@techsoupglobal.org)
 *
 * @created 5/31/17
 *
 *
 */

 function sanitizePO(po){
   var sanitizePO = {};
   angular.forEach(po, function(val,key){

     if(val !== null && val !== undefined && val !== ''){
       sanitizePO[key.toString()]=val.toString();
     }

   });
   return sanitizePO;
 }

 function standardizeOrgInputs(queryTerms){
   var standardizeOrgInputs = {};

   if(queryTerms.org_name !== null && queryTerms.org_name !== undefined && queryTerms.org_name !== ''){
     standardizeOrgInputs['dupl_org_name']=queryTerms.org_name;
   }
   if(queryTerms.dupl_org_name !== null && queryTerms.dupl_org_name !== undefined && queryTerms.dupl_org_name !== ''){
     standardizeOrgInputs['dupl_org_name']=queryTerms.dupl_org_name;
   }
   if(queryTerms.org_address !== null && queryTerms.org_address !== undefined && queryTerms.org_address !== ''){
     standardizeOrgInputs['dupl_org_address']=queryTerms.org_address;
   }
   if(queryTerms.dupl_org_address !== null && queryTerms.dupl_org_address !== undefined && queryTerms.dupl_org_address !== ''){
     standardizeOrgInputs['dupl_org_address']=queryTerms.dupl_org_address;
   }
   if(queryTerms.org_city !== null && queryTerms.org_city !== undefined && queryTerms.org_city !== ''){
     standardizeOrgInputs['dupl_org_city']=queryTerms.org_city;
   }
   if(queryTerms.dupl_org_city !== null && queryTerms.dupl_org_city !== undefined && queryTerms.dupl_org_city !== ''){
     standardizeOrgInputs['dupl_org_city']=queryTerms.dupl_org_city;
   }
   if(queryTerms.org_state_region !== null && queryTerms.org_state_region !== undefined && queryTerms.org_state_region !== ''){
     standardizeOrgInputs['dupl_org_state_region']=queryTerms.org_state_region;
   }
   if(queryTerms.dupl_org_state_region !== null && queryTerms.dupl_org_state_region !== undefined && queryTerms.dupl_org_state_region !== ''){
     standardizeOrgInputs['dupl_org_state_region']=queryTerms.dupl_org_state_region;
   }
   if(queryTerms.org_postal_code !== null && queryTerms.org_postal_code !== undefined && queryTerms.org_postal_code !== ''){
     standardizeOrgInputs['dupl_org_postal_code']=queryTerms.org_postal_code;
   }
   if(queryTerms.dupl_org_postal_code !== null && queryTerms.dupl_org_postal_code !== undefined && queryTerms.dupl_org_postal_code !== ''){
     standardizeOrgInputs['dupl_org_postal_code']=queryTerms.dupl_org_postal_code;
   }
   if(queryTerms.country_code !== null && queryTerms.country_code !== undefined && queryTerms.country_code !== ''){
     standardizeOrgInputs['dupl_country_code']=queryTerms.country_code;
   }
   if(queryTerms.dupl_country_code !== null && queryTerms.dupl_country_code !== undefined && queryTerms.dupl_country_code !== ''){
     standardizeOrgInputs['dupl_country_code']=queryTerms.dupl_country_code;
   }
   if(queryTerms.org_lid !== null && queryTerms.org_lid !== undefined && queryTerms.org_lid !== ''){
     standardizeOrgInputs['dupl_org_lid']=queryTerms.org_lid;
   }
   if(queryTerms.dupl_org_lid !== null && queryTerms.dupl_org_lid !== undefined && queryTerms.dupl_org_lid !== ''){
     standardizeOrgInputs['dupl_org_lid']=queryTerms.dupl_org_lid;
   }
   if(queryTerms.org_website !== null && queryTerms.org_website !== undefined && queryTerms.org_website !== ''){
     standardizeOrgInputs['dupl_org_website']=queryTerms.org_website;
   }
   if(queryTerms.dupl_org_website !== null && queryTerms.dupl_org_website !== undefined && queryTerms.dupl_org_website !== ''){
     standardizeOrgInputs['dupl_org_website']=queryTerms.dupl_org_website;
   }
   if(queryTerms.org_phone !== null && queryTerms.org_phone !== undefined && queryTerms.org_phone !== ''){
     standardizeOrgInputs['dupl_org_phone']=queryTerms.org_phone;
   }
   if(queryTerms.dupl_org_phone !== null && queryTerms.dupl_org_phone !== undefined && queryTerms.dupl_org_phone !== ''){
     standardizeOrgInputs['dupl_org_phone']=queryTerms.dupl_org_phone;
   }
   if(queryTerms.org_email !== null && queryTerms.org_email !== undefined && queryTerms.org_email !== ''){
     standardizeOrgInputs['dupl_org_email']=queryTerms.org_email;
   }
   if(queryTerms.dupl_org_email !== null && queryTerms.dupl_org_email !== undefined && queryTerms.dupl_org_email !== ''){
     standardizeOrgInputs['dupl_org_email']=queryTerms.dupl_org_email;
   }
   return standardizeOrgInputs;
 }


  cooperativeBizQueries.factory('searchRequesterFactory', ['$q','queriesApiFactory', 'transcodeFactory', 'resourceApiFactory',function($q,queriesApiFactory,transcodeFactory,resourceApiFactory) {
    var searchRequesterFactory = {};

    /**
   * @ngdoc method
   * @name advancedSearch
   * @methodOf searchRequesterFactory
   * @description
   * pulls a process object using resourceApiFactory, adds directives properties to the PO,
   * passes request to server,
   *
   * Returns a promise of a requested data matching data points passed
   *
   * Object queryTerms may contain:
   * country_code: organization name
   * org_address: organization address
   * org_city: organization city
   * org_state_region: organization region
   * org_postal_code: organization postal code
   * org_country_code: organization country code (2 character)
   * org_lid: organization government issued ID
   * org_website: organization URL
   * org_phone: organization phone number (private)
   * org_email: organization email address (private)
   *
   *
   *
   * @param {object} queryTerms object containing specified key:values to search
   * @param {string} queryBypassSimilarity number of items to retrieve
   * @param {string} threshold Accuracy measurement for search results.
   * @param {string} returnMode a flag to state whether return the actual data or just meta-data
   * @param {string} queryBypassSimilarity flag for bypassing similariy filters on server (recommend true, unless dupe checking)
   * @param {string} queryBypassPrivacy a flag for bypassing removal of private information from data return (recommend false, unless needing private information)
   *
   * @returns {Promise} Future object of results from query
   *
   *
  */



    //multiquery accepts object, string, string, string, string, string
    searchRequesterFactory.advancedSearch = function(queryTerms, shard, partition, size, threshold, returnMode, queryBypassSimilarity, queryBypassPrivacy, appSession){

      var deferred = $q.defer();

      //pull process object for request
      resourceApiFactory.getModel('ProcessObject_001', 'v_001', true)
          .then(function(processObject) {
              if (processObject.io.length > 0) {
                  var resourceObject = processObject.io[0];
                  if (resourceObject.signature == 'ResourceObject_001') {
                      var po = resourceObject.instance.arrayRes[0];

                      po.directives = standardizeOrgInputs(queryTerms);
                      po.directives['return_mode'] = returnMode;
                      po.directives['threshold'] = threshold;
                      po.directives['query_bypass_similarity'] = queryBypassSimilarity;
                      po.directives['query_bypass_privacy'] = queryBypassPrivacy;
                      po.directives['size'] = size;

                      po.directives = sanitizePO(po.directives);

                      queriesApiFactory.dupCheck(shard, partition,'asynch',po,appSession).then(function(results){
                        deferred.resolve(results);
                      },function(error){
                        deferred.reject(error);
                      });


                  } else {
                    //no resource object
                    deferred.reject('no resource object');
                  }
              } else {
                //no PO returned
                deferred.reject('no process object returned');
              }
          });

    return deferred.promise;

    };

    /**
   * @ngdoc method
   * @name advancedSearchEncryptedQT
   * @methodOf searchRequesterFactory
   * @description
   * pulls a process object using resourceApiFactory, adds directives properties to the PO,
   * passes request to server,
   *
   * Returns a promise of a requested data matching data points passed
   *
   * -- encrypted string will be stringify json object --
   * Object queryTerms may contain:
   * org_name: organization name
   * org_address: organization address
   * org_city: organization city
   * org_state_region: organization region
   * org_postal_code: organization postal code
   * org_country_code: organization country code (2 character)
   * org_lid: organization government issued ID
   * org_website: organization URL
   * org_phone: organization phone number (private)
   * org_email: organization email address (private)
   *
   *
   * The return object can be data matching query
   *
   * @param {string} encryptedQueryTerm stringified, encrypted, json object
   * @param {string} size number of items to retrieve
   * @param {string} threshold Accuracy measurement for search results.
   * @param {string} returnMode a flag to state whether return the actual data or just meta-data
   * @param {string} queryBypassSimilarity flag for bypassing similariy filters on server (recommend true, unless dupe checking)
   * @param {string} queryBypassPrivacy a flag for bypassing removal of private information from data return (recommend false, unless needing private information)
   *
   * @returns {Promise} Future object of results from query
   *
   *
  */

    //accepts encrypted querystring, string, string, string, string, string
    searchRequesterFactory.advancedSearchEncryptedQT = function(encryptedQueryTerm, shard, partition, size, threshold, returnMode, queryBypassSimilarity, queryBypassPrivacy,appSession){
      var deferred = $q.defer();

      //decrypt the string and call advancedSearch function
       transcodeFactory.decrypt(encryptedQueryTerm,null,true).then(function(decryptedQuery){
         searchRequesterFactory.advancedSearch(JSON.parse(decryptedQuery), shard, partition, size, threshold, returnMode, queryBypassSimilarity, queryBypassPrivacy,appSession).then(function(results){
           deferred.resolve(results);
         },function(error){
           deferred.reject(error);
         });
      },function(error){
        deferred.reject(error);
      });

      return deferred.promise;
    };

    /**
   * @ngdoc method
   * @name queryString
   * @methodOf searchRequesterFactory
   * @description
   * pulls a process object using resourceApiFactory, adds directives properties to the PO,
   * passes request to server,
   *
   * Returns a promise of requested data matching query string
   *
   *
   * @param {string} queryString JSONB query string
   * @param {string} size number of items to retrieve
   * @param {string} returnMode a flag to state whether return the actual data or just meta-data
   * @param {string} queryBypassSimilarity flag for bypassing similariy filters on server (recommend true, unless dupe checking)
   * @param {string} queryBypassPrivacy a flag for bypassing removal of private information from data return (recommend false, unless needing private information)
   *
   * @returns {Promise} Future object of results from query
   *
   *
  */

    //json b string, string, string, string, string, string
    searchRequesterFactory.queryString = function(queryString, shard, partition, size, returnMode, queryBypassSimilarity, queryBypassPrivacy,appSession){
      var deferred = $q.defer();

      //pull process object for request
      resourceApiFactory.getModel('ProcessObject_001', 'v_001', true)
          .then(function(processObject) {
              if (processObject.io.length > 0) {
                  var resourceObject = processObject.io[0];
                  if (resourceObject.signature == 'ResourceObject_001') {
                      var po = resourceObject.instance.arrayRes[0];

                      po.directives['query_string'] = queryString;
                      po.directives['return_mode'] = returnMode;
                      po.directives['query_bypass_similarity'] = queryBypassSimilarity;
                      po.directives['query_bypass_privacy'] = queryBypassPrivacy;
                      po.directives['size'] = size;

                      po.directives = sanitizePO(po.directives);

                      queriesApiFactory.query(shard, partition, 'asynch', po,appSession).then(function(results){
                        deferred.resolve(results);
                      },function(error){
                        deferred.reject(error);
                      });


                  } else {
                    //no resource object
                    deferred.reject('no resource object');
                  }
              } else {
                //no PO returned
                deferred.reject('no process object returned');
              }
          });

    return deferred.promise;
    };

    /**
   * @ngdoc method
   * @name queryStringEncrypted
   * @methodOf searchRequesterFactory
   * @description
   * pulls a process object using resourceApiFactory, adds directives properties to the PO,
   * passes request to server,
   *
   * Returns a promise of requested data matching query string
   *
   *
   * @param {string} encryptedString JSONB query string, encrypted
   * @param {string} size number of items to retrieve
   * @param {string} returnMode a flag to state whether return the actual data or just meta-data
   * @param {string} queryBypassSimilarity flag for bypassing similariy filters on server (recommend true, unless dupe checking)
   * @param {string} queryBypassPrivacy a flag for bypassing removal of private information from data return (recommend false, unless needing private information)
   *
   * @returns {Promise} Future object of results from query
   *
   *
  */

    searchRequesterFactory.queryStringEncrypted = function(encryptedString, shard, partition, size, returnMode, queryBypassSimilarity, queryBypassPrivacy,appSession){
      var deferred = $q.defer();
      transcodeFactory.decrypt(encryptedString,null,true).then(function(decrypted){
        searchRequesterFactory.queryString(decrypted, shard, partition, size, returnMode, queryBypassSimilarity, queryBypassPrivacy,appSession).then(function(results){
          deferred.resolve(results);
        },function(error){
          deferred.reject(error);
        });
      },function(error){
        deferred.reject(error);
      });
      return deferred.promise;
    };


    /**
   * @ngdoc method
   * @name keyword
   * @methodOf searchRequesterFactory
   * @description
   * pulls a process object using resourceApiFactory, adds directives properties to the PO,
   * passes request to server,
   *
   * Returns a promise of requested data matching query string
   *
   *
   * @param {string} queryTerm string
   * @param {string} size number of items to retrieve
   * @param {string} threshold Accuracy measurement for search results.
   * @param {string} returnMode a flag to state whether return the actual data or just meta-data
   * @param {string} queryBypassSimilarity flag for bypassing similariy filters on server (recommend true, unless dupe checking)
   * @param {string} queryBypassPrivacy a flag for bypassing removal of private information from data return (recommend false, unless needing private information)
   *
   * @returns {Promise} Future object of results from query
   *
   *
  */
    //trilike -string
    searchRequesterFactory.keyword = function(queryTerm, shard, partition, size, threshold, returnMode, queryBypassSimilarity, queryBypassPrivacy,appSession){
      var deferred = $q.defer();

      //pull process object for request
      resourceApiFactory.getModel('ProcessObject_001', 'v_001', true)
          .then(function(processObject) {
              if (processObject.io.length > 0) {
                  var resourceObject = processObject.io[0];
                  if (resourceObject.signature == 'ResourceObject_001') {
                      var po = resourceObject.instance.arrayRes[0];

                      po.directives['query_trilike'] = queryTerm;
                      po.directives['return_mode'] = returnMode;
                      po.directives['threshold'] = threshold;
                      po.directives['query_bypass_similarity'] = queryBypassSimilarity;
                      po.directives['query_bypass_privacy'] = queryBypassPrivacy;
                      po.directives['size'] = size;

                      po.directives = sanitizePO(po.directives);


                      queriesApiFactory.trilikeQuery(shard,partition,'asynch',po,appSession).then(function(results){
                        deferred.resolve(results);
                      },function(error){
                        deferred.reject(error);
                      });


                  } else {
                    //no resource object
                    deferred.reject('no resource object');
                  }
              } else {
                //no PO returned
                deferred.reject('no process object returned');
              }
          });

    return deferred.promise;
    };

    return searchRequesterFactory;
  }]);

var cooperativeBizRegistration = angular.module('cooperative.biz.registration', []);
var cooperativeBizRegistration; //Defined in 0module.js

/**
 * @ngdoc service
 * @name searchFactory
 *
 * @description
 * Author hiten
 *
 * Date 07/15/15
 *
 * Business service for ?????????????.
 */

cooperativeBizRegistration.factory('duplicateCheckFactory', ['searchFactory', '_', function(searchFactory, _) {
    var duplicateCheckFactory = function(partition, mode, orgLegalName, orgAddress, orgCity, orgStateRegion, orgPostalCode, orgCountryCode, appSession) {
        this.items = [];
        this.orgLegalName = orgLegalName;
        this.orgAddress = orgAddress;
        this.orgCity = orgCity;
        this.orgStateRegion = orgStateRegion;
        this.orgPostalCode = orgPostalCode;
        this.orgCountryCode = orgCountryCode;
        this.appSession = appSession;
        this.partition = partition;
        this.mode = mode;
    };

    duplicateCheckFactory.prototype.run = function(callback) {
        this.running = true;

        var promise = searchFactory.searchDuplicates(this.partition, this.mode, this.orgLegalName, this.orgAddress, this.orgCity, this.orgStateRegion, this.orgPostalCode, this.orgCountryCode, undefined, this.appSession);

        promise.then(function(complete){
            for (var i=0; i < complete.results.length; i++){
                var foundIndex = _.findIndex(this.items, {id: complete.results[i].id});

                if (foundIndex == -1){
                    this.items.push(complete.results[i]);
                } else {
                    this.items[foundIndex].rank  = this.items[foundIndex].rank + complete.results[i].rank;
                }
            }

            if (callback){
                if (angular.isFunction(callback)){
                    callback();
                }
            }

        }.bind(this));
    };

    return duplicateCheckFactory;
}]);
var cooperativeBizRegistration; //Defined in 0module.js
cooperativeBizRegistration.factory('organizationFactory', ['accountUtils', 'authorizationManagement', 'appUserSession', function(accountUtils, authorizationManagement, appUserSession) {
	var organizationFactory = {};

	organizationFactory.addPermissions = function(permissions, additionalContext, reason, returnUrl) {

		authorizationManagement.addPermissions(appUserSession.getUser().getSession(), appUserSession.getUser().getUsername(), permissions, additionalContext, undefined, true)
			.then(function(data) {
				var userName = appUserSession.getUser().getUsername();
				appUserSession.getUser().gotoPlatformPermissionUpdate(returnUrl, 'ctp.sso.org_permissions_updated');
			})
			.catch(function(data) {

			});
	};

	organizationFactory.addOrganization = function(orgProcessObjectId, returnUrl) {


		var add_permission = [];

		var record_org_read_permission = {
			layer: 1,
			action: 'read',
			context: 'get',
			targets: ['poid:' + orgProcessObjectId],
			resource: '*',
			roleAlias: 'read_write_role'
		};

		var record_org_update_permission = {
			layer: 1,
			action: 'write',
			context: 'post',
			targets: ['poid:' + orgProcessObjectId],
			resource: '*',
			roleAlias: 'read_write_role'
		};

		var application_use_permission_read = {
			layer: 5,
			action: 'read',
			context: 'get',
			targets: ['poid:' + orgProcessObjectId],
			resource: 'AC_V002',
			roleAlias: 'read_write_role'
		};

		var application_use_permission_write = {
			layer: 5,
			action: 'write',
			context: 'post',
			targets: ['poid:' + orgProcessObjectId],
			resource: 'AC_V002',
			roleAlias: 'read_write_role'
		};

		var presentation_view_profile_permission = {
			layer: 6,
			action: 'read',
			context: 'get',
			targets: ['poid:' + orgProcessObjectId],
			resource: 'AC_V002_VIEW_PROFILE',
			roleAlias: 'read_write_role'
		};

		add_permission.push(record_org_read_permission);
		add_permission.push(record_org_update_permission);
		add_permission.push(application_use_permission_read);
		add_permission.push(application_use_permission_write);
		add_permission.push(presentation_view_profile_permission);

		accountUtils.getUserIdByOrgId(orgProcessObjectId, undefined, true)
			.then(function(data) {
				if (data.length === 0) {

					var presentation_edit_profile_permission = {
						layer: 6,
						action: 'write',
						context: 'post',
						targets: ['poid:' + orgProcessObjectId],
						resource: 'AC_V002_EDIT_PROFILE',
						roleAlias: 'read_write_role'
					};

					var presentation_assign_permissions = {
						layer: 6,
						action: 'write',
						context: 'post',
						targets: ['poid:' + orgProcessObjectId],
						resource: 'AC_V002_ASSIGN_PERMISSIONS',
						roleAlias: 'read_write_role'
					};

					add_permission.push(presentation_edit_profile_permission);
					add_permission.push(presentation_assign_permissions);

					organizationFactory.addPermissions(add_permission, orgProcessObjectId + ':org', 'ctp.sso.org_permissions_updated', returnUrl);

				} else {
					organizationFactory.addPermissions(add_permission, orgProcessObjectId + ':org', 'ctp.sso.org_permissions_updated', returnUrl);
				}
			})
			.catch(function() {
				organizationFactory.addPermissions(add_permission, orgProcessObjectId + ':org', 'ctp.sso.org_permissions_updated', returnUrl);
			});

	};

	return organizationFactory;
}]);
/**
 * Created by hiten on 05/08/15.
 */
var cooperativeBizRegistration; //Defined in 0module.js
cooperativeBizRegistration.factory('orgRegistrationFactory', ['workflowFactory', 'processObjectFactory', 'dataFactory', 'duplicateCheckFactory', 'LegalIdentificationFactory', '_', 'cooperativeRegistration', '$cookies', 'organizationFactory',function(workflowFactory, processObjectFactory, dataFactory, DuplicateCheckFactory, LegalIdentificationFactory, _, cooperativeRegistration, $cookies, organizationFactory) {
    var orgRegistrationFactory = function() {

        this.ioData = [];

        this.orgProcessObject = {};

        this.wkflowQueue = {
            orgReg: cooperativeRegistration.orgRegQueueName
        };

        this.loggedInUser = {
            sessionId: 'nil'
        };

        this.sessionInformation = {
            orgReg: {
                key: cooperativeRegistration.orgRegCookieName,
                value: 'nil'
            }
        };

        this.loading = {
            submitted: false
        };

        this.duplicates = {};
    };

    orgRegistrationFactory.prototype.generate = function() {
        
        this.loading.submitted = true;
        processObjectFactory.new(undefined, true)
            .then(function(processObject) {
                this.orgProcessObject = processObject;
                dataFactory.getNewEntityObject(this.orgProcessObject.id, undefined, 'EntityObject_001', 'Organization', undefined, undefined, true)
                    .then(function(entityObject) {
                        entityObject.instance.primary = true;
                        this.orgProcessObject.io.push(entityObject);
                        dataFactory.getNewEntityObject(this.orgProcessObject.id, entityObject.instanceId, 'StatusObject_001', 'main', undefined, undefined, true)
                            .then(function(statusObject) {
                                statusObject.typeValue = '0';
                                this.orgProcessObject.io.push(statusObject);

                                workflowFactory.addProcessObjectToQueue(processObject, this.wkflowQueue.orgReg, 'busy', true)
                                    .then(function(data) {
                                        this.sessionInformation.orgReg.value = data.id;
                                        this.ioData = this.orgProcessObject.io;
                                        $cookies.put(this.sessionInformation.orgReg.key, this.sessionInformation.orgReg.value);

                                        this.loading.submitted = false;
                                    }.bind(this));
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
    };

    orgRegistrationFactory.prototype.initiate = function() {
        this.loading.submitted = true;
        if ($cookies.get(this.sessionInformation.orgReg.key) === null ||
            $cookies.get(this.sessionInformation.orgReg.key) === undefined ||
            $cookies.get(this.sessionInformation.orgReg.key) === '') {
            this.generate();
        } else {
            this.sessionInformation.orgReg.value = $cookies.get(this.sessionInformation.orgReg.key);
            workflowFactory.getItems([this.wkflowQueue.orgReg], 1, true, ['busy'], this.sessionInformation.orgReg.value, undefined, true)
                .then(function(data) {
                    if (data.results) {
                        if (data.results.length > 0) {
                            this.orgProcessObject = data.results[0].data;
                            this.ioData = this.orgProcessObject.io;
                            this.loading.submitted = false;
                        } else {
                            this.generate();
                        }
                    } else {
                        this.generate();
                    }
                }.bind(this));
        }
    };

    orgRegistrationFactory.prototype.registerEntity = function(ioObject) {
        this.ioData.push(ioObject);
    };

    orgRegistrationFactory.prototype.update = function(callback) {
        this.loading.submitted = true;
        var entityObject = dataFactory.returnObjects('any', 'EntityObject_001', 0, 'Organization', undefined, this.ioData);

        LegalIdentificationFactory.getLegalIds(entityObject[0].instance.domicileCountryId).then(
            function(data) {
                var legalIds = [];

                if (data) {
                    if (data.id.length > 0) {
                        for (var index in data.id) {
                            var i = _.findLastIndex(this.orgProcessObject.io, {
                                type: data.id[index].name
                            });
                            if (i > -1) {
                                var clonedObject = JSON.parse(JSON.stringify(this.orgProcessObject.io[i]));
                                legalIds.push(clonedObject);
                            }
                        }
                    }
                    
                    if (legalIds.length > 0) {
                        for (var j = this.orgProcessObject.io.length - 1; j >= 0; j--) {
                            if (this.orgProcessObject.io[j].signature == 'LegalIdentifierObject_001') {
                                this.orgProcessObject.io.splice(j, 1);
                            }
                        }
                        for (var k in legalIds) {
                            this.orgProcessObject.io.push(legalIds[k]);
                        }
                    }
                }

                var promise = workflowFactory.addProcessObjectToQueue(this.orgProcessObject, this.wkflowQueue.orgReg, 'busy', true);
                promise.then(function(data) {
                    if (callback) {
                        if (angular.isFunction(callback)) {
                            callback({
                                status: true
                            });
                        }
                    }
                    this.loading.submitted = false;
                }.bind(this));
                promise.catch(function() {
                    if (callback) {
                        if (angular.isFunction(callback)) {
                            callback({
                                status: false
                            });
                        }
                    }
                    this.loading.submitted = false;
                }.bind(this));
            }.bind(this)
        );
    };

    orgRegistrationFactory.prototype.duplicateCheck = function(callback) {
        var entityObject = dataFactory.returnObjects('any', 'EntityObject_001', 0, 'Organization', undefined, this.ioData);

        var orgLegalName = dataFactory.returnObjects(entityObject.instanceId, 'NameObject_001', 0, 'legalName', undefined, this.ioData);
        var orgMainLocation = dataFactory.returnObjects(entityObject.instanceId, 'LocationObject_001', 0, 'main', undefined, this.ioData);

        this.duplicates = new DuplicateCheckFactory(null, null, orgLegalName[0].typeValue, orgMainLocation[0].instance.address, orgMainLocation[0].instance.city, orgMainLocation[0].instance.stateRegion, orgMainLocation[0].instance.postalCode, orgMainLocation[0].instance.countryId, true);
        this.duplicates.run(callback);
    };

    orgRegistrationFactory.prototype.addUserOnly = function(orgProcessObjectId, returnUrl) {
        $cookies.remove(this.sessionInformation.orgReg.key);
        organizationFactory.addOrganization(orgProcessObjectId, returnUrl);
    };

    orgRegistrationFactory.prototype.complete = function(returnUrl, callback) {
        this.update(function(data) {
            this.loading.submitted = true;
            if (data.status) {
                this.orgProcessObject.rev = '0-' + this.orgProcessObject.id;
                var postProcessObjectPromise = dataFactory.postProcessObject('object', null, true, this.orgProcessObject, true);
                    postProcessObjectPromise.then(function() {
                        var addProcessObjectToQueuePromise = workflowFactory.addProcessObjectToQueue(this.orgProcessObject, this.wkflowQueue.orgReg, 'closed', true);
                            addProcessObjectToQueuePromise.then(function(data) {
                                $cookies.remove(this.sessionInformation.orgReg.key);
                                organizationFactory.addOrganization(this.orgProcessObject.id, returnUrl);
                                if (callback) {
                                    if (angular.isFunction(callback)) {
                                        callback({
                                            status: true
                                        });
                                    }
                                }
                                this.loading.submitted = false;
                            }.bind(this));
                            
                            addProcessObjectToQueuePromise.catch(function() {
                                $cookies.remove(this.sessionInformation.orgReg.key);
                                organizationFactory.addOrganization(this.orgProcessObject.id, returnUrl);
                                if (callback) {
                                    if (angular.isFunction(callback)) {
                                        callback({
                                            status: false
                                        });
                                    }
                                }
                                this.loading.submitted = false;
                            }.bind(this));
                    }.bind(this));
                    
                    postProcessObjectPromise.catch(function() {
                        $cookies.remove(this.sessionInformation.orgReg.key);
                        if (callback) {
                            if (angular.isFunction(callback)) {
                                callback({
                                    status: false
                                });
                            }
                        }
                        this.loading.submitted = false;
                    }.bind(this));
            } else {
                this.loading.submitted = false;
            }
        }.bind(this));
    };

    return orgRegistrationFactory;
}]);
/**
 * Created by hiten on 29/10/2015.
 */

var cooperativeBizRegistration; //Defined in 0module.js
cooperativeBizRegistration.factory('orgServiceRegistrationFactory', ['workflowFactory', 'processObjectFactory', 'dataFactory', 'objectsApiFactory', 'appUserSession', 'authorizationManagement', 'accountUtils', function(workflowFactory, processObjectFactory, dataFactory, objectsApiFactory, appUserSession, authorizationManagement, accountUtils) {
    var orgServiceRegistrationFactory = function(serviceName, wkflowQueue) {
        this.organizations = [];
        this.selectedOrg = {};
        this.wkflowQueue = wkflowQueue;
        this.permissions = [];
        this.serviceName = serviceName;
        this.loaded = {
            organizations: false
        };
    };

    orgServiceRegistrationFactory.prototype.getOrgData = function(processObjectId) {

        var permissionObj = {
            application: [{
                layer: 5,
                action: 'read',
                context: 'get',
                targets: ['poid:' + processObjectId],
                resource: this.serviceName,
                roleAlias: 'read_role'
            }, {
                layer: 5,
                action: 'write',
                context: 'post',
                targets: ['poid:' + processObjectId],
                resource: this.serviceName,
                roleAlias: 'read_role'
            }]
        };

        var promise = authorizationManagement.checkPermissions(appUserSession.getUser().getSession(), permissionObj, undefined, true);
        promise.then(function(data) { }.bind(this));
        promise.catch(function(){
            var getSignaturePromise = dataFactory.getBySignatureList('object', null, null, processObjectId, undefined, ['EntityObject_001', 'NameObject_001', 'LocationObject_001']);
            getSignaturePromise.then(function(ioData) {
                var entityObjectList = dataFactory.returnObjects(undefined, 'EntityObject_001', 0, 'Organization', undefined, ioData, undefined);
                var nameObjectList = dataFactory.returnObjects(entityObjectList[0].instanceId, 'NameObject_001', 0, 'legalName', undefined, ioData, undefined);
                var locationObjectList = dataFactory.returnObjects(entityObjectList[0].instanceId, 'LocationObject_001', 0, 'main', undefined, ioData, undefined);

                var org = {
                    processObjectId: processObjectId,
                    entityObjectId: entityObjectList[0].instanceId,
                    orgName: nameObjectList[0].typeValue,
                    location: {
                        address: locationObjectList[0].instance.address,
                        addressExt: locationObjectList[0].instance.addressExt,
                        city: locationObjectList[0].instance.city,
                        stateRegion: locationObjectList[0].instance.stateRegion,
                        postalCode: locationObjectList[0].instance.postalCode,
                        countryId: locationObjectList[0].instance.countryId
                    }
                };

                this.organizations.push(org);
            }.bind(this));
        }.bind(this));
    };

    orgServiceRegistrationFactory.prototype.setServiceName = function(serviceName) {
        if (serviceName !== undefined && serviceName !== null && serviceName !== '') {
            this.serviceName = serviceName;

            return true;
        } else {
            return false;
        }

    };

    orgServiceRegistrationFactory.prototype.addToWorkFlowQueue = function(queueName) {
        if (queueName !== undefined && queueName !== null && queueName !== '') {
            this.wkflowQueue.push(queueName);

            return true;
        } else {
            return false;
        }

    };

    orgServiceRegistrationFactory.prototype.setPermissions = function(permissions) {
        if (permissions !== undefined && permissions !== null && angular.isArray(permissions) && permissions.length > 0) {
            this.permissions = permissions;

            return true;
        } else {
            return false;
        }
    };

    orgServiceRegistrationFactory.prototype.addPermissions = function(permission) {
        if (permission !== undefined && permission !== null) {
            this.permissions.push(permission);

            return true;
        } else {
            return false;
        }
    };

    orgServiceRegistrationFactory.prototype.getOrganizationList = function() {
        accountUtils.getIdsByContext(appUserSession.getUser().getSession(), 'org', undefined, true)
            .then(function(data) {
                for (var ids in data){
                    this.getOrgData((ids));
                }

                this.loaded.organizations = true;
            }.bind(this));
    };

    orgServiceRegistrationFactory.prototype.complete = function(returnUrl, callback) {

        var getNewEntityObjectServiceObjectPromise = dataFactory.getNewEntityObject(this.selectedOrg.processObjectId, this.selectedOrg.entityObjectId, 'ServiceObject_001', this.serviceName, undefined, undefined, true);
        getNewEntityObjectServiceObjectPromise.then(function(entityObject) {
            entityObject.instance.active = true;

            var getNewEntityObjectStatusObjectPromise = dataFactory.getNewEntityObject(this.selectedOrg.processObjectId, this.selectedOrg.entityObjectId, 'StatusObject_001', this.serviceName, undefined, undefined, true);
            getNewEntityObjectStatusObjectPromise.then(function(statusObject) {

                statusObject.typeValue = 0;
                
                var objectsApiFactoryGetPromise = objectsApiFactory.get('object', null, null, this.selectedOrg.processObjectId, 'any', ['nil'], true);
                objectsApiFactoryGetPromise.then(function(processObject) {
                    processObject.io.push(entityObject);
                    processObject.io.push(statusObject);

                    var postProcessObjectPromise = dataFactory.postProcessObject('object', null, null, processObject, true);
                    postProcessObjectPromise.then(function() {
                        processObject.io = [];

                        var getNewEntityObjectEntityObject = dataFactory.getNewEntityObject(this.selectedOrg.processObjectId, undefined, 'EntityObject_001', 'Ticket', 'serviceReg', undefined, true);
                        getNewEntityObjectEntityObject.then(function(ticketEntityObject) {

                            processObject.io.push(ticketEntityObject);

                            var addProcessObjectToManyQueuesPromise = workflowFactory.addProcessObjectToManyQueues(processObject, this.wkflowQueue, 'idle', undefined, true);
                            addProcessObjectToManyQueuesPromise.then(function() {
                                if (this.permissions.length > 0) {
                                    var addPermissionsPromise = authorizationManagement.addPermissions(appUserSession.getUser().getSession(), appUserSession.getUser().getUsername(), this.permissions, processObject.id + ':org', undefined, true);
                                    addPermissionsPromise.then(function(data) {
                                        var userName = appUserSession.getUser().getUsername();
                                        appUserSession.getUser().gotoPlatformPermissionUpdate(returnUrl, 'ctp.sso.org_permissions_updated');
                                    }.bind(this));
                                                            
                                    addPermissionsPromise.catch(function(data) { }.bind(this));
                                }

                                if (callback) {
                                    if (angular.isFunction(callback)) {
                                        callback({ status: true });
                                    }
                                }
                            }.bind(this));

                            addProcessObjectToManyQueuesPromise.catch(function() {
                                if (callback) {
                                    if (angular.isFunction(callback)) {
                                        callback({
                                            status: false
                                        });
                                    }
                                }
                            }.bind(this));

                        }.bind(this));
                        
                        getNewEntityObjectEntityObject.catch(function() {
                            if (callback) {
                                if (angular.isFunction(callback)) {
                                    callback({
                                        status: false
                                    });
                                }
                            }
                        }.bind(this));
                    }.bind(this));
                    
                    postProcessObjectPromise.catch(function() {
                        if (callback) {
                            if (angular.isFunction(callback)) {
                                callback({
                                    status: false
                                });
                            }
                        }
                    }.bind(this));

                }.bind(this));

                objectsApiFactoryGetPromise.catch(function() {
                    if (callback) {
                        if (angular.isFunction(callback)) {
                            callback({ status: false });
                        }
                    }
                }.bind(this));
                
            }.bind(this));

            getNewEntityObjectStatusObjectPromise.catch(function() {
                if (callback) {
                    if (angular.isFunction(callback)) {
                        callback({ status: false });
                    }
                 }
            }.bind(this));
        
        }.bind(this));

        getNewEntityObjectServiceObjectPromise.catch(function() {
            if (callback) {
                if (angular.isFunction(callback)) {
                    callback({
                        status: false
                    });
                }
            }
        }.bind(this));
    };


    return orgServiceRegistrationFactory;
}]);
/**
 * Created by hiten on 05/08/15.
 */
var cooperativeBizRegistration; //Defined in 0module.js
cooperativeBizRegistration.factory('userRegistrationFactory', ['appUserSession', 'workflowFactory', 'processObjectFactory', 'dataFactory', 'ipFactory', 'cooperativeRegistration', '$cookies', 'accountUtils', function(appUserSession, workflowFactory, processObjectFactory, dataFactory, ipFactory, cooperativeRegistration, $cookies, accountUtils) {
    var userRegistrationFactory = function() {
        this.user_auth = {
            userName: '',
            password: ''
        };

        this.ioData = [];

        this.userProcessObject = {};

        this.orgPermissions = {
            orgId: ''
        };

        this.wkflowQueue = {
            userReg: cooperativeRegistration.userRegQueueName
        };

        this.sessionInformation = {
            userReg: {
                key: cooperativeRegistration.userRegCookieName,
                value: 'nil'
            },
            orgReg: {
                key: 'org_signup_key',
                value: 'nil'
            }
        };
    };

    userRegistrationFactory.prototype.generate = function() {
        processObjectFactory.new(undefined, true)
            .then(function(processObject) {
                this.userProcessObject = processObject;
                dataFactory.getNewEntityObject(this.userProcessObject.id, undefined, 'EntityObject_001', 'Person', undefined, undefined, true)
                    .then(function(entityObject) {
                        entityObject.typeValue = 'USER';
                        entityObject.instance.primary = true;

                        ipFactory.getClientCountryCode(undefined, true)
                            .then(function(countryCode) {
                                entityObject.instance.domicileCountryId = countryCode;
                                this.userProcessObject.io.push(entityObject);

                                workflowFactory.addProcessObjectToQueue(processObject, this.wkflowQueue.userReg, 'busy', true)
                                    .then(function(data) {
                                        this.sessionInformation.userReg.value = data.id;
                                        this.ioData = this.userProcessObject.io;

                                        $cookies.put(this.sessionInformation.userReg.key, this.sessionInformation.userReg.value);

                                    }.bind(this));

                            }.bind(this));
                    }.bind(this));
            }.bind(this));
    };

    userRegistrationFactory.prototype.initiate = function() {
        if ($cookies.get(this.sessionInformation.userReg.key) === null ||
            $cookies.get(this.sessionInformation.userReg.key) === undefined ||
            $cookies.get(this.sessionInformation.userReg.key) === '') {

            this.generate();

        } else {
            this.sessionInformation.userReg.value = $cookies.get(this.sessionInformation.userReg.key);
            workflowFactory.getItems([this.wkflowQueue.userReg], 1, true, ['busy'], this.sessionInformation.userReg.value, undefined, true)
                .then(function(data) {
                    if (data.results) {
                        if (data.results.length > 0) {
                            this.userProcessObject = data.results[0].data;
                            if (this.userProcessObject.directives) {
                                this.user_auth.userName = this.userProcessObject.directives.identity_new;
                            }
                            this.ioData = this.userProcessObject.io;
                        } else {
                            this.generate();
                        }
                    } else {
                        this.generate();
                    }
                }.bind(this));
        }
    };

    userRegistrationFactory.prototype.registerEntity = function(ioObject) {
        this.ioData.push(ioObject);
    };

    userRegistrationFactory.prototype.updateOrgId = function(orgId, complete) {
        $cookies.remove(this.sessionInformation.orgReg.key);

        this.orgPermissions.orgId = orgId;

        if (complete) {
            this.complete();
        }
    };

    userRegistrationFactory.prototype.createUserDirective = function(callback) {
        this.userProcessObject.directives = {
            'identity_new': this.user_auth.userName,
            'credential_new': this.user_auth.password,
            'this_context': 'self',
            'default_context': 'self',
            'additional_context': 'self',
            'initial_role': 'read_write_role',
            'session_type': 'default',
            'add_permission': []
        };

        if (this.orgPermissions.orgId !== '') {

            this.userProcessObject.directives.additional_context = this.orgPermissions.orgId + ':org';


            var record_org_read_permission = {
                layer: 1,
                action: 'read',
                context: 'get',
                targets: ['poid:' + this.orgPermissions.orgId],
                resource: '*',
                roleAlias: 'read_write_role'
            };

            var record_org_update_permission = {
                layer: 1,
                action: 'write',
                context: 'post',
                targets: ['poid:' + this.orgPermissions.orgId],
                resource: '*',
                roleAlias: 'read_write_role'
            };

            var application_use_permission_read = {
                layer: 5,
                action: 'read',
                context: 'get',
                targets: ['poid:' + this.orgPermissions.orgId],
                resource: 'AC_V002',
                roleAlias: 'read_write_role'
            };

            var application_use_permission_write = {
                layer: 5,
                action: 'write',
                context: 'post',
                targets: ['poid:' + this.orgPermissions.orgId],
                resource: 'AC_V002',
                roleAlias: 'read_write_role'
            };

            var presentation_view_profile_permission = {
                layer: 6,
                action: 'read',
                context: 'get',
                targets: ['poid:' + this.orgPermissions.orgId],
                resource: 'AC_V002_VIEW_PROFILE',
                roleAlias: 'read_write_role'
            };



            this.userProcessObject.directives.add_permission.push(record_org_read_permission);
            this.userProcessObject.directives.add_permission.push(record_org_update_permission);
            this.userProcessObject.directives.add_permission.push(application_use_permission_read);
            this.userProcessObject.directives.add_permission.push(application_use_permission_write);
            this.userProcessObject.directives.add_permission.push(presentation_view_profile_permission);

            var getUserIdByOrgIdPromise = accountUtils.getUserIdByOrgId(this.orgPermissions.orgId, undefined, true);
            getUserIdByOrgIdPromise.then(function(data) {
                    if (data.length === 0) {


                        var presentation_edit_profile_permission = {
                            layer: 6,
                            action: 'write',
                            context: 'post',
                            targets: ['poid:' + this.orgPermissions.orgId],
                            resource: 'AC_V002_EDIT_PROFILE',
                            roleAlias: 'read_write_role'
                        };

                        var presentation_assign_permissions = {
                            layer: 6,
                            action: 'write',
                            context: 'post',
                            targets: ['poid:' + this.orgPermissions.orgId],
                            resource: 'AC_V002_ASSIGN_PERMISSIONS',
                            roleAlias: 'read_write_role'
                        };

                        this.userProcessObject.directives.add_permission.push(presentation_edit_profile_permission);
                        this.userProcessObject.directives.add_permission.push(presentation_assign_permissions);

                        if (callback) {
                            if (angular.isFunction(callback)) {
                                callback();
                            }
                        }


                    } else {
                        if (callback) {
                            if (angular.isFunction(callback)) {
                                callback();
                            }
                        }
                    }
                }.bind(this));

                getUserIdByOrgIdPromise.catch(function() {
                    if (callback) {
                        if (angular.isFunction(callback)) {
                            callback();
                        }
                    }
                });


        } else {
            if (callback) {
                if (angular.isFunction(callback)) {
                    callback();
                }
            }
        }
    };

    userRegistrationFactory.prototype.update = function(callback) {
        this.createUserDirective(function() {
            var addProcessObjectToQueuePromise = workflowFactory.addProcessObjectToQueue(this.userProcessObject, this.wkflowQueue.userReg, 'busy', true);

            addProcessObjectToQueuePromise.then(function(data) {
                if (callback) {
                    if (angular.isFunction(callback)) {
                        callback({
                            status: true
                        });
                    }
                }
            });

            addProcessObjectToQueuePromise.catch(function() {
                if (callback) {
                    if (angular.isFunction(callback)) {
                        callback({
                            status: false
                        });
                    }
                }
            });
        }.bind(this));
    };


    userRegistrationFactory.prototype.complete = function(callback) {
        this.update(function(data) {
            if (data.status) {
                var updateItemStatePromise = workflowFactory.updateItemState(this.wkflowQueue.userReg, 'ready', this.userProcessObject.id, undefined, true);
                updateItemStatePromise.then(function(data) {
                        $cookies.remove(this.sessionInformation.userReg.key);
                        if (callback) {
                            if (angular.isFunction(callback)) {
                                callback({
                                    status: true
                                });
                            }
                        }
                    }.bind(this));
                    updateItemStatePromise.catch(function() {
                        $cookies.remove(this.sessionInformation.userReg.key);
                        if (callback) {
                            if (angular.isFunction(callback)) {
                                callback({
                                    status: false
                                });
                            }
                        }
                    }.bind(this));
            }
        }.bind(this));
    };

    return userRegistrationFactory;
}]);
var cooperativeBizState = angular.module('cooperative.biz.state', []);

/* jshint -W018 */
var cooperativeBizState; //Defined in 0module.js

(function (cooperativeBizState) {
    'use strict';
/**
 * @ngdoc service
 * @name biz.state.legacyFactory
 *
 * @description
 * Author Max Bye
 *
 * Date 08/02/17
 *
 * Business service that puts directives into or out of legacy mode
 * When legacy is true, directives update objects rather than arrays and a warning is sent out
 * Further development required to move away / update from legacy mode 
 * 
 * @param {string} process (Optional) - returns what process is in legacy
 * @param {string} legacyModeToggle (Optional) - Manually toggles legacy mode
 */
// cooperativeBizState.factory('legacyFactory', Legacy);

 cooperativeBizState.factory('legacyFactory', ['stateProcessFactory', Legacy]);
 
 function Legacy(stateProcessFactory) {    
    var legacyFactory = {};
    
    /**
    * If legacy is false - sets state to attr
    *
    * @param {object} attr attribute of object being passed in
    * @param {string} state what state to set to attr
    * @param {bool} legacy is legacy off or on?
    */
    legacyFactory.returnStatus = function(attr, state, legacy, reason) {        
        if(!legacy){
            
            attr._state = stateProcessFactory.create(['processData']);

            switch(state) {
                case 'success':
                    attr._state.processData.setResolved();
                    break;
                case 'error':
                    attr._state.processData.setError();
                    break;
                case 'rejected':
                    attr._state.processData.setRejected(reason);
                    break;
                default: // pending
                    attr._state.processData.setPending();
            }
        }
    };

    /**
    * Check Legacy
    * Used in Unit Tests
    * Check if parsed data contains data._data if so legacy mode is false
    * @param {string} element the element.text() to be parsed
    * @return {object} data to be tested against 
    */
    legacyFactory.checkLegacy = function(element){
        var data = JSON.parse(element); 
        if(data._data){
            return data._data;
        }
        else{
            return data;
        }
    };
    
    /**
    * Return/format data in correct format depending on legacy state  
    *
    * @param {object} result the data that needs to be assigned to object
    * @param {bool} legacy is legacy off or on?
    * @return formatted data
    */
    legacyFactory.returnData = function(result, legacy) {               
        if(legacy){
            return result;
        }
        else{            
            return { _data: result };
        }        
    };

    /**
     * Set Scopeprefix
     * Amend scopeprefix so that it initiates with scope[attrs.scopeprefix]._data rather than scope[attrs.scopeprefix] if legacy false
     * @private
     * @param {object} prefix What directive is being run
     * @param {bool} legacy is legacy off or on?
     * @returns prefix in right format
     */
    
    legacyFactory.setPrefix = function(prefix, legacy){
        if(legacy){
            return prefix;
        }
        else{
            return prefix._data;
        }
    };

    /**
     * Adds warning to console if legacy is true so user knows to update
     *
     * @private
     * @param {string} process What directive is being run
     * @returns {string} Returns message
     */
    legacyFactory._deprecationWarning = function(process) {        
        if(process){
            var processSpecific = '\ndeprecated: directive ' + process + ' is returning an array. ';
            return processSpecific;
        }              
    };

    /**
    * Checks wether legacy is on or off
    * Either defined as 'true' by legacy attribute in markup 
    * Or left out (undefined is also true) 
    *
    * @param {bool | string} legacy is legacy off or on?
    * @param {bool} legacy is legacy off or on?
    * @param {string} process What directive is being run
    */
    legacyFactory.legacyMode = function(legacy, process) {
        var legacyModeActive = true;
        if('undefined' !== typeof legacy){
            legacy = JSON.parse(legacy);
            legacyModeActive =  legacy;
        }

        if(legacyModeActive){
            var processDetail = this._deprecationWarning(process);
            processDetail += '\n_state is not accessible. \nLegacy mode is on, please consider updating your markup.';
            // console.warn(processDetail);
        }

        return legacyModeActive;
    };
    
    return legacyFactory;   
}   

})(cooperativeBizState);

/* jshint -W018 */
var cooperativeBizState; //Defined in 0module.js

(function (cooperativeBizState) {
    // 'use strict';

    /**
     * @ngdoc service
     * @name biz.state.StateProcessFactory
     *
     * @description
     * Author Andrei Sandu
     *
     * Date 07/31/17
     *
     * Business service for manage state of processes.
     *
     *
     * ***Example***
     *-------
     *
     * ```javascript
     *      var state = StateProcessFactory.create(['processData'])
     * ```
     *
     * **state** will have this properties:
     *
     * - state.isCompleted() - `{boolean}`
     * - state.isError() - `{boolean}` True if any processes had an error
     * - state.isPending() - `{boolean}` True if one of the processes has status pending
     * - state.isResolved() - `{boolean}` True if all processes are resolved
     * - state.isRejected() - `{boolean}` True if one of processes are rrejected
     * - state.update() - Update overall state based on processes
     * - state.STATUSES - `{object}` all type of states. Default states are:
     *   state.STATUSES.error - `-1`
     *   state.STATUSES.pending - `0`
     *   state.STATUSES.resolved - `1`
     *   state.STATUSES.rejected - `2`
     *   state.STATUSES.completed - `3`
     *
     * - state.current - `{process object}` The overall process
     * - state.processData - {process Object}
     *
     *  Process object. Properties and methods of this object are:
     *   - state.processData.error - `-1`
     *   - state.processData.pending - `0`
     *   - state.processData.resolved - `1`
     *   - state.processData.rejected - `2`
     *   - state.processData.setError(errorReason) - Param `{string}` Set process status with error state value `e.g.: state.processData.statu`
     *   - state.processData.isError - `{boolean}` True if the process had error
     *   - state.processData.setRejected(rejectedReason) - Param `{string}` Set process status with rejected state value
     *   - state.processData.isRejected - `{boolean}` True if the process had error
     *   - state.processData.setResolved() -  returns `{boolean}` Set process status with resolved state value
     *   - state.processData.isResolved - `{boolean}` True if the process is resolved
     *
     *
     */
    cooperativeBizState.factory('stateProcessFactory', StateProcess);

    function StateProcess() {

        return {
            create: function (processes) {
                return new StateProcessFactory(processes);
            }
        };
    }

    var OVERALL_STATE_NAME = 'current';
    var STATUSES = {
        'error': -1,
        'pending': 0,
        'resolved': 1,
        'rejected': 2,
        'completed': 3
    };

    /**
     * Determines if a reference is an `Object`. Unlike `typeof` in JavaScript, `null`s are not
     * considered to be objects. Note that JavaScript arrays are objects.
     *
     * @private
     * @returns {object} The default properties for a state.
     */
    var _getDefaultStateProps = function () {

        return {
            status: STATUSES.pending,
            reason: ''
        };
    };

    /**
     * Transform string in to a title case string
     *
     * @private
     * @param {string} string Reference to check.
     * @returns {string} Return title case string transformed.
     */
    var _titleString = function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    };

    var _extend = function (obj, src) {
        Object.keys(src).forEach(function(key) { obj[key] = src[key]; });
        return obj;
    };

    /**
     * Create dynamic set methods based on state (e.g.: 'setPending()')
     *
     * ***Note:*** `setReason(reason)` or `setError(error)` support passing <string> param
     *
     * @private
     * @param {string} state Reference to check.
     * @returns {undefined} True if `value` is an `Object` but not `null`.
     */
    var _createMethodsByState = function (state) {
        this['set' + _titleString(state)] = function (reason) {
            this.status = STATUSES[state];
            if (!!reason) {
                this.setReason(reason);
            }
        };

        _createCheckMethodsByState.call(this, state);
    };

    /**
     * Create dynamic check methods based on state (e.g: `isPending()`)
     *
     * @private
     * @param {string} state Reference to check.
     * @returns {undefined} True if `value` is an `Object` but not `null`.
     */
    var _createCheckMethodsByState = function (state) {
        this['is' + _titleString(state)] = function () {
            return this.status === STATUSES[state];
        };
    };
    var _createOverallStateCheckMethodsByState = function (state) {
        this['is' + _titleString(state)] = function () {
            this.update();
            if (STATUSES[state] === STATUSES.completed) {
                return this[OVERALL_STATE_NAME].status === STATUSES.completed ||
                    this[OVERALL_STATE_NAME].status === STATUSES.error;
            }

            return this[OVERALL_STATE_NAME].status === STATUSES[state];
        };
    };
    var _updateOverallState = function () {
        var isCompleted = Object.keys(this).every(_checkItem, this);

        if (this[OVERALL_STATE_NAME].status !== this.STATUSES.error) {
            this[OVERALL_STATE_NAME].status = isCompleted ? this.STATUSES.completed : this.STATUSES.pending;
        }
    };
    var _checkItem = function (item) {
        if (this[item].status === this.STATUSES.error) {
            this[OVERALL_STATE_NAME].status = this.STATUSES.error;
            this[OVERALL_STATE_NAME].reason = item + ' finish with error: ' + this[item].reason + '\n';
            return true;
        }

        if (this[item].status === this.STATUSES.rejected) {
            this[OVERALL_STATE_NAME].reason += item + ' rejected with reason: ' + this[item].reason + '\n';
        }

        return this[item].status === this.STATUSES.resolved || this[item].status === this.STATUSES.rejected;
    };
    var _setReason = function (reason) {
        this.reason = reason;
    };

    function StateProcessFactory(processes) {
        var stateProcess = this;

        if (!Array.isArray(processes)) {
            throw new Error('No processes defined!');
        }

        processes.forEach(_createProcess, this);

        function _createProcess(process) {
            stateProcess[process] = _createChildProcess.call(this);
        }

        function _createChildProcess() {
            var tempStatus = _extend({}, STATUSES);

            function StateChild() {

                return _extend(this, _getDefaultStateProps());
            }

            StateChild.prototype.setReason = _setReason;

            delete tempStatus.completed;
            Object.keys(tempStatus).forEach(_createMethodsByState, StateChild.prototype);

            return new StateChild(this);
        }

        return stateProcess;
    }

    /**
     * @ngdoc object
     * @name biz.state.StateProcessFactory#STATUSES
     * @propertyOf biz.state.StateProcessFactory
     * @description
     *
     * Default Statuses.
     *
     */
    StateProcessFactory.prototype.STATUSES = STATUSES;

    /**
     * @ngdoc object
     * @name biz.state.StateProcessFactory#current
     * @propertyOf biz.state.StateProcessFactory
     * @description
     *
     * Overall state.
     *
     */
    StateProcessFactory.prototype[OVERALL_STATE_NAME] = _getDefaultStateProps();

    /**
     * @ngdoc function
     * @name biz.state.StateProcessFactory#update
     * @methodOf biz.state.StateProcessFactory
     * @description
     *
     * Update overall state based on processes
     *
     */
    StateProcessFactory.prototype.update = _updateOverallState;

    /**
     * @ngdoc function
     * @name biz.state.StateProcessFactory#setReason
     * @methodOf biz.state.StateProcessFactory
     * @description
     *
     * Set overall state reason
     *
     */
    StateProcessFactory.prototype.setReason = _setReason;
    Object.keys(STATUSES).forEach(_createOverallStateCheckMethodsByState, StateProcessFactory.prototype);

})(cooperativeBizState);

var cooperativeBizUploads = angular.module('cooperative.biz.uploads', []);
var cooperativeBizUploads; //Defined in 0module.js

/**
 * @ngdoc service
 * @name biz.uploads.uploadsFactory
 *
 * @description
 * Artifacts management service
 *
 * @created on 31/07/15.
 *
 * @author Hiten Vaghela (hiten.vaghela@gmail.com)
 *
 */
cooperativeBizUploads.factory('uploadsFactory', ['$q', 'artifactApiFactory', 'dataFactory', function ($q, artifactApiFactory, dataFactory) {
    var uploadsFactory = {};

    var dataURItoBlob = function(dataURI) {
        var binary = atob(dataURI.split(',')[1]);
        var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
        var array = [];
        for(var i = 0; i < binary.length; i++) {
            array.push(binary.charCodeAt(i));
        }
        return new Blob([new Uint8Array(array)], {type: mimeString});
    };

    /**
     * @ngdoc method
     * @name biz.uploads.uploadsFactory#uploadDataURI
     * @methodOf biz.uploads.uploadsFactory
     *
     * @description
     * Post file to be uploaded to server
     *
     * @param {string} processObjectId Id of the element we are looking for
     * @param {string} dataURI Uri we are looking for
     * @param {string} dataName File name we are looking for
     * @param {string} version Version to look for
     * @param {string} appSession session id
     */
    uploadsFactory.uploadDataURI = function(processObjectId, dataURI, dataName, version, appSession){
        var blob = dataURItoBlob(dataURI);
        blob.name = dataName;
        blob.lastModifiedDate = new Date();

        var formData = new FormData();
        formData.append('file', blob, dataName);

        artifactApiFactory.postFile(processObjectId, formData, version, appSession);
    };

    /**
     * @ngdoc method
     * @name biz.uploads.uploadsFactory#getFileUrl
     * @methodOf biz.uploads.uploadsFactory
     *
     * @description
     * Returns a promise containing an artifact's url.
     *
     * @param {string} processObjectId Id of the element we are looking for
     * @param {string} fileName Name of file we are looking for
     * @param {string} version Version to look for
     * @param {string} appSession session id
     *
     * @returns {Promise} Returns a promise containing found url or error
     */
    uploadsFactory.getFileUrl = function(processObjectId, fileName, version, appSession){
        var deferred = $q.defer();

        artifactApiFactory.getFile(processObjectId, fileName, version, appSession).then(function(data){
            deferred.resolve(data.signed_url);
        });

        return deferred.promise;
    };

    /**
     * @ngdoc method
     * @name biz.uploads.uploadsFactory#getFilesObject
     * @methodOf biz.uploads.uploadsFactory
     *
     * @description
     * Finds 1 or more artifacts from provided params and returns a promise with a list.
     *
     * @param {array} arr List of elements we are looking for
     * @param {string} version Version to look for
     * @param {string} appSession session id
     *
     * @returns {Promise} Returns a promise containing list of artifact objects found or else, an error.
     */
    uploadsFactory.getFilesObject = function(arr, version, appSession){
      var deferred = $q.defer();
      var promises = [];
      var promisesResults = [];

      if(!arr.length) {
        deferred.resolve([]);
        return deferred.promise;
      }

      angular.forEach(arr, function(el) {
        promises.push(artifactApiFactory.getFile(el.rootId, el.instance.typeUri, version, appSession).then(function(res) {
          promisesResults.push(res);
        },
        function(err) {
          if(!err) {
            throw 'Promise returned no error message.';
          }
          throw err.returnStatus.status_code + ': ' + err.returnStatus.reason;
        }));
      });

      $q.all(promises).then(function() {
        deferred.resolve(promisesResults);
      });

      return deferred.promise;
    };

    return uploadsFactory;
}]);

var cooperativeBizUtils = angular.module('cooperative.biz.utils', []);
/**
 * Created by hiten on 31/10/2015.
 */
var cooperativeBizUtils; //Defined in 0module.js

cooperativeBizUtils.factory('aesFactory',['CryptoJS', function(CryptoJS){
    var  aesFactory = {};

    var secret = 'fioewfoiepwhfo34235';

    aesFactory.encrypt = function(str){
        var encryptedString = CryptoJS.AES.encrypt(str, secret);

        return encryptedString.toString();
    };

    aesFactory.decrypt = function(str){
        var decryptedString = CryptoJS.AES.decrypt(str, secret);

        return decryptedString.toString(CryptoJS.enc.Utf8);
    };

    return aesFactory;
}]);
var cooperativeBizUtils; //Defined in 0module.js

/**
 * @ngdoc service
 * @name biz.utils.intervalsFactory
 *
 * @requires $interval
 * @requires $rootScope
 *
 * @description
 * Angular intervals with auto-cancellation and callback
 *
 * @author Emiliano Errecalde (eerrecalde@techsoupglobal.org)
 * @created 19/06/17
 */

cooperativeBizUtils.factory('intervalsFactory', ['$interval', '$rootScope', function($interval, $rootScope) {

  // Polyfill for Object.assign used below in the createInterval method. This will only be used when needed.
  if (typeof Object.assign != 'function') {
    Object.assign = function(target, varArgs) {
      'use strict';

      // Not unit testing this branch, as it's an unrelated and temporary functionality
      if (target == null) { // TypeError if undefined or null
        throw new TypeError('Cannot convert undefined or null to object');
      }

      var to = Object(target);

      for (var index = 1; index < arguments.length; index++) {
        var nextSource = arguments[index];

        if (nextSource != null) { // Skip over if undefined or null
          for (var nextKey in nextSource) {
            // Avoid bugs when hasOwnProperty is shadowed
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
      return to;
    };
  }

  var defaults = {
    counter: 0,
    max: 50,
    retryTime: 200
  };

  var intervals = {};

  var resetInterval = function(name) {
    $interval.cancel(intervals[name].interval);
    intervals[name] = null;
  };

  var createInterval = function (name, callback, config) {
    config = config || {};

    // If no name provided, show a message explaining.
    if(!name) {
      console.error('Sorry you must assign a name to the interval, so then you can cancel it ;)');
      return null;
    }

    if(!callback || typeof callback !== 'function') {
      console.error('Sorry you must assign a callback and it has to be a function.');
      return null;
    }

    if(config && typeof config !== 'object') {
      console.error('Sorry config is optional, but it has to be an object.');
      return null;
    }

    intervals[name] = Object.assign({}, defaults, config);
    intervals[name].name = name;

    $rootScope.$on('interval-active', function(event, obj){
      if(obj && obj.name === name) {
        callback(obj);
      }
    });

    intervals[name].interval = $interval(function () {
      intervals[name].counter += 1;

      intervals[name].active = true;

      if(intervals[name].counter > intervals[name].max) {
        resetInterval(name);
      }

      $rootScope.$broadcast('interval-active', intervals[name]);
    }, intervals[name].retryTime);

    return intervals[name];
  };

  var cancelInterval = function (name) {
    if(!intervals[name] || !intervals[name].active) {return;}
    resetInterval(name);
  };

  var getInterval = function (name) {
    return intervals[name] || null;
  };

  return {
    /**
     * @ngdoc method
     * @name biz.utils.intervalsFactory#create
     * @methodOf biz.utils.intervalsFactory
     *
     * @description
     * Creates an interval. It will send a callback repeatedly until it gets cancelled by either user or limits in the defaults (explained below)
     *
     * - counter (0): counter to start with
     * - max (50): number of iterations
     * - retryTime (200): frecuency of iteration
     *
     * @param {string} name name to be used to initialize the interval
     * @param {function} callback function to receive notifications for each iteration of the interval
     * @param {object} [config] config object to overwrite the defaults
     *
     * @returns {object} object containing the interval's details
     */
    create: createInterval,
    /**
     * @ngdoc method
     * @name biz.utils.intervalsFactory#get
     * @methodOf biz.utils.intervalsFactory
     *
     * @description
     * Returns the object created under a given name
     *
     * @param {string} name name to be used to find the interval
     *
     * @returns {object} object created under a given name
     */
    get: getInterval,
    /**
     * @ngdoc method
     * @name biz.utils.intervalsFactory#cancel
     * @methodOf biz.utils.intervalsFactory
     *
     * @description
     * Stops, deletes the interval, and returns null
     *
     * @param {string} name name to be used to find the interval
     *
     * @returns {null} null
     */
    cancel: cancelInterval,
    remove: cancelInterval
  };


}]);

/*jshint undef:true*/
/*global angular*/
var cooperativeBizUtils; //Defined in 0module.js

(function (cooperativeBizUtils) {
  'use strict';

  /**
   * @ngdoc service
   * @name biz.utils.queueFactory
   *
   * @description
   * Author Andrei Sandu
   *
   * Date 09/12/17
   *
   * Service that handle async queue task.
   *
   * @requires $q
   *
   * @returns {Object} An new object instance of type Queue
   */
  cooperativeBizUtils.factory('queueFactory', queueFactory);

  queueFactory.$inject = ['$q'];

  function queueFactory($q) {

    /**
     * @class
     * @description
     *
     * Queue class
     *
     * Initializer
     *
     * @returns {Object} Newly created Queue.
     */
    var Queue = function Queue() {
      this.init();
    };

    var prototype = Queue.prototype;

    prototype.init = function () {
      this._limit = 1;
      this._queue = [];
    };

    /**
     * @ngdoc function
     * @name biz.utils.queueFactory#addInQueue
     * @methodOf biz.utils.queueFactory
     * @description
     *
     * Get a new Entity based on signature.
     *
     * @param {function} fn The method running in queue
     * @param {Object} context The context where the fn runs
     * @param {Array} args Arguments of method
     * @returns {Array} Return the added task in queue.
     */

    prototype.addInQueue = function (fn, context, args) {
      var task = [fn, context, args];
      this._queue.push(task);
      this._removeFromQueue();
      return task;
    };

    /**
     * Get the first task from _queue and remove it from _queue
     *
     * @private
     * @returns {Object} The removed task from _queue list
     */

    prototype._getTaskFromQueue = function () {
      var task = this._queue.shift();

      return {
        fn: task[0],
        context: task[1] || null,
        args: task[2] || null
      };

    };

    /**
     * Create the queue and remove the task from _queue list
     *
     * @private
     */
    prototype._removeFromQueue = function () {
      var self = this;

      if (self._limit <= 0 || self._queue.length === 0) {
        return;
      }

      self._limit--;

      var task = self._getTaskFromQueue();
      var successFn, errorFn;

      successFn = errorFn = function () {
        self._limit++;
        self._removeFromQueue();
      };

      $q.when(task.fn.apply(task.context, task.args))
        .then(successFn, errorFn);
    };

    return function () {

      return new Queue();
    };
  }
})(cooperativeBizUtils);

/**
 * Created by hiten on 24/04/2017.
 */
var cooperativeBizUtils; //Defined in 0module.js

cooperativeBizUtils.factory('transcodeFactory', ['$q', 'utilitiesApiFactory', 'processObjectFactory', function ($q, utilitiesApiFactory, processObjectFactory) {
    var transcodeFactory = {};

    transcodeFactory.encrypt = function (str, version, appSession) {
        var deferred = $q.defer();

        processObjectFactory.new(version, appSession)
            .then(function (processObject) {
                processObject.directives = {
                    transcode_encode: str
                };

                utilitiesApiFactory.postTransclude(processObject, version, appSession)
                    .then(function (data) {
                        deferred.resolve(data);
                    }).catch(function () {
                        deferred.reject('ERROR');
                    });
            }).catch(function(){
                deferred.reject('ERROR');
            });

        return deferred.promise;
    };

    transcodeFactory.decrypt = function (str, version, appSession) {
        var deferred = $q.defer();

        processObjectFactory.new(version, appSession)
            .then(function (processObject) {
                processObject.directives = {
                    transcode_decode: str
                };

                utilitiesApiFactory.postTransclude(processObject, version, appSession)
                    .then(function (data) {
                        deferred.resolve(data);
                    }).catch(function () {
                        deferred.reject('ERROR');
                    });
            }).catch(function(){
                deferred.reject('ERROR');
            });
            
        return deferred.promise;
    };

    return transcodeFactory;
}]);
var cooperativeBizWorkflow = angular.module('cooperative.biz.workflow', []);
var cooperativeBizWorkflow; //Defined in 0module.js
/**
 * @ngdoc service
 * @name dataFactory
 *
 * @description
 * Author hiten
 *
 * Date 05/28/15
 *
 * Business service for workflow
 */
cooperativeBizWorkflow.factory('workflowFactory', ['$q', 'dataFactory', 'queuesApiFactory', 'resourceApiFactory', function ($q, dataFactory, queuesApiFactory, resourceApiFactory) {
    var workflowFactory = {};

    /**
     * @ngdoc method
     * @name workflowFactory#getItems
     * @description
     * returns items in a workflow queue
     *
     * @param {string} Name of workflow stage / queue
     * @param {int} number of items to return in workflow stage / queue
     * @returns promise
     */
    workflowFactory.getItems = function(nameList, size, returnMode,queueStatusList, processObjectId, version, appSession){
        var deferred = $q.defer();

        queuesApiFactory.getByQueueNameList(nameList, size, returnMode, queueStatusList, processObjectId, version, appSession)
            .then(function(data){
                var count = 0;
                var list = [];

                for (var key in data){
                    count = count + data[key].count;
                    for (var index in data[key].results){
                        list.push(data[key].results[index]);
                    }
                }

                deferred.resolve({count: count, results: list});
            }, function(error){
                deferred.reject('Unable to retrieve queue');
            });

        return deferred.promise;
    };

    workflowFactory.cleanQueueName = function(name){
        return name.substring(name.lastIndexOf('.')+1);
    };

    workflowFactory.updateItemState = function(name, status, processObjectId, version, appSession){
        var deferred = $q.defer();

        name = workflowFactory.cleanQueueName(name);

        queuesApiFactory.updateQueueItemStatus(name, status, processObjectId, version, appSession)
            .then(function(data){
                deferred.resolve(data);
            }, function(){
                deferred.reject('Unable to update item status');
            });

        return deferred.promise;
    };

    workflowFactory.assignUserToWorkFlowItem = function(processObject, nameList, status, newAssignee, appSesson){
        if (newAssignee === null || newAssignee === undefined || newAssignee === ''){
            return workflowFactory.addProcessObjectToManyQueues (processObject, nameList, status, undefined, appSesson);
        } else {
            var deferred = $q.defer();
            dataFactory.getNewEntityObject(processObject.id, undefined, 'EntityObject_001', 'Ticket',undefined, undefined, appSesson)
                .then(function(entityObject){
                    var entityList = dataFactory.returnObjects(undefined, 'EntityObject_001', 0, 'Ticket', undefined, processObject.io);
                    if (entityList.length === 0){
                        processObject.io.push(entityObject);
                    } else {
                        entityObject = entityList[0];
                    }
                    dataFactory.getNewEntityObject(processObject.id, entityObject.instanceId, 'AssigneeObject_001', 'assignedTo', newAssignee, undefined, appSesson)
                        .then(function(assigneeObject){
                            var assigneeList = dataFactory.returnObjects(undefined, 'AssigneeObject_001', 0, 'assignedTo', undefined, processObject.io);
                            if (assigneeList.length > 0){
                                for(var i=0; i < assigneeList.length; i++){
                                    assigneeList[i].instance.active = false;
                                }
                            }

                            assigneeObject.instance.active = true;
                            processObject.io.push(assigneeObject);
                            workflowFactory.addProcessObjectToManyQueues (processObject, nameList, status, undefined, appSesson)
                                .then(function(){
                                    deferred.resolve('Queue item updated');
                                }).catch(function(){
                                    deferred.reject('Queue item update failed');
                                });
                        }).catch(function(){
                            deferred.reject('Queue item update failed');
                        });
                }).catch(function(){
                    deferred.reject('Queue item update failed');
                });

            return deferred.promise;
        }
    };

    workflowFactory.addProcessObjectToManyQueues = function(processObject, nameList, status, version, appSession){
        var deferred = $q.defer();

        var strNameList = '';

        for(var i=0; i < nameList.length; i++){
            if (strNameList !== ''){
                strNameList = strNameList + ',';
            }

            var name = workflowFactory.cleanQueueName(nameList[i]);
            strNameList = strNameList + name;
        }

        if (status === null || status === undefined){
            status = 'ready';
        }

        delete processObject.directives['request'];
        
        processObject.directives['queue_name_list'] = strNameList;
        processObject.directives['queue_item_status'] = status;
        processObject.directives['queue_uses_transaction_id'] = false;

        queuesApiFactory.enqueueQueueItem(processObject, version, appSession)
            .then(function(data){
                deferred.resolve(data);
            }, function(){
                deferred.reject('Unable to to add processObject to queue');
            });

        return deferred.promise;
    };

    workflowFactory.addProcessObjectToQueue = function(processObject, name, status, appSession){
        var nameList = [];

        name = workflowFactory.cleanQueueName(name);

        nameList.push(name);

        return workflowFactory.addProcessObjectToManyQueues(processObject, nameList, status, undefined, appSession);
    };

    return workflowFactory;
}]);

var cooperativeBizWorkflow; //Defined in 0module.js

cooperativeBizWorkflow.constant('WorkFlowStatusConstants', {
    IDLE: 'idle',
    READY:'ready',
    SELECTED:'selected' ,
    BUSY:'busy',
    CLOSED:'closed',
    DELETE: 'delete',
    ERROR: 'error'

});
var cooperativeConfig = angular.module('cooperative.config', []);
var cooperativeConfig; //Defined in 0module.js

/**
 * @ngdoc config
 * @name httpProvider
 *
 * @description
 * Author hiten
 *
 * Date 26/03/15
 *
 * Configuration that forces the angular {@link $httpProvider `$httpProvider`} to be intercepted so that it
 * performs custom CTP actions using {@link cooperativeHttpInterceptor `cooperativeHttpInterceptor`}
 */
cooperativeConfig.config(['$httpProvider', function($httpProvider) {
    $httpProvider.interceptors.push('cooperativeHttpInterceptor');
}]);
/**
 * @ngdo module
 * @name dal.core.requester
 * 
 * @description
 * 
 * Services that communicates with the CTP resource API. The resource api allows resources
 * from the CTP to be obtained. example resoureces are CDM objects NameObject_001, EmailObject_001 etc.
 * CTP Network stats can also be obtained via the resource API.
 * 
 * @author Hiten Vaghela (hiten.vaghela@gmail.com)
 * @created 28/04/15
 * 
 * 
 */
var cooperativeDalCoreRequester = angular.module('cooperative.dal.core.requester', []);
var cooperativeDalCoreRequester; //Defined in 0module.js

/**
 * @ngdoc service
 * @name dal.core.requester.noShardRequesterFactory
 *
 * @requires $q
 * @requires $log
 * @requires cooperative.provider.cooperative
 * @requires cooperative.biz.auth2.core.appUserSession
 * @requires cooperative.dal.core.requester.requesterFactory
 *
 * @description
 *
 * Helper function that is used to create the URIs for CTP Non Sharded API requests
 * and then preform the request using the computed URIs to the CTP API servers
 *
 *
 * @author Hiten Vaghela (hiten.vaghela@gmail.com)
 * @created 28/04/15
 *
 *
 */
cooperativeDalCoreRequester.factory('noShardRequesterFactory', ['$q', '$log', 'cooperative', 'appUserSession', 'requesterFactory', function ($q, $log, cooperative, appUserSession, requesterFactory) {
    var noShardRequesterFactory = {};

    var uriBuilder = function (requestURI, segmentArray, queryParamsMap, version, appSession) {

        var requestURLArray = [];

        if (requestURI === null || requestURI === undefined || requestURI === '') {
            $log.error('requestURI not provided');
            return {
                state: false,
                reason: 'requestURI not provided',
                uri: ''
            };
        } else {
            requestURLArray.push(requestURI);
        }

        if(version === null || version === undefined || version === ''){
            version = 'v_001';
        }

        requestURLArray.push(version);

        requestURLArray.push('/' + appUserSession.getUser().getSession(appSession));

        if (segmentArray !== null && segmentArray !== undefined && segmentArray.length > 0) {
            for (var i = 0; i < segmentArray.length; i++) {
                requestURLArray.push('/' + segmentArray[i]);
            }
        }

        if (queryParamsMap !== null && queryParamsMap !== undefined) {
            var queryParamsArray = [''];
            var counter = 0;

            for (var key in queryParamsMap) {
                if (counter === 0) {
                    queryParamsArray.push('?');
                } else {
                    queryParamsArray.push('&');
                }

                queryParamsArray.push(key + '=' + queryParamsMap[key]);

                counter++;
            }
            requestURLArray.push(queryParamsArray.join(''));
        }

        var requestURL = requestURLArray.join('');

        return {
            state: true,
            reason: 'success',
            uri: requestURL
        };
    };

    /**
     * @ngdoc method
     * @name GET
     * @methodOf dal.core.requester.noShardRequesterFactory
     * @description
     * Returns a promise for a No Shard GET request
     *
     * @param {string} requestURI the intial uri segement upto the CTP session. example https://localhost:36352/services/geoip/
     * @param {Array} segmentArray an Array of path segements that are required after the session Id
     * @param {object} queryParamsMap a map of all request params
     * @param {boolean} appSession a flag to state if the users authorization token (User session id) should be used or the apps authorization token (API Key)
     *
     * @returns {Promise} Future object
     *
     * @example
        <example module="cooperative.dal.core.requester.noShardRequesterFactory.GET">
            <file name="index.html">
                <div ng-controller="getCtrl"><button ng-click="getData()">CLICK TO GET</button> </div>
            </file>
            <file name="script.js">
                angular.module('cooperative.dal.core.requester.noShardRequesterFactory.GET', [ 'ui.router', 'ui.bootstrap', 'pascalprecht.translate', 'ngCookies', 'cooperative'])
                    .config(['cooperativeProvider', function (cooperativeProvider) {
                        cooperativeProvider.setBaseDomain('tsgctp');
                        cooperativeProvider.setAppName('Framework Docs');
                        cooperativeProvider.setApiKey('b60c1270-148e-46e7-8f53-cc5b02c18304');
                        cooperativeProvider.setPartition('prod');
                    }])

                    .controller('getCtrl', ['$scope', 'appUserSession', 'noShardRequesterFactory', 'authUser', function($scope, appUserSession, noShardRequesterFactory, AuthUser){
                        appUserSession.setUser(new AuthUser());
                        $scope.getData = function(){
                            var queryParamsMap = {
                                ip_address : '192.168.1.1'
                            };

                            noShardRequesterFactory.GET('https://dev1.tsgctp.org:36352/services/geoip/', [], queryParamsMap, true)
                                .then(function(data){
                                    alert("get request!\n" + JSON.stringify(data));
                                })
                                .catch(function(error){
                                    alert("get request failed!\n" + JSON.stringify(error));
                                });
                        }
                    }]);
            </file>
        </example>
     */
    noShardRequesterFactory.GET = function (requestURI, segmentArray, queryParamsMap, version, appSession) {
        var deferred = $q.defer();

        var requestURL = uriBuilder(requestURI, segmentArray, queryParamsMap, version, appSession);
        if (requestURL.state) {
            requesterFactory.GET(requestURL.uri)
                .then(function (data) {
                    deferred.resolve(data);
                })
                .catch(function (data, status, headers, config) {
                    deferred.reject(data);
                });
        }
        else {
            deferred.reject(requestURL.reason);
        }

        return deferred.promise;
    };

    /**
    * @ngdoc method
    * @name POST
    * @methodOf dal.core.requester.noShardRequesterFactory
    * @description
    * Returns a promise for a Shard POST request
    *
    * @param {string} postURI the intial uri segement upto the CTP session. example https://localhost:37632/services/queue/
    * @param {Array} segmentArray an Array of path segements that are required after the session Id
    * @param {object} queryParamsMap a map of all request params
    * @param {object} postData the data that will be posted
    * @param {string} contentType describes the data contained in the body
    * @param {string} transformRequest unique requestid
    * @param {boolean} appSession a flag to state if the users authorization token (User session id) should be used or the apps authorization token (API Key)
    *
    * @returns {Promise} Future object
    *
    * @example
    <example module="cooperative.dal.core.requester.noShardRequesterFactory.GET">
        <file name="index.html">
            NO LIVE DEMO FOR SHARD POST... IM SURE YOU CAN GUESS WHY...
        </file>
        <file name="script.js">
            angular.module('cooperative.dal.core.requester.noShardRequesterFactory.GET', [ 'ui.router', 'ui.bootstrap', 'pascalprecht.translate', 'ngCookies', 'cooperative'])
                .config(['cooperativeProvider', function (cooperativeProvider) {
                    cooperativeProvider.setBaseDomain('tsgctp');
                    cooperativeProvider.setAppName('Framework Docs');
                    cooperativeProvider.setApiKey('b60c1270-148e-46e7-8f53-cc5b02c18304');
                    cooperativeProvider.setPartition('prod');
                }])

                .controller('getCtrl', ['$scope', 'appUserSession', 'noShardRequesterFactory', 'authUser', function($scope, appUserSession, noShardRequesterFactory, AuthUser){
                    appUserSession.setUser(new AuthUser());
                    $scope.getData = function(){
                        var postDataObject = {this: 'is',
                                        the: 'payload',
                                        that: 'will',
                                        be: 'posted'
                                    };

                        var postData = 'request=' + encodeURIComponent(JSON.stringify(postDataObject));

                        noShardRequesterFactory.POST('https://dev1.tsgctp.org:37632/services/queue/', [], {}, postData, undefined, undefined ,true)
                            .then(function(data){
                                alert("get request!\n" + JSON.stringify(data));
                            })
                            .catch(function(error){
                                alert("get request failed!\n" + JSON.stringify(error));
                            });
                    }
                }]);
        </file>
    </example>
    */
    noShardRequesterFactory.POST = function (postURI, segmentArray, queryParamsMap, postData, contentType, transformRequest, version, appSession) {
        var deferred = $q.defer();

        var postURL = uriBuilder(postURI, segmentArray, queryParamsMap, version, appSession);
        if (postURL.state) {
            requesterFactory.POST(postURL.uri, postData, contentType, transformRequest)
                .then(function (data) {
                    deferred.resolve(data);
                })
                .catch(function (data, status, headers, config) {
                    deferred.reject(data);
                });

        } else {
            return deferred.reject(postURL.reason);
        }

        return deferred.promise;

    };

    noShardRequesterFactory.POSTDirect = function (postURL, postData, contentType, transformRequest) {
        return requesterFactory.POST(postURL, postData, contentType, transformRequest);
    };

    noShardRequesterFactory.GETDirect = function (requestURL) {
        return requesterFactory.GET(requestURL);
    };

    return noShardRequesterFactory;
}]);

var cooperativeDalCoreRequester; //Defined in 0module.js

/**
 * @ngdoc service
 * @name dal.core.requester.requesterFactory
 * 
 * @requires $http
 * @requires $q
 * 
 * @description
 * 
 * Wrapper function around angular.$http this wrapper returns a $q promise instead of a HTTP promise, 
 * ensuring that other factories that make http requests are not relient on $http service.
 * 
 * @author Hiten Vaghela (hiten.vaghela@gmail.com)
 * @created 28/04/15
 * 
 */

cooperativeDalCoreRequester.factory('requesterFactory', ['$http', '$q', function ($http, $q) {
    var requesterFactory = {};

    /**
     * @ngdoc method
     * @name GET
     * @methodOf dal.core.requester.requesterFactory
     * @description
     * Returns a promise for a HTTP GET request
     *
     * @param {string} requestURL the url that will be posted to
     * @returns {Promise} Future object 
     * 
     * @example
        <example module="cooperative.dal.core.requester.requesterFactory.GET">
            <file name="index.html">
                <div ng-controller="getCtrl">
                    <button ng-click="getData()">CLICK TO GET</button>
                </div>
            </file>
            <file name="script.js">
                angular.module('cooperative.dal.core.requester.requesterFactory.GET', ['cooperative.dal.core.requester'])
                    .controller('getCtrl', ['$scope', 'requesterFactory' ,function($scope, requesterFactory){
                        $scope.getData = function(){
                            var promise = requesterFactory.GET('https://httpbin.org/get')
                                promise.then(function(data){
                                    alert("get request!\n" + JSON.stringify(data));
                                })
                                .catch(function(error){
                                    alert("get request failed!\n" + JSON.stringify(error));
                                });
                        }
                    }]);
            </file>
        </example>
     */
    requesterFactory.GET = function (requestURL) {
        var deferred = $q.defer();
        $http.get(requestURL)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(data);
            });
        return deferred.promise;
    };

    /**
     * @ngdoc method
     * @name POST
     * @methodOf dal.core.requester.requesterFactory
     * @description
     * Returns a promise for a HTTP POST request
     *
     * @param {string} postURL the url that will be posted to
     * @param {object} postData the data that will be posted
     * @param {string} contentType describes the data contained in the body 
     * @param {string} transformRequest unique request id
     * @returns {Promise} Future object 
     * 
     * @example
        <example module="cooperative.dal.core.requester.requesterFactory.POST">
            <file name="index.html">
                <div ng-controller="postCtrl">
                    <button ng-click="postData()">CLICK TO POST</button>
                </div>
            </file>
            <file name="script.js">
                angular.module('cooperative.dal.core.requester.requesterFactory.POST', ['cooperative.dal.core.requester'])
                    .controller('postCtrl', ['$scope', 'requesterFactory' ,function($scope, requesterFactory){
                        $scope.postData = function(){
                            var promise = requesterFactory.POST('https://httpbin.org/post',{'title': 'post data test'}, 'application/x-www-form-urlencoded')
                                
                                promise.then(function(data){
                                    alert("data posted!\n" + JSON.stringify(data));
                                })
                                .catch(function(error){
                                    alert("data post failed!\n" + JSON.stringify(error));
                                });
                        }
                    }]);
            </file>
        </example>
     */
    requesterFactory.POST = function (postURL, postData, contentType, transformRequest) {
        var deferred = $q.defer();

        if (contentType === null || contentType === undefined) {
            contentType = 'application/x-www-form-urlencoded';
        } else if (contentType === 'nil') {
            contentType = undefined;
        }

        var postOptions = {
            method: 'POST',
            url: postURL,
            data: postData,
            headers: { 'Content-Type': contentType }
        };

        if (transformRequest !== null && transformRequest !== undefined) {
            postOptions['transformRequest'] = transformRequest;
        }

        $http(postOptions)
            .success(function (data) {
                deferred.resolve(data);
            })
            .error(function (data, status, headers, config) {
                deferred.reject(data);
            });


        return deferred.promise;

    };

    return requesterFactory;
}]);
var cooperativeDalCoreRequester; //Defined in 0module.js

/**
 * @ngdoc service
 * @name dal.core.requester.shardRequesterFactory
 * 
 * @requires $q
 * @requires $log
 * @requires cooperative.provider.cooperative
 * @requires cooperative.biz.auth2.core.appUserSession
 * @requires cooperative.dal.core.requester.requesterFactory
 * 
 * @description
 * 
 * Helper function that is used to create the URIs for CTP Sharded API requests
 * and then preform the request using the computed URIs to the CTP API servers
 *
 * 
 * @author Hiten Vaghela (hiten.vaghela@gmail.com)
 * @created 28/04/15
 * 
 * 
 */

cooperativeDalCoreRequester.factory('shardRequesterFactory', ['$q', '$log', 'cooperative', 'appUserSession', 'requesterFactory', function($q, $log, cooperative, appUserSession, requesterFactory) {
    var shardRequesterFactory = {};
    
    var uriBuilder = function(requestURI, shard, partition, mode, segmentArray, queryParamsMap, appSession) {
        var requestURLArray = [];

        if (requestURI === null || requestURI === undefined || requestURI === '') {
            $log.error('requestURI not provided');
            return {
                state: false,
                reason: 'requestURI not provided',
                uri: ''
            };
        } else {
            requestURLArray.push(requestURI);
        }

        if (shard === null || shard === undefined || shard === '') {
            $log.error('shard not provided');
            return {
                state: false,
                reason: 'shard not provided',
                uri: ''
            };
        } else {
            requestURLArray.push(shard);
        }

        if (partition === null || partition === undefined || partition === '') {
            partition = cooperative.partition;
        }

        requestURLArray.push(':' + partition);

        if (mode === null || mode === undefined || mode === '') {
            mode = 'asynch';
        }

        requestURLArray.push(':' + mode);
       
        requestURLArray.push('/' + appUserSession.getUser().getSession(appSession));
        if (segmentArray !== null && segmentArray !== undefined && segmentArray.length > 0){
            for (var i =0; i < segmentArray.length; i++){
                requestURLArray.push('/' + segmentArray[i]);
            }
        }

        if (queryParamsMap !== null && queryParamsMap !== undefined) {
            var queryParamsArray = [''];
            var counter = 0;

            for (var key in queryParamsMap) {
                if (counter === 0){
                    queryParamsArray.push('?');
                } else {
                    queryParamsArray.push('&');
                }

                queryParamsArray.push(key + '=' + queryParamsMap[key]);

                counter++;
            }
            requestURLArray.push(queryParamsArray.join(''));
        }

        var requestURL = requestURLArray.join('');
        return {
            state: true,
            reason: 'success',
            uri: requestURL
        };
    };

    /**
     * @ngdoc method
     * @name GET
     * @methodOf dal.core.requester.shardRequesterFactory
     * @description
     * Returns a promise for a Shard GET request
     *
     * @param {string} requestURI the intial uri segement upto the CTP session. example https://localhost:36864/services/object/
     * @param {string} shard shard name for the request
     * @param {string} partition partion name for the request
     * @param {string} mode request mode (synch or asynch)
     * @param {Array} segmentArray an Array of path segements that are required after the session Id
     * @param {object} queryParamsMap a map of all request params
     * @param {boolean} appSession a flag to state if the users authorization token (User session id) should be used or the apps authorization token (API Key)
     * 
     * @returns {Promise} Future object 
     * 
     * @example
        <example module="cooperative.dal.core.requester.shardRequesterFactory.GET">
            <file name="index.html">
                <div ng-controller="getCtrl"><button ng-click="getData()">CLICK TO GET</button> </div>
            </file>
            <file name="script.js">
                angular.module('cooperative.dal.core.requester.shardRequesterFactory.GET', [ 'ui.router', 'ui.bootstrap', 'pascalprecht.translate', 'ngCookies', 'cooperative'])
                    .config(['cooperativeProvider', function (cooperativeProvider) {
                        cooperativeProvider.setBaseDomain('tsgctp');
                        cooperativeProvider.setAppName('Framework Docs');
                        cooperativeProvider.setApiKey('b60c1270-148e-46e7-8f53-cc5b02c18304');
                        cooperativeProvider.setPartition('prod');
                    }])
                    
                    .controller('getCtrl', ['$scope', 'appUserSession', 'shardRequesterFactory', 'authUser', function($scope, appUserSession, shardRequesterFactory, AuthUser){
                        appUserSession.setUser(new AuthUser());
                        $scope.getData = function(){
                            shardRequesterFactory.GET('https://dev1.tsgctp.org:36864/services/object/', 'object', 'prod', 'synch', ['5870210280560_85f7','any', 'all'], {}, true)
                                .then(function(data){
                                    alert("get request!\n" + JSON.stringify(data));
                                })
                                .catch(function(error){
                                    alert("get request failed!\n" + JSON.stringify(error));
                                });
                        }
                    }]);
            </file>
        </example>
     */
    shardRequesterFactory.GET = function(requestURI, shard, partition, mode, segmentArray, queryParamsMap, appSession) {
        var deferred = $q.defer();
        var requestURL = uriBuilder(requestURI, shard, partition, mode, segmentArray, queryParamsMap, appSession);
        if (requestURL.state) {
            requesterFactory.GET(requestURL.uri)
                .then(function(data) {
                    deferred.resolve(data);
                })
                .catch(function(data, status, headers, config) {
                    deferred.reject(data);
                });
        }
        else {
            return deferred.reject(requestURL.reason);
        }

        return deferred.promise;
    };

        /**
     * @ngdoc method
     * @name POST
     * @methodOf dal.core.requester.shardRequesterFactory
     * @description
     * Returns a promise for a Shard POST request
     *
     * @param {string} postURI the intial uri segement upto the CTP session. example https://localhost:36864/services/object/
     * @param {string} shard shard name for the request
     * @param {string} partition partion name for the request
     * @param {string} mode request mode (synch or asynch)
     * @param {Array} segmentArray an Array of path segements that are required after the session Id
     * @param {object} queryParamsMap a map of all request params
     * @param {object} postData the data that will be posted
     * @param {string} contentType describes the data contained in the body
     * @param {string} transformRequest unique requestid
     * @param {boolean} appSession a flag to state if the users authorization token (User session id) should be used or the apps authorization token (API Key)
     * 
     * @returns {Promise} Future object 
     * 
     * @example
        <example module="cooperative.dal.core.requester.shardRequesterFactory.GET">
            <file name="index.html">
                NO LIVE DEMO FOR SHARD POST... IM SURE YOU CAN GUESS WHY...
            </file> 
            <file name="script.js">
                angular.module('cooperative.dal.core.requester.shardRequesterFactory.GET', [ 'ui.router', 'ui.bootstrap', 'pascalprecht.translate', 'ngCookies', 'cooperative'])
                    .config(['cooperativeProvider', function (cooperativeProvider) {
                        cooperativeProvider.setBaseDomain('tsgctp');
                        cooperativeProvider.setAppName('Framework Docs');
                        cooperativeProvider.setApiKey('b60c1270-148e-46e7-8f53-cc5b02c18304');
                        cooperativeProvider.setPartition('prod');
                    }])
                    
                    .controller('getCtrl', ['$scope', 'appUserSession', 'shardRequesterFactory', 'authUser', function($scope, appUserSession, shardRequesterFactory, AuthUser){
                        appUserSession.setUser(new AuthUser());
                        $scope.getData = function(){
                            var postDataObject = {this: 'is',
                                            the: 'payload',
                                            that: 'will',
                                            be: 'posted'
                                        };

                            var postData = 'request=' + encodeURIComponent(JSON.stringify(postDataObject));

                            shardRequesterFactory.POST('https://dev1.tsgctp.org:36864/services/object/', 'object', 'prod', 'synch', [], {}, postData, undefined, undefined ,true)
                                .then(function(data){
                                    alert("get request!\n" + JSON.stringify(data));
                                })
                                .catch(function(error){
                                    alert("get request failed!\n" + JSON.stringify(error));
                                });
                        }
                    }]);
            </file>
        </example>
     */
    shardRequesterFactory.POST = function(postURI, shard, partition, mode, segmentArray, queryParamsMap, postData, contentType, transformRequest, appSession) {
        var deferred = $q.defer();

        var postURL = uriBuilder(postURI, shard, partition, mode, segmentArray, queryParamsMap, appSession);
        if (postURL.state) {
            requesterFactory.POST(postURL.uri, postData, contentType, transformRequest)
                .then(function(data) {
                    deferred.resolve(data);
                })
                .catch(function(data, status, headers, config) {
                    deferred.reject(data);
                });

        } else {
            deferred.reject(postURL.reason);
        }

        return deferred.promise;

    };

    return shardRequesterFactory;
}]);
var cooperativeDalCore = angular.module('cooperative.dal.core', []);
/**
 * Created by hiten on 31/07/15.
 */
var cooperativeDalCore; //Defined in 0module.js

/**
 * @ngdo service
 * @name dataApiFactory
 *
 * @description
 * Author hiten
 *
 * Date 29/04/15
 *
 * Core service for getting common data object content.
 */
cooperativeDalCore.factory('artifactApiFactory', ['cooperativeArtifact', 'noShardRequesterFactory', function (cooperativeArtifact, noShardRequesterFactory) {
    var artifactApiFactory = {};

    artifactApiFactory.postFile = function(processObjectId, formData, version, appSession){
        var queryParamsMap = {
            entity_id: processObjectId,
            context_id: processObjectId
        };

        return noShardRequesterFactory.POST(cooperativeArtifact.artifactURL, [], queryParamsMap, formData, 'nil', angular.identity, version, appSession);
    };

    artifactApiFactory.getFile = function(processObjectId, fileName, version, appSession){
        var queryParamsMap = {
            entity_id: processObjectId,
            artifact_id: fileName
        };

        return noShardRequesterFactory.GET(cooperativeArtifact.artifactURL, [], queryParamsMap, version, appSession);
    };

    return artifactApiFactory;
}]);
var cooperativeDalCore; //Defined in 0module.js

/**
 * @ngdo service
 * @name authApiFactory
 *
 * @description
 * Author hiten
 *
 * Date 28/04/15
 *
 * Core service for authentication and authorization
 */
cooperativeDalCore.factory('authApiFactory', ['cooperativeAuth', 'noShardRequesterFactory', function (cooperativeAuth, noShardRequesterFactory) {
    var authApiFactory = {};

    /**
     * @ngdo method
     * @name authApiFactory#postWithCredentials
     * @description
     * Returns the results of the login attempt
     *
     * @param {string} userData
     * @returns {}
     */
    authApiFactory.postWithCredentials = function(userData){
        var postData = 'requestLogin=' + encodeURIComponent(JSON.stringify(userData));
        return noShardRequesterFactory.POSTDirect(cooperativeAuth.authURL, postData);
    };

    /**
     * @ngdo method
     * @name authApiFactory#getRoles
     * @description
     * Returns the roles the logged in user has assigned
     *
     * @param {string} sessionId the session id for the logged in user
     * @returns {Array}
     */
    authApiFactory.getRoles = function(sessionId){
        var authURL = cooperativeAuth.accountMgmtURL + 'v_001/' + sessionId;
        return noShardRequesterFactory.GETDirect(authURL);
    };

    /**
     * @ngdo method
     * @name authApiFactory#createAuthObject
     * @description
     * Returns status on creation of auth object
     *
     * @param {object} processObject containing auth creation data
     * @returns {Array}
     */
    authApiFactory.postAuthUpdate = function(processObject, version, appSession){
        var postData = 'request=' + encodeURIComponent(JSON.stringify(processObject));

        return noShardRequesterFactory.POST(cooperativeAuth.accountMgmtURL, [], {}, postData, null, null, version, appSession);
    };

    authApiFactory.getPermissionsByUserName = function(username, version, appSession){
        var queryParamsMap = {
            auth_object : username
        };

        return noShardRequesterFactory.GET(cooperativeAuth.accountMgmtURL, [], queryParamsMap, version, appSession);
    };

    authApiFactory.getUserIdByOrgId = function(orgProcessObjectId, version, appSession){
        var queryParamsMap = {
            org_id : orgProcessObjectId
        };

        return noShardRequesterFactory.GET(cooperativeAuth.accountMgmtURL, [], queryParamsMap, version, appSession);
    };

    authApiFactory.logout = function(sessionId){
        var authURL = cooperativeAuth.accountMgmtURL + 'v_001/' + sessionId + '?action=logout';
        return noShardRequesterFactory.GETDirect(authURL);
    };

    authApiFactory.postPermissionCheck = function(processObject, version, appSession){
        var postData = 'request=' + encodeURIComponent(JSON.stringify(processObject));

        return noShardRequesterFactory.POST(cooperativeAuth.accountMgmtURL, [], {}, postData, null, null, version, appSession);
    };

    authApiFactory.sessionCheck = function(sessionId, version, appSession){
        var queryParamsMap = {
            auth_session_time : sessionId
        };

        return noShardRequesterFactory.GET(cooperativeAuth.accountMgmtURL, [], queryParamsMap, version, appSession);
    };

    authApiFactory.getIdsByContext = function(sessionId, context, version, appSession){
        var queryParamsMap = {
            auth_context : context,
            auth_session: sessionId
        };

        return noShardRequesterFactory.GET(cooperativeAuth.accountMgmtURL, [], queryParamsMap, version, appSession);
    };

    authApiFactory.accountReset = function(username, version, appSession){
        var queryParamsMap = {
            auth_identity : username
        };

        return noShardRequesterFactory.GET(cooperativeAuth.accountMgmtResetURL, [], queryParamsMap, version, appSession);
    };

    authApiFactory.authServiceContext = function(authContext, authServiceKey, authServiceValue, version, appSession){
        var queryParamsMap = {
            auth_service_context : authContext,
            auth_service_field : 'nil',
            auth_service_value : 'nil'
        };

        if (authServiceKey !== undefined && authServiceKey !== null && authServiceKey !== ''){
            queryParamsMap.auth_service_field = authServiceKey;
        }

        if (authServiceValue !== undefined && authServiceValue !== null && authServiceValue !== ''){
            queryParamsMap.auth_service_value = authServiceValue;
        }

        return noShardRequesterFactory.GET(cooperativeAuth.accountMgmtURL, [], queryParamsMap, version, appSession);
    };

    return authApiFactory;
}]);
/**
 * Created by hiten on 09/06/15.
 */
var cooperativeDalCore; //Defined in 0module.js
cooperativeDalCore.factory('constraintsApiFactory', ['cooperativeConstraints', 'shardRequesterFactory', function (cooperativeConstraints, shardRequesterFactory) {
    var constraintsApiFactory = {};

    constraintsApiFactory.getConstraint = function(shard, partition, mode, processObjectId, constraint, appSession){
        var constraintsList = [];
        constraintsList.push(constraint);

        return constraintsApiFactory.getConstraints(shard, partition, mode, processObjectId, constraintsList, appSession);
    };

    constraintsApiFactory.getConstraints = function(shard, partition, mode, processObjectId, constraintsList, appSession){
        var constraintsListStr = '';
        for (var i=0; i < constraintsList.length; i++){
            constraintsListStr = constraintsListStr + constraintsList[i];
            if (i < constraintsList.length - 1){
                constraintsListStr = constraintsListStr + ',';
            }
        }

        var queryParamsMap = {
            constraint_id : constraintsListStr,
            org_id: processObjectId
        };

        return shardRequesterFactory.GET(cooperativeConstraints.constraintsURL, shard, partition, mode, [], queryParamsMap, appSession);
    };

    return constraintsApiFactory;
}]);

var cooperativeDalCore; //Defined in 0module.js

/**
 * @ngdo service
 * @name contentApiFactory
 *
 * @description
 * Author hiten
 *
 * Date 29/04/15
 *
 * Core service for getting common application content.
 */
cooperativeDalCore.factory('contentApiFactory', ['cooperativeContent', 'shardRequesterFactory', function (cooperativeContent, shardRequesterFactory) {
    var contentApiFactory = {};

    /**
     * @ngdo method
     * @name contentApiFactory#getTranslation
     * @description
     * Returns the translations for a particular language of an application.  The translations
     * are entered through the CTP Admin application.
     *
     * @param {string} appName application name
     * @param {string} lang language code of the language being requested
     * @returns {Array}
     */
    contentApiFactory.getTranslation = function(appName, lang){
        if (lang === undefined || lang === null){
            lang = 'en';
        }

        var queryParamsMap = {
            lang: lang
        };

        return shardRequesterFactory.GET(cooperativeContent.contentURL, 'content', null, null, ['translations', appName], queryParamsMap, true);
    };

    /**
     * @ngdo method
     * @name contentApiFactory#getKeyList
     * @description
     * Returns the list of keys for a key code of an application.  This is the low level call and the
     * most applications will call the {@link LookupFactory#getKeyValueList `LookupFactory.getKeyValueList`} that handles promises instead.
     * The key lists are created using the CTP Admin application.
     *
     * A sample of the data returned for website types: ["main", "blog", "other"]
     *
     * @param {string} keyListName name of the key list
     * @param {string} appName application name
     * @returns {Array}
     */
    contentApiFactory.getKeyList = function (keyListName, appName){
        var queryParamsMap = {
            listid: keyListName
        };

        return shardRequesterFactory.GET(cooperativeContent.contentURL, 'content', null, null, ['keylists', appName], queryParamsMap, true);
    };

    /**
     * @ngdo method
     *
     * @name contentApiFactory#getKeyValueList
     * @description
     * Returns the list of keys and values for a key code of an application.    This is the low level call and the
     * most applications will call the {@link LookupFactory#getKeyValueList `LookupFactory.getKeyValueList`} that handles promises instead.
     * The key value lists are created using the CTP Admin application.
     *
     * A sample of the data returned for primary activity types: [{"key": "primaryActivity1","value": "1"},{"key":"primaryActivity2","value": "2"}]
     *
     * @param {string} keyValueListName name of the key value list
     * @param {string} appName application name
     * @returns {Array}
     */
    contentApiFactory.getKeyValueList = function (keyValueListName, appName){
        var queryParamsMap = {
            listid: keyValueListName
        };
        return shardRequesterFactory.GET(cooperativeContent.contentURL, 'content', null, null, ['keyvaluelists', appName], queryParamsMap, true);
    };

    return contentApiFactory;
}]);
var cooperativeDalCore; //Defined in 0module.js

/**
 * @ngdo service
 * @name dataApiFactory
 *
 * @description
 * Author hiten
 *
 * Date 29/04/15
 *
 * Core service for getting common data object content.
 */
cooperativeDalCore.factory('dataApiFactory', ['cooperativeData', 'shardRequesterFactory', function (cooperativeData, shardRequesterFactory) {
    var dataApiFactory = {};

    /**
     * @ngdo method
     * @name dataApiFactory#getProcessObject
     * @description
     * Returns the process object by process object id
     *
     * @param {string} processObjectId process id for the process object
     * @returns {Object}
     */
    dataApiFactory.getProcessObject = function(shard, partition, mode, processObjectId, appSession){
        return shardRequesterFactory.GET(cooperativeData.dataURL, shard, partition, mode, [processObjectId], {}, appSession);
    };

    dataApiFactory.updateProcessObject = function(shard, partition, mode, processObject, appSession){
        var postData = 'request=' + encodeURIComponent(JSON.stringify(processObject));

        return shardRequesterFactory.POST(cooperativeData.dataURL, shard, partition, mode, [], {}, postData, null, null, appSession);
    };

    return dataApiFactory;
}]);
var cooperativeDalCore; //Defined in 0module.js

/**
 * @ngdo service
 * @name resourceApiFactory
 *
 * @description
 * Author hiten
 *
 * Date 29/04/15
 *
 * Core service for processing common application resources.
 */
cooperativeDalCore.factory('geoipApiFactory', ['cooperativeGeoip', 'noShardRequesterFactory', function (cooperativeGeoip, noShardRequesterFactory) {
    var geoipApiFactory = {};

    geoipApiFactory.getByIp = function(ipAddress, version, appSession){
        var queryParamsMap = {
            ip_address : ipAddress
        };

        return noShardRequesterFactory.GET(cooperativeGeoip.geoipURL, [], queryParamsMap, version, appSession);
    };

    geoipApiFactory.getByKeyword = function(queryKeywords, version, appSession){
        var queryParamsMap = {
            query_keywords : queryKeywords
        };

        return noShardRequesterFactory.GET(cooperativeGeoip.geoipURL, [], queryParamsMap, version, appSession);
    };

    return geoipApiFactory;
}]);
var cooperativeDalCore; //Defined in 0module.js

/**
 * @ngdo service
 * @name objectsApiFactory
 *
 * @description
 * Author hiten
 *
 * Date 29/04/15
 *
 * Core service for getting and updating process objects.
 */
cooperativeDalCore.factory('objectsApiFactory', ['cooperativeObjects', 'shardRequesterFactory', function (cooperativeObjects, shardRequesterFactory) {
    var objectsApiFactory = {};

    /**
     * @ngdo method
     * @name objectsApiFactory#get
     * @description
     * Returns the process object with all items in the signature list
     *
     * @param {string} processObjectId process id for the process object
     * @param {string} parentInstanceId instance id of the parent
     * @param {string} signatureList list of signatures requested for this process object
     *
     * @returns {ProcessObject} process object
     */
    objectsApiFactory.get = function(shard, partition, mode, processObjectId, parentInstanceId, signatureList, appSession){
        if (parentInstanceId === undefined || parentInstanceId === null || parentInstanceId === ''){
            parentInstanceId = 'any';
        }

        var strSignatureList = '';

        for (var i=0; i < signatureList.length; i++){
            if (strSignatureList !== ''){
                strSignatureList = strSignatureList + ',';
            }

            strSignatureList = strSignatureList + signatureList[i];
        }

        return shardRequesterFactory.GET(cooperativeObjects.objectURL, shard, partition, mode, [processObjectId, parentInstanceId, strSignatureList], {}, appSession);
    };

    /**
     * @ngdo method
     * @name objectsApiFactory#post
     * @description
     * Posts the process object
     *
     * @param {ProcessObject} processObjectId process id for the process object
     *
     * @returns {HttpPromise}
     */
    objectsApiFactory.post = function(shard, partition, mode, processObject, appSession){
        var postData = 'request=' + encodeURIComponent(JSON.stringify(processObject));

        return shardRequesterFactory.POST(cooperativeObjects.objectURL, shard, partition, mode, [], {}, postData, null, null, appSession);
    };

    return objectsApiFactory;
}]);
var cooperativeDalCore; //Defined in 0module.js

/**
 * @ngdoc service
 * @name dal.core.outreachApiFactory
 * 
 * @requires cooperative.provider.cooperativeOutreach
 * @requires cooperative.dal.core.requester.noShardRequesterFactory
 * 
 * @description
 * 
 * 
 * @author Hiten Vaghela (hiten.vaghela@gmail.com)
 * @created 02/03/2017
 * 
 * 
 */
cooperativeDalCore.factory('outreachApiFactory', ['cooperativeOutreach', 'noShardRequesterFactory', function (cooperativeOutreach, noShardRequesterFactory) {
    var outreachApiFactory = {};

    outreachApiFactory.send = function (email, template, processObjectId, version, appSession) {
        var segments = [];

        segments.push(email);
        segments.push(template);
        
        if (processObjectId !== null && processObjectId !== undefined && processObjectId !== ''){
            segments.push(processObjectId);
        }

        return noShardRequesterFactory.GET(cooperativeOutreach.outreachURL, segments, {}, version, appSession);
    };

    outreachApiFactory.sendToList = function(emailList, template, processObjectId, version, appSession){
        
        var emailListStr = '';
        for (var i = 0; i < emailList.length; i++) {
            emailListStr = emailListStr + emailList[i];
            if (i < emailList.length - 1) {
                emailListStr = emailListStr + ',';
            }
        }

        outreachApiFactory.send(emailListStr, template, processObjectId, version, appSession);
    };

    return outreachApiFactory;
}]);
var cooperativeDalCore; //Defined in 0module.js

/**
 * @ngdo service
 * @name queriesApiFactory
 *
 * @description
 * Author hiten
 *
 * Date 29/04/15
 *
 * Core service for querying CTP objects.
 */
cooperativeDalCore.factory('queriesApiFactory', ['cooperativeQueries', 'shardRequesterFactory', function (cooperativeQueries, shardRequesterFactory) {
    var queriesApiFactory = {};

    /**
     * @ngdo method
     * @name queriesApiFactory#search
     * @description
     * Returns objects based on search criteria
     *
     * @param {string} queryString the query to search on
     * @param {string} from starting index of data to return
     * @param {string} size number of items to return
     * @param {string} returnMode whether or not to return the actual data or just see if it exists
     *
     * @returns {HttpPromise}
     *
     * @deprecated Please use queriesApiFactory.query, this method is deprecated
     */
    queriesApiFactory.search = function(shard, partition, mode, queryString, from, size, returnMode, appSession){
        if (from === undefined || from === null){
            from = 0;
        }

        if (size === undefined || size === null){
            size = 20;
        }

        if (returnMode === undefined || returnMode === null){
            returnMode = true;
        }

        var queryParamsMap = {
            query_string : queryString,
            size : size,
            from : from,
            return_mode : returnMode
        };

        return shardRequesterFactory.GET(cooperativeQueries.queriesURL, shard, partition, mode, [], queryParamsMap, appSession);
    };

    queriesApiFactory.query = function(shard, partition, mode, processObject, appSession){
        var postData = 'request=' + encodeURIComponent(JSON.stringify(processObject));

        return shardRequesterFactory.POST(cooperativeQueries.queriesURL, shard, partition, mode, [], {}, postData, null, null, appSession);
    };

    queriesApiFactory.dupCheck = function(shard, partition, mode, processObject, appSession){
        var postData = 'request=' + encodeURIComponent(JSON.stringify(processObject));

        return shardRequesterFactory.POST(cooperativeQueries.duplCheckURL, shard, partition, mode, [], {}, postData, null, null, appSession);
    };

    queriesApiFactory.trigramQuery = function(shard, partition, mode, processObject, appSession){
        var postData = 'request=' + encodeURIComponent(JSON.stringify(processObject));

        return shardRequesterFactory.POST(cooperativeQueries.trigramURL, shard, partition, mode, [], {}, postData, null, null, appSession);
    };

    queriesApiFactory.trilikeQuery = function(shard, partition, mode, processObject, appSession){
        var postData = 'request=' + encodeURIComponent(JSON.stringify(processObject));

        return shardRequesterFactory.POST(cooperativeQueries.trilikeURL, shard, partition, mode, [], {}, postData, null, null, appSession);
    };

    queriesApiFactory.queryDirectory = function(shard, partition, mode, country, stateRegion, city, returnMode, appSession){

        if (returnMode === undefined || returnMode === null){
            returnMode = true;
        }

        var queryParamsMap = {
            return_mode : returnMode
        };

        if(country !== undefined && country !== null){
            queryParamsMap.directory_country = country;
        }

        if(stateRegion !== undefined && stateRegion !== null){
            queryParamsMap.directory_state_region = stateRegion;
        }

        if(city !== undefined && city !== null){
            queryParamsMap.directory_city = city;
        }

        // API does not accept this
        // if(orgName !== undefined && orgName !== null){
        //     queryParamsMap.directory_org_name = orgName;
        // }




        // var queryParamsMap = {
        //     directory_state_region : stateRegion,
        //     directory_city : city,
        //     directory_org_name : orgName,
        //     return_mode : returnMode
        // };

        return shardRequesterFactory.GET(cooperativeQueries.directoryURL, shard, partition, mode, [], queryParamsMap, appSession);
    };

    queriesApiFactory.searchTrilike = function(shard, partition, mode, queryTrilike, from, size, returnMode, appSession){
        if (from === undefined || from === null){
            from = 0;
        }

        if (size === undefined || size === null){
            size = 20;
        }

        if (returnMode === undefined || returnMode === null){
            returnMode = true;
        }

        var queryParamsMap = {
            query_trilike : queryTrilike,
            size : size,
            from : from,
            return_mode : returnMode
        };

        return shardRequesterFactory.GET(cooperativeQueries.trilikeURL, shard, partition, mode, [], queryParamsMap, appSession);
    };

    return queriesApiFactory;
}]);

var cooperativeDalCore; //Defined in 0module.js

/**
 * @ngdoc service
 * @name dal.core.queuesApiFactory
 * 
 * @requires cooperative.provider.cooperativeQueues
 * @requires cooperative.dal.core.requester.noShardRequesterFactory
 * 
 * @description
 * 
 * Services that communicates with the CTP queues API. The queues API allows read/write
 * access to the CTP queue/workflow capabilities.
 * 
 * @author Hiten Vaghela (hiten.vaghela@gmail.com)
 * @created 29/04/15
 * 
 * 
 */
cooperativeDalCore.factory('queuesApiFactory', ['cooperativeQueues', 'noShardRequesterFactory', function (cooperativeQueues, noShardRequesterFactory) {
    var queuesApiFactory = {};

    /**
     * @ngdoc method
     * @name getByQueueName
     * @methodOf dal.core.queuesApiFactory
     * @description
     * Returns a promise of a requested item in a queue. The return from a queue can be filtered by, size, 
     * queue statuses and a queue item id. 
     * 
     * The return object can be full queue item data or meta queue item data
     *
     * @param {string} queueName the name of the queue to retrieve
     * @param {string} size number of items to retrieve
     * @param {boolean} returnMode a flag to state whether return the actual data or just see if it exists in a queue
     * @param {array} queueStatusList of list of queue statuses to be included in return data
     * @param {string} processObjectId object identifer of item in queue
     * @param {boolean} appSession a flag to state if the users authorization token (User session id) should be used or the apps authorization token (API Key)
     * 
     * @returns {Promise} Future object of items in a queue
     * 
     * @example
        <example module="cooperative.dal.core.queuesApiFactory.getByQueueName">
            <file name="index.html">
                <div ng-controller="getCtrl">
                    <form>
                    <div class="form-group">
                        <label>queue name</label> 
                        <input type="text" ng-model="testdata.queueName"/>
                    </div>
                    <div class="form-group">
                        <label>size</label>
                        <input type="text" ng-model="testdata.size"/>
                    </div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" ng-model="testdata.returnMode"/>
                            return mode
                        </label>
                    </div>
                    <div class="form-group">
                        <label>statuses</label>
                        <ul>
                            <li ng-repeat="status in testdata.queueStatusList">
                                {{status}}
                            </li>
                        </ul>
                        <input type="text" ng-model="status"/>
                        <button ng-click="testdata.queueStatusList.push(status); console.log(testdata.queueStatusList)"> + add</button>
                    </div>
                    <div class="form-group">
                        <label>processObjectId</label>
                        <input type="text" ng-model="testdata.processObjectId"/>
                    </div>

                    <button ng-click="getData()">CLICK TO GET</button> 
                    </form>
                </div>
            </file>
            <file name="script.js">
                angular.module('cooperative.dal.core.queuesApiFactory.getByQueueName', [ 'ui.router', 'ui.bootstrap', 'pascalprecht.translate', 'ngCookies', 'cooperative'])
                    .config(['cooperativeProvider', function (cooperativeProvider) {
                        cooperativeProvider.setBaseDomain('tsgctp');
                        cooperativeProvider.setAppName('Framework Docs');
                        cooperativeProvider.setApiKey('b60c1270-148e-46e7-8f53-cc5b02c18304');
                        cooperativeProvider.setPartition('prod');
                    }])
                    
                    .controller('getCtrl', ['$scope', 'appUserSession', 'queuesApiFactory', 'authUser', function($scope, appUserSession, queuesApiFactory, AuthUser){
                        appUserSession.setUser(new AuthUser());
                        $scope.testdata = {
                            queueName: 'test_queue',
                            size: '20',
                            returnMode: true,
                            queueStatusList: ['ready'],
                            processObjectId: ''
                        };

                        $scope.getData = function(){
                            queuesApiFactory.getByQueueName(
                                $scope.testdata.queueName, 
                                $scope.testdata.size,
                                $scope.testdata.returnMode,
                                $scope.testdata.queueStatusList,
                                $scope.testdata.processObjectId,
                                true)
                                .then(function(data){
                                    alert("get request!\n" + JSON.stringify(data));
                                })
                                .catch(function(error){
                                    alert("get request failed!\n" + JSON.stringify(error));
                                });
                        }
                    }]);
            </file>
        </example>
    */
    queuesApiFactory.getByQueueName = function (queueName, size, returnMode, queueStatusList, processObjectId, version, appSession) {
        var queueNameList = [];
        queueNameList.push(queueName);

        return queuesApiFactory.getByQueueNameList(queueNameList, size, returnMode, queueStatusList, processObjectId, version, appSession);
    };


    /**
     * @ngdoc method
     * @name getByQueueNameList
     * @methodOf dal.core.queuesApiFactory
     * @description
     * Returns a promise of a requested item in a queue. The return from a queue can be filtered by, size, 
     * queue statuses and a queue item id. 
     * 
     * The return object can be full queue item data or meta queue item data
     *
     * @param {array} queueNameList the lists of names of queues to retrieve
     * @param {string} size number of items to retrieve
     * @param {boolean} returnMode a flag to state whether return the actual data or just see if it exists in a queue
     * @param {array} queueStatusList of list of queue statuses to be included in return data
     * @param {string} processObjectId object identifer of item in queue
     * @param {boolean} appSession a flag to state if the users authorization token (User session id) should be used or the apps authorization token (API Key)
     * 
     * @returns {Promise} Future object of items in a the requested queues
     * 
     * @example
        <example module="cooperative.dal.core.queuesApiFactory.getByQueueNameList">
            <file name="index.html">
                <div ng-controller="getCtrl">
                    <form>
                    <div class="form-group">
                        <label>Queue names</label> 
                        <ul>
                            <li ng-repeat="name in testdata.queueNameList">
                                {{name}}
                            </li>
                        </ul>
                        <input type="text" ng-model="queueName"/>
                        <button ng-click="testdata.queueNameList.push(queueName);"> + add</button>
                    </div>
                    <div class="form-group">
                        <label>size</label>
                        <input type="text" ng-model="testdata.size"/>
                    </div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" ng-model="testdata.returnMode"/>
                            return mode
                        </label>
                    </div>
                    <div class="form-group">
                        <label>statuses</label>
                        <ul>
                            <li ng-repeat="status in testdata.queueStatusList">
                                {{status}}
                            </li>
                        </ul>
                        <input type="text" ng-model="status"/>
                        <button ng-click="testdata.queueStatusList.push(status);"> + add</button>
                    </div>
                    <div class="form-group">
                        <label>processObjectId</label>
                        <input type="text" ng-model="testdata.processObjectId"/>
                    </div>

                    <button ng-click="getData()">CLICK TO GET</button> 
                    </form>
                </div>
            </file>
            <file name="script.js">
                angular.module('cooperative.dal.core.queuesApiFactory.getByQueueNameList', [ 'ui.router', 'ui.bootstrap', 'pascalprecht.translate', 'ngCookies', 'cooperative'])
                    .config(['cooperativeProvider', function (cooperativeProvider) {
                        cooperativeProvider.setBaseDomain('tsgctp');
                        cooperativeProvider.setAppName('Framework Docs');
                        cooperativeProvider.setApiKey('b60c1270-148e-46e7-8f53-cc5b02c18304');
                        cooperativeProvider.setPartition('prod');
                    }])
                    
                    .controller('getCtrl', ['$scope', 'appUserSession', 'queuesApiFactory', 'authUser', function($scope, appUserSession, queuesApiFactory, AuthUser){
                        appUserSession.setUser(new AuthUser());
                        $scope.testdata = {
                            queueNameList: ['test_queue'],
                            size: '20',
                            returnMode: true,
                            queueStatusList: ['ready'],
                            processObjectId: ''
                        };

                        $scope.getData = function(){
                            queuesApiFactory.getByQueueNameList(
                                $scope.testdata.queueNameList, 
                                $scope.testdata.size,
                                $scope.testdata.returnMode,
                                $scope.testdata.queueStatusList,
                                $scope.testdata.processObjectId,
                                true)
                                .then(function(data){
                                    alert("get request!\n" + JSON.stringify(data));
                                })
                                .catch(function(error){
                                    alert("get request failed!\n" + JSON.stringify(error));
                                });
                        }
                    }]);
            </file>
        </example>
    */
    queuesApiFactory.getByQueueNameList = function (queueNameList, size, returnMode, queueStatusList, processObjectId, version, appSession) {
        if (size === undefined || size === null) {
            size = 20;
        }

        if (queueStatusList === undefined || queueStatusList === null || queueStatusList.length === 0) {
            queueStatusList = ['ready'];
        }

        if (returnMode === undefined || returnMode === null) {
            returnMode = false;
        }

        var queueNameListStr = '';
        for (var i = 0; i < queueNameList.length; i++) {
            queueNameListStr = queueNameListStr + queueNameList[i];
            if (i < queueNameList.length - 1) {
                queueNameListStr = queueNameListStr + ',';
            }
        }

        var queueStatusListStr = '';
        for (var j = 0; j < queueStatusList.length; j++) {
            queueStatusListStr = queueStatusListStr + queueStatusList[j];
            if (j < queueStatusList.length - 1) {
                queueStatusListStr = queueStatusListStr + ',';
            }
        }

        var queryParamsMap = {
            queue_name_list: queueNameListStr,
            size: size,
            return_mode: returnMode,
            queue_item_status: queueStatusListStr

        };

        if (processObjectId !== null && processObjectId !== undefined && processObjectId !== '') {
            queryParamsMap['org_id'] = processObjectId;
        }

        return noShardRequesterFactory.GET(cooperativeQueues.queuesURL, [], queryParamsMap, version, appSession);
    };

    queuesApiFactory.updateQueueItemStatus = function (queueName, queueStatus, processObjectId, version, appSession) {
        var queryParamsMap = {
            queue_action: 'change_status',
            queue_name_list: queueName,
            queue_status: queueStatus,
            org_id: processObjectId

        };

        return noShardRequesterFactory.GET(cooperativeQueues.queuesURL, [], queryParamsMap, version, appSession);
    };

    queuesApiFactory.enqueueQueueItem = function (processObject, version, appSession) {
        var postData = 'request=' + encodeURIComponent(JSON.stringify(processObject));

        return noShardRequesterFactory.POST(cooperativeQueues.queuesURL, [], {}, postData, null, null, version, appSession);
    };

    queuesApiFactory.moveQueueItem = function (originalQueueName, newQueueName, processObjectId, version, appSession) {
        var queryParamsMap = {
            queue_action: 'rename_queue',
            orginal_queue: originalQueueName,
            new_queue: newQueueName,
            org_id: processObjectId
        };

        return noShardRequesterFactory.GET(cooperativeQueues.queuesURL, [], queryParamsMap, version, appSession);
    };

    queuesApiFactory.dequeueQueueItem = function (processObjectId, queueName, version, appSession) {
        var queryParamsMap = {
            queue_action: 'remove_queue_entry',
            queue_name_list: queueName,
            org_id: processObjectId,
        };

        return noShardRequesterFactory.GET(cooperativeQueues.queuesURL, [], queryParamsMap, version, appSession);
    };

    return queuesApiFactory;
}]);
var cooperativeDalCore; //Defined in 0module.js

/**
 * @ngdoc service
 * @name dal.core.resourceApiFactory
 * 
 * @requires cooperative.provider.cooperativeResources
 * @requires cooperative.dal.core.requester.noShardRequesterFactory
 * 
 * @description
 * 
 * Services that communicates with the CTP resource API. The resource api allows resources
 * from the CTP to be obtained. example resoureces are CDM objects NameObject_001, EmailObject_001 etc.
 * CTP network resource can also be obtained via the resource API.
 * 
 * @author Hiten Vaghela (hiten.vaghela@gmail.com)
 * @created 29/04/15
 * 
 * 
 */
cooperativeDalCore.factory('resourceApiFactory', ['cooperativeResources', 'noShardRequesterFactory', function (cooperativeResources, noShardRequesterFactory) {
    var resourceApiFactory = {};

    /**
     * @ngdoc method
     * @name getModel
     * @methodOf dal.core.resourceApiFactory
     * @description
     * Returns a promise of a requested by signature CDM instance object
     *
     * @param {string} signature a unique reference string that defines a CDM instance object
     * @param {boolean} appSession a flag to state if the users authorization token (User session id) should be used or the apps authorization token (API Key)
     * 
     * @returns {Promise} Future object (CDM instance object)
     * 
     * @example
        <example module="cooperative.dal.core.resourceApiFactory.getModel">
            <file name="index.html">
                <div ng-controller="getCtrl">
                    <input type="text" ng-model="signature"/>
                    <button ng-click="getData()">CLICK TO GET</button> 
                </div>
            </file>
            <file name="script.js">
                angular.module('cooperative.dal.core.resourceApiFactory.getModel', [ 'ui.router', 'ui.bootstrap', 'pascalprecht.translate', 'ngCookies', 'cooperative'])
                    .config(['cooperativeProvider', function (cooperativeProvider) {
                        cooperativeProvider.setBaseDomain('tsgctp');
                        cooperativeProvider.setAppName('Framework Docs');
                        cooperativeProvider.setApiKey('b60c1270-148e-46e7-8f53-cc5b02c18304');
                        cooperativeProvider.setPartition('prod');
                    }])
                    
                    .controller('getCtrl', ['$scope', 'appUserSession', 'resourceApiFactory', 'authUser', function($scope, appUserSession, resourceApiFactory, AuthUser){
                        appUserSession.setUser(new AuthUser());
                        $scope.signature = 'NameObject_001';

                        $scope.getData = function(){
                            resourceApiFactory.getModel($scope.signature, true)
                                .then(function(data){
                                    alert("get request!\n" + JSON.stringify(data));
                                })
                                .catch(function(error){
                                    alert("get request failed!\n" + JSON.stringify(error));
                                });
                        }
                    }]);
            </file>
        </example>
    */
    resourceApiFactory.getModel = function (signature, version, appSession) {
        return noShardRequesterFactory.GET(cooperativeResources.resourcesURL, ['models', 'prototype', signature], {}, version, appSession);
    };

    /**
     * @ngdoc method
     * @name getCooperative
     * @methodOf dal.core.resourceApiFactory
     * @description
     * Returns a promise of a requested CTP network resource
     *
     * @param {string} resourceName a unique reference string that defines a CTP network resource
     * @param {boolean} appSession a flag to state if the users authorization token (User session id) should be used or the apps authorization token (API Key)
     * 
     * @returns {Promise} Future object
     * 
     * @example
        <example module="cooperative.dal.core.resourceApiFactory.getCooperative">
            <file name="index.html">
                <div ng-controller="getCtrl">
                    <input type="text" ng-model="resourceName"/>
                    <button ng-click="getData()">CLICK TO GET</button> 
                </div>
            </file>
            <file name="script.js">
                angular.module('cooperative.dal.core.resourceApiFactory.getCooperative', [ 'ui.router', 'ui.bootstrap', 'pascalprecht.translate', 'ngCookies', 'cooperative'])
                    .config(['cooperativeProvider', function (cooperativeProvider) {
                        cooperativeProvider.setBaseDomain('tsgctp');
                        cooperativeProvider.setAppName('Framework Docs');
                        cooperativeProvider.setApiKey('b60c1270-148e-46e7-8f53-cc5b02c18304');
                        cooperativeProvider.setPartition('prod');
                    }])
                    
                    .controller('getCtrl', ['$scope', 'appUserSession', 'resourceApiFactory', 'authUser', function($scope, appUserSession, resourceApiFactory, AuthUser){
                        appUserSession.setUser(new AuthUser());
                        $scope.resourceName = 'nodes';

                        $scope.getData = function(){
                            resourceApiFactory.getCooperative($scope.resourceName, true)
                                .then(function(data){
                                    alert("get request!\n" + JSON.stringify(data));
                                })
                                .catch(function(error){
                                    alert("get request failed!\n" + JSON.stringify(error));
                                });
                        }
                    }]);
            </file>
        </example>
    */
    resourceApiFactory.getCooperative = function (resourceName, version, appSession) {
        return noShardRequesterFactory.GET(cooperativeResources.resourcesURL, ['cooperative', 'generate', resourceName], {}, version, appSession);
    };

    return resourceApiFactory;
}]);
var cooperativeDalCore; //Defined in 0module.js

/**
 * @ngdoc service
 * @name dal.core.utilitiesApiFactory
 * 
 * @requires cooperative.provider.cooperativeUtilities
 * @requires cooperative.dal.core.requester.noShardRequesterFactory
 * 
 * @description
 * 
 * Services that communicate with a variety of utilities API.
 * 
 * @author Hiten Vaghela (hiten.vaghela@gmail.com)
 * @created 04/04/75
 * 
 * 
 */
cooperativeDalCore.factory('utilitiesApiFactory', ['cooperativeUtilities', 'noShardRequesterFactory', function (cooperativeUtilities, noShardRequesterFactory) {
    var utilitiesApiFactory = {};


    /**
     * @ngdoc method
     * @name postTransclude
     * @methodOf dal.core.utilitiesApiFactory
     * @description
     * Returns a promise of a requested CTP transclude request to either encode or decode
     *
     * @param {object} processObject a processObject with defined directives. (transcode_encode or transcode_decode)
     * @param {string} version the version of API to use. Default value = 'v_001'
     * @param {boolean} appSession a flag to state if the users authorization token (User session id) should be used or the apps authorization token (API Key)
     * 
     * @returns {Promise} Future object
     * 
     * @example
        <example module="cooperative.dal.core.utilitiesApiFactory.getTranscode">
            <file name="index.html">
                <div ng-controller="getCtrl">
                    <div>
                    ENCODE  <input type="text" ng-model="transcode.encode"/>
                    </div>
                    <div>
                    DECODE <input type="text" ng-model="transcode.decode"/>
                    </div>
                    <button ng-click="encode()">ENCODE</button> 
                    <button ng-click="decode()">DECODE</button> 
                </div>
            </file>
            <file name="script.js">
                angular.module('cooperative.dal.core.utilitiesApiFactory.getTranscode', [ 'ui.router', 'ui.bootstrap', 'pascalprecht.translate', 'ngCookies', 'cooperative'])
                    .config(['cooperativeProvider', 'cooperativeResourcesProvider', 'cooperativeUtilitiesProvider', function (cooperativeProvider, cooperativeResourcesProvider, cooperativeUtilitiesProvider) {
                        
                        cooperativeProvider.setBaseDomain('tsgctp');
                        cooperativeProvider.setAppName('Framework Docs');
                        cooperativeProvider.setApiKey('b60c1270-148e-46e7-8f53-cc5b02c18304');
                        cooperativeProvider.setPartition('prod');

                        cooperativeUtilitiesProvider.setTranscodeURL('https://authex.tsgctp.org:33001/services/transcode/');
                        cooperativeResourcesProvider.setResourcesURL('https://resource.tsgctp.org/services/resource/');
                    }])
                    
                    .controller('getCtrl', ['$scope', 'appUserSession', 'resourceApiFactory', 'authUser', 'utilitiesApiFactory', function($scope, appUserSession, resourceApiFactory, AuthUser, utilitiesApiFactory){
                        appUserSession.setUser(new AuthUser());

                        $scope.transcode = {encode : '', decode:''};

                        $scope.encode = function(){
                            resourceApiFactory.getModel('processobject', 'v_001', true)
                            .then(function (processObject) {
                                processObject.directives = {
                                    transcode_encode: $scope.transcode.encode
                                };

                                utilitiesApiFactory.postTransclude(processObject, 'v_001', true)
                                    .then(function(data){
                                        $scope.transcode.decode = data;
                                    }).catch(function(){
                                        console.log('ERROR: could not post transcode request');
                                    });
                            }).catch(function(data){
                                console.log('ERROR: could not retrieve processObject');
                            });
                        };

                        $scope.decode = function(){
                            resourceApiFactory.getModel('processobject', 'v_001', true)
                            .then(function (processObject) {
                                processObject.directives = {
                                    transcode_decode: $scope.transcode.decode
                                };

                                utilitiesApiFactory.postTransclude(processObject, 'v_001', true)
                                    .then(function(data){
                                        $scope.transcode.encode = data;
                                    }).catch(function(){
                                        console.log('ERROR: could not post transcode request');
                                    });
                                
                            }).catch(function(data){
                                console.log('ERROR: could not retrieve processObject');
                            });
                        };
                    }]);
            </file>
        </example>
    */
    utilitiesApiFactory.postTransclude = function (processObject, version, appSession) {
        var postData = 'request=' + encodeURIComponent(JSON.stringify(processObject));

        return noShardRequesterFactory.POST(cooperativeUtilities.transcodeURL, [], {}, postData, null, null, version, appSession);
    };

    return utilitiesApiFactory;
}]);
var cooperativeDalWs = angular.module('cooperative.dal.ws', []);
// var cooperativeDalWs; //Defined in 0module.js
//
// /**
//  * Created by hiten on 04/05/15.
//  */
// cooperativeDalWs.constant('RWS', ReconnectingWebSocket);
//
// cooperativeDalWs.factory('wsFactory',['$rootScope', '$q', '$timeout', 'RWS', 'authSession', 'resourceApiFactory', 'cooperativeWebSocket', function($rootScope, $q, $timeout, RWS, authSession, resourceApiFactory, cooperativeWebSocket){
//     var wsFactory = {};
//     var callbacks = {};
//
//     var ws = {};
//     //var ws = new RWS(cooperativeWebSocket.webSocketURL);
//
//     wsFactory.opened = false;
//     ws.onopen = function(){
//         wsFactory.opened = true;
//         //console.log('ws opened');
//     };
//
//     ws.onmessage = function(message) {
//         listener(JSON.parse(message.data));
//     };
//
//     function listener(data){
//         var messageObj = data;
//         if (callbacks.hasOwnProperty(messageObj.messageId)){
//             if (messageObj.messageState.toLowerCase() === 'start'.toLowerCase()){
//                 callbacks[messageObj.messageId].status = 'started';
//             } else if (messageObj.messageState.toLowerCase() === 'data'.toLowerCase()){
//                 callbacks[messageObj.messageId].status = 'data-added';
//                 for (var key in messageObj.directives){
//                     if (key.indexOf('iterate') > -1){
//                         callbacks[messageObj.messageId].data.push(messageObj.directives[key]);
//                     }
//                 }
//                 $rootScope.$apply(callbacks[messageObj.messageId].callback.notify(messageObj));
//             } else if (messageObj.messageState.toLowerCase() === 'end'.toLowerCase()){
//                 $rootScope.$apply(callbacks[messageObj.messageId].callback.resolve(callbacks[messageObj.messageId].data));
//                 delete callbacks[messageObj.messageId];
//             } else {
//                 $rootScope.$apply(callbacks[messageObj.messageId].callback.reject('Error'));
//             }
//         }
//     }
//
//     var sendRequest = function(request){
//         var defer = $q.defer();
//
//         request.session = authSession.user.session;
//
//         callbacks[request.messageId] = {
//             time: new Date(),
//             callback: defer,
//             status: 'request',
//             data: []
//         };
//
//         if (!wsFactory.opened){
//             $timeout(function(){
//                 ws.send(JSON.stringify(request));
//             }, 1000);
//
//         } else {
//             ws.send(JSON.stringify(request));
//         }
//
//         return defer.promise;
//     };
//
//     function createRequestObject(dataSource, callback){
//         resourceApiFactory.getModel('WSDataObject_001')
//             .then(function (processObject){
//                 if (processObject.io.length > 0) {
//                     var resourceObject = processObject.io[0];
//                     if (resourceObject.signature == 'ResourceObject_001') {
//                         var wsDataObject = resourceObject.instance.arrayRes[0];
//                         resourceApiFactory.getModel('WSDataObject_001')
//                             .then(function (processObject) {
//                                 if (processObject.io.length > 0) {
//                                     var resourceObject = processObject.io[0];
//                                     if (resourceObject.signature == 'ResourceObject_001') {
//                                         var queryIteratorObject = resourceObject.instance.arrayRes[0];
//                                         wsDataObject.directives[dataSource] = queryIteratorObject;
//                                         callback(wsDataObject);
//                                     } else {
//                                         callback('Error');
//                                     }
//                                 } else {
//                                     callback('Error');
//                                 }
//                             });
//                     } else {
//                         callback('Error');
//                     }
//                 } else {
//                     callback('Error');
//                 }
//             });
//     }
//
//     wsFactory.get = function (dataSource, keyword, signatureList, callback){
//         createRequestObject(dataSource, function(wsDataObject){
//
//             if(wsDataObject == 'Error'){
//                 callback( $q.reject('Error') );
//             } else {
//                 wsDataObject.directives[dataSource].queryString = keyword;
//                 wsDataObject.directives[dataSource].ioOnly = true;
//                 wsDataObject.directives[dataSource].signatureList = signatureList;
//                 wsDataObject.directives[dataSource].size = 1000000;
//                 wsDataObject.state = true;
//                 wsDataObject.messageState = 'request';
//
//                 var promise = sendRequest(wsDataObject);
//
//                 callback( promise );
//             }
//
//         });
//     };
//
//     return wsFactory;
// }]);

var cooperativeDirectiveAuthPermissions = angular.module('cooperative.directive.auth.permissions', []);

var cooperativeDirectiveAuthPermissions; //Defined in 0module.js
/**
 * @ngdoc directive
 * @name ctpCheckAllConstraintsRequester
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of constraint check results against all constraints
 * @param {string} processObjectId
 * @param {string} scopeprefix The name for the returned data
 * @param {boolean} showOnSuccess If set to true it shows the data if they have the permission.
 *                  If set to false it hides the data if they have the permission.
 *                  Defaults to true.
 *
 * @returns {Array} Array of the constraint check results against all constraints
 */
cooperativeDirectiveAuthPermissions.directive('ctpRestrictByPermission', ['authorizationManagement', '$state', 'appUserSession', function (authorizationManagement, $state, appUserSession) {
    var ctpRestrictByPermission = {};
    ctpRestrictByPermission.restrict = 'EA';
    ctpRestrictByPermission.scope = true;
    ctpRestrictByPermission.priority = 100000;
    ctpRestrictByPermission.transclude = true;
    ctpRestrictByPermission.template = '<span ng-if="vm.show"><ng-transclude></ng-transclude></span>';
    ctpRestrictByPermission.controller = [function(){
        this.show = false;
        
        this.updateShow = function(show){
            this.show = show;
        };
    }];
    ctpRestrictByPermission.controllerAs = 'vm';

    ctpRestrictByPermission.link = function (scope, element, attrs, controller) {
        authorizationManagement.checkPermissions(appUserSession.getUser().getSession(), JSON.parse(attrs.permissions), undefined, true)
            .then(function (data) {
                if (attrs.showOnSuccess !== 'false') {
                    controller.updateShow(true);
                } else {
                    if (attrs.redirectTo) {
                        if (attrs.promptAuthentication === 'true') {
                            if (appUserSession.getUser().getIsAuthenticated() === false) {
                                appUserSession.getUser().gotoPlatformLogin(undefined, attrs.promptAuthenticationReason, undefined);
                            }
                        }

                        $state.go(attrs.redirectTo);
                    } else {
                        controller.updateShow(false);
                    }

                }
            }).catch(function () {
                if (attrs.showOnSuccess !== 'false') {
                    if (attrs.redirectTo) {
                        if (attrs.promptAuthentication === 'true') {
                            if (appUserSession.getUser().getIsAuthenticated() === false) {
                                appUserSession.getUser().gotoPlatformLogin(undefined, attrs.promptAuthenticationReason, undefined);
                            }
                        }
                        $state.go(attrs.redirectTo);
                    } else {
                        controller.updateShow(false);
                    }
                } else {
                    controller.updateShow(true);
                }
            });
    };

    return ctpRestrictByPermission;
}]);

var cooperativeDirectiveAuthRequest = angular.module('cooperative.directive.auth.request', []);

var cooperativeDirectiveAuthRequest; //Defined in 0module.js

cooperativeDirectiveAuthRequest.directive('ctpGetAuthUserByUsername',['legacyFactory','stateProcessFactory','accountUtils', function(legacyFactory, stateProcessFactory, accountUtils){
    var ctpGetAuthUserByUsername = {};
    ctpGetAuthUserByUsername.restrict = 'E';
    ctpGetAuthUserByUsername.scope = true;

    ctpGetAuthUserByUsername.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpGetAuthUserByUsername');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 

        accountUtils.getUserByUsername(attrs.username, undefined, true).then(function(data){
            scope[attrs.scopeprefix] = data;
            legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);
        });
    };

    return ctpGetAuthUserByUsername;
}]);
var cooperativeDirectiveAuthUser = angular.module('cooperative.directive.auth.user', []);

var cooperativeDirectiveAuthUser; //Defined in 0module.js

cooperativeDirectiveAuthUser.directive('ctpUserGetIdsByContext',['legacyFactory', 'stateProcessFactory','appUserSession', 'accountUtils', function(legacyFactory, stateProcessFactory, appUserSession, accountUtils){
    var ctpSearchPagingRequester = {};
    ctpSearchPagingRequester.restrict = 'E';
    ctpSearchPagingRequester.scope = true;

    ctpSearchPagingRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpUserGetIdsByContext');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 

        var ids = [];

        accountUtils.getIdsByContext(appUserSession.getUser().getSession(), attrs.contextType, undefined, false)
            .then(function(data) {
                for (var key in data) {
                    ids.push(key);
                }
                 scope[attrs.scopeprefix] = legacyFactory.returnData(ids, legacy);
                 legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success' ,legacy);
            }).catch(function() {
                scope[attrs.scopeprefix] = legacyFactory.returnData(ids, legacy);
                legacyFactory.returnStatus(scope[attrs.scopeprefix], 'rejected' ,legacy, 'accountUtils.getIdsByContext didn\'t return any IDs' );
        });
    };

    return ctpSearchPagingRequester;
}]);
    /**
 * Created by hiten on 02/11/2015.
 */
var cooperativeDirectiveAuthUser; //Defined in 0module.js


cooperativeDirectiveAuthUser.directive('ctpGetUserIdsByOrgId',['accountUtils', 'legacyFactory', 'stateProcessFactory', function(accountUtils, legacyFactory, stateProcessFactory){
    var ctpSearchPagingRequester = {};
    ctpSearchPagingRequester.restrict = 'E';
    ctpSearchPagingRequester.scope = true;

    ctpSearchPagingRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpGetUserIdsByOrgId');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 
        
        scope.$watchGroup([
            function() { return attrs.poid;},
            function() { return attrs.scopeprefix;},
            function() { return attrs.useAppApiKey; }
        ], function () {
            accountUtils.getUserIdByOrgId(attrs.poid, undefined, attrs.useAppApiKey)
                .then(function(data){
                    scope[attrs.scopeprefix] = legacyFactory.returnData(data, legacy);
                    legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success' ,legacy);
                });
        });

        scope[attrs.scopeprefix] = legacyFactory.returnData([], legacy);

    };

    return ctpSearchPagingRequester;
}]);

var cooperativeDirectiveConstraintsRequest = angular.module('cooperative.directive.constraints.request', []);

var cooperativeDirectiveConstraintsRequest; //Defined in 0module.js
/**
 * @ngdoc directive
 * @name ctpCheckAllConstraintsRequester
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of constraint check results against all constraints
 * @param {string} processObjectId
 * @param {string} scopeprefix The name for the returned data
 *
 * @returns {Array} Array of the constraint check results against all constraints
 */
cooperativeDirectiveConstraintsRequest.directive('ctpCheckAllConstraintsRequester',['constraintsFactory', '$rootScope', 'legacyFactory', 'stateProcessFactory', function(constraintsFactory, $rootScope, legacyFactory, stateProcessFactory){
    var ctpCheckAllConstraintsRequester = {};
    ctpCheckAllConstraintsRequester.restrict = 'E';
    ctpCheckAllConstraintsRequester.scope = true;

    ctpCheckAllConstraintsRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpCheckAllConstraintsRequester');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 

        // function to get data collection from resource api
        var getDataCollection = function () {

            scope.updateInProgress = true;
            var promise = constraintsFactory.checkAllConstraints(attrs.shard, attrs.partition, attrs.mode, attrs.poid, attrs.useAppApiKey);
            if(attrs.shard === undefined){
                legacyFactory.returnStatus(scope[attrs.scopeprefix],'rejected',legacy,'shards attribute must be defined');         
            }
            else {
                promise.then(function(datacollection){
                    scope[attrs.scopeprefix] = legacyFactory.returnData(datacollection, legacy);
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);          
                    scope.updateInProgress = false;           
                })
                .catch(function(){
                    legacyFactory.returnStatus(scope[attrs.scopeprefix], 'error', legacy); 
                });
            }
        };

        // listener to watch for changes in delay-interval value
        scope.$watchGroup([
                function() { return attrs.scopeprefix; },
                function() { return attrs.poid; },
                function() { return attrs.shard; },
                function() { return attrs.partition; },
                function() { return attrs.mode; },
                function() { return attrs.useAppApiKey; }
            ],
            function (newVals, oldVals){
                getDataCollection();
            }
        );

        $rootScope.$on('data_update_success', function(event, data){
            getDataCollection();
        });
    };
    return ctpCheckAllConstraintsRequester;
}]);
var cooperativeDirectiveConstraintsRequest; //Defined in 0module.js
/**
 * @ngdoc directive
 * @name ctpCheckAllConstraintsRequester
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of constraint check results against all constraints
 * @param {string} processObjectId
 * @param {string} scopeprefix The name for the returned data
 *
 * @returns {Array} Array of the constraint check results against all constraints
 */
cooperativeDirectiveConstraintsRequest.directive('ctpCheckConstraintsRequester',['constraintsFactory', '$rootScope', 'legacyFactory', 'stateProcessFactory', function(constraintsFactory, $rootScope, legacyFactory, stateProcessFactory){
    var ctpCheckConstraintsRequester = {};
    ctpCheckConstraintsRequester.restrict = 'E';
    ctpCheckConstraintsRequester.scope = true;

    ctpCheckConstraintsRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpCheckConstraintsRequester');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 
        
        // function to get data collection from resource api
        var getDataCollection = function () {
            scope.updateInProgress = true;
            if(attrs.name === undefined){
                legacyFactory.returnStatus(scope[attrs.scopeprefix],'rejected',legacy, 'name attribute must be defined');
            }            
            else if(attrs.shard === undefined){
                legacyFactory.returnStatus(scope[attrs.scopeprefix],'rejected',legacy, 'shard attribute must be defined');
            }
            else{
                var promise = constraintsFactory.checkConstraint(attrs.shard, attrs.partition, attrs.mode, attrs.poid, attrs.name, attrs.useAppApiKey);
                promise.then(function(datacollection){
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);
                    scope[attrs.scopeprefix] = legacyFactory.returnData(datacollection, legacy);
                    scope.updateInProgress = false;
                })
                .catch(function(){
                    scope[attrs.scopeprefix]._state.processData.setError();
                });
            }
        };

        // initial run
        //getDataCollection();

        // listener to watch for changes in delay-interval value
        scope.$watchGroup([
                function() { return attrs.scopeprefix; },
                function() { return attrs.poid; },
                function() { return attrs.name; },
                function() { return attrs.shard; },
                function() { return attrs.partition; },
                function() { return attrs.mode; },
                function() { return attrs.useAppApiKey; }
            ],
            function (newVals, oldVals){
                getDataCollection();
            }
        );

        $rootScope.$on('data_update_success', function(event, data){
            getDataCollection();
        });
    };

    return ctpCheckConstraintsRequester;
}]);
var cooperativeDirectiveConstraintsRequest; //Defined in 0module.js
/**
 * @ngdoc directive
 * @name ctpCheckAllConstraintsRequester
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of constraint check results against all constraints
 * @param {string} processObjectId
 * @param {string} scopeprefix The name for the returned data
 *
 * @returns {Array} Array of the constraint check results against all constraints
 */
cooperativeDirectiveConstraintsRequest.directive('ctpCheckConstraintsListRequester',['constraintsFactory', '$rootScope', 'legacyFactory', 'stateProcessFactory', function(constraintsFactory, $rootScope, legacyFactory, stateProcessFactory){
    var ctpCheckConstraintsRequester = {};
    ctpCheckConstraintsRequester.restrict = 'E';
    ctpCheckConstraintsRequester.scope = true;

    ctpCheckConstraintsRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpCheckConstraintsListRequester');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 

        // function to get data collection from resource api
        var getDataCollection = function () {
            
            
            scope.updateInProgress = true;
            if(attrs.constraintsList === undefined){
                legacyFactory.returnStatus(scope[attrs.scopeprefix],'rejected',legacy,'constraints list must be defined');  
            } 
            else if(attrs.shard === undefined){
                legacyFactory.returnStatus(scope[attrs.scopeprefix],'rejected',legacy,'shards attribute must be defined');
            }
            else{
                var promise = constraintsFactory.checkConstraints(attrs.shard, attrs.partition, attrs.mode, attrs.poid, JSON.parse(attrs.constraintsList), attrs.useAppApiKey);
                promise.then(function(datacollection){                    
                    scope[attrs.scopeprefix] = legacyFactory.returnData(datacollection, legacy);                
                    scope.updateInProgress = false;
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);

                })
                .catch(function(){
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'error',legacy);
                });
            }
        };

        // initial run
        //getDataCollection();

        // listener to watch for changes in delay-interval value
        scope.$watchGroup([
                function() { return attrs.scopeprefix; },
                function() { return attrs.poid; },
                function() { return attrs.name; },
                function() { return attrs.shard; },
                function() { return attrs.partition; },
                function() { return attrs.mode; },
                function() { return attrs.useAppApiKey; }
            ],
            function (newVals, oldVals){
                getDataCollection();
            }
        );

        $rootScope.$on('data_update_success', function(event, data){
            getDataCollection();
        });
    };

    return ctpCheckConstraintsRequester;
}]);
var cooperativeDirectiveContentActivityCode = angular.module('cooperative.directive.content.activitycode', []);

var cooperativeDirectiveContentActivityCode; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpActivitycodeNteeConverter
 * @restrict A
 * @require ?ngModel
 *
 * @description
 * Author hiten
 *
 * Date 02/04/15
 *
 * Sets the value of the component to the NTEE value that is looked up
 *
 * @param {string} subActivity The value of the purpose sub activity
 * @param {string} onlyPrimary true=only the primary code will be returned
 */
cooperativeDirectiveContentActivityCode.directive('ctpActivitycodeNteeConverter',['LookupFactory', 'legacyFactory', 'stateProcessFactory', function(LookupFactory, legacyFactory, stateProcessFactory){
    var ctpActivitycodeNteeConverter = {};
    ctpActivitycodeNteeConverter.restrict = 'A';
    ctpActivitycodeNteeConverter.require = '?ngModel';

    ctpActivitycodeNteeConverter.link = function(scope, element, attrs, ngModel){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpActivitycodeNteeConverter');
        var list = {
            _state: stateProcessFactory.create(['processData'])
        };  

        LookupFactory.getKeyValueList('subToSubXrefTypes').then(function(datacollection){
            list._state.processData.setResolved();
            list = legacyFactory.returnData(datacollection, legacy);
        });

        element.attr('disabled', 'true');

        attrs.$observe('subActivity', function(){
            scope.$evalAsync(function(){
                if (list.length > 0) {
                    var nteeCode = LookupFactory.utils.getKeyForValue(list, attrs.subActivity);
                    if (attrs.onlyPrimary == 'true' && nteeCode !== '') {
                        nteeCode = nteeCode.substring(0, 1);
                    }
                    ngModel.$setViewValue(nteeCode);
                    ngModel.$render();
                }
            });
        });

    };

    return ctpActivitycodeNteeConverter;
}]);
var cooperativeDirectiveContentRequest = angular.module('cooperative.directive.content.request', []);

var cooperativeDirectiveContentRequest; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpLookupKeyListRequester
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of keys that match the given lookup name and app name.
 *
 * The app name is optional and will default to the app name setup in the project.
 *
 * @param {string=} appName The app name to search for
 *
 *                  - If the attribute is not set, it uses the default app name {@link cooperative#getters `cooperative.appName`}.
 *
 * @param {string} lookupName The lookup name to search for
 * @param {string} scopeprefix The name for the returned data
 *
 * @returns {Array} Array of the keys that matched
 */
cooperativeDirectiveContentRequest.directive('ctpLookupKeyListRequester', ['LookupFactory', 'legacyFactory', 'stateProcessFactory', function(LookupFactory, legacyFactory, stateProcessFactory){
    var ctpLookupRequester = {};
    ctpLookupRequester.restrict = 'E';
    ctpLookupRequester.scope = true;

    ctpLookupRequester.link = function(scope, element, attrs){        
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpLookupKeyListRequester');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 

        scope.$watchGroup([
            function() { return attrs.lookupName;},
            function() { return attrs.scopeprefix;},
            function() { return attrs.appName;}
        ], function () {

            LookupFactory.getKeyList(attrs.lookupName, attrs.appName)
                .then(function(datacollection){
                    scope[attrs.scopeprefix] = [];
                    scope[attrs.scopeprefix] = legacyFactory.returnData(datacollection, legacy);
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);        

                    // error handling                                 
                    if(!attrs.lookupName){ // breaks the site                                
                        legacyFactory.returnStatus(scope[attrs.scopeprefix],'rejected',legacy, 'Lookup name must be specified.'); 
                    }
                    else if(!attrs.appName){ // just a warning
                        scope[attrs.scopeprefix]._state.processData.reason = 'Attribute not set. Using the default app name {@link cooperative#getters `cooperative.appName`}.';
                    }
                })
                .catch(function(error){
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'error',legacy);
                });
        });

        scope[attrs.scopeprefix] = [];
    };

    return ctpLookupRequester;
}]);
var cooperativeDirectiveContentRequest; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpLookupKeyValueListRequester
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of objects that contain keys and values that match the given lookup name and app name.
 *
 * The app name is optional and will default to the app name setup in the project.
 *
 * @param {string=} appName The app name to search for
 *
 *                  - If the attribute is not set, it uses the default app name {@link cooperative#getters `cooperative.appName`}.
 *
 * @param {string} lookupName The lookup name to search for
 * @param {string} scopeprefix The name the returned data

 * @returns {Array} Array of the keys that matched
 */
cooperativeDirectiveContentRequest.directive('ctpLookupKeyValueListRequester', ctpLookupKeyValueListRequester, ['LookupFactory', 'legacyFactory', 'stateProcessFactory']);

function ctpLookupKeyValueListRequester(LookupFactory, legacyFactory, stateProcessFactory){

    var ctpLookupRequester = {};
    ctpLookupRequester.restrict = 'E';
    ctpLookupRequester.scope = true;

    ctpLookupRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy,'ctpLookupKeyValueListRequester');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 

        scope.$watchGroup([
            function() { return attrs.lookupName;},
            function() { return attrs.scopeprefix;},
            function() { return attrs.appName;}
         ], function () {

            LookupFactory.getKeyValueList(attrs.lookupName, attrs.appName)
                .then(function(datacollection){
                    scope[attrs.scopeprefix] = legacyFactory.returnData(datacollection, legacy);
                    scope[attrs.scopeprefix]._state = stateProcessFactory.create(['processData']);
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);
                                                
                    // error handling                                       
                    if(!attrs.lookupName){ // breaks the site                                
                        legacyFactory.returnStatus(scope[attrs.scopeprefix],'rejected',legacy, 'Lookup name must be specified.'); 
                    }
                    else if(!attrs.appName){ // just a warning
                        scope[attrs.scopeprefix]._state.processData.reason = 'Attribute not set. Using the default app name {@link cooperative#getters `cooperative.appName`}.';
                    }                       
                })
                .catch(function(error){    
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'error',legacy);
                });
        });

        scope[attrs.scopeprefix] = [];
    };

    return ctpLookupRequester;
}
var cooperativeDirectiveContentRequest; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpPurposeSelect
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of objects that contain keys and values that match the given lookup name and app name.
 *
 * The app name is optional and will default to the app name setup in the project.
 *
 * @param {string=} appName The app name to search for
 *
 *                  - If the attribute is not set, it uses the default app name {@link cooperative#getters `cooperative.appName`}.
 *
 * @param {string} lookupName The lookup name to search for
 * @param {string} scopeprefix The name the returned data

 * @returns {Array} Array of the keys that matched
 */
cooperativeDirectiveContentRequest.directive('ctpPurposeSelect', ['LookupFactory', 'legacyFactory', 'stateProcessFactory', '$compile', function(LookupFactory, legacyFactory, stateProcessFactory, $compile){
    var ctpPurposeSelect = {};
    ctpPurposeSelect.restrict = 'E';
    //ctpPurposeSelect.scope = true;
    //ctpPurposeSelect.require = 'ngModel';

    ctpPurposeSelect.scope = {
        transprefix: '@',
        ngModel: '=',
        childLink: '=',
        parentLink: '='
    };

    var singleTemplate = '<select name="singleTmpl" ' +
        'ng-model="ngModel" ' +
        'class="form-control" ' +
        'required ng-options="item.value as (transprefix + \'.\' + item.key) | translate for item in itemList">' +
        '</select>';

    var parentTemplate = '<select name="parentTmpl" ' +
        'ng-model="ngModel" ' +
        'class="form-control" ' +
        'ng-change="childLink = undefined"' +
        'required ng-options="item.value as (transprefix + \'.\' + item.key) | translate for item in itemList">' +
        '</select>';

    var childTemplate = '<select name="childTmpl" ' +
        'ng-model="ngModel" ' +
        'class="form-control" ' +
        'required ng-options="item.value as (transprefix + \'.\' + item.key) | translate for item in itemList | LookupKeyValue:\'xref\': parentLink">' +
        '</select>';


    ctpPurposeSelect.link = function(scope, element, attrs) {
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpPurposeSelect');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 

        scope.$watchGroup([
            function() { return attrs.lookupName;},
            //function() { return attrs.transprefix;},
            function() { return attrs.appName;}
        ], function () {
        LookupFactory.getKeyValueList(attrs.lookupName, attrs.appName)
            .then(function (datacollection) {
                scope.itemList = legacyFactory.returnData(datacollection, legacy);
                scope.itemList = datacollection;
                scope.itemList._state = stateProcessFactory.create(['processData']);
                legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);
                //scope.transprefix = attrs.transprefix;
                //scope.ngModel = attrs.ngModel;
                //scope.LookupFactoryUtils = LookupFactory.utils;
            })
            .catch(function(){
                legacyFactory.returnStatus(scope[attrs.scopeprefix],'error',legacy);
            });
        });
        
        if(attrs.childLink){
            element.html(parentTemplate).show();

        } else if(attrs.parentLink){
            element.html(childTemplate).show();

        } else {
            element.html(singleTemplate).show();
        }

        $compile(element.contents())(scope);


        //scope.$watch('directiveModel', function(value) {
        //
        //    scope.$parent.myModel[attrs.name]=$scope.directiveModel;
        //});
    };

    return ctpPurposeSelect;
}]);
var cooperativeDirectiveDataRequest = angular.module('cooperative.directive.data.request', []);
var cooperativeDirectiveDataRequest; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.data.request.directive:ctpAdminDataRequester
 * @restrict E
 *
 * @description
 * Created by hiten on 30/04/15.
 */
cooperativeDirectiveDataRequest.directive('ctpAdminDataRequester',['adminDataFactory', '$interval', 'legacyFactory', 'stateProcessFactory', 
function(adminDataFactory, $interval, legacyFactory, stateProcessFactory){
    
    var ctpAdminDataRequester = {};
    ctpAdminDataRequester.restrict = 'E';
    ctpAdminDataRequester.scope = true;

    ctpAdminDataRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpAdminDataRequester');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy);
        
        scope[attrs.scopeprefix] = [];
        // function to get data collection from resource api
        var getDataCollection = function () {            
            var promise = adminDataFactory.getCooperativeResourceObject(attrs.resourceName, undefined, attrs.useAppApiKey);
            promise.then(function(datacollection){
                scope[attrs.scopeprefix] = legacyFactory.returnData(datacollection, legacy);
                legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success', legacy);
            })
            .catch(function(){
                scope[attrs.scopeprefix] = legacyFactory.returnData([], legacy);
                legacyFactory.returnStatus(scope[attrs.scopeprefix], 'error', legacy);
            });
        };

        // initial run
        getDataCollection();

        // set up repeat call
        var loop = $interval(getDataCollection, attrs.delayInterval);

        // listener to watch for changes in delay-interval value
        scope.$watchGroup([
                function() { return attrs.scopeprefix; },
                function() { return attrs.resourceName; },
                function() { return attrs.delayInterval; },
                function() { return attrs.useAppApiKey; }
            ],
            function (newVals, oldVals){

                // cancel the current repeat call and set up a new one if the value changes and is at least 1 second (1000 milliseconds)
                if (parseInt(attrs.delayInterval) > 999) {
                    $interval.cancel(loop);
                    loop = $interval(getDataCollection, attrs.delayInterval);
                }
            }
        );

        element.on('$destroy', function() {
            $interval.cancel(loop);
        });
    };

    return ctpAdminDataRequester;
}]);

var cooperativeDirectiveDataRequest; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.data.request.directive:ctpDataRequester
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of matching entity objects
 *
 * @param {string} scopeprefix The name for the returned data
 * @param {string} poid The poid of the object to retrieve
 * @param {string} parentinstanceid The parent instance id of the object to retrieve
 * @param {Array} signaturelist The signature list of resources needed that are tied to the object to retrieve.<br>
 * Example signatureList: '["EntityObject_001", "NameObject_001", "DescriptiveTextObject_001"]'<br>
 * Use ["all"] for returning all that are available<br>
 *
 * @returns {Array} Array of matching process objects
 */
cooperativeDirectiveDataRequest.directive('ctpDataRequester',['dataFactory', '_', '$timeout','legacyFactory', 'stateProcessFactory', function(dataFactory, _, $timeout, legacyFactory, stateProcessFactory){
    var ctpDataRequester = {};
    ctpDataRequester.restrict = 'EA';
    ctpDataRequester.scope = true;
    

    ctpDataRequester.controller = ['$scope', '$attrs', function($scope, $attrs){
        $scope[$attrs.scopeprefix] = [];
        var legacy = legacyFactory.legacyMode($attrs.legacy, 'ctpDataRequester');
        $scope[$attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus($scope[$attrs.scopeprefix],'pending',legacy);

        this.$setScopeCollection = function(collection){
            $scope[$attrs.scopeprefix] = collection;
        };


        this.$updateItemScopeCollection = function(formRegisteredEntities){
            for (var key in formRegisteredEntities) {
                var entity = formRegisteredEntities[key].updatable;
                
                var scopePrefix;
                if(legacy){
                    scopePrefix = $scope[$attrs.scopeprefix];
                }
                else{
                    scopePrefix = $scope[$attrs.scopeprefix]._data;
                }
                
                var foundEntityIndex = _.findIndex(scopePrefix, {instanceId: entity.instanceId, signature: entity.signature, associateId: entity.associateId});
                if (foundEntityIndex > -1){
                        scopePrefix[foundEntityIndex] = entity;
                }              
            }

            $timeout(function() {
                $scope.$apply();
            });
        };

        this.$addItemsScopeCollection = function(formRegisteredEntities){
            for (var key in formRegisteredEntities){
                var entity = formRegisteredEntities[key].updatable;
                var scopePrefix;
                if(legacy){
                    scopePrefix = $scope[$attrs.scopeprefix];
                }
                else{
                    scopePrefix = $scope[$attrs.scopeprefix]._data;
                }
                var filteredEntity = _.where(scopePrefix, {instanceId: entity.instanceId});
                if (filteredEntity === undefined || filteredEntity.length === 0){                    
                    scopePrefix.push(entity);
                }
            }
        };
    }];

    ctpDataRequester.link = function(scope, element, attrs, ctrl){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpDataRequester');
        scope.$watchGroup([
                function() { return attrs.scopeprefix; },
                function() { return attrs.poid; },
                function() { return attrs.parentinstanceid; },
                function() { return attrs.signaturelist; },
                function() { return attrs.useAppApiKey; },
                function() { return attrs.shard; },
                function() { return attrs.partition; },
                function() { return attrs.mode; }
            ],
            function(){
                
                var promise = dataFactory.getBySignatureList(attrs.shard, attrs.partition, attrs.mode, attrs.poid, attrs.parentinstanceid, JSON.parse(attrs.signaturelist), attrs.useAppApiKey);
                promise.then(function(datacollection){
                    // ctrl.$setScopeCollection(datacollection);
                    scope[attrs.scopeprefix] = legacyFactory.returnData(datacollection, legacy); 
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);
                }).catch( function(){
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'error',legacy);
                });
            });
    };

    return ctpDataRequester;
}]);

var cooperativeDirectiveDataRequest; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.data.request.directive:ctpLegalIdentifierRequester
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of legal identifiers required for the given country
 *
 * @param {string} country The country code to find legal identifiers for
 * @param {string} scopeprefix The name for the returned data
 *
 * @returns {Array} Array of the legal identifiers required for the given country
 */
cooperativeDirectiveDataRequest.directive('ctpLegalIdentifierRequester', ['LegalIdentificationFactory', 'legacyFactory', 'stateProcessFactory', function (LegalIdentificationFactory, legacyFactory, stateProcessFactory) {
    var ctpLegalIdentifierRequester = {};
    ctpLegalIdentifierRequester.restrict = 'E';
    ctpLegalIdentifierRequester.scope = { country: '=', scopeprefix: '=' };

    ctpLegalIdentifierRequester.link = function (scope, element, attrs) {
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpLegalIdentifierRequester');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy);

        scope.$watchGroup([
            function () { return scope.country; }
        ],  function () {
            LegalIdentificationFactory.getLegalIds(scope.country)
                .then(function (datacollection) {
                    scope.scopeprefix = legacyFactory.returnData(datacollection, legacy);
                    legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success', legacy);
                }).catch(function (){
                    legacyFactory.returnStatus(scope[attrs.scopeprefix], 'error', legacy);
                });
        });
    };

    return ctpLegalIdentifierRequester;
}]);

var cooperativeDirectiveData = angular.module('cooperative.directive.data', []);
var cooperativeDirectiveData; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.data.directive:ctpActivityInsert
 * @restrict E
 *
 * @description
 * Author frank
 *
 * Date 30/10/15
 *
 * Transcludes all the controls necessary to insert an orgs purpose objects
 *
 * @param {Array} ioActivityData The instance object array for the entity
 * @param {string} nonprofit 'true' or 'false'
 *
 */
cooperativeDirectiveData.directive('ctpActivityInsert',['$compile', '$templateRequest', function($compile, $templateRequest){
    var ctpActivityInsert = {};
    ctpActivityInsert.restrict = 'E';

    ctpActivityInsert.scope = {
        ioActivityData: '=',
        orgType: '@',
        showNtee: '@',
        useAppApiKey: '@'
    };

    ctpActivityInsert.templateUrl = 'template/directive.data/activity_insert.html';
    ctpActivityInsert.transclude = true;

    ctpActivityInsert.controller = ['$scope', '$attrs',function($scope, $attrs){

        $scope.setup = function(typeValue) {

            if (typeValue == 'FPO') {
                $scope.npo = false;
                $scope.transPriPath = 'common.activity.forProfitPrimaryActivity';
                $scope.transSubPath = 'common.activity.forProfitSubActivity';
                $scope.prilookupName = 'forProfitPrimaryActivityTypes';
                $scope.sublookupName = 'forProfitSubActivityTypes';
            } else {
                $scope.npo = true;
                $scope.transPriPath = 'common.activity.primaryActivity';
                $scope.transSubPath = 'common.activity.subActivity';
                $scope.prilookupName = 'primaryActivityTypes';
                $scope.sublookupName = 'subActivityTypes';
            }

            if ($scope.showNtee == 'false') {
                $scope.showfour = false;
            } else {
                $scope.showfour = true;
            }
        };

        $scope.setup($scope.orgType);

    }];

    ctpActivityInsert.link = function(scope, element, attrs, exception){
        // console.log(attrs.ioActivityData);
        if(!attrs.ioActivityData) {
            exception.message = 'The attribute io-activity-data must be privided for <ctp-activity-insert>';
            throw exception;
        }

        scope.$watch('orgType',function(newValue,oldValue) {
            //This gets called when data changes.
            scope.setup(newValue);

        });
    };

    return ctpActivityInsert;
}]);

var cooperativeDirectiveData; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.data.directive:ctpActivityUpdate
 * @restrict E
 *
 * @description
 * Author frank
 *
 * Date 30/10/15
 *
 * Transcludes all the controls necessary to update an orgs purpose objects
 *
 * @param {Array} ioActivityData The instance object array for the entity
 * @param {string} nonprofit 'true' or 'false'
 *
 */
cooperativeDirectiveData.directive('ctpActivityUpdate',function(){
    var ctpActivityUpdate = {};
    ctpActivityUpdate.restrict = 'E';

    ctpActivityUpdate.scope = {
        ioActivityData: '=',
        orgType: '@',
        showNtee: '@'
    };

    ctpActivityUpdate.templateUrl = 'template/directive.data/activity_update.html';
    ctpActivityUpdate.transclude = true;

    ctpActivityUpdate.controller = ['$scope', '$attrs',function($scope, $attrs){

        $scope.setup = function(typeValue) {
            if(typeValue == 'FPO'){
                $scope.npo = false;
                $scope.prilookupName = 'forProfitPrimaryActivityTypes';
                $scope.sublookupName = 'forProfitSubActivityTypes';
                $scope.transPriPath = 'common.activity.forProfitPrimaryActivity';
                $scope.transSubPath = 'common.activity.forProfitSubActivity';
            } else {
                $scope.npo = true;
                $scope.prilookupName = 'primaryActivityTypes';
                $scope.sublookupName = 'subActivityTypes';
                $scope.transPriPath = 'common.activity.primaryActivity';
                $scope.transSubPath = 'common.activity.subActivity';
            }

            if($scope.showNtee == 'false'){
                $scope.showfour = false;
            } else {
                $scope.showfour = true;
            }
        };

        $scope.setup($scope.orgType);
    }];

    ctpActivityUpdate.link = function(scope, element, attrs, exception){

        if(!attrs.ioActivityData) {
            exception.message = 'The attribute io-activity-data must be privided for <ctp-activity-update>';
            throw exception;
        }

        scope.$watch('orgType',function(newValue,oldValue) {
            //This gets called when data changes.
            scope.setup(newValue);

        });
    };

    return ctpActivityUpdate;
});

var cooperativeDirectiveData; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.data.directive:ctpDataCreator
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 29/03/15
 *
 * Adds to the scope a new entity object based on the parameters
 *
 * @param {string} scopeprefix The name for the new object
 * @param {string} poid The poid of the new object
 * @param {string} parentinstanceid The parent instance id of the new object
 * @param {string} signature The signature of the new object
 * @param {string} type The type of the new object
 */
cooperativeDirectiveData.directive('ctpDataCreator',['dataFactory', 'legacyFactory', 'stateProcessFactory',function(dataFactory, legacyFactory, stateProcessFactory){
    var ctpDataCreator = {};
    ctpDataCreator.restrict = 'E';
    ctpDataCreator.scope = true;

    ctpDataCreator.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpDataCreator');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 

        scope.$watchGroup([
                function() {return attrs.scopeprefix;},
                function() {return attrs.poid;},
                function() {return attrs.parentinstanceid;},
                function() {return attrs.signature;},
                function() {return attrs.type;},
                function() {return attrs.typevalue;},
                function() {return attrs.useAppApiKey;}
            ],
            function(){
                if(attrs.poid !== ''){
                    var promise = dataFactory.getNewEntityObject(attrs.poid, attrs.parentinstanceid, attrs.signature, attrs.type, attrs.typevalue, undefined, attrs.useAppApiKey);
                    promise.then(function (entity) {                        
                        scope[attrs.scopeprefix] = legacyFactory.returnData(entity, legacy);
                        scope[attrs.scopeprefix]._state = stateProcessFactory.create(['processData']);
                        legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);
                    });
                }
                else{
                    scope[attrs.scopeprefix]._state = stateProcessFactory.create(['processData']);
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'rejected',legacy,'poid is == \'\' ');
                }
            });
    };

    return ctpDataCreator;
}]);

var cooperativeDirectiveData; //Defined in 0module.js

(function (cooperativeDirectiveData) {
  'use strict';

  /**
   * @ngdoc directive
   * @name directive.data.directive:ctpDataCreatorFields
   * @restrict E

   * @description
   * Author Andrei
   *
   * Date 17/07/17
   *
   * Returns an object with newEntityObject at each scopeprefix defined in fields.
   *
   * @element ctp-data-creator-fields
   * @requires biz.data.dataFactory#getNewEntityObject
   * @requires biz.state.StateProcessFactory
   * @scope
   * @param {Object|string} root Object which has mandatory properties:
   *   - {string} rootId
   *   - {string} instanceId
   *
   * @param {Array.<Object>|string} fields Array of fields which we want to create.
   *
   * *Each field should have properties:*
   *
   *  - {string} signature
   *  - {string} type
   *  - {string} scopeprefix
   *  - {Array.<Object> =} _children
   *
   *
   *  **Note:** Each child field object should have mandatory properties:
   *  - {string} signature
   *  - {string} type
   *  - {string} scopeprefix
   *
   * @param {string} scopeprefix Scope of directive
   * @param {string=} version Version
   * @param {boolean=} use-app-api-key useAppApiKey
   *
   * @example
   <example module="cooperative.directive.data.directive.ctpDataCreatorFieldsDemo">
   <file name="index.html">
   <div ng-controller="getCtrl">
    <h3>Root is added like an object</h3>
    <ctp-data-creator-fields
     root="data.rootLikeObjectIO" fields="[
     {
         signature: 'ConversationObject_001',
         type: 'reference',
         scopeprefix: 'convo',
         _children: [
             {
                 signature: 'DetailObject_001',
                 type: 'refRating',
                 typevalue:'5',
                 scopeprefix: 'refRating'
             },
             {
                 signature: 'StatusObject_001',
                 type: 'refStatus',
                 typevalue: 'true',
                 scopeprefix: 'refStatus'
             }
         ]
     }
     ]" version="v_001" use-app-api-key="true"  scopeprefix="dataFields">
      <pre>{{ dataFields | json }}</pre>
     </ctp-data-creator-fields>
     <hr/>
     <h3>Root is added like string</h3>
     <ctp-data-creator-fields
     root="{ rootId: 'DemoCtpDataCreatorFieldsRootId001', instanceId: 'DemoCtpDataCreatorFieldsInstanceId001'  }" fields="[
     {
         signature: 'ConversationObject_001',
         type: 'reference',
         scopeprefix: 'convo',
         _children: [
             {
                 signature: 'DetailObject_001',
                 type: 'refRating',
                 typevalue:'5',
                 scopeprefix: 'refRating'
             },
             {
                 signature: 'StatusObject_001',
                 type: 'refStatus',
                 typevalue: 'true',
                 scopeprefix: 'refStatus'
             }
         ]
     }
     ]" version="v_001" use-app-api-key="true"  scopeprefix="dataFields">
      <pre>{{ dataFields | json }}</pre>
     </ctp-data-creator-fields>
   </div>
   </file>
   <file name="script.js">
   var cooperativeModuleDemo = angular.module('cooperative.directive.data.directive.ctpDataCreatorFieldsDemo', ['ui.router', 'ui.bootstrap', 'ngCookies', 'cooperative']);

   cooperativeModuleDemo.config(['cooperativeProvider', function (cooperativeProvider) {
    cooperativeProvider.setBaseDomain('tsgctp');
    cooperativeProvider.setAppName('Framework Docs');
    cooperativeProvider.setApiKey('b60c1270-148e-46e7-8f53-cc5b02c18304');
    cooperativeProvider.setPartition('prod');
   }]);
   cooperativeModuleDemo.config(['cooperativeResourcesProvider', function (cooperativeResourcesProvider) {
    cooperativeResourcesProvider.setResourcesURL('https://dev1.tsgctp.org:39168/services/resource/');
   }]);

   cooperativeModuleDemo.controller('getCtrl', ['$scope', 'appUserSession', 'authUser', function ($scope, appUserSession, AuthUser) {
    appUserSession.setUser(new AuthUser());
    $scope.data = {
      rootLikeObjectIO: {
        instance: {
          active: false,
          state: true
        },
        crc: 'ead8ba37',
        timestamp: 1506081318811,
        version: 0,
        protected: false,
        type: 'refRating',
        signature: 'DetailObject_001',
        public: true,
        state: true,
        typeValue: '5',
        rootId: 'DemoCtpDataCreatorFieldsRootId_LikeObject_001',
        instanceId: 'DemoCtpDataCreatorFieldsInstanceId5754737081189_6a90',
        associateId: 'DemoCtpDataCreatorFieldsAssociateId5754737081877_c9dd',
        statustimestamp: 0
      }
    }
   }]);
   </file>
   </example>
   *
   */

  cooperativeDirectiveData.directive('ctpDataCreatorFields', ctpDataCreatorFields);
  ctpDataCreatorFields.$inject = ['stateProcessFactory', 'dataFactory'];

  function ctpDataCreatorFields(stateProcessFactory, dataFactory) {
    var ctpDataCreatorFields = {};
    ctpDataCreatorFields.restrict = 'E';
    ctpDataCreatorFields.link = link;

    function link(scope, element, attrs) {
      var _state = stateProcessFactory.create(['processData']);
      activate();

      function activate() {
        scope[attrs.scopeprefix] = _getInitialScope();
        _initWatch();
      }

      function _initWatch() {
        scope.$watchGroup([
            function () {
              return attrs.scopeprefix;
            },
            function () {
              return attrs.root;
            },
            function () {
              return attrs.fields;
            },
            function () {
              return attrs.version;
            },
            function () {
              return attrs.useAppApiKey;
            }
          ],
          function () {
            if (_hasValidAttrs(attrs)) {
              var root = scope.$eval(attrs.root);
              var fields = scope.$eval(attrs.fields);
              _getFields(root.rootId, root.instanceId, fields, attrs.version, attrs.useAppApiKey);
            }
          });
      }

      function _hasValidAttrs(attrs) {
        var valid = true;
        var requiredAttributes = ['root', 'fields', 'scopeprefix'];
        var msg = '[ctpDataCreatorFields Directive] ';

        requiredAttributes.forEach(function (requiredAttr) {
          if (!(!!attrs[requiredAttr])) {
            msg += ' ' + requiredAttr + ' attribute missing! ';
            valid = false;
          }
        });

        if (!!attrs.root) {
          var root = scope.$eval(attrs.root);
          if (root.rootId === undefined || root.instanceId === undefined) {
            valid = false;
            msg += ' root object does not have the required properties: rootId, instanceId!';
          }
        }

        if (!!attrs.fields && !Array.isArray(scope.$eval(attrs.fields))) {
          msg += ' fields are not array!';
          valid = false;
        }

        if (!valid) {
          console.error(msg);
          _state.processData.setError(msg);
        }

        return valid;

      }

      function _getInitialScope() {
        return {_state: _state};
      }

      function _isValid(field) {
        var isValid = true;
        var message = '[ctpDataCreatorFields Directive] ';

        if (!field.signature || field.signature === '') {
          message += 'Not assigned signature! ';
          isValid = false;
        }

        if (!field.type || field.type === '') {
          message += 'Not assigned type! ';
          isValid = false;
        }

        if (!field.scopeprefix || field.scopeprefix === '') {
          message += 'Not assigned scopeprefix! ';
          isValid = false;
        }

        if (!isValid) {
          _state.processData.setError(message);
          console.error(message + ' Field error: ' + JSON.stringify(field));
        }

        return isValid;
      }

      function _getFields(rootId, parentInstanceId, fields, version, useAppApiKey) {
        _state.processData.setPending();

        fields.forEach(function (field) {
          if (!_isValid(field)) {
            return false;
          }

          var promise = dataFactory.getNewEntityObject(rootId, parentInstanceId, field.signature, field.type, field.typevalue, version, useAppApiKey);
          promise.then(function (newField) {
            scope[attrs.scopeprefix][field.scopeprefix] = newField;
            _state.processData.setResolved();
            if (Array.isArray(field._children) && field._children.length > 0) {
              _getFields(rootId, newField.instanceId, field._children, version, useAppApiKey);
            }
          }).catch(function (err) {
            _state.processData.setRejected(err);
            return err;
          });
        });
      }
    }

    return ctpDataCreatorFields;
  }
})(cooperativeDirectiveData);

var cooperativeDirectiveData; //Defined in 0module.js
/**
 * @ngdoc directive
 * @name directive.data.directive:ctpFilterIo
 * @restrict E
 *
 * @description
 * Author Emiliano
 *
 * Date 23/06/17
 *
 * Returns an array of objects based on the parameters
 *
 * @param {array} items Items must be in ctp format, such as the resulting array from a ctp data requester.
 * @param {array} filters Filters to be used against the list of items. Must be an array of objects, in which each object will have the format: {type: <String>, filterList: <Array>} where 'type' is the type in the data.io array to filter by.
 *
 * Filters could come in any of these forms (or its combinations):
 *
 * 1) [{propA: 'value', propB: 'value', ..., propZ}]
 *
 * 2) [{propA: 'value', propB: ['value1', 'value2', 'value3']}]
 *
 * 3) [{propA: 'value', propB: 'value'}, {propA: 'value', propB: 'value'}]
 *
 * Where propA and propB can be any property in data.io object, included nested objects such as "instance.active"
 *
 * Each object in the filters list represents a single data io object. So if we're looking to filter for more than 1 object (such as brand and category), we should add more than 1 object in the filters as you can see in the specs.
 *
 * @param {boolean} [isPartial=false] Will define the filter criteria with partial if true or exact otherwise
 * @param {array} scopeprefix Variable to store the resulting items after filter.
 */

 cooperativeDirectiveData.directive('ctpFilterIo', ['legacyFactory', 'stateProcessFactory', function(legacyFactory, stateProcessFactory) {
  var ctpFilterIo = {};
  ctpFilterIo.restrict = 'E';

  ctpFilterIo.link = function (scope, element, attrs) {

    var data = {
      filterArray: [],
      itemsArray: null,
      isPartial: null,
      func: null
    };

    /**
     * getObjectValueFromString - Process and returns the value of the property found from given string
     *
     * @param  {object} o object to look at
     * @param  {string} s string to process
     * @return {type}     returns the value of property found from the given string
     */
    function getObjectValueFromString(o, s) {
      s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
      s = s.replace(/^\./, ''); // strip a leading dot
      var a = s.split('.');
      for (var i = 0, n = a.length; i < n; ++i) {
        var k = a[i];
        if (k in o) {
          o = o[k];
        } else {
          return;
        }
      }
      return o;
    }


    /**
     * isInArray - Returns true if the element is found in the array.
     *
     * @param  {string}  el        Item property string retrieved from the list of properties from each item
     * @param  {string}  flt       Filter string retrieved from the list of filters
     * @param  {boolean} isPartial Filters partially when true, or exact otherwise
     * @return {boolean}           Returns true if the element is found in the array
     */
    function isInArray(el, flt, isPartial) {
      return isPartial ? el.indexOf(flt) > -1 : el === flt;
    }


    /**
     * getLowrCaseOrNumber - Returns the same value, but converted into float if number, or lowercase if string
     * This is to facilitate the comparission
     *
     * @param  {any} value to process
     * @return {type} processed value
     */
    function getLowerCaseOrNumber (el) {
      if(typeof el === 'number') {
        return parseFloat(el);
      }

      if(typeof el === 'string') {
        return isNaN(el) ? el.toLowerCase() : parseFloat(el);
      }

      return el;
    }

    /**
     * atLeastOneMatch - Returns true if 1 of the elements matches the criteria
     *
     * @param  {array}   filters    List of filters match against
     * @param  {string}  el         Element to look for in the filters array
     * @param  {boolean} isPartial  If true, the match rule will be equal|contained in. If false, it'll be equal.
     * @return {boolean}            Returns boolean for element found condition
     */
    function atLeastOneMatch(filters, el, isPartial) {
      var counter = 0;

      // Unify filters. Make it an array if it isn't yet
      if(!Array.isArray(filters)) {
        filters = [filters];
      }

      filters.forEach(function(flt) {
        // VALIDATIONS

        flt = getLowerCaseOrNumber(flt);
        el = getLowerCaseOrNumber(el);

        if(typeof el !== typeof flt) {
          return;
        }

        if(el === flt) {
          counter += 1;
          return;
        }

        if(isPartial && typeof el === 'number') {
          el = el + '';
        }

        if(isPartial && typeof flt === 'number') {
          flt = flt + '';
        }

        if(!filters.length || (el && isInArray(el, flt, isPartial))) {
          counter += 1;
          return;
        }
      });

      return !!counter;
    }

    /**
     * var getFiltredCollection - Filters given array against given filters.
     *
     * @param  {array} items   Array to loop through
     * @param  {array} filters Filters to be used to remove or not elements from the resulting array
     * @return {array}         Final filtred array
     */
    var getFiltredCollection = function(items, filters, isPartial) {
      if (!filters || !filters.length) {return items;}

      return items.filter(function (j) {
        var ioObjectMatchingCounter = 0;

        return j.data.io.filter(function (item) {

          filters.forEach(function(filterUnit) {
            var filtersMatchingCounter = 0;
            var filterKeys = Object.keys(filterUnit);

            filterKeys.forEach(function(key) {
              var propInItem = item[key] || undefined;

              if(!item[key] && getObjectValueFromString(item, key) !== undefined) {
                propInItem = getObjectValueFromString(item, key);
              }

              if(key === 'undefined' || filterUnit[key] === 'undefined') {
                return;
              }

              if(propInItem && atLeastOneMatch(filterUnit[key], propInItem, isPartial)) {
                filtersMatchingCounter += 1;
              }

              return;
            });

            if(filtersMatchingCounter === filterKeys.length) {
              ioObjectMatchingCounter += 1;
            }

          });

          return ioObjectMatchingCounter === filters.length;
        }).length > 0;
      });
    };

    /**
     * var fillUpScopePrefix - Prepares data object to be processed
     *
     * @param  {object} obj  object that has been updated
     * @param  {string} type attribute that has been updated
     * @return {undefined}   undefined
     */
    var fillUpScopePrefix = function(obj, type) {
      // This is just in case usr sends the product object rather than the product.items array
      if(obj) {data[type] = obj.items || obj;}

      if(data['itemsArray'] && typeof data['itemsArray'] === 'object') {
        scope[attrs.scopeprefix] = getFiltredCollection(data['itemsArray'], data['filterArray'], data['isPartial']);
      } else {
        scope[attrs.scopeprefix] = [];
      }
    };

    scope.$watchCollection(attrs.items, function(newValue, oldValue) {
      fillUpScopePrefix(newValue, 'itemsArray');
    });

    scope.$watchCollection(attrs.filters, function(newValue, oldValue) {
      fillUpScopePrefix(newValue, 'filterArray');
    });

    scope.$watchCollection(attrs.isPartial, function(newValue, oldValue) {
      fillUpScopePrefix(newValue, 'isPartial');
    });

  };

  return ctpFilterIo;

 }]);

var cooperativeDirectiveData; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.data.directive:ctpDataFinder
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 29/03/15
 *
 * Returns an array of objects based on the parameters
 *
 * @param {string} parentinstanceid The parent instance id of the object to retrieve
 * @param {string} signature The signature of the object to retrieve
 * @param {string} index The index of matching items to return (returns only the one item)
 * @param {string} type The type of the object to retrieve
 * @param {string} typeValue The typeValue of the object to retrieve
 *
 * @return {Array} Array of objects that match the parameters
 */
cooperativeDirectiveData.directive('ctpDataFinder', ['dataFactory', 'legacyFactory', 'stateProcessFactory', function(dataFactory, legacyFactory, stateProcessFactory) {
  var ctpDataFinder = {};
  ctpDataFinder.restrict = 'E';
  

  ctpDataFinder.link = function(scope, element, attrs) {
    var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpDataFinder');
    scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
    legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 

    
    scope.$watchCollection(attrs.collection, function(newVal, oldVal) {
      var collection = dataFactory.returnObjects(attrs.parentinstanceid, attrs.signature, attrs.index, attrs.type, attrs.typevalue, newVal, attrs.excludedtypes);
      scope[attrs.scopeprefix] = legacyFactory.returnData(collection, legacy);
      legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);
    });

    scope.$watchGroup([
        function() {
          return attrs.parentinstanceid;
        },
        function() {
          if(attrs.signature === undefined){
            legacyFactory.returnStatus(scope[attrs.scopeprefix],'rejected',legacy, 'signature attribute must be defined.');
          }
          return attrs.signature;
        },
        function() {
          return attrs.index;
        },
        function() {
          return attrs.type;
        },
        function() {
          return attrs.typevalue;
        },
        attrs.collection,
        function() {
          return attrs.excludedtypes;
        },
        function() {
          if(attrs.collection === undefined){
            legacyFactory.returnStatus(scope[attrs.scopeprefix],'rejected',legacy, 'collection attribute must be defined.');
          }
          return attrs.collection;
        }
      ],
      function(newVal, oldVal) {
        if (newVal[5] && newVal[5].length > 0) {

          if (!attrs.parentinstanceid) {
            attrs.parentinstanceid = 'any';
          }
          var collection = dataFactory.returnObjects(newVal[0], newVal[1], newVal[2], newVal[3], newVal[4], newVal[5], newVal[6]);
          scope[attrs.scopeprefix] = legacyFactory.returnData(collection, legacy);
          legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);       
        }
      });
  };

  return ctpDataFinder;
}]);

var cooperativeDirectiveData; //Defined in 0module.js

(function (cooperativeDirectiveData) {
    'use strict';

    /**
     * @ngdoc directive
     * @name directive.data.directive:ctpDataFinderV2
     * @restrict E
     * @element ctp-data-v2
     * @requires module:biz.data.processDataFactory
     * @requires biz.data.dataFactory#returnFormattedObject
     *
     * @description
     * Author Andrei
     *
     * Date 17/07/17
     *
     * Returns an array with signature keys of object based on the parameters
     *
     * @param {Array} collection  Array of objects to  be filtered
     * @param {Object=} parent Parent object to retrieve
     * @param {string} parent.signature Parent signature
     * @param {*=} parent.* Other object property
     * @param {Object[]} children List of children objects to retrieve
     * @param {string} children[].signature The array of the objects to retrieve
     * @param {string} children[].key The array of the objects to retrieve
     * @param {string} children[].key The array of the objects to retrieve
     * @param {*=} children[].* Other object property
     *
     * @return {Object} Object with keys on child property that match the parameters.
     */

    cooperativeDirectiveData.directive('ctpDataFinderV2', ctpDataFinderV2);
    ctpDataFinderV2.$inject = ['stateProcessFactory', 'processDataFactory'];

    function ctpDataFinderV2(stateProcessFactory, processDataFactory) {
        var ctpDataFinderV2 = {};
        ctpDataFinderV2.restrict = 'E';
        ctpDataFinderV2.link = link;

        function link(scope, element, attrs) {
            var params = {};
            var _state = stateProcessFactory.create(['processData']);
            var _getInitialScope = function () {
                return {_state: _state};
            };

            var _filterCollection = function () {

                if (!params.collection || !params.children ||
                    (params.collection.length === 0) || (params.children.length === 0)) {
                    return;
                }
                _state.processData.setPending();
                processDataFactory.process(params.collection, params.children, params.parent)
                    .then(function (data) {
                        _state.processData.setResolved();
                        scope[attrs.scopeprefix] = Object.assign(_getInitialScope(), data);
                    })
                    .catch(function (err) {
                        _state.processData.setError(err);
                        scope[attrs.scopeprefix] = Object.assign(_getInitialScope());
                    });
            };

            scope[attrs.scopeprefix] = _getInitialScope();

            scope.$watchCollection(attrs.collection, function(newVal, oldVal) {
                if (!angular.equals(params.collection, newVal)) {
                    params.collection = Array.isArray(newVal) ? newVal.slice() : newVal;
                    _filterCollection();
                }
            });

            ['children', 'parent'].forEach(function (item) {
                scope.$watch(attrs[item], function(newVal, oldVal) {
                    if (!angular.equals(params[item], newVal)) {
                        params[item] = Array.isArray(newVal) ? newVal.slice() : Object.assign({}, newVal);
                        _filterCollection();
                    }
                }, true);
            });
        }

        return ctpDataFinderV2;
    }
})(cooperativeDirectiveData);

var cooperativeDirectiveData; //Defined in 0module.js
/**
 * @ngdoc directive
 * @name directive.data.directive:ctpForm
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Creates an html form for use with a group of items that should be submitted together
 *
 * @param {string} formName The name of the form
 * @param {function} beforePost Function to call before the post is made
 * @param {function} afterPost Function to call after the post is made
 * @param {function} postError Function to call if there is an error
 */
cooperativeDirectiveData.directive('ctpForm', ['dataFactory', '$rootScope', '$q', function (dataFactory, $rootScope, $q) {
    var ctpForm = {};
    ctpForm.restrict = 'E';

    ctpForm.scope = {
        formName: '@',
        beforePost: '&',
        afterPost: '&',
        postError: '&',
        forceNew: '@',
        useAppApiKey: '@',
        shard: '@',
        partition: '@',
        mode: '@'
    };
    ctpForm.templateUrl = 'template/directive.data/form.html';
    ctpForm.transclude = true;

    ctpForm.controller = ['$scope', function ($scope) {
        var registeredEntities = {};
        $scope.formSubmitted = false;

        this.$registerEntity = function (formItem, entity) {
            registeredEntities[formItem] = { clone: angular.copy(entity), updatable: entity };
        };

        this.$getRegisteredEntities = function () {
            return registeredEntities;
        };

        this.$formName = function () {
            return $scope.formName;
        };

        this.$formSubmitted = function () {
            return $scope.formSubmitted;
        };

        this.$cancelForm = function () {
            for (var key in registeredEntities) {
                registeredEntities[key].updatable = registeredEntities[key].clone;
            }
        };

        this.$submitForm = function () {
            var deferred = $q.defer();
            $scope.formSubmitted = true;
            var ioList = [];

            //if (angular.isFunction($scope.beforePost)){
            $scope.beforePost();
            //}

            for (var key in registeredEntities) {
                var entity = registeredEntities[key].updatable;
                entity.timestamp = new Date().getTime();
                entity.version = entity.version + 1;
                if (typeof entity.typeValue === 'string') {
                    entity.typeValue = entity.typeValue.replace(/(?:\r\n|\r|\n)/g, '\n');
                }

                ioList.push(entity);
            }
            dataFactory.postIoList($scope.shard, $scope.partition, $scope.mode, ioList, $scope.forceNew, $scope.useAppApiKey).then(function (data) {
                //if (angular.isFunction($scope.afterPost)){

                $scope.afterPost();
                //}
                $scope.formSubmitted = false;

                for (var key in registeredEntities) {
                    registeredEntities[key].clone = angular.copy(registeredEntities[key].updatable);
                }

                deferred.resolve(data);

                $rootScope.$broadcast('data_update_success', data);

            }, function (error) {
                $scope.postError({ error: error });
                $scope.formSubmitted = false;

                deferred.reject('Error with submit');
                $rootScope.$broadcast('data_update_failure', error);
            });

            return deferred.promise;
        };
    }];

    ctpForm.link = function (scope, element, attrs, ctpFormCtrl) {


    };

    return ctpForm;
}]);

/**
 * @ngdoc directive
 * @name directive.data.directive:ctpFormRegister
 * @restrict A
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Registers a form item with a form.  This must be placed inside of a {@link ctpForm `ctpForm`} tag.
 *
 * @param {Object} entity The object that is to be registered with the form
 * @param {string} formItem The name of the item that is registered
 */
cooperativeDirectiveData.directive('ctpFormRegister', [function () {
    var ctpFormRegister = {};

    ctpFormRegister.restrict = 'A';
    ctpFormRegister.require = '^ctpForm';

    ctpFormRegister.scope = {
        entity: '=',
        formItem: '@'
    };

    ctpFormRegister.link = function (scope, element, attrs, ctpFormCtrl) {
        scope.$watchGroup(['entity'], function () {
            if (scope.formItem && scope.entity) {
                ctpFormCtrl.$registerEntity(scope.formItem, scope.entity);
            }
        });

    };

    return ctpFormRegister;
}]);

/**
 * @ngdoc directive
 * @name directive.data.directive:ctpFormSubmit
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Adds the registered entities to the scope collection upon form submission.
 */
cooperativeDirectiveData.directive('ctpFormSubmit', [function () {
    var ctpFormSubmit = {};

    ctpFormSubmit.restrict = 'E';
    ctpFormSubmit.require = ['^ctpForm', '^form', '?^ctpDataRequester', '?^ctpDataViews'];

    ctpFormSubmit.scope = true;

    ctpFormSubmit.link = function (scope, element, attrs, ctrls) {
        var ctpFormCtrl = ctrls[0];
        scope[ctpFormCtrl.$formName()] = ctrls[1];
        var ctpDataRequesterCtrl = ctrls[2];
        var ctpDataViewsCtrl = ctrls[3];

        scope.$watch(function () { return ctpFormCtrl.$formSubmitted(); }, function () {
            scope.formSubmitted = ctpFormCtrl.$formSubmitted();
        });

        element.on('click', '[form-target="' + ctpFormCtrl.$formName() + '"]', function () {
            ctpFormCtrl.$submitForm().then(
                function () {
                    if (ctpDataRequesterCtrl) {
                        var registeredEntities = ctpFormCtrl.$getRegisteredEntities();
                        ctpDataRequesterCtrl.$addItemsScopeCollection(registeredEntities);
                    }

                    if (ctpDataViewsCtrl) {
                        if (attrs.ctpDataViewTarget && attrs.ctpDataViewsGroup) {
                            ctpDataViewsCtrl.$switchTo(attrs.ctpDataViewTarget, attrs.ctpDataViewsGroup);
                        }
                    }
                }
            );
        });

    };

    return ctpFormSubmit;
}]);

/**
 * @ngdoc directive
 * @name directive.data.directive:ctpFormCancel
 * @restrict E
 *
 * @description
 * Author hiten
 *
 */
cooperativeDirectiveData.directive('ctpFormCancel', [function () {
    var ctpFormCancel = {};

    ctpFormCancel.restrict = 'E';
    ctpFormCancel.require = ['^ctpForm', '^form', '?^ctpDataRequester', '?^ctpDataViews'];

    ctpFormCancel.scope = true;

    ctpFormCancel.link = function (scope, element, attrs, ctrls) {
        var ctpFormCtrl = ctrls[0];
        scope[ctpFormCtrl.$formName()] = ctrls[1];
        var ctpDataRequesterCtrl = ctrls[2];
        var ctpDataViewsCtrl = ctrls[3];


        element.on('click', '[form-cancel-target="' + ctpFormCtrl.$formName() + '"]', function () {
            ctpFormCtrl.$cancelForm();

            if (ctpDataRequesterCtrl) {
                var registeredEntities = ctpFormCtrl.$getRegisteredEntities();
                ctpDataRequesterCtrl.$updateItemScopeCollection(registeredEntities);
            }

            if (ctpDataViewsCtrl) {
                if (attrs.ctpDataViewTarget && attrs.ctpDataViewsGroup) {
                    ctpDataViewsCtrl.$switchTo(attrs.ctpDataViewTarget, attrs.ctpDataViewsGroup);
                }
            }

        });

    };

    return ctpFormCancel;
}]);

var cooperativeDirectiveData; //Defined in 0module.js
/**
 * @ngdoc directive
 * @name directive.data.directive:ctpPaginator
 * @restrict E
 *
 * @description
 * Author Emiliano Errecalde
 *
 * Date 07/07/17
 *
 * Return a subset of element according to the parameters passed.
 *
 * @param {array} items Items to create pagination from
 * @param {number} [itemsPerPage=items.length] Amount of elements per page. I will default to the length of items list
 * @param {number} [currentPage=0] Page number to start with, (0 is the first page)
 * @param {object} scopeprefix Variable to store the resulting items after processed
 */
cooperativeDirectiveData.directive('ctpPaginator', ['legacyFactory', 'stateProcessFactory', function(legacyFactory, stateProcessFactory) {   
   var paginator = {};
   paginator.restrict = 'E';
   paginator.link = function(scope, element, attrs) {
     var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpPaginator');
     scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
     legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy); 

     var data = {};
     var dataReceived = {};

     if(attrs.items) {
       data.items = attrs.items;
     }

     data.itemsPerPage = attrs.itemsPerPage;

     data.currentPage = parseInt(attrs.currentPage) || 0;


     /**
      * getNumberOfPages - Gets number of pages from given items length and page size
      *
      * @return {number}  Returns calculation of number of pages
      */
     function getNumberOfPages(iLength, itemsPerPage){
       return Math.ceil(iLength / itemsPerPage);
     }

     /**
      * generatePagesArray - Generates page array to be used as repeater for page indexes creation
      *
      * @return {array}  Return an array from 1 to n where n is the amount of pages.
      */
     function generatePagesArray(numberOfPages){
       var arr = [],
         max = numberOfPages;

       for (var i = 0; i < max; i++) { arr.push(i); }

       return arr;
     }


     /**
      * getFiltredItems - Generates subset of items from provided page details.
      *
      * @param  {array} items      List of items
      * @param  {number} currPage  Current page number
      * @param  {number} itemsPerPage  Desired page size
      * @return {array}            Subset of items
      */
     function getFiltredItems(items, currPage, itemsPerPage) {
       var start = currPage * itemsPerPage; //parse to int
       return items.slice(start, start + itemsPerPage);
     }


     /**
      * fillUpData - Fills up data object with provided values from attributes
      *
      * @param  {any} val        Any value
      * @param  {string} type    Property to fill up
      * @return {undefined}      undefined
      */
     function fillUpDataObject(val, type) {
       data[type] = val;
       dataReceived[type] = true;
       fillUpScopePrefix();
     }

     /**
      * fillUpScopePrefix - Fills up scopeprefix variable with data object
      *
      * @return {undefined}      undefined
      */
     function fillUpScopePrefix() {      
       if(data.items && data.items.length) {
         if(!data.itemsPerPage) {
           data.itemsPerPage = data.items.length;
          }
          data.pagesArray = generatePagesArray(getNumberOfPages(data.items.length, data.itemsPerPage));
          if(dataReceived.currentPage && dataReceived.itemsPerPage && dataReceived.items && data.currentPage >= data.pagesArray.length) {
            data.currentPage = data.pagesArray.length - 1;
          }
          data.paginatedItems = getFiltredItems(data['items'], data.currentPage, data.itemsPerPage);
          scope[attrs.scopeprefix] = legacyFactory.returnData(data, legacy);
          legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);
        } else {
          
          var emptyObj = {
            items: [],
            paginatedItems: [],
            pagesArray: []
          };
          scope[attrs.scopeprefix] = legacyFactory.returnData(emptyObj, legacy);
          legacyFactory.returnStatus(scope[attrs.scopeprefix],'error',legacy); 
      }
    }


     /**
      * getItemFrom function - Calculates the itemFrom value based on current itemsPerPage and currentPage.
      *
      * @return {number}  Returns first item index shown in list.
      */
     data.getItemFrom = function () {
       return data.currentPage * data.itemsPerPage + 1;
     };


     /**
      * getItemTo function - Calculates the itemTo value based on current itemsPerPage and currentPage.
      *
      * @return {number}  Returns last item index shown in list.
      */
     data.getItemTo = function () {
       var toItemIndex = data.currentPage * data.itemsPerPage + data.itemsPerPage;
       return (toItemIndex > data.items.length) ? data.items.length : toItemIndex;
     };


     /**
      * paginate - Updates currentPage based on the parameter passed.
      *
      * @param  {number|string} page  Accepts either next/prev or a number corresponding to the page number.
      * @return {undefined}                undefined
      */
     data.paginate = function(page) {
       if(typeof page === 'string' && page.indexOf('prev') > -1) {
         data.currentPage = data.currentPage - 1;
       } else if (typeof page === 'string' && page.indexOf('next') > -1) {
         data.currentPage = data.currentPage + 1;
       } else {
         if(data.currentPage === parseInt(page)) {
           return;
         }
         data.currentPage = parseInt(page);
       }

       fillUpScopePrefix();
     };


     /**
      * setItemsPerPage - Updates the value of items per page according to the parameters passed
      *
      * @param  {number} size    Number of items per page
      * @return {undefined}      undefined
      */
     data.setItemsPerPage = function(size) {
       data.itemsPerPage = parseInt(size);
       fillUpScopePrefix();
     };

     scope.$watchCollection(attrs.items, function(newValue, oldValue) {
       fillUpDataObject(newValue, 'items');
     });

 		scope.$watchCollection(attrs.currentPage, function(newValue, oldValue) {
       if(newValue !== undefined) {
         fillUpDataObject((newValue === null) ? 0 : newValue, 'currentPage');
       }
 		});

     scope.$watchCollection(attrs.itemsPerPage, function(newValue, oldValue) {
       if(newValue !== undefined) {
         fillUpDataObject(newValue, 'itemsPerPage');
       }
 		});

   };

   return paginator;
 }]);

var cooperativeDirectiveData; //Defined in 0module.js
/**
 * @ngdoc directive
 * @name directive.data.directive:ctpProcessList
 * @restrict E
 *
 * @description
 * Author Emiliano
 *
 * Date 26/06/17
 *
 * Returns an array of objects after processing each element against given function
 *
 * @param {array} items Items must be in ctp format, such as the resulting array from a ctp data requester
 * @param {function} func Function to process the list of items with
 * @param {array} scopeprefix Variable to store the resulting items after processed
 */
cooperativeDirectiveData.directive('ctpProcessList', ['legacyFactory', 'stateProcessFactory', function(legacyFactory, stateProcessFactory) {
	
	var ctpProcessList = {};

	ctpProcessList.restrict = 'E';

	ctpProcessList.link = function(scope, element, attrs) {
		var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpProcessList');
		scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
		legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy);

		var data = {
			itemsArray: [],
			func: null
		};

		/**
		 * var fillUpScopePrefix - Prepares data object to be processed
		 *
		 * @param  {object} obj  object that has been updated
		 * @param  {string} type attribute that has been updated
		 * @return {undefined}   undefined
		 */
		var fillUpScopePrefix = function(obj, type) {
			
			// This is just in case usr sends the product object rather than the product.items array
			if(obj) {data[type] = obj.items || obj;}

			if(data['itemsArray'].length && typeof data['func'] === 'function') {
				// scope[attrs.scopeprefix] = data['func'](data['itemsArray']);
				scope[attrs.scopeprefix] = legacyFactory.returnData(data['func'](data['itemsArray']), legacy);
				legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);				
			} else {
				// We send back empty array so user knows there's something wrong
				scope[attrs.scopeprefix] = legacyFactory.returnData([], legacy);
				legacyFactory.returnStatus(scope[attrs.scopeprefix],'error',legacy);
							
			}
		};

    scope.$watchCollection(attrs.items, function(newValue, oldValue) {
			fillUpScopePrefix(newValue, 'itemsArray');
    });

		scope.$watch(attrs.func, function(newValue, oldValue) {
			fillUpScopePrefix(newValue, 'func');
    });

  };

  return ctpProcessList;
}]);

var cooperativeDirectiveData; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.data:ctpDataCreator
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 29/03/15
 *
 * Adds to the scope a new entity object based on the parameters
 *
 * @param {string} scopeprefix The name for the new object
 * @param {string} poid The poid of the new object
 * @param {string} parentinstanceid The parent instance id of the new object
 * @param {string} signature The signature of the new object
 * @param {string} type The type of the new object
 */
cooperativeDirectiveData.directive('ctpProcessObjectCreator',['legacyFactory', 'stateProcessFactory', 'processObjectFactory',function(legacyFactory, stateProcessFactory, processObjectFactory){
    var ctpProcessObjectCreator = {};
    ctpProcessObjectCreator.restrict = 'E';
    ctpProcessObjectCreator.scope = true;

    ctpProcessObjectCreator.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpProcessObjectCreator');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy);
        
        scope.$watchGroup([
                function() {return attrs.scopeprefix;},
                function() {return attrs.useAppApiKey;}
            ],
            function(){
                var promise = processObjectFactory.new(undefined, attrs.useAppApiKey);
                promise.then(function (processObject) {
                    scope[attrs.scopeprefix] = legacyFactory.returnData(processObject, legacy);                    
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);
                })
                .catch(function(){
                    legacyFactory.returnStatus(scope[attrs.scopeprefix],'error',legacy);
                });
            });
    };

    return ctpProcessObjectCreator;
}]);

var cooperativeDirectiveData; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.data.directive:ctpProperty
 * @restrict E
 *
 * @description
 * Author swhittemore
 *
 * Date 11/11/16
 *
 * Returns the value of a json property
 *
 * @param {string} iodataobject json object to get info from
 * @param {string} property the property in dot notation to retrieve
 *
 * @return {object} value of the property in the iodataobject
 */
cooperativeDirectiveData.directive('ctpProperty', ['legacyFactory', 'stateProcessFactory', 'dataFactory', function(legacyFactory, stateProcessFactory, dataFactory) {
  var ctpProperty = {};
  ctpProperty.restrict = 'E';

  ctpProperty.link = function(scope, element, attrs) {
    var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpProperty');
    scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
    legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy);
    
    scope.$watchGroup([
        function() {
          return attrs.property;
        },
        attrs.iodataobject
      ],
      function(newVal, oldVal) {

        if (newVal[1]) {

          var obj = newVal[1];
          var stack = newVal[0].split('.');
          for(var i = 0; i < stack.length-1; i++){
            if(obj[stack[i]] === undefined){
              obj[stack[i]] = {};
            }
            obj = obj[stack[i]];
          }
          var item = obj[stack[stack.length-1]];
          //var item = newVal[1][newVal[0]];
          
          scope[attrs.scopeprefix] = legacyFactory.returnData(item, legacy);
          legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success', legacy);
        }
        else{
          scope[attrs.scopeprefix] = legacyFactory.returnData([], legacy);
          legacyFactory.returnStatus(scope[attrs.scopeprefix], 'error', legacy);
        } 
      });
  };

  return ctpProperty;
}]);

/* jshint loopfunc: true */

var cooperativeDirectiveData; //Defined in 0module.js
 /**
 * @ngdoc directive
 * @name directive.data.directive:ctpSortIo
 * @restrict E
 *
 * @description
 * Author Emiliano
 *
 * Date 28/06/17
 *
 * Returns an array of objects based on the parameters
 *
 * @param {array} items Items must be in ctp format, such as the resulting array from a ctp data requester
 * @param {object} propToFind Set of properties to find a single object from data.io array. Resulting Item will be used to look for the 'propToSortBy' in order to retrieve a single value
 * @param {string} propToSortBy The object selection string to look for in order to retrieve a single value to sort by.
 * @param {boolean} [desc] When defined, it sets the sorting as descending.
 * @param {array} scopeprefix Variable to store the resulting items after sorting
 */
cooperativeDirectiveData.directive('ctpSortIo', ['legacyFactory', 'stateProcessFactory', function(legacyFactory, stateProcessFactory) {
	var ctpSortIo = {};
	ctpSortIo.restrict = 'E';

	ctpSortIo.link = function(scope, element, attrs) {
		var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpSortIo');
		scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
		legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy);

		var data = {
			itemsArray: attrs.items || [],
			propToFind: (typeof attrs.propToFind === 'object') ? attrs.propToFind : '',
			propToSortBy: attrs.propToSortBy || '',
			desc: (attrs.hasOwnProperty('desc') && attrs.desc) ? attrs.desc : false,
		};

		/**
		 * getObjectValueFromString - Process and returns the value of the property found from given string
		 *
		 * @param  {object} o object to look at
		 * @param  {string} s string to process
		 * @return {type}     returns the value of property found from the given string
		 */
		function getObjectValueFromString(o, s) {
			s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
			s = s.replace(/^\./, ''); // strip a leading dot
			var a = s.split('.');
			for (var i = 0, n = a.length; i < n; ++i) {
			  var k = a[i];
			  if (k in o) {
			    o = o[k];
			  } else {
			    return;
			  }
			}
			return o;
		}


		/**
		 * getValueFromProperty - Find and returns the value from the given properties
		 *
		 * @param  {object} item         Object to look at
		 * @param  {object} [propToFind] If defined, will help to find 1 element from the io list, where our 'sort by' property will be located
		 * @param  {string} propToSortBy Defines where is our sort by property
		 * @return {string|number}       Returns a single string or number to sort by
		 */
		function getValueFromProperty(item, propToFind, propToSortBy) {
			if(!propToFind) {
				return getObjectValueFromString(item, propToSortBy) || null;
			}

      if(!item.data.io.length || !propToSortBy) {
        return null;
      }

      var keys = Object.keys(propToFind);
			var foundElement = null;
      for(var i = 0; i < item.data.io.length; i++) {
        var el = item.data.io[i];
        var counter = 0;
				keys.forEach(function(key) {
					if(el[key] === propToFind[key]) {counter += 1;}
				});

				if(counter === keys.length) {
          foundElement = el;
          break;
        }
			}

			if(!foundElement) {return null;}

			return getObjectValueFromString(foundElement, propToSortBy) || null;
		}


		/**
		 * var getSortedCollection - Sorts and returns the sorted list from a given array
		 *
		 * @param  {array} items        Items to sort
		 * @param  {object} [propToFind] To pass to getValueFromProperty function
		 * @param  {string} propToSortBy To pass to getValueFromProperty function
		 * @param  {boolean} [desc=false] Defines if the sorting is ascending or descending
		 * @return {type}              Returns the sorted list
		 */
		var getSortedCollection = function(items, propToFind, propToSortBy, desc) {
			desc = !(desc === false || desc === 'false' || desc === undefined);
			return items.sort(function(a, b) {
				var propA = getValueFromProperty(a, propToFind, propToSortBy);
				var propB = getValueFromProperty(b, propToFind, propToSortBy);

        if(!propA || !propB) {return 0;}

				if(typeof propA === 'string') {
					propA = (isNaN(propA)) ? propA.toLowerCase() : parseFloat(propA);
					propB = (isNaN(propB)) ? propB.toLowerCase() : parseFloat(propB);
				}

				if(propA < propB) {
					return (desc) ? 1 : -1;
				} else if (propA > propB){
					return (desc) ? -1 : 1;
				} else {
					return 0;
				}
			});
		};


		/**
		 * var fillUpScopePrefix - Prepares data object to be processed
		 *
		 * @param  {object} obj  object that has been updated
		 * @param  {string} type attribute that has been updated
		 * @return {undefined}   undefined
		 */
		var fillUpScopePrefix = function(obj, type) {
			data[type] = obj;
			if(data['itemsArray'] && typeof data['itemsArray'] === 'object' && data['itemsArray'].length) {

				var result = getSortedCollection(data.itemsArray, data.propToFind, data.propToSortBy, data.desc);
				scope[attrs.scopeprefix] = legacyFactory.returnData(result, legacy);
				legacyFactory.returnStatus(scope[attrs.scopeprefix],'success',legacy);

      } else {
				scope[attrs.scopeprefix] = legacyFactory.returnData([], legacy);
				legacyFactory.returnStatus(scope[attrs.scopeprefix],'error',legacy);
      }
		};

		scope.$watchCollection(attrs.items, function(newValue, oldValue) {
      fillUpScopePrefix(angular.copy(newValue), 'itemsArray');
    });

		scope.$watch(attrs.propToFind, function(newValue) {
      fillUpScopePrefix(newValue, 'propToFind');
    });

		scope.$watch(attrs.propToSortBy, function(newValue) {
      fillUpScopePrefix(newValue, 'propToSortBy');
    });

		scope.$watch(attrs.desc, function(newValue) {
      fillUpScopePrefix(newValue, 'desc');
    });

  };

  return ctpSortIo;

}]);

var cooperativeDirectiveData; //Defined in 0module.js
/**
 * @ngdoc directive
 * @name directive.data.directive:ctpDataViews
 * @restrict E
 *
 * @description
 * Created by hiten on 28/03/15.
 */
cooperativeDirectiveData.directive('ctpDataViews', [function() {
    var ctpViews = {};

    ctpViews.restrict = 'E';
    ctpViews.scope = true;

    ctpViews.controller = ['$element', '$attrs', function($element, $attrs) {
        this.registeredViews = {};

        this.$registerView = function(ctrl) {
            this.registeredViews[ctrl.$name] = ctrl;
        };

        this.$groupName = function() {
            return $attrs.name;
        };

        this.$switchTo = function(viewName, viewsGroup) {
            if ($attrs.name == viewsGroup) {
                for (var k in this.registeredViews) {
                    if (k === viewName) {
                        this.registeredViews[k].$show();
                    } else {
                        this.registeredViews[k].$hide();
                    }
                }
            }
        };
    }];

    ctpViews.link = function(scope, element, attrs, viewsCtrl) {
        element.on('click', '[views-target],[views-group="' + attrs.name + '"]', function() {
            var viewName = angular.element(this).attr('view-target');
            var viewsGroup = attrs.name;

            viewsCtrl.$switchTo(viewName, viewsGroup);
        });

        scope.$views = viewsCtrl;
    };

    return ctpViews;
}]);

var ctpView = function(name, directiveName) {
    var ctpView = {};

    ctpView.restrict = 'E';
    ctpView.require = [directiveName, '^ctpDataViews'];
    ctpView.transclude = 'true';
    ctpView.scope = {};

    ctpView.template = '<div ng-if="show"><ng-transclude></ng-transclude></div>';

    ctpView.controller = ['$scope', '$element', '$attrs', '$timeout', function($scope, $element, $attrs, $timeout) {
        if (name === undefined){
            this.$name = $attrs.name;
        } else {
            this.$name = name;
            console.warn('deprecated: directive ' + directiveName + ' please use ctpDataView');

        }


        this.$show = function() {
            $scope.show = true;
            $timeout(function() {
            });

        };

        this.$hide = function() {
            $scope.show = false;
            $timeout(function() {
            });

        };
    }];

    ctpView.link = function(scope, element, attrs, ctrls, transclude) {
        var viewCtrl = ctrls[0];
        var viewsCtrl = ctrls[1];

        viewsCtrl.$registerView(viewCtrl);

        if (attrs.initial !== undefined) {
            viewCtrl.$show();
        } else {
            viewCtrl.$hide();
        }

    };

    return ctpView;
};

cooperativeDirectiveData.directive('ctpDataView', [function() { return ctpView(undefined, 'ctpDataView'); }]);

cooperativeDirectiveData.directive('ctpDataViewDisplay', [function() { return ctpView('display', 'ctpDataViewDisplay'); }]);

cooperativeDirectiveData.directive('ctpDataViewUpdate', [function() { return ctpView('update', 'ctpDataViewUpdate'); }]);

cooperativeDirectiveData.directive('ctpDataViewInsert', [function() { return ctpView('insert', 'ctpDataViewInsert'); }]);

var cooperativeDirectiveRegistrationRequest = angular.module('cooperative.directive.registration.request', []);

/**
 * Created by hiten on 07/08/15.
 */
var cooperativeDirectiveRegistrationRequest; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpOrganizationRegistrationRequester
 * @restrict E
 * @scope true
 *
 * @description
 * Author hiten
 *
 * Date 04/05/15
 *
 * Calls the organizationRegistrationFactory to store the poid in the cookie
 *
 * @param {string} currentUser Current signed in user
 * @param {string} scopeprefix The name for the returned data
 */
cooperativeDirectiveRegistrationRequest.directive('ctpOrganizationDuplicateCheckRequester',['legacyFactory', 'stateProcessFactory', 'duplicateCheckFactory',function(legacyFactory, stateProcessFactory, DuplicateCheckFactory){
    var ctpOrganizationDuplicateCheckRequester = {};
    ctpOrganizationDuplicateCheckRequester.restrict = 'E';
    ctpOrganizationDuplicateCheckRequester.scope = true;

    ctpOrganizationDuplicateCheckRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpOrganizationDuplicateCheckRequester');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy);

        var isNullUndefinedEmpty = function(attr){
            if (attr !== undefined &&
                attr !== null &&
                attr !== ''){
                return false;
            } else {
                return true;
            }
        };

        scope.$watchGroup([
                function() { return attrs.scopeprefix; },
                function() { return attrs.orgLegalName; },
                function() { return attrs.orgAddress; },
                function() { return attrs.orgCity; },
                function() { return attrs.orgStateRegion; },
                function() { return attrs.orgPostalCode; },
                function() { return attrs.orgCountryCode; },
                function() { return attrs.useAppApiKey; },
                function() { return attrs.mode; },
                function() { return attrs.partition; }
            ],
            function(){
                if (!isNullUndefinedEmpty(attrs.orgLegalName) &&
                    !isNullUndefinedEmpty(attrs.orgAddress) &&
                    !isNullUndefinedEmpty(attrs.orgCity) &&
                    !isNullUndefinedEmpty(attrs.orgStateRegion) &&
                    !isNullUndefinedEmpty(attrs.orgPostalCode) &&
                    !isNullUndefinedEmpty(attrs.orgCountryCode)){

                    var duplicateCheck = new DuplicateCheckFactory(attrs.partition, attrs.mode, attrs.orgLegalName, attrs.orgAddress, attrs.orgCity, attrs.orgStateRegion, attrs.orgPostalCode, attrs.orgCountryCode);
                    scope[attrs.scopeprefix] = legacyFactory.returnData(duplicateCheck, legacy);
                    legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success', legacy);    
                    var scopePrefix = legacyFactory.setPrefix(scope[attrs.scopeprefix], legacy);
                    scopePrefix.run();

                } else {
                    scope[attrs.scopeprefix] = legacyFactory.returnData([], legacy);
                    if(!legacy){
                        legacyFactory.returnStatus(scope[attrs.scopeprefix], 'rejected', legacy, 'Organizations\'s legal name or address is undefined');
                    }
                }
            });
    };

    return ctpOrganizationDuplicateCheckRequester;
}]);
var cooperativeDirectiveRegistrationRequest; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpOrganizationRegistrationRequester
 * @restrict E
 * @scope true
 *
 * @description
 * Author hiten
 *
 * Date 04/05/15
 *
 * Calls the organizationRegistrationFactory to store the poid in the cookie
 *
 * @param {string} currentUser Current signed in user
 * @param {string} scopeprefix The name for the returned data
 */
cooperativeDirectiveRegistrationRequest.directive('ctpOrganizationRegistrationRequester',['legacyFactory', 'stateProcessFactory','orgRegistrationFactory',function(legacyFactory, stateProcessFactory, OrgRegistrationFactory){
    var ctpOrganizationRegistrationRequester = {};
    ctpOrganizationRegistrationRequester.restrict = 'E';
    ctpOrganizationRegistrationRequester.scope = true;

    ctpOrganizationRegistrationRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpOrganizationRegistrationRequester');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy);        
        scope[attrs.scopeprefix] = legacyFactory.returnData(new OrgRegistrationFactory(), legacy);        
        var scopePrefix = legacyFactory.setPrefix(scope[attrs.scopeprefix], legacy);
        scopePrefix.initiate();
        legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success', legacy);
    };

    return ctpOrganizationRegistrationRequester;
}]);
var cooperativeDirectiveRegistrationRequest; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpOrganizationRegistrationRequester
 * @restrict E
 * @scope true
 *
 * @description
 * Author hiten
 *
 * Date 04/05/15
 *
 * Calls the organizationRegistrationFactory to store the poid in the cookie
 *
 * @param {string} currentUser Current signed in user
 * @param {string} scopeprefix The name for the returned data
 */
cooperativeDirectiveRegistrationRequest.directive('ctpOrganizationServiceRegistrationRequester',['legacyFactory', 'stateProcessFactory', 'orgServiceRegistrationFactory',function(legacyFactory, stateProcessFactory, OrgServiceRegistrationFactory){
    var ctpOrganizationServiceRegistrationRequester = {};
    ctpOrganizationServiceRegistrationRequester.restrict = 'E';
    ctpOrganizationServiceRegistrationRequester.scope = true;

    ctpOrganizationServiceRegistrationRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpOrganizationServiceRegistrationRequester');     
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus(scope[attrs.scopeprefix],'pending',legacy);       
        scope[attrs.scopeprefix] = legacyFactory.returnData(new OrgServiceRegistrationFactory(attrs.serviceName, JSON.parse(attrs.workflowQueueList)), legacy);        
        var scopePrefix = legacyFactory.setPrefix(scope[attrs.scopeprefix], legacy);        
        scopePrefix.getOrganizationList();
        legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success', legacy);
    };

    return ctpOrganizationServiceRegistrationRequester;
}]);
var cooperativeDirectiveRegistrationRequest; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpDataRequester
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of matching entity objects
 *
 * @param {string} scopeprefix The name for the returned data
 * @param {string} poid The poid of the object to retrieve
 * @param {string} parentinstanceid The parent instance id of the object to retrieve
 * @param {Array} signaturelist The signature list of resources needed that are tied to the object to retrieve.<br>
 * Example signatureList: '["EntityObject_001", "NameObject_001", "DescriptiveTextObject_001"]'<br>
 * Use ["all"] for returning all that are available<br>
 *
 * @returns {Array} Array of matching process objects
 */
cooperativeDirectiveRegistrationRequest.directive('ctpUserRegistrationRequester',['legacyFactory','stateProcessFactory','userRegistrationFactory',function(legacyFactory, stateProcessFactory, UserRegistrationFactory){     
    var ctpUserRegistrationRequester = {};
    ctpUserRegistrationRequester.restrict = 'E';
    ctpUserRegistrationRequester.scope = true;

    ctpUserRegistrationRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpUserRegistrationRequester'); 
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};    
        scope[attrs.scopeprefix] = legacyFactory.returnData(new UserRegistrationFactory(), legacy);
        legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success', legacy);
        var scopePrefix = legacyFactory.setPrefix(scope[attrs.scopeprefix], legacy);
        scopePrefix.initiate();
    };

    return ctpUserRegistrationRequester;
}]);
var cooperativeDirectiveRegistration = angular.module('cooperative.directive.registration', []);

/**
 * Created by hiten on 14/09/2015.
 */
var cooperativeDirectiveRegistration; //Defined in 0module.js
cooperativeDirectiveRegistration.directive('ctpPasswordValidator',[function(){
    var ctpPasswordValidator = {};

    ctpPasswordValidator.restrict = 'A';
    ctpPasswordValidator.require = 'ngModel';

    ctpPasswordValidator.link = function(scope, element, attrs, ngModel){
        scope.$watch(attrs.ngModel, function(value){
            var strongRegex = new RegExp('((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{8,40})');

            ngModel.$setValidity('passwordValidator', strongRegex.test(value));
        });
    };

    return ctpPasswordValidator;
}]);

/**
 * Created by hiten on 14/09/2015.
 */
var cooperativeDirectiveRegistration; //Defined in 0module.js
cooperativeDirectiveRegistration.directive('ctpUniqueUsernameValidator',['accountUtils', function(accountUtils){
    var ctpUniqueUsernameValidator = {};

    ctpUniqueUsernameValidator.restrict = 'A';
    ctpUniqueUsernameValidator.require = 'ngModel';

    ctpUniqueUsernameValidator.link = function(scope, element, attrs, ngModel){
        scope.$watch(attrs.ngModel, function(value){
            var isUserNameUniquePromise = accountUtils.isUserNameUnique(value, undefined, true);
            isUserNameUniquePromise.then(function(data){
                ngModel.$setValidity('uniqueUsername', data);
            });
            isUserNameUniquePromise.catch(function(){
                ngModel.$setValidity('uniqueUsername', false);
            });
        });
    };

    return ctpUniqueUsernameValidator;
}]);

var cooperativeDirectiveSearch = angular.module('cooperative.directive.search', []);
 var cooperativeDirectiveSearch2 = angular.module('cooperative.directive.search.directive.search2',[]);

var cooperativeDirectiveSearch2;
  cooperativeDirectiveSearch2.controller('searchRequesterCtrl', ['legacyFactory', 'stateProcessFactory', '$q','$scope','searchRequesterFactory','_',function(legacyFactory, stateProcessFactory, $q,$scope,searchRequesterFactory,_) {


        var vm = this;

        //reasonable defaults
        var size = '20';
        var threshold = '.8';
        var returnMode = 'true';
        var queryBypassPrivacy = 'false';
        var queryBypassSimilarity = 'true';
        var shard = 'object';
        //var partition = 'prod';
        var useAppApiKey = 'false';
        var legacy = legacyFactory.legacyMode(vm.legacy, 'ctpSearchRequester');
        $scope[vm.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};
        legacyFactory.returnStatus($scope[vm.scopeprefix], 'pending', legacy);

        vm.performSearch = function(){
          // if these are not provided, give defaults
          if(vm.size === undefined || vm.size === null || vm.size === ''){
            vm.size = size;
          }

          if(vm.shard === undefined || vm.shard === null || vm.shard === ''){
            vm.shard = shard;
          }

          if(vm.threshold === undefined || vm.threshold === null || vm.threshold === ''){
            vm.threshold = threshold;
          }

          if(vm.returnMode === undefined || vm.returnMode === null || vm.returnMode === ''){
            vm.returnMode = returnMode;
          }

          if(vm.queryBypassPrivacy === undefined || vm.queryBypassPrivacy === null || vm.queryBypassPrivacy === ''){
            vm.queryBypassPrivacy = queryBypassPrivacy;
          }

          if(vm.queryBypassSimilarity === undefined || vm.queryBypassSimilarity === null || vm.queryBypassSimilarity === ''){
            vm.queryBypassSimilarity = queryBypassSimilarity;
          }

          if(vm.useAppApiKey === undefined || vm.useAppApiKey === null || vm.useAppApiKey === ''){
            vm.useAppApiKey = useAppApiKey;
          }

          var promiseArray = [];

          //add to the promise array any passed queries
          if(vm.queryString !== undefined && vm.queryString !== null && vm.queryString !== ''){
            promiseArray.push(searchRequesterFactory.queryString(vm.queryString,vm.shard,vm.partition,vm.size,vm.returnMode,vm.queryBypassSimilarity,vm.queryBypassPrivacy,vm.useAppApiKey));
          }

          if(vm.keyword !== undefined && vm.keyword !== null && vm.keyword !== ''){
            promiseArray.push(searchRequesterFactory.keyword(vm.keyword,vm.shard,vm.partition,vm.size,vm.threshold,vm.returnMode,vm.queryBypassSimilarity,vm.queryBypassPrivacy,vm.useAppApiKey));
          }

          if(vm.queryTerms !== undefined && vm.queryTerms !== null && vm.queryTerms !== ''){
                promiseArray.push(searchRequesterFactory.advancedSearch(vm.queryTerms,vm.shard,vm.partition,vm.size,vm.threshold,vm.returnMode,vm.queryBypassSimilarity,vm.queryBypassPrivacy,vm.useAppApiKey));
              }

          if(vm.encryptedQueryTerm !== undefined && vm.encryptedQueryTerm !== null && vm.encryptedQueryTerm !== ''){
            promiseArray.push(searchRequesterFactory.advancedSearchEncryptedQT(vm.encryptedQueryTerm,vm.shard,vm.partition,vm.size,vm.threshold,vm.returnMode,vm.queryBypassSimilarity,vm.queryBypassPrivacy,vm.useAppApiKey));
          }
          if(vm.encryptedQueryString !== undefined && vm.encryptedQueryString !== null && vm.encryptedQueryString !== ''){
            promiseArray.push(searchRequesterFactory.queryStringEncrypted(vm.encryptedQueryString,vm.shard,vm.partition,vm.size,vm.returnMode,vm.queryBypassSimilarity,vm.queryBypassPrivacy,vm.useAppApiKey));
          }

          //execute any queries we have
          if(promiseArray.length>0){

            // state to busy attribute
            $scope[vm.busy] = legacyFactory.returnData(true, legacy);
            // $scope[vm.busy]=true;

            $q.all(promiseArray).then(function(results){
              //forward facing data set variable
              var filteredResults = [];
              if(results.length>0){
                //loop through the results array created by $q.all
                //could return N arrays or results
                for(var i=0;i<results.length;i++){

                  //look through results row, if it exists
                  var resultSet = results[i];

                  if(resultSet.results){

                    //loop through the result in row
                    for(var j = 0; j<resultSet.results.length; j++){
                      //if the id of Jth index does not exist in our forward facing data set, add it
                      if (_.findIndex(filteredResults, {id: resultSet.results[j].id}) === -1){
                        filteredResults.push(resultSet.results[j]);
                        }
                    }
                  }
                }
              }

              // state to busy attribute
              $scope[vm.busy] = legacyFactory.returnData(false, legacy);

              // filtered results to scopeprefix
              $scope[vm.scopeprefix] = legacyFactory.returnData(filteredResults, legacy);
              legacyFactory.returnStatus($scope[vm.scopeprefix], 'success', legacy);
              // $scope[vm.scopeprefix] = filteredResults;


            },function(error){
              //whoops
              $scope[vm.busy] = legacyFactory.returnData(false, legacy); // state to busy attribute
              $scope[vm.scopeprefix]=[];
              legacyFactory.returnStatus($scope[vm.scopeprefix], 'rejected', legacy, 'Framework MSG: ctpSearchRequesterCtrl, upsteam error occured' + error);
            });
          }
        };


        if(vm.refreshOnQueryChange){
          $scope.$watchGroup([
              function(){return vm.queryString;}, //string (jsonBquery)
              function(){return vm.keyword;}, //string
              function(){return vm.queryTerms;}, //object
              function(){return vm.encryptedQueryTerm;}, //string, object stringified, encrypted
              function(){return vm.encryptedQueryString;}, //string, json b query, encrypted
              function(){return vm.threshold;}, //string
              function(){return vm.size;},  //string
              function(){return vm.queryBypassSimilarity;}, //string
              function(){return vm.queryBypassPrivacy;}, //string
              function(){return vm.returnMode;}, //string
              function(){return vm.scopeprefix;}, //string
              function(){return vm.busy;}, //string
              function(){return vm.useAppApiKey;},
              function(){return vm.shard;},
              function(){return vm.partition;}
            ], function(){
              vm.performSearch();
            });
        }else{
          vm.performSearch();
        }

  }]);

var cooperativeDirectiveSearch2;
  cooperativeDirectiveSearch2.directive('ctpSearchRequester', function() {

    var directive = {};
    directive.restrict = 'E';
    directive.controller = 'searchRequesterCtrl';
    directive.controllerAs = 'searchRequesterCtrl';
    directive.scope = true;
    directive.bindToController = {

      queryString:'@', //string (jsonBquery)
      keyword:'@', //string
      queryTerms:'=', //object
      encryptedQueryTerm:'@', //string, object stringified, encrypted
      encryptedQueryString:'@', //string, json b query, encrypted
      threshold:'@', //string
      size:'@',  //string
      queryBypassSimilarity:'@', //string
      queryBypassPrivacy:'@', //string
      legacy:'@',
      returnMode:'@', //string
      scopeprefix:'@', //string
      busy:'@', //string
      useAppApiKey:'@',
      shard:'@',
      partition:'@',
      refreshOnQueryChange:'@'
    };


    return directive;
  });

var cooperativeDirectiveSearch; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpDataRequester
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of matching entity objects
 *
 * @param {string} scopeprefix The name for the returned data
 * @param {string} poid The poid of the object to retrieve
 * @param {string} parentinstanceid The parent instance id of the object to retrieve
 * @param {Array} signaturelist The signature list of resources needed that are tied to the object to retrieve.<br>
 * Example signatureList: '["EntityObject_001", "NameObject_001", "DescriptiveTextObject_001"]'<br>
 * Use ["all"] for returning all that are available<br>
 *
 * @returns {Array} Array of matching process objects
 */
cooperativeDirectiveSearch.directive('ctpSearchPagingRequester',['legacyFactory', 'stateProcessFactory','searchPagingFactory',function(legacyFactory, stateProcessFactory, SearchPagingFactory){
    var ctpSearchPagingRequester = {};
    ctpSearchPagingRequester.restrict = 'E';
    ctpSearchPagingRequester.scope = true;

    ctpSearchPagingRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpSearchPagingRequester');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};        
        legacyFactory.returnStatus(scope[attrs.scopeprefix], 'pending', legacy);

        scope.$watchGroup([
                function() { return attrs.scopeprefix; },
                function() { return attrs.trilike; },
                function() { return attrs.queryString; },

                // trigram attributes
                function() { return attrs.orgLegalName; },
                function() { return attrs.orgAddress; },
                function() { return attrs.orgWebsite;},
                function() { return attrs.orgPhone;},
                function() { return attrs.orgEmal;},
                function() {return attrs.legalId;},
                function() {return attrs.rankThreshold;},

                function() { return attrs.signatureList; },
                function() { return attrs.size; },
                function() { return attrs.returnMode; },
                function() { return attrs.useAppApiKey; },
                function() { return attrs.shard; },
                function() { return attrs.mode; },
                function() { return attrs.queryBypassSimilarity;},
                function() { return attrs.queryBypassPrivacy; },
                function() { return attrs.partition; }
            ],
            function(){
                
                var searchData = new SearchPagingFactory(attrs.shard, attrs.partition, attrs.mode, attrs.trilike, attrs.queryString, JSON.parse(attrs.signatureList), attrs.size, attrs.returnMode, undefined, attrs.rankThreshold, attrs.orgLegalName, attrs.orgAddress, attrs.orgWebsite, attrs.orgPhone, attrs.orgEmail, attrs.legalId, attrs.useAppApiKey, attrs.queryBypassSimilarity, attrs.queryBypassPrivacy);
                scope[attrs.scopeprefix] = legacyFactory.returnData(searchData, legacy);
                legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success', legacy);
                var data = scope[attrs.scopeprefix];
                if(!legacy){
                    data = scope[attrs.scopeprefix]._data; 
                }
                data.nextPage();
            });
    };

    return ctpSearchPagingRequester;
}]);

var cooperativeDirectiveUiFormControlValidation = angular.module('cooperative.directive.ui.formControl.validation', []);

var cooperativeDirectiveUiFormControlValidation; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpFormControlValidationMessage
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Creates messages for validation failures
 *
 * @returns {template} A set of validation messages
 */
cooperativeDirectiveUiFormControlValidation.directive('ctpFormControlValidationMessage',['legacyFactory', 'stateProcessFactory',
    function(legacyFactory, stateProcessFactory){
    var formControlValidationMessage = {};

    formControlValidationMessage.restrict = 'E';
    formControlValidationMessage.scope = {
        controlName: '@'
    };
    formControlValidationMessage.require = '^form';
    formControlValidationMessage.templateUrl = 'template/directive.ui.formControl.validation/formControl_validationMessages.html';
    formControlValidationMessage.transclude = true;

    formControlValidationMessage.link = function(scope, element, attrs, formCtrl){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpFormControlValidationMessage');
        scope.form = { _state : stateProcessFactory.create(['processData'])};        
        legacyFactory.returnStatus(scope.form, 'pending', legacy);
        // scope.form = formCtrl;
        scope.form = legacyFactory.returnData(formCtrl, legacy);
        legacyFactory.returnStatus(scope.form, 'success', legacy);
    };

    return formControlValidationMessage;
}]);
var cooperativeDirectiveUiFormControl = angular.module('cooperative.directive.ui.formControl', []);

var cooperativeDirectiveUiFormControl; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpFormControlBasic
 * @restrict E
 * @require = ^form
 * @scope = {title: '@',controlName: '@'}
 * @templateUrl = 'template/directive.ui.formControl/formControl_basic.html'
 * @transclude = true
 *
 * @description
 * Author hiten
 *
 * Date 02/04/15
 *
 * Creates a vertical relationship between label and component with validation
 *
 */
cooperativeDirectiveUiFormControl.directive('ctpFormControlBasic',['legacyFactory', 'stateProcessFactory',
function(legacyFactory, stateProcessFactory){
    var formControlBasic = {};

    formControlBasic.restrict = 'E';
    formControlBasic.scope = {
        title: '@',
        controlName: '@'
    };
    formControlBasic.require = '^form';
    formControlBasic.templateUrl = 'template/directive.ui.formControl/formControl_basic.html';
    formControlBasic.transclude = true;

    formControlBasic.link = function(scope, element, attrs, formCtrl){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpFormControlBasic');
        scope.form = { _state : stateProcessFactory.create(['processData'])};        
        legacyFactory.returnStatus(scope.form, 'pending', legacy);
        // scope.form = formCtrl;
        scope.form = legacyFactory.returnData(formCtrl, legacy);
        legacyFactory.returnStatus(scope.form, 'success', legacy);
    };

    return formControlBasic;
}]);
var cooperativeDirectiveUiFormControl; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpFormControlHorizontal
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Creates a horizontal relationship between label and component with validation
 *
 * @param title The title for the label
 * @param controlName The name of the component that is tied to this control
 *
 * @returns {template} A horizontal label and component with validation
 */
cooperativeDirectiveUiFormControl.directive('ctpFormControlHorizontal',['legacyFactory', 'stateProcessFactory',
function(legacyFactory, stateProcessFactory){
    var formControlHorizontal = {};

    formControlHorizontal.restrict = 'E';
    formControlHorizontal.scope = {
        title: '@',
        controlName: '@'
    };
    formControlHorizontal.require = '^form';
    formControlHorizontal.templateUrl = 'template/directive.ui.formControl/formControl_horizontal.html';
    formControlHorizontal.transclude = true;

    formControlHorizontal.link = function(scope, element, attrs, formCtrl){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpFormControlHorizontal');
        scope.form = { _state : stateProcessFactory.create(['processData'])};        
        legacyFactory.returnStatus(scope.form, 'pending', legacy);

        // scope.form = formCtrl;
        scope.form = legacyFactory.returnData(formCtrl, legacy);
        legacyFactory.returnStatus(scope.form, 'success', legacy);
    };

    return formControlHorizontal;
}]);
var cooperativeDirectiveUiPanel = angular.module('cooperative.directive.ui.panel', []);

var cooperativeDirectiveUiPanel; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpPanel
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 23/03/15
 *
 * Creates a panel
 *
 * @returns {template} A panel
 */
cooperativeDirectiveUiPanel.directive('ctpPanel',[function(){
    var ctpPanel = {};
    ctpPanel.restrict = 'E';
    ctpPanel.transclude = true;
    ctpPanel.templateUrl = 'template/directive.ui.panel/panel.html';

    return ctpPanel;
}]);




var cooperativeDirectiveUiPanel; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpPanelBody
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/03/15
 *
 * Creates a panel body
 *
 * @returns {template} A panel body
 */
cooperativeDirectiveUiPanel.directive('ctpPanelBody',[function(){
    var ctpPanelBody = {};
    ctpPanelBody.restrict = 'E';
    ctpPanelBody.transclude = true;
    ctpPanelBody.templateUrl = 'template/directive.ui.panel/panel_body.html';

    return ctpPanelBody;
}]);


var cooperativeDirectiveUiPanel; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpPanelHeader
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/03/15
 *
 * Creates a panel header
 *
 * @param {title} title The title for the panel header
 *
 * @returns {template} A panel header
 */
cooperativeDirectiveUiPanel.directive('ctpPanelHeader',[function(){
    var ctpPanelHeader = {};
    ctpPanelHeader.restrict = 'E';
    ctpPanelHeader.scope = {
        title: '@'
    };
    ctpPanelHeader.transclude = true;
    ctpPanelHeader.templateUrl = 'template/directive.ui.panel/panel_header.html';

    return ctpPanelHeader;
}]);
var cooperativeDirectiveUiSearchBox = angular.module('cooperative.directive.ui.searchBox', []);

var cooperativeDirectiveUiSearchBox;

  cooperativeDirectiveUiSearchBox.controller('advancedSearchBoxCtrl', ['legacyFactory', 'stateProcessFactory', '$state','$stateParams','transcodeFactory',
  function(legacyFactory, stateProcessFactory, $state,$stateParams,transcodeFactory) {
    var vm = this;
    var legacy = legacyFactory.legacyMode(vm.legacy, 'advancedSearchBoxCtrl');

      //if no config object is passed, create it for config set.

      if(!vm.configObject){
        vm.configObject={};
      }

      //when submited
      vm.doSearch = function(){
        
        //object for the state.go funtion
        var stateOptions = {};
        
        angular.forEach($stateParams, function(val,key){
          if(val){
            delete $stateParams[key];
          }
        });

        //
        var requestParamName = 'queryTerms';
        if(vm.goStateParamName !== undefined && vm.goStateParamName !== null && vm.goStateParamName !== ''){
          requestParamName = vm.goStateParamName;
        }

        //set redirectState for state.go
        //default state
        var redirectState = 'ctpbase.searchResults';
        //if redirectState was provided, use it
        if(vm.goState !== undefined && vm.goState !== null && vm.goState !== ''){
          redirectState = vm.goState;
        }

        stateOptions[requestParamName] = { _state : stateProcessFactory.create(['processData'])};        
        legacyFactory.returnStatus(stateOptions[requestParamName], 'pending', legacy);
        
        //encrypt our parameter (will appear in the URL)
        //transcodeFactory.encrypt = function (str, version, appSession)


        transcodeFactory.encrypt(JSON.stringify(vm.searchOptions),null,true).then(
          function(transCoded){

            // stateOptions[requestParamName]=transCoded;
            stateOptions[requestParamName] = legacyFactory.returnData(transCoded, legacy);
            legacyFactory.returnStatus(stateOptions[requestParamName], 'success' ,legacy);
            
            //redirect to specified state with specified options
            $state.go(redirectState,stateOptions);

          }).catch(function(error){
          legacyFactory.returnStatus(stateOptions[requestParamName], 'error' ,legacy);
        });
      };


      // configuration set is provided, we will show precoded labels and boxes.
      if(vm.configurationSet){
        //hard coded set or possibly server retrieved sets...
        //dupe checking:
        if(vm.configurationSet=='dupeCheck'){
        vm.configObject = {
          name:{hide:false,label:{text:'Organization Name'}},
          address:{hide:false,label:{text:'Street Address'}},
          city:{hide:false,label:{text:'City'}},
          state:{hide:false,label:{text:'State/Region'}},
          postal:{hide:false,label:{text:'Postal Code'}},
          country:{hide:false,label:{text:'Country Code'}},
          lid:{hide:false,label:{text:'Legal Identifier'}},
          website:{hide:false,label:{text:'Website'}},
          phone:{hide:false,label:{text:'Phone Number'}},
          email:{hide:false,label:{text:'Email Address'}}
        };
        }

        //general searching no private data
        else if(vm.configurationSet=='generalPrivateExcluded'){
          vm.configObject = {
            name:{hide:false,label:{text:'Organization Name'}},
            address:{hide:false,label:{text:'Street Address'}},
            city:{hide:true},
            state:{hide:true},
            postal:{hide:true},
            country:{hide:true},
            lid:{hide:false,label:{text:'Legal Identifier'}},
            website:{hide:false,label:{text:'Website'}},
            phone:{hide:true,label:{text:'Phone Number'}},
            email:{hide:true,label:{text:'Email Address'}}
          };
        }

        else if(vm.configurationSet=='generalPrivateIncluded'){
          vm.configObject = {
            name:{hide:false,label:{text:'Organization Name'}},
            address:{hide:false,label:{text:'Street Address'}},
            city:{hide:true},
            state:{hide:true},
            postal:{hide:true},
            country:{hide:true},
            lid:{hide:false,label:{text:'Legal Identifier'}},
            website:{hide:false,label:{text:'Website'}},
            phone:{hide:false,label:{text:'Phone Number'}},
            email:{hide:false,label:{text:'Email Address'}}
          };
        }else{
          legacyFactory.returnStatus(vm.searchOptions, 'rejected',legacy, 'ctpAdvancedSearchBox configurationSet not valid');
          // console.warn('ctpAdvancedSearchBox configurationSet not valid');
        }
      }
  }]);

var cooperativeDirectiveUiSearchBox;

cooperativeDirectiveUiSearchBox.directive('ctpAdvancedSearchBox', function() {

    var directive = {};
    directive.templateUrl = 'template/directive.ui.searchBox/advancedSearchBox.html';
    directive.restrict = 'E';
    directive.controller = 'advancedSearchBoxCtrl';
    directive.controllerAs = 'advancedSearchBoxCtrl';
    directive.transclude = false;
    directive.scope = {

    };
    directive.bindToController = {
      goStateParamName :'@',
      goState : '@',
      legacy: '@',
      configurationSet:'@', //dupeCheck, generalPrivateExcluded, generalPrivateIncluded
      configObject: '=?'
      /*
        configObject template
        {
        name:{hide:'',label:''},
        address:{hide:'',label:''},
        city:{hide:'',label:''},
        state:{hide:'',label:''},
        postal:{hide:'',label:''},
        lid:{hide:'',label:''},
        website:{hide:'',label:''},
        phone:{hide:'',label:''},
        email:{hide:'',label:''}
        }
        */

    };



    directive.link = function(scope, element, attrs) {

    };

    return directive;
  });

var cooperativeDirectiveUiSearchBox;
cooperativeDirectiveUiSearchBox.controller('keywordSearchBoxCtrl', ['legacyFactory', 'stateProcessFactory', '$state','$stateParams',function(legacyFactory, stateProcessFactory, $state,$stateParams) {
    var vm = this;

    if(!vm.configObject){
      vm.configObject = {};
    }

    vm.doSearch = function(){
      
      var legacy = legacyFactory.legacyMode(vm.legacy, 'keywordSearchBoxCtrl');

      //object for state.go
      var stateOptions = {};
      //default parameter name

      //clear previous stateparams
      angular.forEach($stateParams, function(val,key){
        if(val){
          delete $stateParams[key];
        }
      });
      
      var requestParamName = 'keyword';      
      //if a custom parameter name has been provided, set that
      if(vm.goStateParamName !== undefined && vm.goStateParamName !== null && vm.goStateParamName !== ''){
        requestParamName = vm.goStateParamName;
      }

      stateOptions[requestParamName] = { _state : stateProcessFactory.create(['processData'])};        
      legacyFactory.returnStatus(stateOptions[requestParamName], 'pending', legacy);

      

      //assign search text, to parameter
      // stateOptions[requestParamName]=vm.keyword;
      stateOptions[requestParamName] = legacyFactory.returnData(vm.keyword, legacy);
      legacyFactory.returnStatus(stateOptions[requestParamName], 'success' ,legacy); 

      //default ui.router state
      var redirectState = 'ctpbase.searchResults';
      //if a custom redirect state is provided, use it
      if(vm.goState !== undefined && vm.goState !== null && vm.goState !== ''){
        redirectState = vm.goState;
      }
      //redirect.
      $state.go(redirectState,stateOptions);
    };


  }]);

var cooperativeDirectiveUiSearchBox;
cooperativeDirectiveUiSearchBox.directive('ctpKeywordSearchBox', function() {

    var directive = {};
    directive.templateUrl = 'template/directive.ui.searchBox/keywordSearchBox.html';
    directive.restrict = 'E';
    directive.controller = 'keywordSearchBoxCtrl';
    directive.controllerAs = 'keywordSearchBoxCtrl';
    directive.transclude = false;
    directive.scope = {

    };
    directive.bindToController = {
      //custom name for parameter
      goStateParamName: '@',
      //state for ui.router redirect
      goState: '@',
      legacy: '@',
      //configuration object
      /*
      {
      keyword:{
      'hide':boolean
        'label':string
      },
      searchButton:{
        'label':string
        }
      }
      */
      configObject: '=?'
    };

    directive.link = function(scope, element, attrs) {

    };

    return directive;
  });

var cooperativeDirectiveUiUtilsForm = angular.module('cooperative.directive.ui.utils.form', []);

var cooperativeDirectiveUiUtilsForm; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name compareTo
 * @require ngModel
 *
 * @description
 * Author hiten
 *
 * Date 23/03/15
 *
 * compares contents of ngModel value with another ngModel
 *
 *
 */
cooperativeDirectiveUiUtilsForm.directive('compareTo', [function() {
    var compareTo = {};

    compareTo.require = 'ngModel';
    compareTo.scope = {
        otherModelValue: '=compareTo'
    };

    compareTo.link = function(scope, element, attrs, ngModel) {
        ngModel.$validators.compareTo = function(modelValue) {
            return modelValue == scope.otherModelValue;
        };

        scope.$watch('otherModelValue', function() {
            ngModel.$validate();
        });
    };

    return compareTo;
}]);
var cooperativeDirectiveUiUtilsForm; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ngEnter
 *
 * @description
 * Author unknown
 *
 * Date 08/12/16
 *
 * allow enter key to execute a method
 *
 *
 */
cooperativeDirectiveUiUtilsForm.directive('ngEnter', [function() {
  return function(scope, element, attrs) {
      element.bind('keydown keypress', function(event) {
          if(event.which === 13) {
              scope.$apply(function(){
                  scope.$eval(attrs.ngEnter, {'event': event});
              });

              event.preventDefault();
          }
      });
  };
}]);

var cooperativeDirectiveUi = angular.module('cooperative.directive.ui', []);

var cooperativeDirectiveUi; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpFooter
 * @restrict E
 * @scope {copyright: '@', version: '@'}
 *
 * @description
 * Author hiten
 *
 * Date 03/22/15
 *
 * Loads the footer.html
 *
 */
cooperativeDirectiveUi.directive('ctpFooter',[function(){
    var ctpFooter = {};
    ctpFooter.restrict = 'E';
    ctpFooter.scope = {
        copyright: '@',
        version: '@'
    };
    ctpFooter.templateUrl = 'template/directive.ui/footer.html';

    return ctpFooter;
}]);
var cooperativeDirectiveUploads = angular.module('cooperative.directive.uploads', ['ui.bootstrap']);

var cooperativeDirectiveUploads; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.uploads:ctpArtifactObjRequester
 * @restrict E
 *
 * @requires biz.uploads.uploadsFactory
 * @description
 * Author Emiliano
 *
 * Date 26/06/17
 *
 * Returns an array of artifact objects for the provided list of items
 *
 * @param {array} items List of io objects to be processed
 * @param {string} [type='any'] Type of io object to look for
 * @param {string} [signature='ArtifactObject_001'] Signature to look for
 * @param {array} scopeprefix Variable to store the resulting items after processed
 */
cooperativeDirectiveUploads.directive('ctpArtifactObjRequester', ['legacyFactory', 'stateProcessFactory', '$timeout', 'uploadsFactory', 'dataFactory' ,function(legacyFactory, stateProcessFactory, $timeout, uploadsFactory, dataFactory) {
  var ctpArtifactObjRequester = {};
  var signatureTrack = {};

  ctpArtifactObjRequester.restrict = 'E';

  ctpArtifactObjRequester.link = function(scope, element, attrs) {
    var data = {};
    var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpArtifactObjRequester');
    scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};        
    legacyFactory.returnStatus(scope[attrs.scopeprefix], 'pending', legacy);


    attrs.parentinstanceid = attrs.parentinstanceid || 'any';

    function fillUpScopePrefix(items, instanceid, type, signature, useAppApiKey) {
      var typeValue = type;      
      signature = signature || 'ArtifactObject_001';

      if (type === 'any') {
        typeValue = '';
      }

      signatureTrack[attrs.scopeprefix] = signature;

      var collection = dataFactory.returnObjects(instanceid, signature, undefined, typeValue, undefined, items, undefined);

      if(useAppApiKey === undefined || !collection.length) {
        scope[attrs.scopeprefix] = [];
        return;
      }

      uploadsFactory.getFilesObject(collection, undefined, useAppApiKey)
      .then(function(objs){
        scope[attrs.scopeprefix] = legacyFactory.returnData((signature === signatureTrack[attrs.scopeprefix]) ? objs : [], legacy);
        legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success', legacy);
        // scope[attrs.scopeprefix] = (signature === signatureTrack[attrs.scopeprefix]) ? objs : [];
        return;
      }, function (err) {
        legacyFactory.returnStatus(scope[attrs.scopeprefix], 'error', legacy);
      });
    }

    /**
		 * var fillUpScopePrefix - Prepares data object to be processed
		 *
		 * @param  {object} obj  object that has been updated
		 * @param  {string} type attribute that has been updated
		 * @return {undefined}   undefined
		 */
		var fillUpDataObject = function(value, type) {
			if(type === 'instanceid' && !value) {
        return scope[attrs.scopeprefix] = [];
      }

			data[type] = value;

      if(data['itemsArray'] && typeof data['itemsArray'] === 'object' && data['type']) {
				fillUpScopePrefix(data['itemsArray'], data['instanceid'], data['type'], data['signature'], data['useAppApiKey']);
			} else {
        // legacyFactory.returnStatus(scope[attrs.scopeprefix], 'rejected', legacy, 'Data object returned empty');
        scope[attrs.scopeprefix] = legacyFactory.returnData([], legacy);
      }
		};

    scope.$watchCollection(attrs.items, function(newValue, oldValue) {
			fillUpDataObject(newValue, 'itemsArray');
    });

    scope.$watch(attrs.parentinstanceid, function(newValue) {
      fillUpDataObject(newValue, 'instanceid');
    });

    scope.$watch(attrs.type, function(newValue) {
      fillUpDataObject(newValue, 'type');
    });

    scope.$watch(attrs.signature, function(newValue) {
      fillUpDataObject(newValue, 'signature');
    });

    scope.$watch(attrs.useAppApiKey, function(newValue) {
      fillUpDataObject(newValue, 'useAppApiKey');
    });

  };

  return ctpArtifactObjRequester;
}]);

var cooperativeDirectiveUploads; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.uploads:ctpFileUpload
 * @restrict E
 * 
 * @description
 * Author hiten
 *
 * Date 04/08/15
 *
 *
 *
 *
 */
cooperativeDirectiveUploads.directive('ctpFileUpload', ['$timeout', 'legacyFactory', 'stateProcessFactory', '$q', 'uploadsFactory', 'dataFactory', function ($timeout, legacyFactory, stateProcessFactory, $q, uploadsFactory, dataFactory) {
    var ctpFileUpload = {};

    ctpFileUpload.restrict = 'E';

    ctpFileUpload.template = '<div ng-if="!viewOnly" ngf-drop ngf-select ngf-change="onFileSelect($file)" ng-model="file" ' +
        'drag-over-class="dragover" allow-dir="false"' +
        'ngf-pattern="\'application/pdf\'" ng-disabled="fileUploading" ngf-accept="\'application/pdf\'">' +
        '<div class="btn btn-default" ng-bind-html="dropAreaText"></div>' +
        '<i class="fa fa-spin fa-spinner text-lg" ng-show="fileUploading"></i>' +
        '</div>' +
        '<div ng-if="fileUri"><a ng-href="{{fileUri}}">{{artifactObject.instance.typeUri}}</a></div>';

    ctpFileUpload.scope = {
        dropAreaText: '=',
        collection: '=',
        poid: '=',
        legacy: '@',
        parentinstanceid: '=?',
        type: '=',
        useAppApiKey: '@',
        shard: '@',
        partition: '@',
        mode: '@',
        viewOnly: '=?'
    };

    ctpFileUpload.controller = ['$scope', function ($scope) {
        var legacy = legacyFactory.legacyMode($scope.legacy, 'ctpFileUpload');
        var _state = stateProcessFactory.create(['processData']);
        $scope.artifactObject = {};
        $scope.fileUploading = false;

        if ($scope.viewOnly !== true){
            $scope.viewOnly = false;
        }

        $scope.fileUri = undefined;

        var init = function () {

            if (!$scope.parentinstanceid) {
                $scope.parentinstanceid = 'any';
            }

            var collection = dataFactory.returnObjects($scope.parentinstanceid, 'ArtifactObject_001', 0, $scope.type, undefined, $scope.collection, undefined);

            if (collection.length > 0) {
                $scope.artifactObject = collection[0];

                $scope.fileUri = '';
                uploadsFactory.getFileUrl($scope.artifactObject.rootId, $scope.artifactObject.instance.typeUri, undefined, $scope.useAppApiKey)
                    .then(function (uri) {
                        $scope.fileUri = uri;
                    },
                    function (error) {
                        _state.processData.setError(error); 
                    }
                );
            } else {
                $scope.artifactObject = {};
            }

            
        };

        init();

        $scope.onFileSelect = function ($file) {
            if ($file !== null) {
                $scope.fileUploading = true;
                var hasKey = false;
                for (var key in $scope.artifactObject) {
                    hasKey = true;
                    break;
                }
                if (!hasKey) {
                    //create new artifact!
                    dataFactory.getNewEntityObject($scope.poid, $scope.parentinstanceid, 'ArtifactObject_001', $scope.type, undefined, undefined, $scope.useAppApiKey)
                        .then(
                            function (entity) {
                                $scope.artifactObject = entity;
                                save($file);
                                _state.processData.setResolved();
                            },
                            function(error){
                                _state.processData.setError(error);
                            }
                        );
                    } else {
                        save($file);
                        _state.processData.setResolved();
                    }
            } else {
                $scope.fileUploading = false;
                // _state.processData.setRejected('No file selected');
            }
        
            if(!legacy){
                $scope = ({ _data: $scope, _state : _state });
            }
        };

        var createReader = function (file) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var dataUri = e.target.result;
                uploadsFactory.uploadDataURI($scope.poid, dataUri, file.name, undefined, $scope.useAppApiKey);
                $scope.fileUploading = false;
            };

            reader.readAsDataURL(file);
        };

        var save = function (file) {
            $scope.artifactObject.timestamp = new Date().getTime();
            $scope.artifactObject.version = $scope.artifactObject.version + 1;
            $scope.artifactObject.instance.typeUri = file.name;

            var ioList = [];
            ioList.push($scope.artifactObject);

            dataFactory.postIoList($scope.shard, $scope.partition, $scope.mode, ioList, false, $scope.useAppApiKey).then(function (data) {
                createReader(file);
            });
        };
    }];

    ctpFileUpload.link = function (scope, element, attrs) {
        var legacy = legacyFactory.legacyMode(scope.legacy, 'ctpFileUpload');
        $timeout(function() {
            if(!legacy){
                scope = ({ _data: scope });
            }
        });
    };

    return ctpFileUpload;
}]);

/**
  * @ngdoc directive
  * @name directive.uploads:ctpImageCrop
  * @restrict E
  *
  * @description
  * Created by hiten on 04/08/15.
  */
var cooperativeDirectiveUploads;

cooperativeDirectiveUploads.directive('ctpImageCrop', ['$timeout','legacyFactory', 'stateProcessFactory', function ($timeout, legacyFactory, stateProcessFactory ) {
    var ctpImageCrop = {};
    ctpImageCrop.restrict = 'E';

    ctpImageCrop.template = '<div class="component"><div class="overlay" ng-style="{width: cropwidth, height: cropheight}"><div class="overlay-inner"></div></div><div class="resize-container"><span class="resize-handle resize-handle-nw"></span><span class="resize-handle resize-handle-ne"></span><img class="resize-image" ng-src="{{imagesrc}}" alt="Image" /><img style="display: none;" ng-src="{{imagesrc}}" /><span class="resize-handle resize-handle-se"></span><span class="resize-handle resize-handle-sw"></span></div><button class="btn-crop js-crop">Crop <img class="icon-crop" src="img/crop.svg"></button></div>';

    ctpImageCrop.scope = {
        imagesrc: '@',
        cropwidth: '@',
        cropheight: '@',
        oncrop: '&',
        legacy: '@'
    };

    ctpImageCrop.link = function(scope, elm){
        var _state = stateProcessFactory.create(['processData']);
        var legacy = legacyFactory.legacyMode(scope.legacy, 'ctpImageCrop');

        var resizeableImage = function() {

            var $container,
                orig_src = elm.find('img')[1],
                image_target = elm.find('img')[0],
                event_state = {},
                constrain = false,
                resize_canvas = document.createElement('canvas');

            var init = function(){

                // Assign the container to a variable
                $container =  $(image_target).parent('.resize-container');

                // Add events
                $container.on('mousedown touchstart', '.resize-handle', startResize);
                $container.on('mousedown touchstart', 'img', startMoving);

                //$('.js-crop').on('click', crop);
                $(elm.find('.js-crop')[0]).on('click', crop);
            };

            var startResize = function(e){
                e.preventDefault();
                e.stopPropagation();
                saveEventState(e);
                $(document).on('mousemove touchmove', resizing);
                $(document).on('mouseup touchend', endResize);
            };

            var endResize = function(e){
                e.preventDefault();
                $(document).off('mouseup touchend', endResize);
                $(document).off('mousemove touchmove', resizing);
            };

            var saveEventState = function(e){
                // Save the initial event details and container state
                event_state.container_width = $container.width();
                event_state.container_height = $container.height();
                event_state.container_left = $container.offset().left;
                event_state.container_top = $container.offset().top;
                event_state.mouse_x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft();
                event_state.mouse_y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();

                // This is a fix for mobile safari
                // For some reason it does not allow a direct copy of the touches property
                if(typeof e.originalEvent.touches !== 'undefined'){
                    event_state.touches = [];
                    $.each(e.originalEvent.touches, function(i, ob){
                        event_state.touches[i] = {};
                        event_state.touches[i].clientX = 0+ob.clientX;
                        event_state.touches[i].clientY = 0+ob.clientY;
                    });
                }

                event_state.evnt = e;
            };

            var resizing = function(e){

                var mouse={},width,height,left,top;
                mouse.x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft();
                mouse.y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();
                // Position image differently depending on the corner dragged and constraints
                if( $(event_state.evnt.target).hasClass('resize-handle-se') ){
                    width = mouse.x - event_state.container_left;
                    height = mouse.y  - event_state.container_top;
                    left = event_state.container_left;
                    top = event_state.container_top;
                } else if($(event_state.evnt.target).hasClass('resize-handle-sw') ){
                    width = event_state.container_width - (mouse.x - event_state.container_left);
                    height = mouse.y  - event_state.container_top;
                    left = mouse.x;
                    top = event_state.container_top;
                } else if($(event_state.evnt.target).hasClass('resize-handle-nw') ){
                    width = event_state.container_width - (mouse.x - event_state.container_left);
                    height = event_state.container_height - (mouse.y - event_state.container_top);
                    left = mouse.x;
                    top = mouse.y;
                    if(constrain || e.shiftKey){
                        top = mouse.y - ((width / orig_src.width * orig_src.height) - height);
                    }
                } else if($(event_state.evnt.target).hasClass('resize-handle-ne') ){
                    width = mouse.x - event_state.container_left;
                    height = event_state.container_height - (mouse.y - event_state.container_top);
                    left = event_state.container_left;
                    top = mouse.y;
                    if(constrain || e.shiftKey){
                        top = mouse.y - ((width / orig_src.width * orig_src.height) - height);
                    }
                }

                // Optionally maintain aspect ratio
                if(constrain || e.shiftKey){
                    height = width / orig_src.width * orig_src.height;
                }

                //if(width > min_width && height > min_height && width < max_width && height < max_height){
                // To improve performance you might limit how often resizeImage() is called
                resizeImage(width, height);
                // Without this Firefox will not re-calculate the the image dimensions until drag end
                $container.offset({'left': left, 'top': top});
                //}
            };

            var resizeImage = function(width, height){
                resize_canvas.width = width;
                resize_canvas.height = height;
                resize_canvas.getContext('2d').drawImage(orig_src, 0, 0, width, height);
                $(image_target).attr('src', resize_canvas.toDataURL('image/png'));
            };

            var startMoving = function(e){
                e.preventDefault();
                e.stopPropagation();
                saveEventState(e);
                $(document).on('mousemove touchmove', moving);
                $(document).on('mouseup touchend', endMoving);
            };

            var endMoving = function(e){
                e.preventDefault();
                $(document).off('mouseup touchend', endMoving);
                $(document).off('mousemove touchmove', moving);
            };

            var moving = function(e){
                var  mouse={}, touches;
                e.preventDefault();
                e.stopPropagation();

                touches = e.originalEvent.touches;

                mouse.x = (e.clientX || e.pageX || touches[0].clientX) + $(window).scrollLeft();
                mouse.y = (e.clientY || e.pageY || touches[0].clientY) + $(window).scrollTop();
                $container.offset({
                    'left': mouse.x - ( event_state.mouse_x - event_state.container_left ),
                    'top': mouse.y - ( event_state.mouse_y - event_state.container_top )
                });
                // Watch for pinch zoom gesture while moving
                if(event_state.touches && event_state.touches.length > 1 && touches.length > 1){
                    var width = event_state.container_width, height = event_state.container_height;
                    var a = event_state.touches[0].clientX - event_state.touches[1].clientX;
                    a = a * a;
                    var b = event_state.touches[0].clientY - event_state.touches[1].clientY;
                    b = b * b;
                    var dist1 = Math.sqrt( a + b );

                    a = e.originalEvent.touches[0].clientX - touches[1].clientX;
                    a = a * a;
                    b = e.originalEvent.touches[0].clientY - touches[1].clientY;
                    b = b * b;
                    var dist2 = Math.sqrt( a + b );

                    var ratio = dist2 /dist1;

                    width = width * ratio;
                    height = height * ratio;
                    // To improve performance you might limit how often resizeImage() is called
                    resizeImage(width, height);
                }
            };

            var crop = function(){
                //Find the part of the image that is inside the crop box
                var overlay = $(elm.find('.overlay')[0]);

                var crop_canvas,
                    left = overlay.offset().left - $container.offset().left,
                    top =  overlay.offset().top - $container.offset().top,
                    width = overlay.width(),
                    height = overlay.height();

                crop_canvas = document.createElement('canvas');
                crop_canvas.width = width;
                crop_canvas.height = height;

                crop_canvas.getContext('2d').drawImage(image_target, left, top, width, height, 0, 0, width, height);

                
                if(scope._data){
                    scope = scope._data;    
                }
                scope.oncrop({$croppedImage: crop_canvas.toDataURL('image/png')});
                _state.processData.setResolved();
            };

            init();
        };

        resizeableImage();
        $timeout(function() {
            if(!legacy){
                scope = ({ _data: scope, _state : _state });
            }
        });
    };



    return ctpImageCrop;


}]);

var cooperativeDirectiveUploads; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.uploads:ctpImageUploadViewer
 * @restrict E
 * 
 * @description
 * Author hiten
 *
 * Date 04/08/15
 *
 *
 *
 *
 */
cooperativeDirectiveUploads.directive('ctpImageUploadViewer', ['$timeout', 'uploadsFactory', 'dataFactory' ,'legacyFactory', 'stateProcessFactory', 
function($timeout, uploadsFactory, dataFactory, legacyFactory, stateProcessFactory) {
    var ctpImageUploadViewer = {};
    

    ctpImageUploadViewer.restrict = 'E';

    ctpImageUploadViewer.scope = {
        collection : '=',
        poid: '=',
        parentinstanceid: '=',
        type: '=',
        legacy: '@',
        altText: '=',
        useAppApiKey: '@'
    };

    ctpImageUploadViewer.template = '<img ng-src="{{imageSrc}}" alt="{{altText}}" />';


    ctpImageUploadViewer.link = function(scope, element, attrs) {
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpImageUploadViewer');
        var _state = stateProcessFactory.create(['processData']);

        if (!attrs.parentinstanceid) {
            attrs.parentinstanceid = 'any';
        }
        var collection = dataFactory.returnObjects(scope.parentinstanceid, 'ArtifactObject_001', 0, scope.type, undefined, scope.collection, undefined);

        if (collection.length > 0){
            scope.entity = collection[0];

            scope.imageSrc = '';
            uploadsFactory.getFileUrl(scope.entity.rootId, scope.entity.instance.typeUri, undefined, scope.useAppApiKey)
                .then(function(uri){
                    scope.imageSrc = uri;
                    _state.processData.setResolved();
                });
        } else {
            scope.entity = {};
            // _state.processData.setRejected('No collection');
        }
        $timeout(function() {
            if(!legacy){
                scope = ({ _data: scope, _state : _state });
            }
        });
    };

    return ctpImageUploadViewer;
}]);

var cooperativeDirectiveUploads; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name directive.uploads:ctpImageUploadWithCrop
 * @restrict E
 * 
 * @description
 * Author hiten
 *
 * Date 04/08/15
 *
 *
 *
 *
 */
cooperativeDirectiveUploads.directive('ctpImageUploadWithCrop', ['$timeout', '$uibModal', 'uploadsFactory', 'dataFactory', 'legacyFactory', 'stateProcessFactory',function($timeout, $uibModal, uploadsFactory, dataFactory, legacyFactory, stateProcessFactory) {
    var ctpImageUploadWithCrop = {};

    ctpImageUploadWithCrop.restrict = 'E';

    ctpImageUploadWithCrop.templateUrl = 'template/directive.uploads/imageUploadWithCrop.html';

    ctpImageUploadWithCrop.scope = {
        dropAreaText : '=',
        cropModalSize : '@',
        collection : '=',
        poid: '=',
        parentinstanceid: '=',
        type: '=',
        cropwidth: '@',
        cropheight: '@',
        useAppApiKey: '@',
        shard: '@',
        partition: '@',
        legacy: '@',
        mode: '@',
        outputFilename : '='
    };

    ctpImageUploadWithCrop.controller = ['$scope', function($scope){
        $scope.entity = {};


        $scope.onFileSelect = function ($files) {

            $scope.files = $files;
            $scope.upload = [];

            var createReader = function(file){

                var reader = new FileReader();
                $scope.filename = file.name;

                reader.onload = function (e) {
                    $scope.previewImgSrc = e.target.result;
                    open($scope.cropModalSize);
                    $scope.$apply();
                };
                reader.readAsDataURL(file);
            };

            for (var i = 0; i < $files.length; i++) {
                var file = $files[i];
                createReader(file);
            }
        };

        var open = function (size) {
            var modalInstance = $uibModal.open({
                templateUrl: 'template/directive.uploads/imageUploadWithCropModal.html',
                controller: 'imageUploadWithCropUploadModal',
                backdrop: 'static',
                size: size,
                resolve: {
                    previewImgSrc: function () {
                        return $scope.previewImgSrc;
                    },
                    cropwidth: function () {
                        return $scope.cropwidth;
                    },
                    cropheight: function () {
                        return $scope.cropheight;
                    }
                }                
            });

            modalInstance.result.then(function (croppedImageSrc) {
                $scope.croppedImageSrc = croppedImageSrc;

                var hasKey = false;

                for (var key in $scope.entity){
                    hasKey = true;
                    break;
                }

                if (!hasKey){
                    //create new artifact!
                    dataFactory.getNewEntityObject($scope.poid, $scope.parentinstanceid, 'ArtifactObject_001', $scope.type, undefined, undefined, $scope.useAppApiKey)
                        .then(function(entity){
                            entity.instance.typeUri = $scope.filename;
                            $scope.entity.version = $scope.entity.version + 1;
                            var ioList = [];
                            ioList.push(entity);
                            dataFactory.postIoList($scope.shard, $scope.partition, $scope.mode, ioList, false, $scope.useAppApiKey).then(function(data){
                                uploadsFactory.uploadDataURI($scope.poid, croppedImageSrc, $scope.filename, undefined, $scope.useAppApiKey);
                                if($scope.outputFilename !== undefined){
                                    $scope.outputFilename = $scope.filename;
                                }
                            });
                        });
                } else {
                    //update artifact
                    $scope.entity.timestamp = new Date().getTime();
                    $scope.entity.version = $scope.entity.version + 1;
                    $scope.entity.instance.typeUri = $scope.filename;
                    var ioList = [];
                    ioList.push($scope.entity);
                    dataFactory.postIoList($scope.shard, $scope.partition, $scope.mode, ioList, false, $scope.useAppApiKey).then(function(data){
                        uploadsFactory.uploadDataURI($scope.poid, croppedImageSrc, $scope.filename, undefined, $scope.useAppApiKey);
                        if($scope.outputFilename !== undefined){
                            $scope.outputFilename = $scope.filename;
                        }
                    });
                }

                $timeout(function() {
                    $scope.$apply();                    
                });
            });
        };
    }];

    ctpImageUploadWithCrop.link = function(scope, element, attrs, ctrl) {

        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpImageUploadWithCrop');
        var _state = stateProcessFactory.create(['processData']);

        if (!attrs.parentinstanceid) {
            attrs.parentinstanceid = 'any';
        }

        if (!attrs.cropwidth) { attrs.cropwidth = '250px'; }
        if (!attrs.cropheight) { attrs.cropheight = '250px'; }

        var collection = dataFactory.returnObjects(scope.parentinstanceid, 'ArtifactObject_001', 0, scope.type, undefined, scope.collection, undefined);

        if (collection.length > 0){
            scope.entity = collection[0];

            scope.croppedImageSrc = '';
            uploadsFactory.getFileUrl(scope.entity.rootId, scope.entity.instance.typeUri, undefined, scope.useAppApiKey)
                .then(function(uri){
                    scope.croppedImageSrc = uri;
                });
        } else {
            scope.entity = {};
        }
        $timeout(function() {
            if(!legacy){
                scope = ({ _data: scope, _state : _state });
            }
        });
    };
    return ctpImageUploadWithCrop;
}]);

cooperativeDirectiveUploads.directive('imgOnError', function() {
    return {
        restrict:'A',
        link: function(scope, element, attr) {
            element.on('error', function() {
                element.addClass('user-photo-blank');
            });
        }
    };
});

cooperativeDirectiveUploads.controller('imageUploadWithCropUploadModal', ['$scope', '$uibModalInstance', 'previewImgSrc', 'cropwidth', 'cropheight', function ($scope, $uibModalInstance, previewImgSrc, cropwidth, cropheight) {

    $scope.previewImgSrc = previewImgSrc;
    $scope.cropwidth = cropwidth;
    $scope.cropheight = cropheight;

    $scope.cropHandler = function($croppedImage){
        $uibModalInstance.close($croppedImage);
    };

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}]);

var cooperativeDirectiveWorkflowRequest = angular.module('cooperative.directive.workflow.request', []);

var cooperativeDirectiveWorkflowRequest; //Defined in 0module.js

/**
 * @ngdoc directive
 * @name ctpWorkflowRequester
 * @restrict E
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Returns an array of items in a workflow queue
 *
 * @param {string} workflow/queue name
 * @param {string} scopeprefix The name for the returned data
 *
 * @returns {Array} Array of the workflow items
 */
cooperativeDirectiveWorkflowRequest.directive('ctpWorkflowRequester',['$rootScope', 'workflowFactory', 'WorkFlowStatusConstants', 'legacyFactory', 'stateProcessFactory', function($rootScope, workflowFactory, WorkFlowStatusConstants, legacyFactory, stateProcessFactory){
    var ctpWorkflowRequester = {};
    ctpWorkflowRequester.restrict = 'E';
    ctpWorkflowRequester.scope = true;

    ctpWorkflowRequester.link = function(scope, element, attrs){
        var legacy = legacyFactory.legacyMode(attrs.legacy, 'ctpWorkflowRequester');
        scope[attrs.scopeprefix] = { _state : stateProcessFactory.create(['processData'])};        
        legacyFactory.returnStatus(scope[attrs.scopeprefix], 'pending', legacy);

        // function to get data collection from resource api
        scope.workflowItemMeta = {};
        var getDataCollection = function () {
            var queueStatusList = [];
            if (!(attrs.queueStatus === undefined || attrs.queueStatus === null)){
                queueStatusList = JSON.parse(attrs.queueStatus);
            }

            var queueNameList = [];
            if (!(attrs.workflowName === undefined || attrs.workflowName === null)){
                queueNameList = JSON.parse(attrs.workflowName);
            }

            workflowFactory.getItems(queueNameList, attrs.size, true, queueStatusList, attrs.poid, undefined, attrs.useAppApiKey)
            .then(function(datacollection){
                    for(var i=0; i < datacollection.results.length; i++){
                        // scope.workflowItemMeta[datacollection.results[i].idSerial] = { updateInProgress : false };
                        if(legacy){
                            scope.workflowItemMeta[datacollection.results[i].idSerial] = { updateInProgress : false };
                        }
                        else{
                            scope.workflowItemMeta[datacollection.results[i].idSerial] = {};
                            scope.workflowItemMeta[datacollection.results[i].idSerial]._data = { updateInProgress : false };
                        }                        
                    }

                    scope[attrs.scopeprefix] = legacyFactory.returnData(datacollection, legacy);
                    legacyFactory.returnStatus(scope[attrs.scopeprefix], 'success', legacy);
                    
                });

        };

        // initial run
        getDataCollection();

        // listener to watch for changes in delay-interval value
        scope.$watchGroup([
                function() { return attrs.scopeprefix; },
                function() { return attrs.workflowName; },
                function() { return attrs.size; },
                function() { return attrs.queueStatus; },
                function() { return attrs.poid; },
                function() { return attrs.useAppApiKey; }
            ],
            function (newVals, oldVals){

               getDataCollection();
            }
        );

        var performAction = function(itemString, assignTo ,workFlowStatus){

            var item = JSON.parse(itemString);
            // scope.workflowItemMeta[item.idSerial].updateInProgress = true;

            if(legacy){
                scope.workflowItemMeta[item.idSerial].updateInProgress = true;
            }
            else{
                scope.workflowItemMeta[item.idSerial]._data.updateInProgress = true;
            }



            var queueList = [];
            queueList.push(item.queueName);

            workflowFactory.assignUserToWorkFlowItem(item.data, queueList, workFlowStatus, assignTo, attrs.useAppApiKey)
                .then(function(){
                    $rootScope.$broadcast('workflow_update_triggered', item.id);
                });
        };

        element.on('click', '[action-target="' + WorkFlowStatusConstants.SELECTED + '"]', function(){
            performAction($(this).attr( 'data-item' ), $(this).attr( 'data-assignee' ), WorkFlowStatusConstants.SELECTED);
        });

        element.on('click', '[action-target="' + WorkFlowStatusConstants.BUSY + '"]', function(){
            performAction($(this).attr( 'data-item' ), $(this).attr( 'data-assignee' ), WorkFlowStatusConstants.BUSY);
        });

        element.on('click', '[action-target="' + WorkFlowStatusConstants.CLOSED + '"]', function(){
            performAction($(this).attr( 'data-item' ), $(this).attr( 'data-assignee' ), WorkFlowStatusConstants.CLOSED);
        });

        element.on('click', '[action-target="' + WorkFlowStatusConstants.IDLE + '"]', function(){
            performAction($(this).attr( 'data-item' ), WorkFlowStatusConstants.IDLE);
        });

        element.on('click', '[action-target="' + WorkFlowStatusConstants.READY + '"]', function(){
            performAction($(this).attr( 'data-item' ), WorkFlowStatusConstants.READY);
        });

        $rootScope.$on('workflow_update_triggered', function(event, poid){
            getDataCollection();
        });

    };

    return ctpWorkflowRequester;
}]);

var cooperativeFiltersContent = angular.module('cooperative.filters.content', []);

var cooperativeFiltersContent; //Defined in 0module.js

/**
 * @ngdoc filter
 * @name LookupKeyValueFilter
 *
 * @description
 * Author hiten
 *
 * Date 02/04/15
 *
 * Gets data that has the specified property set to a specific value
 * @param {string} items The array of items to filter
 * @param {string} property The property of the items to check
 * @param {string} propertyValue The value of the property that should be kept in the results
 *
 * @returns {Array} Array of filtered items
 */
cooperativeFiltersContent.filter('LookupKeyValue', ['LookupFactory',function (LookupFactory) {
    return function (items, property, propertyValue) {
        return LookupFactory.utils.getItemsFromListByProperty(items, property, propertyValue);
    };
}]);

/**
 * @ngdoc filter
 * @name LookupSubNteeCodeFilter
 *
 * @description
 * Author hiten
 *
 * Date 02/04/15
 *
 * Gets the first key where the specified property is set to a specific value.  Returns 'nil' if no matches.
 *
 * @param {string} items The array of items to filter
 * @param {string} property The property of the items to check
 * @param {string} propertyValue The value of the property that should be kept in the results
 *
 * @returns {string} key of sub ntee code
 */
cooperativeFiltersContent.filter('LookupSubNteeCode', ['LookupFactory',function (LookupFactory) {
    return function (items, property, propertyValue) {
        var returnList = LookupFactory.utils.getItemsFromListByProperty(items, property, propertyValue);

        if (returnList.length > 0){
            return returnList[0].key;
        } else{
            return 'nil';
        }

    };
}]);

/**
 * @ngdoc filter
 * @name LookupKeyForValue
 *
 * @description
 * Author hiten
 *
 * Date 02/04/15
 *
 * Converts the data that is passed in by value to its key as specified in the array of key values
 * @param {string} value The value of the item
 * @param {Array} list The array of key values
 *
 * @returns {string} The key for the value
 */
cooperativeFiltersContent.filter('LookupKeyForValue', ['LookupFactory',function (LookupFactory) {
    return function (value, list) {
        return LookupFactory.utils.getKeyForValue(list, value);
    };
}]);
var cooperativeFilters = angular.module('cooperative.filters', []);

var cooperativeFilters; //Defined in 0module.js
/**
 * @ngdoc filter
 * @name ExpireDateFilter
 *
 * @description
 * Author swhittemore
 *
 * Date 02/04/15
 *
 * Converts a date to an expiration date
 * @param {object} dateTimeValue A string or Date representation of a date time value
 * @param {number=} expireDays The number of days to expire in.  The default is 60.
 *
 * @returns {Date} The expiration date
 */
cooperativeFilters.filter('ExpireDate', function () {
    return function (dateTimeValue, expireDays) {
        var newDate;
        // Default to 60 days if expireDays not passed in
        expireDays = typeof expireDays !== 'undefined' ? expireDays : 60;

        if (Object.prototype.toString.call(dateTimeValue) === '[object String]') {
            newDate = new Date(1000 * 60 * 60 * 24 * expireDays + Date.parse(dateTimeValue));
        } else if(Object.prototype.toString.call(dateTimeValue) === '[object Date]') {
            newDate = new Date(1000 * 60 * 60 * 24 * expireDays + dateTimeValue.getTime());
        } else if (!isNaN(dateTimeValue) && dateTimeValue != null && dateTimeValue !== undefined) {
            newDate = new Date(1000 * 60 * 60 * 24 * expireDays + dateTimeValue);
        } else {
            return dateTimeValue;
        }

        // 1000ms * 60s * 60m * 24h * (expireDays)
        return newDate;
    };
});
var cooperativeInterceptor = angular.module('cooperative.interceptor', []);

var cooperativeInterceptor; //Defined in 0module.js

/**
 * @ngdoc service
 * @name cooperativeHttpInterceptor
 *
 * @description
 * Author hiten
 *
 * Date 18/03/15
 *
 * Interceptor for for custom CTP actions
 */
cooperativeInterceptor.factory('cooperativeHttpInterceptor', ['$q', 'cooperative', 'appUserSession', function($q, cooperative, appUserSession) {
    var cooperativeHttpInterceptor = {};


    /**
     * @ngdoc method
     * @name cooperativeHttpInterceptor#response
     * @description
     * Intercepts the returned data.  If it is a successful call, it only returns the actual data without the additional CTP information.
     *
     * @param {string} response the standard http response
     *
     * @returns {response} the updated http response
     */
    cooperativeHttpInterceptor.response = function(response){
        var failed = false;
        var intercept = false;

        if (cooperative.baseDomains && cooperative.baseDomains.length > 0){
            for (var i=0; i < cooperative.baseDomains.length; i++){
                if (response.config.url.indexOf(cooperative.baseDomains[i]) > -1){
                    intercept = true;
                    break;
                }
            }
        }

        if (intercept === true){
            var data = response.data;
            if (data.returnStatus.status) {
                if (data.returnStatus.status === 'ok' || data.returnStatus.status === 'request_processed') {
                    if (data.returnStatus.reason === 'success') {
                        if (data.returnStatus.data) {
                            response.data = data.returnStatus.data;
                        } else {
                            failed = true;
                        }
                    } else {
                        failed = true;
                    }
                } else {
                    failed = true;
                }
            } else {
                failed = true;
            }
        }

        if (failed){
            response.status = 500;
            response.statusText = 'invalid object returned';
            return $q.reject(response);
        } else {
            return response;
        }

    };

    /**
     * @ngdoc method
     * @name cooperativeHttpInterceptor#responseError
     * @description
     * Handles errors when unauthorized or forbidden and retries the operation
     *
     * @param {string} response the standard http response
     *
     * @returns {HttpPromise} the updated http response
     */
    cooperativeHttpInterceptor.responseError = function(response){
        var intercept = false;

        if (cooperative.baseDomains && cooperative.baseDomains.length > 0){
            for (var i=0; i < cooperative.baseDomains.length; i++){
                if (response.config.url.indexOf(cooperative.baseDomains[i]) > -1){
                    intercept = true;
                    break;
                }
            }
        }

        if (intercept === true){
            if(response.status === 403 || response.status === 408) {
                //DO NOT RUN gotoPlatformLogout if actioning a serverside logout.

                var logUserOut = false;

                if(response.config.url.indexOf('?action=logout') === -1){
                    logUserOut = true;
                }

                if(logUserOut){
                    if(response.data){
                        if (response.data.returnStatus){
                            if (response.data.returnStatus.reason == 'failed_authentication'){
                                logUserOut = false;
                            }
                        }
                    }
                }

                if(logUserOut && !document.debugMode){
                    appUserSession.getUser().gotoPlatformLogout();
                }
            }
        }

        return $q.reject(response);
    };

    return cooperativeHttpInterceptor;
}]);

var cooperativeProvider = angular.module('cooperative.provider', []);

/**
 * Created by hiten on 31/07/15.
 */
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeS3
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the s3 URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeArtifact', [function() {
    var cooperativeArtifactProvider = {};
    var artifactURL = 'https://localhost:33536/services/artifacts/';

    /**
     * @ngdoc method
     * @name cooperativeResources#setResourcesURL
     * @description
     * Sets a new resources URL.
     *
     * @param {string} _artifactURL resources URL to set
     */
    cooperativeArtifactProvider.setArtifactURL = function(_artifactURL) {
        artifactURL = _artifactURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeResources#$get
     * @description
     * Gets the resources URL.
     *
     * @returns {string} resources URL
     */
    cooperativeArtifactProvider.$get = function() {
        var getter = {};

        getter.artifactURL = artifactURL;

        return getter;
    };

    return cooperativeArtifactProvider;
}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeAuth
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the auth URLs.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeAuth', [function() {
    var cooperativeAuthProvider = {};
    var authURL = 'https://localhost:33792/services/auth';
    var accountMgmtURL = 'https://localhost:33280/services/amgmt/';
    var accountMgmtResetURL = 'https://localhost:39936/services/amgmtreset/';
    var authSessionKey = 'ctpauthtoken';
    var oAuthURL = 'https://localhost:46848/services/oauthsignin/';
    /**
     * @ngdoc method
     * @name cooperativeAuth#setAuthURL
     * @description
     * Sets a new auth URL.
     *
     * @param {string} _authURL auth URL to set
     */
    cooperativeAuthProvider.setAuthURL = function(_authURL) {
        authURL = _authURL;
    };

    cooperativeAuthProvider.setAuthRegURL = function(_authRegURL) {
        accountMgmtURL = _authRegURL;
        console.warn('deprecated: cooperativeAuthProvider.setAuthRegURL please use cooperativeAuthProvider.setAccountMgmtURL');
    };

    cooperativeAuthProvider.setAuthResetURL = function(_authResetURL) {
        accountMgmtResetURL = _authResetURL;
        console.warn('deprecated: cooperativeAuthProvider.setAuthResetURL please use cooperativeAuthProvider.setAccountMgmtResetURL');

    };
    cooperativeAuthProvider.setAccountMgmtURL = function(_accountMgmtURL) {
        accountMgmtURL = _accountMgmtURL;
    };

    cooperativeAuthProvider.setAccountMgmtResetURL = function(_accountMgmtResetURL) {
        accountMgmtResetURL = _accountMgmtResetURL;
    };

    cooperativeAuthProvider.setAuthSessionKey = function(_authSessionKey) {
        authSessionKey = _authSessionKey;
    };

    cooperativeAuthProvider.setOAuthURL = function(_oAuthURL) {
        oAuthURL = _oAuthURL;
    };
    
    /**
     * @ngdoc method
     * @name cooperativeAuth#$get
     * @description
     * Gets the shared auth or auth URL.
     *
     * @returns {string} buffers URL
     */
    cooperativeAuthProvider.$get = function() {
        var getter = {};

        getter.authURL = authURL;
        getter.accountMgmtURL = accountMgmtURL;
        getter.accountMgmtResetURL = accountMgmtResetURL;
        getter.authSessionKey = authSessionKey;
        getter.oAuthURL = oAuthURL;

        return getter;
    };

    return cooperativeAuthProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeBuffers
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the buffers URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeBuffers', [function () {
    var cooperativeBuffersProvider = {};
    var buffersURL = 'https://localhost:51462/services/buffers/';

    /**
     * @ngdoc method
     * @name cooperativeBuffers#setBuffersURL
     * @description
     * Sets a new buffers URL.
     *
     * @param {string} _buffersURL buffers URL to set
     */
    cooperativeBuffersProvider.setBuffersURL = function (_buffersURL) {
        buffersURL = _buffersURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeBuffers#$get
     * @description
     * Gets the buffers URL.
     *
     * @returns {string} buffers URL
     */
    cooperativeBuffersProvider.$get = function(){
        var getter = {};

        getter.buffersURL = buffersURL;

        return getter;
    };

    return cooperativeBuffersProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeConstraints
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the constraints URL.  A default URL will be returned unless one is set.
 *
 */
cooperativeProvider.provider('cooperativeConstraints', [function () {
    var cooperativeConstraintsProvider = {};
    var constraintsURL = 'https://localhost:34816/services/constraint/';

    /**
     * @ngdoc method
     * @name cooperativeConstraints#setConstraintsURL
     * @description
     * Sets a new constraints URL.
     *
     * @param {string} _constraintsURL constraints URL to set
     */
    cooperativeConstraintsProvider.setConstraintsURL = function (_constraintsURL) {
        constraintsURL = _constraintsURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeConstraints#$get
     * @description
     * Gets the constraints URL.
     *
     * @returns {string} constraints URL
     */
    cooperativeConstraintsProvider.$get = function(){
        var getter = {};

        getter.constraintsURL = constraintsURL;

        return getter;
    };

    return cooperativeConstraintsProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeContent
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the content URLs.  The default URLs will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeContent', [function() {
    var cooperativeContentProvider = {};

    var contentURL = 'https://localhost:35072/services/content/';

    /**
     * @ngdoc method
     * @name cooperativeContent#setContentURL
     * @description
     * Sets a new content URL.
     *
     * @param {string} _contentURL content URL to set
     */
    cooperativeContentProvider.setContentURL = function(_contentURL) {
        contentURL = _contentURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeContent#$get
     * @description
     * Gets the content URLs.
     *
     * @returns {string} content URLs
     */
    cooperativeContentProvider.$get = function() {
        var getter = {};

        getter.contentURL = contentURL;
        getter.getTranslatedContent = function(session, appName, partition) {
            if (partition === undefined || partition === null || partition === '' ){
                partition = 'test';
            }
            return contentURL + 'content:' + partition + '/' + session + '/translations/' + appName;
        };

        return getter;
    };

    return cooperativeContentProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperative
 *
 * @description
 * Author hiten
 *
 * Date 30/04/15
 *
 * Stores common information needed for ctp applications
 */
cooperativeProvider.provider('cooperative', [function () {
    var cooperativeProvider = {};
    var apiKey = 'nil';
    var appName = 'nil';
    var baseDomains = ['localhost'];
    var partition = 'test';

    /**
     * @ngdoc method
     * @name cooperative#setSignUpOrgPath
     * @description
     * Sets a new sign up org path.
     *
     * @param {string} _signUpOrgPath sign up org path to set
     */
    //cooperativeProvider.setSignUpOrgPath = function (_signUpOrgPath) {
    //    signUpOrgPath = _signUpOrgPath;
    //};

    /**
     * @ngdoc method
     * @name cooperative#setApiKey
     * @description
     * Sets a new api key.
     *
     * @param {string} _apiKey api key to set
     */
    cooperativeProvider.setApiKey = function (_apiKey){
        apiKey = _apiKey;
    };

    /**
     * @ngdoc method
     * @name cooperative#setAppName
     * @description
     * Sets a new app name.
     *
     * @param {string} _appName app name to set
     */
    cooperativeProvider.setAppName = function (_appName) {
        appName = _appName;
    };

    /**
     * @ngdoc method
     * @name cooperative#setBaseDomain
     * @description
     * Sets a new base domain.
     *
     * @param {string} _baseDomain base domain to set
     */
    cooperativeProvider.setBaseDomain = function (_baseDomain) {
        var _baseDomains = [];
        _baseDomains.push(_baseDomain);

        baseDomains = _baseDomains;
    };

    cooperativeProvider.setBaseDomains = function(_baseDomains){
        baseDomains = _baseDomains;
    };

    cooperativeProvider.setPartition = function(_partition){
        partition = _partition;
    };

    /**
     * @ngdoc property
     * @name cooperative#getters
     * @description
     * - **`apiKey`** - {string} - api key
     *
     * - **`appName`** - {string} - name of the app (used in translations, keylists, etc...)
     *
     * - **`baseDomain`** - {string} - base domain for the URL
     */
    cooperativeProvider.$get = function(){
        var getter = {};

        getter.apiKey = apiKey;
        getter.appName = appName;
        getter.baseDomains = baseDomains;
        getter.partition = partition;

        return getter;
    };

    return cooperativeProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeData
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the data URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeData', [function () {
    var cooperativeDataProvider = {};
    var dataURL = 'https://localhost:35584/services/data/';

    /**
     * @ngdoc method
     * @name cooperativeData#setDataURL
     * @description
     * Sets a new data URL.
     *
     * @param {string} _dataURL data URL to set
     */
    cooperativeDataProvider.setDataURL = function (_dataURL) {
        dataURL = _dataURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeData#$get
     * @description
     * Gets the data URL.
     *
     * @returns {string} data URL
     */
    cooperativeDataProvider.$get = function(){
        var getter = {};

        getter.dataURL = dataURL;

        return getter;
    };

    return cooperativeDataProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeGeoip
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the data URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeGeoip', [function() {
    var cooperativeGeoip = {};
    var geoipURL = 'https://localhost:36352/services/geoip/';

    /**
     * @ngdoc method
     * @name cooperativeData#setDataURL
     * @description
     * Sets a new data URL.
     *
     * @param {string} _dataURL data URL to set
     */
    cooperativeGeoip.setGeoipURL = function(_geoipURL) {
        geoipURL = _geoipURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeData#$get
     * @description
     * Gets the data URL.
     *
     * @returns {string} data URL
     */
    cooperativeGeoip.$get = function() {
        var getter = {};

        getter.geoipURL = geoipURL;

        return getter;
    };

    return cooperativeGeoip;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeLookup
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the lookup URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeLookup', [function () {
    var cooperativeLookupProvider = {};
    var lookupURL = 'https://localhost:51462/services/lookup/';

    /**
     * @ngdoc method
     * @name cooperativeLookup#setLookupURL
     * @description
     * Sets a new lookup URL.
     *
     * @param {string} _lookupURL lookup URL to set
     */
    cooperativeLookupProvider.setLookupURL = function (_lookupURL) {
        lookupURL = _lookupURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeLookup#$get
     * @description
     * Gets the lookup URL.
     *
     * @returns {string} lookup URL
     */
    cooperativeLookupProvider.$get = function(){
        var getter = {};

        getter.lookupURL = lookupURL;

        return getter;
    };

    return cooperativeLookupProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeObjects
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the object URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeObjects', [function () {
    var cooperativeObjectsProvider = {};
    var objectURL = 'https://localhost:36864/services/object/';

    /**
     * @ngdoc method
     * @name cooperativeObjects#setObjectsURL
     * @description
     * Sets a new object URL.
     *
     * @param {string} _objectURL object URL to set
     */
    cooperativeObjectsProvider.setObjectsURL = function (_objectURL) {
        objectURL = _objectURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeObjects#$get
     * @description
     * Gets the object URL.
     *
     * @returns {string} object URL
     */
    cooperativeObjectsProvider.$get = function(){
        var getter = {};

        getter.objectURL = objectURL;

        return getter;
    };

    return cooperativeObjectsProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeOutreach
 *
 * @description
 * Author hiten
 *
 * Date 02/03/17
 *
 * Provider for the object URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeOutreach', [function () {
    var cooperativeOutreachProvider = {};
    var outreachURL = 'https://localhost:34304/services/outreach/';
    var outreachTemplates = {};

    cooperativeOutreachProvider.setOutreachURL = function (_outreachURL) {
        outreachURL = _outreachURL;
    };

    cooperativeOutreachProvider.setOutreachTemplates = function(_outreachTemplates){
        outreachTemplates = _outreachTemplates;
    };

    cooperativeOutreachProvider.$get = function(){
        var getter = {};

        getter.outreachURL = outreachURL;
        getter.outreachTemplates = outreachTemplates;

        return getter;
    };

    return cooperativeOutreachProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeQueries
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the queries URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeQueries', [function() {
    var cooperativeQueriesProvider = {};
    var queriesURL = 'https://localhost:37376/services/query/';
    var duplCheckURL = 'https://localhost:37376/services/duplcheck/';
    var trigramURL = 'https://localhost:37376/services/trigram/';
    var keywordURL = 'https://localhost:37376/services/keyword/';
    var trilikeURL = 'https://localhost:37376/services/trilike/';
    var directoryURL = 'https://localhost:37376/services/directory/';

    /**
     * @ngdoc method
     * @name cooperativeQueries#setQueriesURL
     * @description
     * Sets a new queries URL.
     *
     * @param {string} _queriesURL queries URL to set
     */
    cooperativeQueriesProvider.setQueriesURL = function(_queriesURL) {
        queriesURL = _queriesURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeQueries#setDuplCheckURL
     * @description
     * Sets a new duplicate check URL.
     *
     * @param {string} _duplCheckURL queries URL to set
     */
    cooperativeQueriesProvider.setDuplCheckURL = function(_duplCheckURL) {
        duplCheckURL = _duplCheckURL;
    };

    cooperativeQueriesProvider.setTrigramURL = function(_trigramURL){
        trigramURL = _trigramURL;
    };

    cooperativeQueriesProvider.setKeywordURL = function(_keywordURL){
        keywordURL = _keywordURL;
    };

    cooperativeQueriesProvider.setTrilikeURL = function(_trilikeURL){
        trilikeURL = _trilikeURL;
    };

    cooperativeQueriesProvider.setDirectoryURL = function(_directoryURL){
        directoryURL = _directoryURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeQueries#$get
     * @description
     * Gets the queries URL.
     *
     * @returns {string} queries URL
     */
    cooperativeQueriesProvider.$get = function() {
        var getter = {};

        getter.queriesURL = queriesURL;
        getter.duplCheckURL = duplCheckURL;
        getter.trigramURL = trigramURL;
        getter.keywordURL = keywordURL;
        getter.trilikeURL = trilikeURL;
        getter.directoryURL = directoryURL;

        return getter;
    };

    return cooperativeQueriesProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeQueues
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the queues URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeQueues', [function() {
    var cooperativeQueuesProvider = {};
    var queuesURL = 'https://localhost:37632/services/queue/';

    /**
     * @ngdoc method
     * @name cooperativeQueues#setQueuesURL
     * @description
     * Sets a new queues URL.
     *
     * @param {string} _queuesURL queues URL to set
     */
    cooperativeQueuesProvider.setQueuesURL = function(_queuesURL) {
        queuesURL = _queuesURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeQueues#$get
     * @description
     * Gets the queues URL.
     *
     * @returns {string} queues URL
     */
    cooperativeQueuesProvider.$get = function() {
        var getter = {};

        getter.queuesURL = queuesURL;

        return getter;
    };

    return cooperativeQueuesProvider;

}]);
/**
 * Created by hiten on 02/11/2015.
 */
var cooperativeProvider; //Defined in 0module.js

cooperativeProvider.provider('cooperativeRegistration', [function () {
    var cooperativeRegistration = {};
    var userRegQueueName = 'registration_user';
    var orgRegQueueName = 'registration_org';

    var userRegCookieName = 'user_signup_key';
    var orgRegCookieName = 'org_signup_key';

    cooperativeRegistration.setUserRegQueueName = function (_userRegQueueName) {
        userRegQueueName = _userRegQueueName;
    };

    cooperativeRegistration.setOrgRegQueueName = function (_orgRegQueueName) {
        orgRegQueueName = _orgRegQueueName;
    };

    cooperativeRegistration.setUserRegCookieName = function (_userRegCookieName) {
        userRegCookieName = _userRegCookieName;
    };

    cooperativeRegistration.setOrgRegCookieName = function (_orgRegCookieName) {
        orgRegCookieName = _orgRegCookieName;
    };

    /**
     * @ngdoc method
     * @name cooperativeData#$get
     * @description
     * Gets the data URL.
     *
     * @returns {string} data URL
     */
    cooperativeRegistration.$get = function(){
        var getter = {};

        getter.userRegQueueName = userRegQueueName;
        getter.orgRegQueueName = orgRegQueueName;
        getter.userRegCookieName = userRegCookieName;
        getter.orgRegCookieName = orgRegCookieName;

        return getter;
    };

    return cooperativeRegistration;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeResources
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the resources URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeResources', [function() {
    var cooperativeResourcesProvider = {};
    var resourcesURL = 'https://localhost:39168/services/resource/';

    /**
     * @ngdoc method
     * @name cooperativeResources#setResourcesURL
     * @description
     * Sets a new resources URL.
     *
     * @param {string} _resourcesURL resources URL to set
     */
    cooperativeResourcesProvider.setResourcesURL = function(_resourcesURL) {
        resourcesURL = _resourcesURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeResources#$get
     * @description
     * Gets the resources URL.
     *
     * @returns {string} resources URL
     */
    cooperativeResourcesProvider.$get = function() {
        var getter = {};

        getter.resourcesURL = resourcesURL;

        return getter;
    };

    return cooperativeResourcesProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeSqs
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the sqs URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeSqs', [function () {
    var cooperativeSqsProvider = {};
    var sqsURL = 'https://localhost:51462/services/sqs/';

    /**
     * @ngdoc method
     * @name cooperativeSqs#setSqsURL
     * @description
     * Sets a new sqs URL.
     *
     * @param {string} _sqsURL sqs URL to set
     */
    cooperativeSqsProvider.setSqsURL = function (_sqsURL) {
        sqsURL = _sqsURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeSqs#$get
     * @description
     * Gets the sqs URL.
     *
     * @returns {string} sqs URL
     */
    cooperativeSqsProvider.$get = function(){
        var getter = {};

        getter.sqsURL = sqsURL;

        return getter;
    };

    return cooperativeSqsProvider;

}]);
var cooperativeProvider;

/**
 * Created by Hiten on 30/11/2015.
 */
cooperativeProvider.provider('cooperativeSso', [function() {
    var cooperativeSsoProvider = {};

    var platformLogin = {
        url: {
            path: 'http://0.0.0.0:8001/#/login'
        }
    };

    var platformLogout = {
        url: {
            path: 'http://0.0.0.0:8001/#/logout'
        }
    };

    var platformPermissionUpdate = {
        url: {
            path: 'http://0.0.0.0:8001/#/updatePermission'
        }
    };

    var platformSignup = {
        url: {
            path: 'http://0.0.0.0:8001/#/signup'
        }
    };

    var platformAccount = {
        url: {
            path: 'http://0.0.0.0:8001/#/account'
        }
    };

    cooperativeSsoProvider.setPlatformLogout = function(_platformLogout) {
        platformLogout = _platformLogout;
    };

    cooperativeSsoProvider.setPlatformLogin = function(_platformLogin) {
        platformLogin = _platformLogin;
    };

    cooperativeSsoProvider.setPlatformPermissionUpdate = function(_platformPermissionUpdate) {
        platformPermissionUpdate = _platformPermissionUpdate;
    };

    cooperativeSsoProvider.setPlatformSignup = function(_platformSignup) {
        platformSignup = _platformSignup;
    };

    cooperativeSsoProvider.setPlatformAccount = function(_platformAccount) {
        platformAccount = _platformAccount;
    };

    cooperativeSsoProvider.$get = function() {
        var getter = {};

        getter.platformLogin = platformLogin;
        getter.platformSignup = platformSignup;
        getter.platformLogout = platformLogout;
        getter.platformPermissionUpdate = platformPermissionUpdate;
        getter.platformAccount = platformAccount;
        
        return getter;
    };

    return cooperativeSsoProvider;
}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeStacks
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the stacks URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeStacks', [function () {
    var cooperativeStacksProvider = {};
    var stacksURL = 'https://localhost:51462/services/stack/';

    /**
     * @ngdoc method
     * @name cooperativeStacks#setStacksURL
     * @description
     * Sets a new stacks URL.
     *
     * @param {string} _stacksURL stacks URL to set
     */
    cooperativeStacksProvider.setStacksURL = function (_stacksURL) {
        stacksURL = _stacksURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeStacks#$get
     * @description
     * Gets the stacks URL.
     *
     * @returns {string} stacks URL
     */
    cooperativeStacksProvider.$get = function(){
        var getter = {};

        getter.stacksURL = stacksURL;

        return getter;
    };

    return cooperativeStacksProvider;

}]);
/**
 * Created by Hiten on 10/02/2016.
 */
var cooperativeProvider; //Defined in 0module.js

cooperativeProvider.provider('cooperativeSysrw', [function () {
    var cooperativeSysrwProvider = {};

    var mergeQueue = '';
    var writeQueue = '';

    cooperativeSysrwProvider.setMergeQueue = function(_mergeQueue){
        mergeQueue = _mergeQueue;
    };

    cooperativeSysrwProvider.setWriteQueue = function(_writeQueue){
        writeQueue = _writeQueue;
    };

    cooperativeSysrwProvider.$get = function(){
        var getter = {};

        getter.mergeQueue = mergeQueue;
        getter.writeQueue = writeQueue;

        return getter;
    };

    return cooperativeSysrwProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeUtilities
 *
 * @description
 * Author hiten
 *
 * Date 04/04/17
 *
 * 
 */
cooperativeProvider.provider('cooperativeUtilities', [function () {
    var cooperativeUtilitiesProvider = {};
    var transcodeURL = 'https://localhost:33001/services/transcode/';


    cooperativeUtilitiesProvider.setTranscodeURL = function(_transcodeURL){
        transcodeURL = _transcodeURL;
    };


    cooperativeUtilitiesProvider.$get = function(){
        var getter = {};

        getter.transcodeURL = transcodeURL;

        return getter;
    };

    return cooperativeUtilitiesProvider;

}]);
var cooperativeProvider; //Defined in 0module.js

/**
 * @ngdoc provider
 * @name cooperativeWebSocket
 *
 * @description
 * Author hiten
 *
 * Date 04/28/15
 *
 * Provider for the web socket URL.  A default URL will be returned unless one is set.
 */
cooperativeProvider.provider('cooperativeWebSocket', [function () {
    var cooperativeWebSocketProvider = {};
    var webSocketURL = 'wss://localhost:8888/query/iterate/';

    /**
     * @ngdoc method
     * @name cooperativeWebSocket#setWebSocketURL
     * @description
     * Sets a new web socket URL.
     *
     * @param {string} _webSocketURL web socket URL to set
     */
    cooperativeWebSocketProvider.setWebSocketURL = function(_webSocketURL){
        webSocketURL = _webSocketURL;
    };

    /**
     * @ngdoc method
     * @name cooperativeWebSocket#$get
     * @description
     * Gets the web socket URL.
     *
     * @returns {string} web socket URL
     */
    cooperativeWebSocketProvider.$get = function(){
        var getter = {};

        getter.webSocketURL = webSocketURL;

        return getter;
    };

    return cooperativeWebSocketProvider;

}]);
var cooperativeUtils = angular.module('cooperative.utils', []);

/**
 * Created by hiten on 31/10/2015.
 */
var cooperativeUtils; //Defined in 0module.js

/**
 * @ngdoc factory
 * @name ReconnectingWebSocket
 *
 * @description
 * Author hiten
 *
 * Date 04/05/15
 *
 * Wraps the ReconnectingWebSocket 3rd party tools.
 */
cooperativeUtils.factory( 'CryptoJS', function() {
    return window.CryptoJS;
});

var cooperativeUtils; //Defined in 0module.js

cooperativeUtils.directive('uiToggleClass', ['$timeout', '$document', function($timeout, $document) {
    return {
      restrict: 'AC',
      link: function(scope, el, attr) {
        el.on('click', function(e) {
          e.preventDefault();
          var classes = attr.uiToggleClass.split(','),
              targets = (attr.target && attr.target.split(',')) || new Array(el),
              key = 0;

          angular.forEach(classes, function( _class ) {
            var target = targets[(targets.length && key)];
            if ( _class.indexOf( '*' ) !== -1 ) {
              magic(_class, target);
            }

            $( target ).toggleClass(_class);
            key ++;
          });
     
          $(el).toggleClass('active');

          function magic(_class, target){
            var patt = new RegExp( '\\s' +
                _class.
                  replace( /\*/g, '[A-Za-z0-9-_]+' ).
                  split( ' ' ).
                  join( '\\s|\\s' ) +
                '\\s', 'g' );

            var cn = ' ' + $(target).className + ' ';
            while ( patt.test( cn ) ) {
              cn = cn.replace( patt, ' ' );
            }
            $(target).className = $.trim( cn );
          }
        });
      }
    };
  }]);
var cooperativeUtils; //Defined in 0module.js

/**
 * @ngdoc factory
 * @name _
 *
 * @description
 * Author hiten
 *
 * Date 02/04/15
 *
 * Wraps the _ 3rd party tools.
 */
cooperativeUtils.factory( '_', function() {
    return window._;
});

var cooperativeUtils; //Defined in 0module.js

/**
 * @ngdoc factory
 * @name _
 *
 * @description
 * Author hiten
 *
 * Date 02/04/15
 *
 * Wraps the _ 3rd party tools.
 */
cooperativeUtils.factory( '_xdomaincookie', function() {
    return window.xDomainCookie;
});

angular.module('cooperative.tpls', ['template/directive.auth.permissions/login_form.html', 'template/directive.data/activity_insert.html', 'template/directive.data/activity_update.html', 'template/directive.data/form.html', 'template/directive.ui.components.appDrawer/appDrawer.html', 'template/directive.ui.components.osNavbar/osNavbar.html', 'template/directive.ui.components.userDrawer/userDrawer.html', 'template/directive.ui.formControl.validation/formControl_validationMessages.html', 'template/directive.ui.formControl/formControl_basic.html', 'template/directive.ui.formControl/formControl_horizontal.html', 'template/directive.ui.panel/panel_body.html', 'template/directive.ui.panel/panel_header.html', 'template/directive.ui.panel/panel.html', 'template/directive.ui.searchBox/advancedSearchBox.html', 'template/directive.ui.searchBox/keywordSearchBox.html', 'template/directive.ui/footer.html', 'template/directive.uploads/imageUploadWithCrop.html', 'template/directive.uploads/imageUploadWithCropModal.html']);

angular.module("template/directive.auth.permissions/login_form.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.auth.permissions/login_form.html",
    "<div class=\"text-danger wrapper\" ng-show=authError>{{authError}}</div><div class=form-group><label for=email class=sr-only><span translate=common.access.yourEmailAddress></span></label><input id=email type=email placeholder=\"{{'common.access.yourEmailAddress' | translate}}\" class=form-control ng-model=user.email required></div><div class=form-group><label for=password class=sr-only><span translate=common.access.yourPassword></span></label><input id=password type=password placeholder=\"{{'common.access.yourPassword' | translate}}\" class=form-control ng-model=user.password required></div>");
}]);

angular.module("template/directive.data/activity_insert.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.data/activity_insert.html",
    "<div ng-if=ioActivityData><ctp-data-finder collection=ioActivityData signature=EntityObject_001 type=Organization index=0 scopeprefix=orgEntity><ctp-data-creator poid={{orgEntity[0].rootId}} parentinstanceid={{orgEntity[0].instanceId}} signature=PurposeObject_001 type=priActivity scopeprefix=orgPriActivity use-app-api-key={{useAppApiKey}}><ctp-data-creator poid={{orgEntity[0].rootId}} parentinstanceid={{orgEntity[0].instanceId}} signature=PurposeObject_001 type=subActivity scopeprefix=orgSubActivity use-app-api-key={{useAppApiKey}}><div ctp-form-register=\"\" entity=orgPriActivity form-item=priActNew><ctp-form-control-basic title=\"{{'common.activity.PrimaryActivity' | translate}}\" control-name=priAct><ctp-purpose-select lookup-name={{prilookupName}} name=priAct transprefix={{transPriPath}} ng-model=orgPriActivity.typeValue child-link=orgSubActivity.typeValue required></ctp-purpose-select></ctp-form-control-basic></div><div ctp-form-register=\"\" entity=orgSubActivity form-item=subActNew><ctp-form-control-basic title=\"{{'common.activity.SubActivity' | translate}}\" control-name=subAct><ctp-purpose-select lookup-name={{sublookupName}} name=subAct transprefix={{transSubPath}} ng-model=orgSubActivity.typeValue parent-link=orgPriActivity.typeValue required></ctp-purpose-select></ctp-form-control-basic></div><div ng-show=\"npo && showfour\"><ctp-data-creator poid={{orgEntity[0].rootId}} parentinstanceid={{orgEntity[0].instanceId}} signature=PurposeObject_001 type=priActivityNTEE scopeprefix=orgPriActivityNTEE><ctp-data-creator poid={{orgEntity[0].rootId}} parentinstanceid={{orgEntity[0].instanceId}} signature=PurposeObject_001 type=subActivityNTEE scopeprefix=orgSubActivityNTEE><div ng-if=orgPriActivityNTEE><div ctp-form-register=\"\" entity=orgPriActivityNTEE form-item=priActNtee><div ng-if=npo><ctp-form-control-basic title=\"{{'common.activity.PrimaryActivityNTEE' | translate}}\" control-name=priActNtee><input ng-model=orgPriActivityNTEE.typeValue ctp-activitycode-ntee-converter sub-activity={{orgSubActivity.typeValue}} only-primary=true name=priActNtee class=form-control ng-hide=true><label class=w-full><span translate=common.activity.primaryNTEE.primaryActivityNtee{{orgPriActivityNTEE.typeValue}} class=form-control></span></label></ctp-form-control-basic></div></div></div><div ng-if=orgSubActivityNTEE><div ctp-form-register=\"\" entity=orgSubActivityNTEE form-item=subActNtee><div ng-if=npo><ctp-form-control-basic title=\"{{'common.activity.SubActivityNTEE' | translate}}\" control-name=subActNtee><input ng-model=orgSubActivityNTEE.typeValue ctp-activitycode-ntee-converter sub-activity={{orgSubActivity.typeValue}} name=subActNtee class=form-control ng-hide=true><label class=w-full><span translate=common.activity.subNTEE.subActivityNtee{{orgSubActivityNTEE.typeValue}} class=form-control></span></label></ctp-form-control-basic></div></div></div></ctp-data-creator></ctp-data-creator></div></ctp-data-creator></ctp-data-creator></ctp-data-finder></div>");
}]);

angular.module("template/directive.data/activity_update.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.data/activity_update.html",
    "<div ng-if=ioActivityData><ctp-data-finder collection=ioActivityData signature=EntityObject_001 type=Organization index=0 scopeprefix=orgEntity><ctp-data-finder collection=ioActivityData parentinstanceid={{orgEntity[0].instanceId}} signature=PurposeObject_001 type=priActivity scopeprefix=orgPurposePriActivity excludedtypes='[\"priActivityNTEE\",\"subActivity\",\"subActivityNTEE\"]'><ctp-data-finder collection=ioActivityData parentinstanceid={{orgEntity[0].instanceId}} signature=PurposeObject_001 type=subActivity scopeprefix=orgPurposeSubActivity excludedtypes='[\"priActivityNTEE\",\"priActivity\",\"subActivityNTEE\"]'><ctp-data-finder collection=ioActivityData parentinstanceid={{orgEntity[0].instanceId}} signature=PurposeObject_001 type=priActivityNTEE scopeprefix=orgPurposePriActivityNTEE excludedtypes='[\"subActivity\",\"priActivity\",\"subActivityNTEE\"]'><ctp-data-finder collection=ioActivityData parentinstanceid={{orgEntity[0].instanceId}} signature=PurposeObject_001 type=subActivityNTEE scopeprefix=orgPurposeSubActivityNTEE excludedtypes='[\"subActivity\",\"priActivity\",\"priActivityNTEE\"]'><div ctp-form-register=\"\" entity=orgPurposePriActivity[0] form-item=priActEdit><ctp-form-control-basic title=\"{{'common.activity.PrimaryActivity' | translate}}\" control-name=priAct><ctp-purpose-select lookup-name={{prilookupName}} name=priAct transprefix={{transPriPath}} ng-model=orgPurposePriActivity[0].typeValue child-link=orgPurposeSubActivity[0].typeValue required></ctp-purpose-select></ctp-form-control-basic></div><div ctp-form-register=\"\" entity=orgPurposeSubActivity[0] form-item=subActEdit><ctp-form-control-basic title=\"{{'common.activity.SubActivity' | translate}}\" control-name=subAct><ctp-purpose-select lookup-name={{sublookupName}} name=subAct transprefix={{transSubPath}} ng-model=orgPurposeSubActivity[0].typeValue parent-link=orgPurposePriActivity[0].typeValue required></ctp-purpose-select></ctp-form-control-basic></div><div ng-show=\"npo && showfour && showNtee == 'true'\"><div ng-if=orgPurposePriActivityNTEE[0]><div ctp-form-register=\"\" entity=orgPurposePriActivityNTEE[0] form-item=priActNtee><ctp-form-control-basic title=\"{{'common.activity.PrimaryActivityNTEE' | translate}}\" control-name=priActNtee><input ng-model=orgPurposePriActivityNTEE[0].typeValue ctp-activitycode-ntee-converter sub-activity={{orgPurposeSubActivity[0].typeValue}} only-primary=true name=priActNtee class=form-control ng-hide=true><br><label class=w-full><span translate=common.activity.primaryNTEE.primaryActivityNtee{{orgPurposePriActivityNTEE[0].typeValue}} class=form-control></span></label></ctp-form-control-basic></div></div><div ng-if=orgPurposeSubActivityNTEE[0]><div ctp-form-register=\"\" entity=orgPurposeSubActivityNTEE[0] form-item=subActNtee><ctp-form-control-basic title=\"{{'common.activity.SubActivityNTEE' | translate}}\" control-name=subActNtee><input ng-model=orgPurposeSubActivityNTEE[0].typeValue ctp-activitycode-ntee-converter sub-activity={{orgPurposeSubActivity[0].typeValue}} name=subActNtee class=form-control ng-hide=true><br><label class=w-full><span translate=common.activity.subNTEE.subActivityNtee{{orgPurposeSubActivityNTEE[0].typeValue}} class=form-control></span></label></ctp-form-control-basic></div></div></div></ctp-data-finder></ctp-data-finder></ctp-data-finder></ctp-data-finder></ctp-data-finder></div>");
}]);

angular.module("template/directive.data/form.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.data/form.html",
    "<form novalidate class=form-vertical name=outerForm><ng-form name={{formName}} ng-transclude></ng-form></form>");
}]);

angular.module("template/directive.ui.components.appDrawer/appDrawer.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui.components.appDrawer/appDrawer.html",
    "<style>.ctp-app-btn-sq-md {\n" +
    "width: 75px !important;\n" +
    "height: 75px !important;\n" +
    "font-size: 10px;\n" +
    "}</style><ul ng-class=appsDrawerClass><li ng-class=appDrawerPopupType uib-dropdown auto-close=outsideClick><a id=single-button uib-dropdown-toggle><span class=\"glyphicon glyphicon-th\"></span></a><ul class=\"dropdown-menu animated fadeIn\" uib-dropdown-menu role=menu><li role=menuitem><ctp-search-paging-requester scopeprefix=results query-string=\"data @> '{&quot;io&quot;:[{&quot;signature&quot;: &quot;EntityObject_001&quot;, &quot;type&quot;: &quot;System&quot;, &quot;typeValue&quot;: &quot;APP&quot;}]}'\" signature-list='[\"EntityObject_001\", \"NameObject_001\", \"DescriptiveTextObject_001\",\"StatusObject_001\", \"WebsiteObject_001\", \"ArtifactObject_001\", \"RelationshipObject_001\"]' size=300 return-mode=true union=false intersection=false use-app-api-key=true shard=user><uib-tabset active=active class=nav-tabs-alt justified=true><uib-tab ng-repeat=\"(k,v) in results.items | appGroupings | orderBy: k\"><uib-tab-heading><ctp-data-requester poid={{k}} signaturelist='[\"NameObject_001\"]' scopeprefix=ioDataGroup use-app-api-key=true shard=content><ctp-data-finder collection=ioDataGroup signature=NameObject_001 type=groupName index=0 scopeprefix=groupName>{{groupName[0].typeValue}}</ctp-data-finder></ctp-data-requester><i class=\"glyphicons glyphicons-cluster\"></i></uib-tab-heading><div><p><span ng-repeat=\"a in v\"><ctp-data-requester poid={{a.id}} signaturelist='[\"NameObject_001\", \"EntityObject_001\", \"ArtifactObject_001\", \"WebsiteObject_001\"]' scopeprefix=ioDataGroup use-app-api-key=true shard=user><ctp-data-finder collection=ioDataGroup signature=WebsiteObject_001 type=appUrl index=0 scopeprefix=appUrl><a class=\"btn ctp-app-btn-sq-md\" target=_blank ng-href={{appUrl[0].typeValue}}><span ng-if=\"ioDataGroup.length >0\"><ctp-image-upload-viewer collection=ioDataGroup poid=a.id type=productImg use-app-api-key=true></ctp-image-upload-viewer></span><ctp-data-finder collection=ioDataGroup signature=NameObject_001 type=appName index=0 scopeprefix=appName>{{appName[0].typeValue}}</ctp-data-finder></a></ctp-data-finder></ctp-data-requester></span></p></div></uib-tab></uib-tabset></ctp-search-paging-requester></li></ul></li></ul>");
}]);

angular.module("template/directive.ui.components.osNavbar/osNavbar.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui.components.osNavbar/osNavbar.html",
    "<ctp-navbar ctp-navbar-class=\"app-header navbar\"><ctp-navbar-header ctp-navbar-container-class><button ng-class=\"{'hidden': !displayAsideToggle}\" class=\"pull-left visible-xs\" ui-toggle-class=off-screen data-target=.app-aside ui-scroll-to=app><i class=\"glyphicon glyphicon-align-justify\"></i></button> <button class=\"pull-right visible-xs dk\" ui-toggle-class=show data-target=.navbar-collapse><i class=\"glyphicon glyphicon glyphicon-chevron-down\"></i></button> <a class=\"navbar-brand hidden-xs\" href=#><img src=https://accounts.tsgctp.org/img/ts_icon.jpg alt=TechSoup width=20 height=20></a><a class=\"navbar-brand visible-xs bg-white\" style=\"display: block; margin: 0 auto\" href=#><img src=https://accounts.tsgctp.org/img/ts_icon.jpg alt=TechSoup width=20 height=20></a></ctp-navbar-header><ctp-navbar-body ctp-navbar-body-class=\"collapse pos-rlt navbar-collapse box-shadow bg-white-only\" collapse-id=bs-example-navbar-collapse-1><div class=\"nav navbar-nav hidden-xs hidden\"><a href class=\"btn no-shadow navbar-btn\" ng-click=\"app.settings.asideFolded = !app.settings.asideFolded\"><i class=\"fa {{app.settings.asideFolded ? 'fa-indent' : 'fa-dedent'}} fa-fw\"></i></a></div><div class=navbar-text>One account for everything TechSoup.</div><ctp-user-drawer current-user=currentUser></ctp-user-drawer><ctp-apps-drawer current-user=currentUser app-drawer-popup-type=dropdown apps-drawer-class=\"nav navbar-nav navbar-right\" apps-drawer-width=300px></ctp-apps-drawer><ul class=\"nav navbar-nav navbar-right\"><li></li><li class=dropdown uib-dropdown auto-close=outsideClick><a href class=dropdown-toggle uib-dropdown-toggle><i class=\"glyphicon glyphicon-bell\"></i> <span class=visible-xs-inline>Notifications</span> <span class=\"badge badge-sm up bg-danger pull-right-xs\">3</span></a><div class=\"dropdown-menu w-xl animated fadeIn\"><div class=\"panel bg-white\"><div class=\"panel-heading b-light bg-light\"><strong>You have <span>1</span> notifications</strong></div><div class=list-group><span class=list-group-item><span class=\"clear block m-b-none\"><a href=http://localhost:8002/#/ target=_blank>EMS</a>: A change has occurred to Ticket #: <a href=\"http://localhost:8002/#/profile/5796873615221_a519/main?wfId=32749730\" target=_blank>32749730</a><br><small class=text-muted>10 minutes ago</small></span></span></div><div class=\"panel-footer text-sm\"><a class=pull-right><i class=\"fa fa-angle-right\"></i></a> <a href=#notes data-toggle=\"class:show animated fadeInRight\">See all the notifications</a></div></div></div></li></ul></ctp-navbar-body></ctp-navbar>");
}]);

angular.module("template/directive.ui.components.userDrawer/userDrawer.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui.components.userDrawer/userDrawer.html",
    "<ul class=\"nav navbar-nav navbar-right\"><li uib-dropdown auto-close=outsideClick><a uib-dropdown-toggle data-toggle=dropdown role=button aria-haspopup=true aria-expanded=false><i class=\"glyphicon glyphicon-user\"></i> <span class=hidden-xs ng-if=currentUser.getIsAuthenticated()>{{ currentUser.username }}</span> <span class=hidden-xs ng-if=!currentUser.getIsAuthenticated()>Not Logged in</span> <span class=caret></span></a><ul uib-dropdown-menu class=\"UserDrawer w-xl animated fadeIn\"><li ng-if=!currentUser.getIsAuthenticated()><a ng-click=currentUser.gotoPlatformLogin();>Log in<p class=text-muted>View and edit your profile and organizations.</p></a></li><li ng-if=!currentUser.getIsAuthenticated() role=separator class=divider></li><li ng-if=!currentUser.getIsAuthenticated()><a ng-click=currentUser.gotoPlatformLogin();>Create an Account<p class=text-muted>A TechSoup Account provides you with a single place to manage all of your applications within the TechSoup universe.</p></a></li><li ng-if=!currentUser.getIsAuthenticated() role=separator class=divider></li><li ng-if=currentUser.getIsAuthenticated()><a href=http://localhost:8001/#/account target=_blank>View my account<p class=text-muted>Manage your profile and your organizations.</p></a></li><li ng-if=currentUser.getIsAuthenticated() role=separator class=divider></li><li ng-if=currentUser.getIsAuthenticated()><a href=\"\" ng-click=currentUser.gotoPlatformLogout()>Log out<p class=text-muted>This will log you out of all participating apps.</p></a></li><li ng-if=currentUser.getIsAuthenticated() role=separator class=divider></li><li><a><em>We're in BETA!</em><p class=text-muted>TechSoup Accounts is not yet active for all applications within the TechSoup universe. We’re working fast to get them all connected for you!</p></a></li></ul></li></ul>");
}]);

angular.module("template/directive.ui.formControl.validation/formControl_validationMessages.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui.formControl.validation/formControl_validationMessages.html",
    "<span class=\"help-block m-b-none\" ng-show=\"form[controlName].$invalid && form[controlName].$dirty\"><div ng-repeat=\"(key, value) in form[controlName].$error\"><span><i class=\"fa fa-warning\"></i><span translate=error.{{form.$name}}.{{controlName}}.{{key}}></span></span></div></span>");
}]);

angular.module("template/directive.ui.formControl/formControl_basic.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui.formControl/formControl_basic.html",
    "<div class=form-group ng-class=\"{'has-error' : form[controlName].$invalid && form[controlName].$dirty}\"><label>{{title}}</label><ng-transclude></ng-transclude></div>");
}]);

angular.module("template/directive.ui.formControl/formControl_horizontal.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui.formControl/formControl_horizontal.html",
    "<div class=form-group ng-class=\"{'has-error' : form[controlName].$invalid && form[controlName].$dirty}\"><label class=\"col-lg-2 control-label\">{{title}}</label><div class=col-lg-10><ng-transclude></ng-transclude></div></div>");
}]);

angular.module("template/directive.ui.panel/panel_body.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui.panel/panel_body.html",
    "<div class=panel-body ng-transclude></div>");
}]);

angular.module("template/directive.ui.panel/panel_header.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui.panel/panel_header.html",
    "<div class=\"panel-heading no-border bg-info\"><span class=h3>{{title}}</span> <span ng-transclude></span></div>");
}]);

angular.module("template/directive.ui.panel/panel.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui.panel/panel.html",
    "<div class=panel ng-transclude></div>");
}]);

angular.module("template/directive.ui.searchBox/advancedSearchBox.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui.searchBox/advancedSearchBox.html",
    "<div class=advancedSearchBox><form ng-submit=advancedSearchBoxCtrl.doSearch()><div ng-if=!advancedSearchBoxCtrl.configObject.name.hide><label for=dupl_org_name ng-if=!advancedSearchBoxCtrl.configObject.name.label.hide>{{advancedSearchBoxCtrl.configObject.name.label.text ? advancedSearchBoxCtrl.configObject.name.label.text : \"dupl_org_name\"}}</label><input id=dupl_org_name placeholder={{advancedSearchBoxCtrl.configObject.name.placeholder}} ng-model=advancedSearchBoxCtrl.searchOptions.dupl_org_name></div><div ng-if=!advancedSearchBoxCtrl.configObject.address.hide><label for=dupl_org_address ng-if=!advancedSearchBoxCtrl.configObject.address.label.hide>{{advancedSearchBoxCtrl.configObject.address.label.text ? advancedSearchBoxCtrl.configObject.address.label.text : \"dupl_org_address\"}}</label><input id=dupl_org_address placeholder={{advancedSearchBoxCtrl.configObject.address.placeholder}} ng-model=advancedSearchBoxCtrl.searchOptions.dupl_org_address></div><div ng-if=!advancedSearchBoxCtrl.configObject.city.hide><label for=dupl_org_city ng-if=!advancedSearchBoxCtrl.configObject.city.label.hide>{{advancedSearchBoxCtrl.configObject.city.label.text ? advancedSearchBoxCtrl.configObject.city.label.text : \"dupl_org_city\"}}</label><input id=dupl_org_city placeholder={{advancedSearchBoxCtrl.configObject.city.placeholder}} ng-model=advancedSearchBoxCtrl.searchOptions.dupl_org_city></div><div ng-if=!advancedSearchBoxCtrl.configObject.state.hide><label for=dupl_org_state_region ng-if=!advancedSearchBoxCtrl.configObject.state.label.hide>{{advancedSearchBoxCtrl.configObject.state.label.text ? advancedSearchBoxCtrl.configObject.state.label.text : \"dupl_org_state_region\"}}</label><input id=dupl_org_state_region placeholder={{advancedSearchBoxCtrl.configObject.state.placeholder}} ng-model=advancedSearchBoxCtrl.searchOptions.dupl_org_state_region></div><div ng-if=!advancedSearchBoxCtrl.configObject.postal.hide><label for=dupl_org_postal_code ng-if=!advancedSearchBoxCtrl.configObject.postal.label.hide>{{advancedSearchBoxCtrl.configObject.postal.label.text ? advancedSearchBoxCtrl.configObject.postal.label.text : \"dupl_org_postal_code\"}}</label><input id=dupl_org_postal_code placeholder={{advancedSearchBoxCtrl.configObject.postal.placeholder}} ng-model=advancedSearchBoxCtrl.searchOptions.dupl_org_postal_code></div><div ng-if=!advancedSearchBoxCtrl.configObject.country.hide><label for=dupl_county_code ng-if=!advancedSearchBoxCtrl.configObject.country.label.hide>{{advancedSearchBoxCtrl.configObject.country.label.text ? advancedSearchBoxCtrl.configObject.country.label.text : \"dupl_country_code\"}}</label><input id=dupl_country_code placeholder={{advancedSearchBoxCtrl.configObject.country.placeholder}} ng-model=advancedSearchBoxCtrl.searchOptions.dupl_country_code></div><div ng-if=!advancedSearchBoxCtrl.configObject.lid.hide><label for=dupl_org_lid ng-if=!advancedSearchBoxCtrl.configObject.lid.label.hide>{{advancedSearchBoxCtrl.configObject.lid.label.text ? advancedSearchBoxCtrl.configObject.lid.label.text : \"dupl_org_lid\"}}</label><input id=dupl_org_lid placeholder={{advancedSearchBoxCtrl.configObject.lid.placeholder}} ng-model=advancedSearchBoxCtrl.searchOptions.dupl_org_lid></div><div ng-if=!advancedSearchBoxCtrl.configObject.website.hide><label for=dupl_org_website ng-if=!advancedSearchBoxCtrl.configObject.website.label.hide>{{advancedSearchBoxCtrl.configObject.website.label.text ? advancedSearchBoxCtrl.configObject.website.label.text : \"dupl_org_website\"}}</label><input id=dupl_org_website placeholder={{advancedSearchBoxCtrl.configObject.website.placeholder}} ng-model=advancedSearchBoxCtrl.searchOptions.dupl_org_website></div><div ng-if=!advancedSearchBoxCtrl.configObject.phone.hide><label for=dupl_org_phone ng-if=!advancedSearchBoxCtrl.configObject.phone.label.hide>{{advancedSearchBoxCtrl.configObject.phone.label.text ? advancedSearchBoxCtrl.configObject.phone.label.text : \"dupl_org_phone\"}}</label><input id=dupl_org_phone placeholder={{advancedSearchBoxCtrl.configObject.phone.placeholder}} ng-model=advancedSearchBoxCtrl.searchOptions.dupl_org_phone></div><div ng-if=!advancedSearchBoxCtrl.configObject.email.hide><label for=dupl_org_email ng-if=!advancedSearchBoxCtrl.configObject.name.label.hide>{{advancedSearchBoxCtrl.configObject.email.label.text ? advancedSearchBoxCtrl.configObject.email.label.text : \"dupl_org_email\"}}</label><input id=dupl_org_email placeholder={{advancedSearchBoxCtrl.configObject.email.placeholder}} ng-model=advancedSearchBoxCtrl.searchOptions.dupl_org_email></div><button type=submit>{{advancedSearchBoxCtrl.configObject.searchButton.label ? advancedSearchBoxCtrl.configObject.searchButton.label : \"Search\"}}</button></form></div>");
}]);

angular.module("template/directive.ui.searchBox/keywordSearchBox.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui.searchBox/keywordSearchBox.html",
    "<div class=keywordSearchBox><form ng-submit=keywordSearchBoxCtrl.doSearch()><label for=searchBox ng-if=!keywordSearchBoxCtrl.configObject.keyword.label.hide>{{keywordSearchBoxCtrl.configObject.keyword.label.text ? keywordSearchBoxCtrl.configObject.keyword.label.text : 'Enter keyword'}}</label><input id=searchBox placeholder={{keywordSearchBoxCtrl.configObject.keyword.placeholder}} ng-model=keywordSearchBoxCtrl.keyword> <button type=submit>{{keywordSearchBoxCtrl.configObject.searchButton.label ? keywordSearchBoxCtrl.configObject.searchButton.label:'Search'}}</button></form></div>");
}]);

angular.module("template/directive.ui/footer.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.ui/footer.html",
    "<div class=\"app-footer wrapper b-t bg-info container\"><span class=pull-right>{{version}} <a href ui-scroll=app class=\"m-l-sm text-muted\"><i class=\"fa fa-long-arrow-up\"></i></a></span>&copy; {{copyright}}</div>");
}]);

angular.module("template/directive.uploads/imageUploadWithCrop.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.uploads/imageUploadWithCrop.html",
    "<div ng-if=\"croppedImageSrc == '' || croppedImageSrc == undefined || croppedImageSrc == null\"><div class=user-photo-overlay></div><div ngf-drop ngf-select ngf-change=onFileSelect($files) ng-model=files class=\"wrapper user-photo-blank\" drag-over-class=dragover ng-multiple=false allow-dir=false accept=image/*><div ng-bind-html=dropAreaText></div></div><div class=user-photo-tools><span ngf-select ngf-change=onFileSelect($files) ng-model=files><button class=\"btn upload-btn\"><i class=\"glyphicon glyphicon-plus-sign\"></i><span translate=crcommon.UploadImage></span></button></span><div class=help-text><span translate=crcommon.UploadImageHelpText></span></div></div></div><div ng-if=\"croppedImageSrc != '' && croppedImageSrc != undefined && croppedImageSrc != null\"><div class=user-photo-overlay></div><div class=user-photo><img ng-src={{croppedImageSrc}} img-on-error alt=\"User photo\"> <i class=\"glyphicon glyphicon-pencil\" ngf-select ngf-change=onFileSelect($files) ng-model=files></i></div><div class=user-photo-tools><span ngf-select ngf-change=onFileSelect($files) ng-model=files><button class=\"btn upload-btn\"><i class=\"glyphicon glyphicon-plus-sign\"></i><span translate=crcommon.UploadImage></span></button></span><div class=help-text><span translate=crcommon.UploadImageHelpText></span></div></div></div>");
}]);

angular.module("template/directive.uploads/imageUploadWithCropModal.html", []).run(["$templateCache", function ($templateCache) {
  "use strict";
  $templateCache.put("template/directive.uploads/imageUploadWithCropModal.html",
    "<div class=modal-body><ctp-image-crop oncrop=cropHandler($croppedImage) imagesrc={{previewImgSrc}} cropwidth={{cropwidth}} cropheight={{cropheight}}></ctp-image-crop></div>");
}]);
