/**
 * Created by Hiten on 17/04/2016.
 */
/* jshint node: true */
module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-html2js');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-ngdocs');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-aws-s3');
  grunt.loadNpmTasks('grunt-available-tasks');
  grunt.loadNpmTasks('grunt-template');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-bump');

  grunt.template.addDelimiters('myDelimiters', '{%', '%}');

  // Project configuration.
  grunt.util.linefeed = '\n';

  grunt.initConfig({
    modules: [],
    pkg: grunt.file.readJSON('package.json'),
    filename: 'facebookHandleLookup',
    meta: {
      modules: 'angular.module("facebookHandleLookup.source", <%= JSON.stringify(modules) %>);'
    },
    aws: grunt.file.readJSON('aws-keys.json'),
    aws_s3: {
      options: {
        accessKeyId: '<%= aws.AWSAccessKeyId %>', // Use the variables
        secretAccessKey: '<%= aws.AWSSecretKey %>', // You can also use env variables
        region: 'us-west-2',
        uploadConcurrency: 5, // 5 simultaneous uploads
        downloadConcurrency: 5 // 5 simultaneous downloads
      },
      dev: {
        options: {
          bucket: 'account.dev.tsgctp.org'
        },
        files: [{
          expand: true,
          cwd: 'dist/dev/',
          src: ['**'],
          dest: '/',
          differential: true
        }]
      },
      qa: {
        options: {
          bucket: 'account.qa.tsgctp.org'
        },
        files: [{
          expand: true,
          cwd: 'dist/qa/',
          src: ['**'],
          dest: '/',
          differential: true
        }]
      }
    },
    availabletasks: { // task
      tasks: {
        options: {
          filter: 'include',
          tasks: ['common', 'build-local', 'build-dev', 'build-qa', 'build-prod', 'deploy-dev', /*IMPORTANT-PkgAT*/ 'deploy-qa', 'clean-local', 'clean-dev', 'clean-qa', 'clean-prod', 'bowercopy', 'run-local', 'run-dev', 'run-qa', 'run-prod']
        }
      } // target
    },
    connect: {
      local: {
        options: {
          port: '8111',
          hostname: '*',
          open: true,
          keepalive: false,
          base: {
            path: 'dist/local/',
            options: {
              index: 'index.html',
              maxAge: 300000
            }
          }
        }

      },
      dev: {

        options: {
          port: '8111',
          hostname: '*',
          open: true,
          keepalive: false,
          base: {
            path: 'dist/dev/',
            options: {
              index: 'index.html',
              maxAge: 300000
            }
          }
        }

      },
      qa: {

        options: {
          port: '8111',
          hostname: '*',
          open: true,
          keepalive: false,
          base: {
            path: 'dist/qa/',
            options: {
              index: 'index.html',
              maxAge: 300000
            }
          }
        }

      },
      prod: {

        options: {
          port: '8111',
          hostname: '*',
          open: true,
          keepalive: false,
          base: {
            path: 'dist/prod/',
            options: {
              index: 'index.html',
              maxAge: 300000
            }
          }
        }

      }

    },

    clean: {
      local: {
        src: ['dist/local/']
      },
      dev: {
        src: ['dist/dev/']
      },
      qa: {
        src: ['dist/qa/']
      },
      prod: {
        prod: ['dist/prod/']
      } /*IMPORTANT-PkgCL*/
    },

    concat: {
      local: {
        options: {
          banner: '<%= meta.modules %>\n'
        },
        src: ['src/client/app/**/*.js', '!src/client/app/**/*.*.js', 'src/client/app/**/*.*.js', '!src/client/app/config.prod.js', '!src/client/app/config.dev.js', '!src/client/app/config.qa.js'],
        dest: 'dist/local/js/app.js'
      },
      dev: {
        options: {
          banner: '<%= meta.modules %>\n'
        },
        src: ['src/client/app/**/*.js', '!src/client/app/**/*.*.js', 'src/client/app/**/*.*.js', '!src/client/app/config.prod.js', '!src/client/app/config.local.js', '!src/client/app/config.qa.js'],
        dest: 'dist/dev/js/app.js'
      },
      qa: {
        options: {
          banner: '<%= meta.modules %>\n'
        },
        src: ['src/client/app/**/*.js', '!src/client/app/**/*.*.js', 'src/client/app/**/*.*.js', '!src/client/app/config.prod.js', '!src/client/app/config.dev.js', '!src/client/app/config.local.js'],
        dest: 'dist/qa/js/app.js'
      },
      prod: {
        options: {
          banner: '<%= meta.modules %>\n'
        },
        src: ['src/client/app/**/*.js', '!src/client/app/**/*.*.js', 'src/client/app/**/*.*.js', '!src/client/app/config.local.js', '!src/client/app/config.dev.js', '!src/client/app/config.qa.js'],
        dest: 'dist/prod/js/app.js'
      } /*IMPORTANT-PkgCC*/
    },
    copy: {
      local: {
        expand: true,
        cwd: 'src/client/',
        src: [
          'css/**/*.css',
          'files/**/*',
          'fonts/**/*',
          'img/**/*',
          'l10n/**/*',
          'vendor/**/*',
          'json/**/*',
          'index.html',
          'splash.html',
          'app/components/**/*.css'
        ],
        dest: 'dist/local'
      },
      dev: {
        expand: true,
        cwd: 'src/client/',
        src: [
          'css/**/*.css',
          'files/**/*',
          'fonts/**/*',
          'img/**/*',
          'l10n/**/*',
          'vendor/**/*',
          'json/**/*',
          'index.html',
          'splash.html',
          'app/components/**/*.css'
        ],
        dest: 'dist/dev'
      },
      qa: {
        expand: true,
        cwd: 'src/client/',
        src: [
          'css/**/*.css',
          'files/**/*',
          'fonts/**/*',
          'img/**/*',
          'l10n/**/*',
          'vendor/**/*',
          'json/**/*',
          'index.html',
          'splash.html',
          'app/components/**/*.css'
        ],
        dest: 'dist/qa'
      },
      prod: {
        expand: true,
        cwd: 'src/client/',
        src: [
          'css/**/*.css',
          'files/**/*',
          'fonts/**/*',
          'img/**/*',
          'l10n/**/*',
          'vendor/**/*',
          'json/**/*',
          'index.html',
          'splash.html',
          'app/components/**/*.css'
        ],
        dest: 'dist/prod'
      },
      /*IMPORTANT-PkgCP*/
      bower: {
        nonull: true,
        files: [
          // Include our bower JS dependencies

          // angular
          {
            src: 'bower_components/angular/angular.min.js',
            dest: 'src/client/vendor/angular/angular.min.js'
          }, {
            src: 'bower_components/angular-animate/angular-animate.min.js',
            dest: 'src/client/vendor/angular/angular-animate/angular-animate.min.js'
          }, {
            src: 'bower_components/angular-cookies/angular-cookies.min.js',
            dest: 'src/client/vendor/angular/angular-cookies/angular-cookies.min.js'
          }, {
            src: 'bower_components/angular-resource/angular-resource.min.js',
            dest: 'src/client/vendor/angular/angular-resource/angular-resource.min.js'
          }, {
            src: 'bower_components/angular-sanitize/angular-sanitize.min.js',
            dest: 'src/client/vendor/angular/angular-sanitize/angular-sanitize.min.js'
          }, {
            src: 'bower_components/angular-touch/angular-touch.min.js',
            dest: 'src/client/vendor/angular/angular-touch/angular-touch.min.js'
          },

          // bootstrap
          {
            src: ['**'],
            cwd: 'bower_components/bootstrap/dist/',
            dest: 'src/client/vendor/bootstrap/',
            expand: true
          }, {
            src: 'bower_components/animate.css/animate.min.css',
            dest: 'src/client/vendor/animate.css/animate.min.css'
          },

          // core
          {
            src: 'bower_components/angular-ui-router/release/angular-ui-router.min.js',
            dest: 'src/client/vendor/angular/angular-ui-router/angular-ui-router.min.js'
          }, {
            src: 'bower_components/angular-ui-select/dist/select.min.js',
            dest: 'src/client/vendor/angular/angular-ui-select/select.min.js'
          }, {
            src: ['**'],
            cwd: 'bower_components/angular-ui-select/dist/',
            dest: 'src/client/vendor/angular/angular-ui-select/',
            expand: true
          }, {
            src: 'bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
            dest: 'src/client/vendor/angular/angular-bootstrap/ui-bootstrap-tpls.min.js'
          }, {
            src: 'bower_components/angular-translate/angular-translate.min.js',
            dest: 'src/client/vendor/angular/angular-translate/angular-translate.min.js'
          }, {
            src: 'bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
            dest: 'src/client/vendor/angular/angular-translate/angular-translate-loader-static-files.min.js'
          }, {
            src: 'bower_components/angular-translate-loader-url/angular-translate-loader-url.min.js',
            dest: 'src/client/vendor/angular/angular-translate/angular-translate-loader-url.min.js'
          }, {
            src: 'bower_components/angular-translate-storage-cookie/angular-translate-storage-cookie.min.js',
            dest: 'src/client/vendor/angular/angular-translate/angular-translate-storage-cookie.min.js'
          }, {
            src: 'bower_components/angular-translate-storage-local/angular-translate-storage-local.js',
            dest: 'src/client/vendor/angular/angular-translate/angular-translate-storage-local.min.js'
          }, {
            src: 'bower_components/oclazyload/dist/ocLazyLoad.js',
            dest: 'src/client/vendor/angular/oclazyload/ocLazyLoad.js'
          }, {
            src: 'bower_components/angular-ui-select/dist/select.min.css',
            dest: 'src/client/vendor/modules/angular-ui-select/select.min.css'
          }, {
            src: ['**/*.js', '**/*.map', '!karma*'],
            cwd: 'bower_components/underscore/',
            dest: 'src/client/vendor/underscore/',
            expand: true
          }, {
            src: 'bower_components/jquery/dist/jquery.min.js',
            dest: 'src/client/vendor/jquery/jquery.min.js'
          }, {
            src: 'bower_components/xdomaincookie/dist/xdomaincookie.min.js',
            dest: 'src/client/vendor/libs/xdomaincookie.min.js'
          }, {
            src: 'bower_components/xwindowcomms/dist/xwindowcomms.min.js',
            dest: 'src/client/vendor/libs/xwindowcomms.min.js'
          }, {
            src: 'bower_components/simple-line-icons/css/simple-line-icons.css',
            dest: 'src/client/vendor/simple-line-icons/css/simple-line-icons.css'
          }, {
            src: ['**'],
            cwd: 'bower_components/font-source-sans-pro/',
            dest: 'src/client/vendor/font-source-sans-pro/',
            expand: true
          }, {
            src: ['**'],
            cwd: 'bower_components/font-awesome/',
            dest: 'src/client/vendor/font-awesome/',
            expand: true
          }, {
            src: 'bower_components/crypto-js/rollups/aes.js',
            dest: 'src/client/vendor/libs/aes.js'
          }, {
            src: 'bower_components/ng-file-upload/ng-file-upload.min.js',
            dest: 'src/client/vendor/modules/ng-file-upload/ng-file-upload.min.js'
          }, {
            src: 'bower_components/ng-file-upload/ng-file-upload-shim.min.js',
            dest: 'src/client/vendor/modules/ng-file-upload/ng-file-upload-shim.min.js'
          }, {
            src: 'bower_components/angular-sanitize/angular-sanitize.js',
            dest: 'src/client/vendor/angular/angular-sanitize/angular-sanitize.js'
          }, {
            src: 'bower_components/ngInfiniteScroll/ng-infinite-scroll.min.js',
            dest: 'src/client/vendor/libs/ng-infinite-scroll.min.js'
          }, {
            src: ['cooperative**/*.js', 'cooperative**/*.css'],
            cwd: 'bower_components/',
            dest: 'src/client/vendor/',
            expand: true
          }, {
            src: ['**/*', '!**/*.json'],
            cwd: 'bower_components/osHeader/',
            dest: 'src/client/vendor/cooperative/osHeader',
            expand: true
          }

        ]
      },
      'dev-dest-to-efs': {
        expand: true,
        cwd: 'dist/dev',
        src: ['**/*', '!**/_archive/**'],
        dest: process.env['HOME'] + '/efs/ctp.apps/facebookHandleLookup.tsgctp.org/dev/dist/',
      },
      'dev-archive-to-efs': {
        expand: true,
        cwd: 'dist/dev/_archive',
        src: ['**/*'],
        dest: process.env['HOME'] + '/efs/ctp.apps/facebookHandleLookup.tsgctp.org/dev/archive/',
      },
      'qa-dest-to-efs': {
        expand: true,
        cwd: 'dist/qa',
        src: ['**/*', '!**/_archive/**'],
        dest: process.env['HOME'] + '/efs/ctp.apps/facebookHandleLookup.tsgctp.org/qa/dist/',
      },
      'qa-archive-to-efs': {
        expand: true,
        cwd: 'dist/qa/_archive',
        src: ['**/*'],
        dest: process.env['HOME'] + '/efs/ctp.apps/facebookHandleLookup.tsgctp.org/qa/archive/',
      },
      'prod-dest-to-efs': {
        expand: true,
        cwd: 'dist/prod',
        src: ['**/*', '!**/_archive/**'],
        dest: process.env['HOME'] + '/efs/ctp.apps/facebookHandleLookup.tsgctp.org/prod/dist/',
      },
      'prod-archive-to-efs': {
        expand: true,
        cwd: 'dist/prod/_archive',
        src: ['**/*'],
        dest: process.env['HOME'] + '/efs/ctp.apps/facebookHandleLookup.tsgctp.org/prod/archive/',
      }

    },
    uglify: {
      local: {
        src: ['dist/local/js/app.js'],
        dest: 'dist/local/js/app.min.js'
      },
      dev: {
        src: ['dist/dev/js/app.js'],
        dest: 'dist/dev/js/app.min.js'
      },
      qa: {
        src: ['dist/qa/js/app.js'],
        dest: 'dist/qa/js/app.min.js'
      },
      prod: {
        src: ['dist/prod/js/app.js'],
        dest: 'dist/prod/js/app.min.js'
      } /*IMPORTANT-PkgUG*/
    },
    jshint: {
      files: ['Gruntfile.js', 'src/client/app/*.js', 'src/server/**/*.js'],
      options: {
        jshintrc: '.jshintrc'
      }
    },
    html2js: {
      local: {
        options: {
          base: 'src/client/app',
          module: 'facebookHandleLookup.templates',
          singleModule: false,
          useStrict: true,
          htmlmin: {
            collapseBooleanAttributes: true,
            collapseWhitespace: true,
            removeAttributeQuotes: true,
            removeComments: true,
            removeEmptyAttributes: true,
            removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true
          }
        },
        files: [{
          src: ['src/client/app/**/*.html'],
          dest: 'dist/local/js/app-tpl.js'
        }]
      },
      dev: {
        options: {
          base: 'src/client/app',
          module: 'facebookHandleLookup.templates',
          singleModule: true,
          useStrict: true,
          htmlmin: {
            collapseBooleanAttributes: true,
            collapseWhitespace: true,
            removeAttributeQuotes: true,
            removeComments: true,
            removeEmptyAttributes: true,
            removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true
          }
        },
        files: [{
          src: ['src/client/app/**/*.html'],
          dest: 'dist/dev/js/app-tpl.js'
        }]
      },
      qa: {
        options: {
          base: 'src/client/app',
          module: 'facebookHandleLookup.templates',
          singleModule: true,
          useStrict: true,
          htmlmin: {
            collapseBooleanAttributes: true,
            collapseWhitespace: true,
            removeAttributeQuotes: true,
            removeComments: true,
            removeEmptyAttributes: true,
            removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true
          }
        },
        files: [{
          src: ['src/client/app/**/*.html'],
          dest: 'dist/qa/js/app-tpl.js'
        }]
      },
      prod: {
        options: {
          base: 'src/client/app',
          module: 'facebookHandleLookup.templates',
          singleModule: true,
          useStrict: true,
          htmlmin: {
            collapseBooleanAttributes: true,
            collapseWhitespace: true,
            removeAttributeQuotes: true,
            removeComments: true,
            removeEmptyAttributes: true,
            removeRedundantAttributes: true,
            removeScriptTypeAttributes: true,
            removeStyleLinkTypeAttributes: true
          }
        },
        files: [{
          src: ['src/client/app/**/*.html'],
          dest: 'dist/prod/js/app-tpl.js'
        }]
      } /*IMPORTANT-PkgH2J*/

    },
    less: {
        dev: {
            options: {
                compress: true,
                yuicompress: true,
                optimization: 2
            },
            files: {
                'src/client/css/app.css': 'src/client/less/app.less'
            }
        }
    },
    watch: {
      local: {

        files: ['src/client/app/**/*.js', 'src/client/app/**/*.css', 'src/**/*.html', 'src/client/less/**/*.less', 'src/client/app/**/*.less', 'src/client/vendor/**/*.js', 'src/client/css/**/*.css', 'src/client/img/**/*.*'],
        tasks: ['build-local']

      },
      dev: {
        files: ['src/client/app/**/*.js', 'src/client/app/**/*.css', 'src/**/*.html', 'src/client/less/**/*.less', 'src/client/app/**/*.less', 'src/client/vendor/**/*.js', 'src/client/css/**/*.css', 'src/client/img/**/*.*'],
        tasks: ['build-dev']

      },
      qa: {
        files: ['src/client/app/**/*.js', 'src/client/app/**/*.css', 'src/**/*.html', 'src/client/less/**/*.less', 'src/client/app/**/*.less', 'src/client/vendor/**/*.js', 'src/client/css/**/*.css', 'src/client/img/**/*.*'],
        tasks: ['build-qa']

      },
      prod: {
        files: ['src/client/app/**/*.js', 'src/client/app/**/*.css', 'src/**/*.html', 'src/client/less/**/*.less', 'src/client/app/**/*.less', 'src/client/vendor/**/*.js', 'src/client/css/**/*.css', 'src/client/img/**/*.*'],
        tasks: ['build-prod']

      }
    },
    template: {
      local: {
        options: {
          delimiters: 'myDelimiters',
          data: {
            ssoHostURL: 'http://0.0.0.0:8001/ssoHost.html'
          }
        },
        files: {
          'dist/local/index.html': ['dist/local/index.html']
        }
      },
      dev: {
        options: {
          delimiters: 'myDelimiters',
          data: {
            ssoHostURL: 'https://accounts.tsgctp.org:38401/ssoHost.html'
          }
        },
        files: {
          'dist/dev/index.html': ['dist/dev/index.html']
        }
      },
      qa: {
        options: {
          delimiters: 'myDelimiters',
          data: {
            ssoHostURL: 'https://accounts.tsgctp.org:38402/ssoHost.html'
          }
        },
        files: {
          'dist/qa/index.html': ['dist/qa/index.html']
        }
      },
      prod: {
        options: {
          delimiters: 'myDelimiters',
          data: {
            ssoHostURL: 'https://accounts.tsgctp.org/ssoHost.html'
          }
        },
        files: {
          'dist/prod/index.html': ['dist/prod/index.html']
        }
      },
    },
      compress: {
        'local-tar': {
          options: {
            mode: 'tar',
            archive: 'dist/local/_archive/facebookHandleLookup-local-<%= pkg.version %>.tar.gz'
          },
          expand: true,
          cwd: 'dist/local/',
          src: ['**/*', '!**/*.zip', '!**/*.tar.gz'],
          dest: '/'
        },
        'local-zip': {
          options: {
            mode: 'zip',
            archive: 'dist/local/_archive/facebookHandleLookup-local-<%= pkg.version %>.zip'
          },
          expand: true,
          cwd: 'dist/local/',
          src: ['**/*', '!**/*.zip', '!**/*.tar.gz'],
          dest: '/'
        },
        'dev-tar': {
          options: {
            mode: 'tar',
            archive: 'dist/dev/_archive/facebookHandleLookup-dev-<%= pkg.version %>.tar.gz'
          },
          expand: true,
          cwd: 'dist/dev/',
          src: ['**/*', '!**/*.zip', '!**/*.tar.gz'],
          dest: '/'
        },
        'dev-zip': {
          options: {
            mode: 'zip',
            archive: 'dist/dev/_archive/facebookHandleLookup-dev-<%= pkg.version %>.zip'
          },
          expand: true,
          cwd: 'dist/dev/',
          src: ['**/*', '!**/*.zip', '!**/*.tar.gz'],
          dest: '/'
        },
        'qa-tar': {
          options: {
            mode: 'tar',
            archive: 'dist/qa/_archive/facebookHandleLookup-qa-<%= pkg.version %>.tar.gz'
          },
          expand: true,
          cwd: 'dist/qa/',
          src: ['**/*', '!**/*.zip', '!**/*.tar.gz'],
          dest: '/'
        },
        'qa-zip': {
          options: {
            mode: 'zip',
            archive: 'dist/qa/_archive/facebookHandleLookup-qa-<%= pkg.version %>.zip'
          },
          expand: true,
          cwd: 'dist/qa/',
          src: ['**/*', '!**/*.zip', '!**/*.tar.gz'],
          dest: '/'
        },
        'prod-tar': {
          options: {
            mode: 'tar',
            archive: 'dist/prod/_archive/facebookHandleLookup-prod-<%= pkg.version %>.tar.gz'
          },
          expand: true,
          cwd: 'dist/prod/',
          src: ['**/*', '!**/*.zip', '!**/*.tar.gz'],
          dest: '/'
        },
        'prod-zip': {
          options: {
            mode: 'zip',
            archive: 'dist/prod/_archive/facebookHandleLookup-prod-<%= pkg.version %>.zip'
          },
          expand: true,
          cwd: 'dist/prod/',
          src: ['**/*', '!**/*.zip', '!**/*.tar.gz'],
          dest: '/'
        }
      
    },
    bump: {
      options: {
        files: ['package.json', 'bower.json'],
        updateConfigs: ['pkg'],
        commit: false,
        createTag: false,
        push: false
      }
    }
  });

  grunt.registerTask('custom-GenModuleNames', 'your description', function() {

    var modules = grunt.config('modules');

    grunt.file.expand({
      cwd: 'src/client/app/components'
    }, '*').forEach(function(dir) {
      if (grunt.file.isDir('src/client/app/components/' + dir)) {
        modules.push('facebookHandleLookup.components.' + dir.replace('/', '.'));
      }
    });

    grunt.config('modules', modules);

  });

  grunt.registerTask('default', ['availabletasks']);
  grunt.registerTask('help', ['availabletasks']);

  grunt.registerTask('common', ['jshint', 'less', 'custom-GenModuleNames']);

  grunt.registerTask('build-local', ['common', 'clean:local', 'html2js:local', 'concat:local', 'uglify:local', 'copy:local', 'template:local', 'compress:local-tar', 'compress:local-zip']);
  grunt.registerTask('build-dev', ['common', 'clean:dev', 'html2js:dev', 'concat:dev', 'uglify:dev', 'copy:dev', 'template:dev', 'compress:dev-tar', 'compress:dev-zip']);
  grunt.registerTask('build-qa', ['common', 'clean:qa', 'html2js:qa', 'concat:qa', 'uglify:qa', 'copy:qa', 'template:qa', 'compress:qa-tar', 'compress:qa-zip']);
  grunt.registerTask('build-prod', ['common', 'clean:prod', 'html2js:prod', 'concat:prod', 'uglify:prod', 'copy:prod', 'template:prod', 'compress:prod-tar', 'compress:prod-zip']);

  /*IMPORTANT-PkgRT*/

  grunt.registerTask('deploy-dev-efs', ['build-dev', 'copy:dev-dest-to-efs', 'copy:dev-archive-to-efs']);
  grunt.registerTask('deploy-qa-efs', ['build-qa', 'copy:qa-dest-to-efs', 'copy:qa-archive-to-efs']);
  grunt.registerTask('deploy-prod-efs', ['build-prod', 'copy:prod-dest-to-efs', 'copy:prod-archive-to-efs']);

  grunt.registerTask('clean-local', ['clean:local']);
  grunt.registerTask('clean-dev', ['clean:dev']);
  grunt.registerTask('clean-qa', ['clean:qa']);
  grunt.registerTask('clean-prod', ['clean:prod']);

  grunt.registerTask('copy-bower', ['copy:bower']);

  grunt.registerTask('run-local', ['build-local', 'connect:local', 'watch:local']);
  grunt.registerTask('run-dev', ['build-dev', 'connect:dev', 'watch:dev']);
  grunt.registerTask('run-qa', ['build-qa', 'connect:qa', 'watch:qa']);
  grunt.registerTask('run-prod', ['build-prod', 'connect:prod', 'watch:prod']);

  grunt.registerTask('prep-commit', ['bump:patch']);
  grunt.registerTask('prep-commit-patch', ['prep-commit']);
  grunt.registerTask('prep-commit-minor', ['bump:minor']);
  grunt.registerTask('prep-commit-major', ['bump:major']);
  grunt.registerTask('prep-commit-prerelease', ['bump:prerelease']);

  return grunt;
};
